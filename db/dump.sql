--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: active_admin_comments; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.active_admin_comments (
    id integer NOT NULL,
    namespace character varying,
    body text,
    resource_id character varying NOT NULL,
    resource_type character varying NOT NULL,
    author_id integer,
    author_type character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: active_admin_comments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.active_admin_comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: active_admin_comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.active_admin_comments_id_seq OWNED BY public.active_admin_comments.id;


--
-- Name: admin_users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.admin_users (
    id integer NOT NULL,
    email character varying DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip inet,
    last_sign_in_ip inet,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: admin_users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.admin_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: admin_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.admin_users_id_seq OWNED BY public.admin_users.id;


--
-- Name: assets; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.assets (
    id integer NOT NULL,
    storage_uid character varying,
    storage_name character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    storage_width integer,
    storage_height integer,
    storage_aspect_ratio double precision,
    storage_depth integer,
    storage_format character varying,
    storage_mime_type character varying,
    storage_size character varying
);


--
-- Name: assets_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.assets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: assets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.assets_id_seq OWNED BY public.assets.id;


--
-- Name: card_items; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.card_items (
    id integer NOT NULL,
    card_id integer,
    name character varying,
    price character varying,
    meta character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    priority integer DEFAULT 0,
    image character varying,
    temp_key character varying,
    units character varying,
    vendor_code character varying,
    weight double precision DEFAULT 1.0
);


--
-- Name: card_items_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.card_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: card_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.card_items_id_seq OWNED BY public.card_items.id;


--
-- Name: cards; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.cards (
    id integer NOT NULL,
    category_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    image character varying,
    title character varying,
    card_logo character varying,
    temp_key character varying,
    priority integer DEFAULT 0,
    image_style boolean DEFAULT false,
    size_cd integer DEFAULT 0
);


--
-- Name: cards_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.cards_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cards_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.cards_id_seq OWNED BY public.cards.id;


--
-- Name: categories; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.categories (
    id integer NOT NULL,
    name character varying,
    slug character varying,
    priority integer,
    parent_id integer,
    lft integer NOT NULL,
    rgt integer NOT NULL,
    depth integer DEFAULT 0 NOT NULL,
    children_count integer DEFAULT 0 NOT NULL
);


--
-- Name: categories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.categories_id_seq OWNED BY public.categories.id;


--
-- Name: clients; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.clients (
    id integer NOT NULL,
    name character varying,
    tel character varying,
    password character varying,
    discount character varying,
    address character varying,
    delivery_city_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    remember_token character varying
);


--
-- Name: clients_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.clients_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: clients_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.clients_id_seq OWNED BY public.clients.id;


--
-- Name: delivering_cities; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.delivering_cities (
    id integer NOT NULL,
    name character varying,
    estimated_delivery_time character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: delivering_cities_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.delivering_cities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: delivering_cities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.delivering_cities_id_seq OWNED BY public.delivering_cities.id;


--
-- Name: images; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.images (
    id integer NOT NULL,
    file character varying,
    temp_key character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    image_type character varying DEFAULT 'image'::character varying
);


--
-- Name: images_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.images_id_seq OWNED BY public.images.id;


--
-- Name: order_items; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.order_items (
    id integer NOT NULL,
    order_id integer,
    card_item_id integer,
    count character varying
);


--
-- Name: order_items_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.order_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: order_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.order_items_id_seq OWNED BY public.order_items.id;


--
-- Name: orders; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.orders (
    id integer NOT NULL,
    name character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    address character varying,
    contacts character varying,
    phone character varying,
    comments character varying,
    delivering_city_id integer
);


--
-- Name: orders_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.orders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.orders_id_seq OWNED BY public.orders.id;


--
-- Name: post_category_relations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.post_category_relations (
    id integer NOT NULL,
    post_id integer,
    category_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: post_category_relations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.post_category_relations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: post_category_relations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.post_category_relations_id_seq OWNED BY public.post_category_relations.id;


--
-- Name: post_images; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.post_images (
    id integer NOT NULL,
    post_id integer,
    image character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: post_images_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.post_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: post_images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.post_images_id_seq OWNED BY public.post_images.id;


--
-- Name: posts; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.posts (
    id integer NOT NULL,
    title character varying,
    first_text_section character varying,
    second_text_section character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    url character varying
);


--
-- Name: posts_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.posts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: posts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.posts_id_seq OWNED BY public.posts.id;


--
-- Name: promo_cards; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.promo_cards (
    id integer NOT NULL,
    category_id integer,
    title character varying,
    list character varying,
    image character varying,
    card_class character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: promo_cards_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.promo_cards_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: promo_cards_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.promo_cards_id_seq OWNED BY public.promo_cards.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.schema_migrations (
    version character varying NOT NULL
);


--
-- Name: taggings; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.taggings (
    id integer NOT NULL,
    tag_id integer,
    taggable_id integer,
    taggable_type character varying,
    tagger_id integer,
    tagger_type character varying,
    context character varying(128),
    created_at timestamp without time zone
);


--
-- Name: taggings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.taggings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: taggings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.taggings_id_seq OWNED BY public.taggings.id;


--
-- Name: tags; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.tags (
    id integer NOT NULL,
    name character varying,
    taggings_count integer DEFAULT 0
);


--
-- Name: tags_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tags_id_seq OWNED BY public.tags.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.active_admin_comments ALTER COLUMN id SET DEFAULT nextval('public.active_admin_comments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.admin_users ALTER COLUMN id SET DEFAULT nextval('public.admin_users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.assets ALTER COLUMN id SET DEFAULT nextval('public.assets_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.card_items ALTER COLUMN id SET DEFAULT nextval('public.card_items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cards ALTER COLUMN id SET DEFAULT nextval('public.cards_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.categories ALTER COLUMN id SET DEFAULT nextval('public.categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.clients ALTER COLUMN id SET DEFAULT nextval('public.clients_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.delivering_cities ALTER COLUMN id SET DEFAULT nextval('public.delivering_cities_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.images ALTER COLUMN id SET DEFAULT nextval('public.images_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.order_items ALTER COLUMN id SET DEFAULT nextval('public.order_items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orders ALTER COLUMN id SET DEFAULT nextval('public.orders_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.post_category_relations ALTER COLUMN id SET DEFAULT nextval('public.post_category_relations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.post_images ALTER COLUMN id SET DEFAULT nextval('public.post_images_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.posts ALTER COLUMN id SET DEFAULT nextval('public.posts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.promo_cards ALTER COLUMN id SET DEFAULT nextval('public.promo_cards_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.taggings ALTER COLUMN id SET DEFAULT nextval('public.taggings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tags ALTER COLUMN id SET DEFAULT nextval('public.tags_id_seq'::regclass);


--
-- Data for Name: active_admin_comments; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.active_admin_comments (id, namespace, body, resource_id, resource_type, author_id, author_type, created_at, updated_at) FROM stdin;
\.


--
-- Name: active_admin_comments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.active_admin_comments_id_seq', 1, false);


--
-- Data for Name: admin_users; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.admin_users (id, email, encrypted_password, reset_password_token, reset_password_sent_at, remember_created_at, sign_in_count, current_sign_in_at, last_sign_in_at, current_sign_in_ip, last_sign_in_ip, created_at, updated_at) FROM stdin;
1	admin@example.com	$2a$11$EeBlhPVOUJqnhRWaVUxYA.OtSkJdnY2jDvZ.OZblQiT0UovOG0ize	\N	\N	2017-11-21 13:02:35.774834	99	2018-05-29 08:52:50.454694	2018-05-18 12:42:56.887219	37.144.55.11	109.252.83.125	2016-06-06 13:23:37.223586	2018-05-29 08:52:50.464715
\.


--
-- Name: admin_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.admin_users_id_seq', 2, false);


--
-- Data for Name: assets; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.assets (id, storage_uid, storage_name, created_at, updated_at, storage_width, storage_height, storage_aspect_ratio, storage_depth, storage_format, storage_mime_type, storage_size) FROM stdin;
1	2017/02/15/10_33_16_619_file	\N	2017-02-15 07:33:16.623653	2017-02-15 07:33:16.623653	750	857	0.875145857642940506	8	png	image/png	276356
2	2017/02/15/10_36_54_503_file	\N	2017-02-15 07:36:54.503961	2017-02-15 07:36:54.503961	448	600	0.746666666666666701	8	jpeg	image/jpeg	37318
3	2017/02/15/13_24_41_566_file	\N	2017-02-15 10:24:41.568503	2017-02-15 10:24:41.568503	1024	768	1.33333333333333326	8	jpeg	image/jpeg	280532
4	2017/02/15/13_24_41_906_file	\N	2017-02-15 10:24:41.906794	2017-02-15 10:24:41.906794	1024	768	1.33333333333333326	8	jpeg	image/jpeg	208707
5	2017/02/15/13_24_42_38_file	\N	2017-02-15 10:24:42.039893	2017-02-15 10:24:42.039893	1024	768	1.33333333333333326	8	jpeg	image/jpeg	294355
6	2017/02/15/13_24_42_606_file	\N	2017-02-15 10:24:42.607026	2017-02-15 10:24:42.607026	1024	768	1.33333333333333326	8	jpeg	image/jpeg	211502
7	2017/02/15/13_24_42_996_file	\N	2017-02-15 10:24:42.996795	2017-02-15 10:24:42.996795	1024	768	1.33333333333333326	8	jpeg	image/jpeg	259536
8	2017/05/26/15_31_24_526_file	\N	2017-05-26 12:31:24.528867	2017-05-26 12:31:24.528867	1280	720	1.77777777777777768	8	jpeg	image/jpeg	153572
9	2017/05/26/15_31_24_578_file	\N	2017-05-26 12:31:24.580289	2017-05-26 12:31:24.580289	960	1280	0.75	8	jpeg	image/jpeg	210202
10	2017/05/26/15_31_24_623_file	\N	2017-05-26 12:31:24.623837	2017-05-26 12:31:24.623837	1280	1280	1	8	jpeg	image/jpeg	269060
11	2017/05/26/15_31_24_758_file	\N	2017-05-26 12:31:24.759685	2017-05-26 12:31:24.759685	1280	1280	1	8	jpeg	image/jpeg	135878
12	2017/05/26/15_31_25_215_file	\N	2017-05-26 12:31:25.216499	2017-05-26 12:31:25.216499	1280	960	1.33333333333333326	8	jpeg	image/jpeg	189475
13	2017/05/26/15_31_25_385_file	\N	2017-05-26 12:31:25.385747	2017-05-26 12:31:25.385747	960	1280	0.75	8	jpeg	image/jpeg	185899
14	2017/05/26/15_31_45_818_file	\N	2017-05-26 12:31:45.81919	2017-05-26 12:31:45.81919	1280	1280	1	8	jpeg	image/jpeg	362288
15	2017/05/26/15_31_45_852_file	\N	2017-05-26 12:31:45.853283	2017-05-26 12:31:45.853283	720	1280	0.5625	8	jpeg	image/jpeg	94557
16	2017/05/26/15_31_45_889_file	\N	2017-05-26 12:31:45.890193	2017-05-26 12:31:45.890193	960	1280	0.75	8	jpeg	image/jpeg	247290
17	2017/05/26/15_31_46_30_file	\N	2017-05-26 12:31:46.031503	2017-05-26 12:31:46.031503	720	1280	0.5625	8	jpeg	image/jpeg	109639
18	2017/05/26/15_31_46_615_file	\N	2017-05-26 12:31:46.615988	2017-05-26 12:31:46.615988	1280	960	1.33333333333333326	8	jpeg	image/jpeg	262870
19	2017/05/26/15_31_46_642_file	\N	2017-05-26 12:31:46.643489	2017-05-26 12:31:46.643489	1280	1280	1	8	jpeg	image/jpeg	302302
20	2017/05/26/15_31_46_693_file	\N	2017-05-26 12:31:46.69357	2017-05-26 12:31:46.69357	960	1280	0.75	8	jpeg	image/jpeg	243717
21	2017/05/26/15_31_47_235_file	\N	2017-05-26 12:31:47.23621	2017-05-26 12:31:47.23621	960	1280	0.75	8	jpeg	image/jpeg	158865
22	2017/05/26/15_31_47_262_file	\N	2017-05-26 12:31:47.262625	2017-05-26 12:31:47.262625	960	1280	0.75	8	jpeg	image/jpeg	158865
23	2017/05/26/15_31_47_459_file	\N	2017-05-26 12:31:47.459819	2017-05-26 12:31:47.459819	960	1280	0.75	8	jpeg	image/jpeg	159998
24	2017/05/26/15_31_47_853_file	\N	2017-05-26 12:31:47.854302	2017-05-26 12:31:47.854302	1280	960	1.33333333333333326	8	jpeg	image/jpeg	164424
25	2017/05/26/15_31_47_911_file	\N	2017-05-26 12:31:47.912552	2017-05-26 12:31:47.912552	960	1280	0.75	8	jpeg	image/jpeg	167881
26	2017/05/26/15_31_47_941_file	\N	2017-05-26 12:31:47.941873	2017-05-26 12:31:47.941873	960	1280	0.75	8	jpeg	image/jpeg	164692
27	2017/05/26/15_31_48_345_file	\N	2017-05-26 12:31:48.345663	2017-05-26 12:31:48.345663	960	1280	0.75	8	jpeg	image/jpeg	122275
28	2017/05/26/15_31_48_372_file	\N	2017-05-26 12:31:48.372561	2017-05-26 12:31:48.372561	960	1280	0.75	8	jpeg	image/jpeg	98145
29	2017/05/26/15_31_48_401_file	\N	2017-05-26 12:31:48.401808	2017-05-26 12:31:48.401808	960	1280	0.75	8	jpeg	image/jpeg	178658
30	2017/05/26/15_31_48_781_file	\N	2017-05-26 12:31:48.781924	2017-05-26 12:31:48.781924	960	1280	0.75	8	jpeg	image/jpeg	143645
31	2017/05/26/15_31_48_808_file	\N	2017-05-26 12:31:48.809547	2017-05-26 12:31:48.809547	960	1280	0.75	8	jpeg	image/jpeg	146409
32	2017/05/26/15_31_49_45_file	\N	2017-05-26 12:31:49.046506	2017-05-26 12:31:49.046506	960	1280	0.75	8	jpeg	image/jpeg	247290
33	2017/05/26/15_31_49_491_file	\N	2017-05-26 12:31:49.492614	2017-05-26 12:31:49.492614	960	1280	0.75	8	jpeg	image/jpeg	196991
\.


--
-- Name: assets_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.assets_id_seq', 33, true);


--
-- Data for Name: card_items; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.card_items (id, card_id, name, price, meta, created_at, updated_at, priority, image, temp_key, units, vendor_code, weight) FROM stdin;
11894	3048	Диафрагма тонкая	554	наружная,1 кг	2016-06-07 08:47:11.672758	2016-06-07 08:47:11.672758	0	\N	\N	\N	\N	1
11895	3049	Покромка	554	1 кг	2016-06-07 08:47:12.929784	2016-06-07 08:47:12.929784	0	\N	\N	\N	\N	1
11896	3050	Рёбрышки традиционные	327.15	1 кг	2016-06-07 08:47:14.217434	2016-06-07 08:47:14.217434	0	\N	\N	\N	\N	1
11897	3051	Ростбиф	595.85	сирлойн,1 кг	2016-06-07 08:47:15.429987	2016-06-07 08:47:15.429987	0	\N	\N	\N	\N	1
11902	3055	Щёчки	375.50	1 кг	2016-06-07 08:47:20.637691	2016-06-07 08:47:20.637691	0	\N	\N	\N	\N	1
11904	3057	Тонкий край "Striploin"	1218.65	MB2+ (Австралия),1 кг	2016-06-07 08:47:23.144758	2016-06-07 08:47:23.144758	0	\N	\N	\N	\N	1
11907	3060	Рёбра "Short Ribs"	768.25	1 кг	2016-06-07 08:47:26.814795	2016-06-07 08:47:26.814795	0	\N	\N	\N	\N	1
13132	3340	Тесто «Фило»	155.20	0.5 КГ	2017-02-10 13:26:31.014886	2017-02-10 13:47:42.732835	0	\N	bhyaeLsXMENxBlGqkYZvuIdcwKSzAWDQfFJoVOUmiCPHpjTtnr	\N	\N	1
11940	3069	Пицца пикколини в ассортименте	229.15	Буитони; 270 г,1 шт.	2016-06-07 08:47:29.067866	2017-02-23 12:27:23.762278	0	\N	\N	\N	\N	1
13133	3341	Тесто «Спринг Роллз»	204.85	20к*550 г; 1 кг	2017-02-10 13:28:03.616651	2017-02-23 12:27:21.531133	0	\N	EGPWxHJMuhgqNjeczTpSmFUZKvRDILknBCQdstlbwrYyfiOAaV	piece		1
11938	3069	Пельмени «Сибирские»	122.50	Мистраль 1 кг/6 шт,1 уп.	2016-06-07 08:47:29.044335	2016-06-07 08:47:29.044335	0	\N	\N	\N	\N	1
11941	3070	Колбаски-гриль «Бараньи»	633.65	1 кг	2016-06-07 08:47:29.18896	2016-06-07 08:47:29.18896	0	\N	\N	\N	\N	1
11942	3071	Колбаски-гриль «Мюнхенские»	569	свиные,1 кг	2016-06-07 08:47:29.776011	2016-06-07 08:47:29.776011	0	\N	\N	\N	\N	1
11939	3069		203.50	Буитони; 350 г,1 шт.	2016-06-07 08:47:29.054579	2017-02-23 12:27:23.766163	0	\N	\N	\N	\N	1
11936	3067	Бекон	122.85	нарезка; 200 г; "Alfoldi",1 уп.	2016-06-07 08:47:28.076639	2017-02-23 12:27:23.777197	0	\N	\N	\N	\N	1
11908	3061	Стейк «Osso Buco» 	580.90	молодая говядина 	2016-06-07 08:47:26.965616	2017-02-17 16:07:17.806859	0	\N		piece		1
11906	3059	Ribeye Cuberoll	962.10	б/к; Finexcor (Аргентина),1 кг	2016-06-07 08:47:26.630385	2017-02-23 12:27:23.834672	0	\N		\N	\N	1
11935	3066	Наггетсы куриные	336.45	1,5 кг; «Мираторг»,1 уп.	2016-06-07 08:47:27.955523	2017-02-23 12:27:23.781038	0	\N	\N	\N	\N	1
11934	3065	«Коппа»	1843.08	шейка сыровяленая; 1,5/2 кг 	2016-06-07 08:47:27.780653	2017-03-03 09:06:44.696006	0	Сырокопч-2.jpg		piece		1
11925	3064	Шея без кости	307.35	1*25	2016-06-07 08:47:27.544927	2017-02-17 16:13:50.019834	0	foto-356.jpg		piece		1
11914	3062	Щёчки	491.76	«Русский Мрамор»	2016-06-07 08:47:27.177931	2017-02-17 16:09:55.764316	0	\N		piece		1
11922	3064	Окорок 	319.44	б/к	2016-06-07 08:47:27.511022	2017-02-17 16:13:50.035148	0	\N		piece		1
11889	3044	Вырезка 	1460.94	Россия	2016-06-07 08:47:07.930939	2017-02-17 16:10:20.726244	0	\N		piece		1
11911	3062	Печень 	189.03		2016-06-07 08:47:27.138708	2017-02-17 16:09:55.766028	0	\N		piece		1
11915	3062	Язык 	362.06	зачищенный; 1*12	2016-06-07 08:47:27.188854	2017-02-23 12:27:23.823678	0	\N		piece		1
11921	3064	Корейка на кости	250.09	1*20	2016-06-07 08:47:27.498558	2017-02-17 16:13:50.037161	0	foto-257.jpg		piece		1
11920	3064	Карбонад 	280.62		2016-06-07 08:47:27.485684	2017-02-17 16:13:50.039136	0	\N		piece		1
11905	3058	Ribeye Cuberoll Premium	1479.35	б/к; Affco (Н. Зеландия),1 кг	2016-06-07 08:47:24.06744	2017-02-23 12:27:23.838951	0	\N	\N	\N	\N	1
11903	3056	Вырезка "Tenderloin"	1575.80	MB2+; зачищенная,1 кг	2016-06-07 08:47:21.951097	2017-02-23 12:27:23.845687	0	\N	\N	\N	\N	1
11901	3054	Тонкий край "Striploin Choice"	1240.4	б/к; зачищенный; ~3кг/уп.,1 кг	2016-06-07 08:47:19.323955	2017-02-23 12:27:23.85178	0	\N	\N	\N	\N	1
11900	3054	Тонкий край "Striploin Choice"	1292.65	б/к; ~1,2 кг/уп.,1 кг	2016-06-07 08:47:19.302869	2017-02-23 12:27:23.856059	0	\N	\N	\N	\N	1
11899	3053	Толстый край "Ribeye Prime"	2023.50	б/к; зачищенный; 7 рёбер,1 кг	2016-06-07 08:47:18.067794	2017-02-23 12:27:23.860274	0	\N	\N	\N	\N	1
11898	3052	Толстый край "Ribeye Choice"	2025.60	б/к; зачищенный; ~4-5 кг/уп.,1 кг	2016-06-07 08:47:16.754925	2017-02-23 12:27:23.864739	0	\N	\N	\N	\N	1
11893	3047	Глазной мускул	646.35	 б/к; ~2кг/уп.,1 кг	2016-06-07 08:47:10.435762	2017-02-23 12:27:23.875238	0	\N	\N	\N	\N	1
11892	3046	Вырезка "Tenderloin Top choice"	2669	зачищенная; ~2 кг/уп.,1 кг	2016-06-07 08:47:09.200036	2017-02-23 12:27:23.879557	0	\N	\N	\N	\N	1
11891	3046	Вырезка "Tenderloin Choice"	2363.65	зачищенная; ~2 кг/уп.,1 кг	2016-06-07 08:47:09.18534	2017-02-23 12:27:23.883707	0	\N	\N	\N	\N	1
11890	3045	Вырезка	2585.30	«Зелёные просторы» мраморная; 2 кг,1 уп.	2016-06-07 08:47:08.041655	2017-02-23 12:27:23.887989	0	\N	\N	\N	\N	1
11931	3065	Салями «Милано»	1725.06	2 кг	2016-06-07 08:47:27.741105	2017-03-03 09:06:44.698514	0	\N		piece		1
11944	3073	Купаты «Домашние»	519.55	телячьи,1 кг	2016-06-07 08:47:30.887783	2016-06-07 08:47:30.887783	0	\N	\N	\N	\N	1
11923	3064	Рёбрышки 	306.64	с/м, 550 г, 1*5	2016-06-07 08:47:27.523024	2017-06-01 08:11:03.24252	0	foto-259.jpg		piece		1
11882	3043	Задняя голень на кости	609.79	Дагестан, с/м, 1*10	2016-06-07 08:47:07.762195	2017-06-01 08:12:28.759744	0	foto-343.jpg		piece		1
11888	3044	Корейка 7-8 рёбер	1460.94	Дагестан, с/м, 400-500 г, 1*10	2016-06-07 08:47:07.921673	2017-06-01 08:13:12.006194	0	\N		piece		1
11887	3044	Корейка 8 рёбер	1587.98	"KIWI", 8-10 кг/кор	2016-06-07 08:47:07.913765	2017-06-01 08:13:12.008074	0	\N		piece		1
13127	3336	Филе сёмги	1194.16	слабосолёное, с/м, вакуум	2017-02-10 12:57:18.33288	2017-06-26 12:37:02.250283	0	foto-219.jpg	WYxwIZotilXQRVAyPOJUDMnFEdpresqaLGgumCKjBTzHScNhkv	kilo		1
13131	3338	Филе трески без шкуры	350.08	7% глазировка	2017-02-10 13:06:12.89703	2017-06-07 07:18:47.517437	0	\N	HdOoTugCKrbMDVGjvJFNfRimyshIqSaEwWXAxPBYzZpQnLckeU	piece		1
13128	3337	Филе тунца «Yellowfin»	1241.70	«ААА»	2017-02-10 13:01:18.91337	2017-06-29 14:10:46.46015	0	\N	SgDaCWLxubidVIlJnUkENMhZRemsKvfTzjXQAyqwpotGHcPBYO	kilo		1
13126	3336	Филе сёмги	1372.02	подкопчённое, вакуум, 1,5-2,5 кг	2017-02-10 12:57:18.329813	2017-06-07 07:26:58.311101	0	\N	WYxwIZotilXQRVAyPOJUDMnFEdpresqaLGgumCKjBTzHScNhkv	kilo		1
12899	3469	Чизкейк с шоколадной крошкой	2196.12	2,13 кг, 16 порций	2017-01-29 10:23:13.377428	2017-06-07 08:09:13.065193	0	\N	kqtWfbszJAPXBpvidghTnrcaFmIexYNDyRKVHEwolLuGCUOZMj	piece		1
13129	3337	Филе тунца	1139.76	2-4 кг	2017-02-10 13:01:18.91697	2017-06-29 14:11:31.714564	0	foto-199.jpg	OxroLiCGtBebRjuUQhJIcZzwKdEMqgvHYnyDVmapFXAPlfSkWs	piece		1
11945	3074	Сосиски-гриль «Охотничьи»	406.60	1 кг	2016-06-07 08:47:31.0524	2016-06-07 08:47:31.0524	0	\N	\N	\N	\N	1
11946	3075	Сосиски-гриль «С сыром»	375.05	1 кг	2016-06-07 08:47:31.554988	2016-06-07 08:47:31.554988	0	\N	\N	\N	\N	1
11947	3076	Грудка цыплёнка	360.75	в/к,1 кг	2016-06-07 08:47:31.794476	2016-06-07 08:47:31.794476	0	\N	\N	\N	\N	1
11948	3076	Колбаса «Пепперони»	632.20	в нарезке,1 кг	2016-06-07 08:47:31.806062	2016-06-07 08:47:31.806062	0	\N	\N	\N	\N	1
11949	3076	Сосиски «Фермерские»	165.55	1 кг	2016-06-07 08:47:31.817211	2016-06-07 08:47:31.817211	0	\N	\N	\N	\N	1
11950	3077	Кролик	442.60	ножка,1 кг	2016-06-07 08:47:31.952309	2016-06-07 08:47:31.952309	0	\N	\N	\N	\N	1
11962	3082	Грудка	444.64	250-280 г/шт; подложка	2016-06-07 08:47:34.215036	2017-02-23 12:27:23.707299	0	\N		kilo		1
11968	3084	Картофель молодой без кожуры	426.60	"McCain" 2,5 кг,1 уп.	2016-06-07 08:47:35.442723	2016-06-07 08:47:35.442723	0	\N	\N	\N	\N	1
11969	3084	Картофельное пюре	406.05	"McCain" 3 кг,1 уп.	2016-06-07 08:47:35.452408	2016-06-07 08:47:35.452408	0	\N	\N	\N	\N	1
11970	3084	Картофельные дольки со специями	288.85	"McCain" 2,5 кг,1 уп.	2016-06-07 08:47:35.463922	2016-06-07 08:47:35.463922	0	\N	\N	\N	\N	1
11971	3084	Картофельные чипсы	395.40	"McCain" 2,5 кг * 5,1 уп.	2016-06-07 08:47:35.474672	2016-06-07 08:47:35.474672	0	\N	\N	\N	\N	1
11972	3085	Картофель фри	213.25	«Valley Farm» McCain 2,5 кг,1 уп.	2016-06-07 08:47:36.815474	2016-06-07 08:47:36.815474	0	\N	\N	\N	\N	1
11975	3087	Картофельные изделия «Улыбка»	287.50	"McCain" 1,5 кг,1 уп.	2016-06-07 08:47:39.648938	2016-06-07 08:47:39.648938	0	\N	\N	\N	\N	1
11976	3088	Картофельные оладьи	248.40	4*1500 г,1 уп.	2016-06-07 08:47:41.006828	2016-06-07 08:47:41.006828	0	\N	\N	\N	\N	1
11977	3089	Картофельные треугольные котлеты	518.10	2*2500 г,1 уп.	2016-06-07 08:47:42.339878	2016-06-07 08:47:42.339878	0	\N	\N	\N	\N	1
11979	3091	Кукуруза в початках	135.55	1/12 кг,Венгрия,1 шт	2016-06-07 08:47:44.530492	2016-06-07 08:47:44.530492	0	\N	\N	\N	\N	1
11980	3092	Кукуруза в зёрнах	151.25	1 кг	2016-06-07 08:47:45.387029	2016-06-07 08:47:45.387029	0	\N	\N	\N	\N	1
11951	3078	Вырезка	2274.90	0,5-1 кг/уп.; NZ,1 кг	2016-06-07 08:47:32.095001	2017-02-23 12:27:23.739425	0	\N	\N	\N	\N	1
11998	3095	Клюква садовая	2090	10 кг	2016-06-07 08:47:46.037171	2016-06-07 08:47:46.037171	0	\N	\N	\N	\N	1
11999	3095	Черная смородина	747.00	5 кг	2016-06-07 08:47:46.049697	2016-06-07 08:47:46.049697	0	\N	\N	\N	\N	1
12000	3095	Черника	2203.00	10 кг	2016-06-07 08:47:46.061774	2016-06-07 08:47:46.061774	0	\N	\N	\N	\N	1
12003	3097		894.85	1,1 кг (10 порций),1 шт	2016-06-07 08:47:47.00107	2016-06-07 08:47:47.00107	0	\N	\N	\N	\N	1
12004	3098		765.55	1,1 кг (10 порций),1 шт	2016-06-07 08:47:48.250592	2016-06-07 08:47:48.250592	0	\N	\N	\N	\N	1
12005	3099		785	1,1 кг (10 порций),1 шт	2016-06-07 08:47:49.419676	2016-06-07 08:47:49.419676	0	\N	\N	\N	\N	1
12006	3100		675.55	1,1 кг (10 порций),1 шт	2016-06-07 08:47:50.596159	2016-06-07 08:47:50.596159	0	\N	\N	\N	\N	1
12007	3101		837.95	1,1 кг (10 порций),1 шт.	2016-06-07 08:47:51.219771	2016-06-07 08:47:51.219771	0	\N	\N	\N	\N	1
12008	3102		770.75	0,9 кг (10 порций),1 шт.	2016-06-07 08:47:52.320677	2016-06-07 08:47:52.320677	0	\N	\N	\N	\N	1
12009	3103		910.40	1,1 кг (10 порций),1 шт.	2016-06-07 08:47:53.658616	2016-06-07 08:47:53.658616	0	\N	\N	\N	\N	1
11961	3081	Цыплёнок 1 категории	148.76	1 кг	2016-06-07 08:47:34.04059	2017-01-13 08:40:36.29905	0	\N		\N	\N	1
11959	3080	Цыплёнок-корнишон	155.20	500+ г,1 шт	2016-06-07 08:47:33.255833	2017-01-12 13:22:33.272965	0	Frozen_chick.jpg		\N	\N	1
11958	3079	Сердца куриные	276.35	1 кг	2016-06-07 08:47:32.481226	2017-01-12 13:26:34.149052	0	\N		\N	\N	1
11965	3082	Окорочок 	368.42	250-280 г/шт; подложка	2016-06-07 08:47:34.263712	2017-02-23 12:27:23.702074	0	\N		kilo		1
11967	3083	Тушка 1 категории 	533.56	премиум; 200+ г; «Углич»; 1*10; 1 кг	2016-06-07 08:47:34.399147	2017-02-23 12:27:23.6978	0	\N		\N	\N	1
13134	3342	Вафли «Венские» с карамелизированным сахаром	36.35	90 г; 1 уп.	2017-02-10 13:36:13.665107	2017-02-23 12:27:21.527139	0	\N	NxjPtHKfnLUpiGICWkAoMrTOVwavbgzysuqeRcSlZFYEXQdBhm	kilo		1
11978	3090	Луковые кольца в пивной панировке	256.03	"McCain"; 1 кг; 1*10; 1 уп.	2016-06-07 08:47:43.711069	2017-02-23 12:27:23.682394	0	\N		\N	\N	1
11988	3093	Брокколи 	175.4		2016-06-07 08:47:45.671756	2017-06-08 08:31:35.364166	0	Broccoli_1.png		kilo		1
11986	3093	Кукуруза 	149.79	в зёрнах	2016-06-07 08:47:45.644379	2017-06-08 08:31:35.366202	0	foto-514.jpg		kilo		1
12002	3096	Белый гриб «экстра» 	732.01	2-4 см	2016-06-07 08:47:46.199168	2017-06-08 08:28:35.500128	0	foto-504.jpg		kilo		1
13143	3345	Креветка северная, 40/60 	835	пивная, без глазировки, Магадан	2017-02-10 15:22:12.635797	2017-07-17 12:42:29.182939	0	\N	dkhaeVlSGuKfiNWMIsjDCYEAwqxyzbomTvQRBFZgOcPHrXJUnL	kilo	00000001926	1
12001	3096	Подосиновик 	95.8	целый, 0,3 кг	2016-06-07 08:47:46.183389	2017-06-08 08:28:35.50431	0	\N		piece		1
13140	3343	Креветки тигровые целые 13/15	1149	с/м, 1*10	2017-02-10 15:09:04.693827	2017-07-18 14:29:11.910205	0	\N	tMFWZLSbQemRHNBsxcoTviOIrqyCfnlKkpwXVdzGJguEPhjYAa	kilo	00000000036	1
13139	3343	Креветки тигровые целые, 16/20	980	с/м	2017-02-10 15:09:04.691265	2017-07-18 14:29:11.912111	0	foto-203.jpg	tMFWZLSbQemRHNBsxcoTviOIrqyCfnlKkpwXVdzGJguEPhjYAa	kilo	00000000097	1
13142	3344	Креветки тигровые 21/25	1730	в блоке льда, без головы; чистый вес 1,816 кг	2017-02-10 15:16:08.976154	2017-07-17 13:15:40.7311	0	\N	XYPywxDokeEMhacUQstWKBGTVmIJONZfAijCHnuvlFdbgSqLzR	piece	00000002508	1
13141	3344	Креветки тигровые, 16/20	2280	в блоке льда, без головы; чистый вес 1,816 кг	2017-02-10 15:16:08.972667	2017-07-17 13:15:40.73361	0	\N	FLNentyhjgYTaHPiDdxZQRfuUOBGJMmwCslIqScVkWEXArbKov	piece	00000001444	1
13138	3343	Креветки тигровые целые, 2/4	2170	с/м	2017-02-10 15:09:04.68845	2017-07-18 14:29:11.913448	0	\N	tMFWZLSbQemRHNBsxcoTviOIrqyCfnlKkpwXVdzGJguEPhjYAa	kilo	00000000293	1
11985	3093	Спаржа зелёная	195.79	0,4 кг	2016-06-07 08:47:45.629725	2017-06-08 08:31:35.368004	0	foto-498.jpg		piece		1
11943	3072	Купаты «Бюргерские»	548.30	свино-говяжьи; с/м,1 кг	2016-06-07 08:47:30.325212	2017-02-23 12:27:23.755361	0	\N	\N	\N	\N	1
12010	3104		881.40	1,3 кг (10 порций),1 шт.	2016-06-07 08:47:54.89973	2016-06-07 08:47:54.89973	0	\N	\N	\N	\N	1
12011	3105		979.70	1,4 кг (10 порций),1 шт.	2016-06-07 08:47:56.109642	2016-06-07 08:47:56.109642	0	\N	\N	\N	\N	1
12012	3106		1155.55	1,1 кг (10 порций),1 шт.	2016-06-07 08:47:57.282359	2016-06-07 08:47:57.282359	0	\N	\N	\N	\N	1
12013	3107		982.80	1,1 кг (10 порций),1 шт.	2016-06-07 08:47:58.654657	2016-06-07 08:47:58.654657	0	\N	\N	\N	\N	1
12014	3108		1194.85	1,5 кг (10 порций),1 шт.	2016-06-07 08:47:59.79562	2016-06-07 08:47:59.79562	0	\N	\N	\N	\N	1
12016	3110	«Профитроли» классический	696.55	1 шт.	2016-06-07 08:48:01.417018	2016-06-07 08:48:01.417018	0	\N	\N	\N	\N	1
12017	3110	«Профитроли» шоколадный	696.55	1 шт.	2016-06-07 08:48:01.429518	2016-06-07 08:48:01.429518	0	\N	\N	\N	\N	1
12018	3110	Торт "Сельва Нера"	927.55	12 порц. 1,15 кг "Bindi",1 шт.	2016-06-07 08:48:01.44211	2016-06-07 08:48:01.44211	0	\N	\N	\N	\N	1
12019	3110	Торт "Тирамису"	844.20	1,15 кг 12 порций "Bindi",1 шт.	2016-06-07 08:48:01.453881	2016-06-07 08:48:01.453881	0	\N	\N	\N	\N	1
12020	3110	Торт морковный "Сладкое искушение"	1564.70	16 порций,1 шт.	2016-06-07 08:48:01.465653	2016-06-07 08:48:01.465653	0	\N	\N	\N	\N	1
12021	3110	Чизкейк "Клубничный вихрь"	1808.85	 16 порций 1,93 кг,1 шт.	2016-06-07 08:48:01.477683	2016-06-07 08:48:01.477683	0	\N	\N	\N	\N	1
12022	3110	Чизкейк "Малиновый вихрь"	1808.85	16 порций 1,93 кг,1 шт.	2016-06-07 08:48:01.48946	2016-06-07 08:48:01.48946	0	\N	\N	\N	\N	1
12023	3110	Чизкейк "Монтероза"	1339.70	"Bindi",1 шт.	2016-06-07 08:48:01.499832	2016-06-07 08:48:01.499832	0	\N	\N	\N	\N	1
12024	3110	Чизкейк "Пралине с шоколадной крошкои и орехом пекан"	1543.40	1,93 кг (16 порций),1 шт.	2016-06-07 08:48:01.513811	2016-06-07 08:48:01.513811	0	\N	\N	\N	\N	1
12025	3110	Чизкейк "С яблоками и карамелью"	2065.40	 16 порций 1,93 кг,1 шт.	2016-06-07 08:48:01.527798	2016-06-07 08:48:01.527798	0	\N	\N	\N	\N	1
12027	3110	Чизкейк Миндальный Амаретто	1452.45	16 порц 1,93 кг,1 шт.	2016-06-07 08:48:01.550596	2016-06-07 08:48:01.550596	0	\N	\N	\N	\N	1
12029	3110	Чизкейк Шоколадная трилогия	847.70	12 порций 1,2 кг Bindi,1 шт.	2016-06-07 08:48:01.572825	2016-06-07 08:48:01.572825	0	\N	\N	\N	\N	1
12030	3110	Штрудель Яблочный	925.90	1,1 кг,1 шт.	2016-06-07 08:48:01.585721	2016-06-07 08:48:01.585721	0	\N	\N	\N	\N	1
12060	3331	Классическая	19.80	80 г; 1 шт	2016-06-07 08:48:03.240019	2017-02-23 12:27:23.453209	0	\N		\N	\N	1
12062	3114	Дрожжевое	73.90	0,5 кг	2016-06-07 08:48:03.850055	2017-02-15 11:50:11.841626	0	Talosto_red.jpg		kilo		1
12051	3112	Улитка с корицей	39.85	Бельгия; 1 шт.	2016-06-07 08:48:02.95439	2017-02-23 12:27:23.478868	0	\N		kilo		1
12059	3113	Тортийяс пшеничный 6"	123.15	(15 см) 330 г/12 шт; 1 уп.	2016-06-07 08:48:03.227465	2017-02-23 12:27:23.456168	0	\N		kilo		1
12061	3114	Бездрожжевое	70.35	0,5 кг	2016-06-07 08:48:03.837649	2017-02-15 11:50:11.843301	0	Talosto_blue.jpg		kilo		1
12058	3113	Тортийяс пшеничный 10"	253.50	(25 см) охлажденные 12 шт; 1 уп.	2016-06-07 08:48:03.218472	2017-02-23 12:27:23.459159	0	\N		kilo		1
13027	3294	Бедро без кости, без кожи 	259.19	Куриное филе «ЕС АГРО», подложка	2017-02-01 12:52:19.307885	2017-03-03 09:23:48.622286	0	\N	mLEftWJnwpqehAdGNUHxTvsFrVoDSIcyXOaRlBjCbKgZMPQzkY	kilo		1
12175	3136	Тальятелле	160.35	№203; 0,5 кг	2016-06-07 08:48:12.616797	2017-02-23 12:27:23.24804	0	\N		piece		1
12067	3115	в ассортименте	179.75	0,175 л,1 уп.	2016-06-07 08:48:06.147835	2016-06-07 08:48:06.147835	0	\N	\N	\N	\N	1
12068	3115	Ванильное	2491.95	4,8 л (2,410 кг),1 уп.	2016-06-07 08:48:06.158797	2016-06-07 08:48:06.158797	0	\N	\N	\N	\N	1
12069	3115	Грецкий Орех	2634.50	4,8 л,1 уп.	2016-06-07 08:48:06.16965	2016-06-07 08:48:06.16965	0	\N	\N	\N	\N	1
12037	3111	Лепестки миндаля	1446.25	1 кг	2016-06-07 08:48:01.802681	2017-01-12 10:11:26.428364	0	\N		\N	\N	1
12031	3111	Вишня коктейльная	295.90	красная с веточкой "Делькофф",1 уп.	2016-06-07 08:48:01.718663	2017-01-12 10:11:26.437803	0	\N		\N	\N	1
12050	3112	Розан с абрикосом	44	Бельгия; 1 шт.	2016-06-07 08:48:02.942661	2017-02-23 12:27:23.48304	0	\N		kilo		1
12057	3113	Чипсы Начос Тортилья	318.65	Мексиканские специи; 500 г	2016-06-07 08:48:03.190278	2017-02-23 12:27:23.462095	0	\N		kilo		1
12056	3330	Для хот-дога	12.25	с кунжутом 160 мм; 1 шт	2016-06-07 08:48:03.178487	2017-02-23 12:27:23.465033	0	\N		kilo		1
12066	34234	«Звездное»	67.80	слоёное дрожжевое,0,5 кг	2016-06-07 08:48:03.896839	2017-02-09 15:20:09.118791	0	\N	\N	\N	\N	1
12065	3114	«Звёздное»	66.55	бездрожжевое; 0,5 кг	2016-06-07 08:48:03.888141	2017-02-23 12:27:23.446541	0	Star_dough.jpg		kilo		1
12028	3110	Чизкейк Нью-йорк классический	1654.80	16 порций; 1,93 кг,1 шт.	2016-06-07 08:48:01.56233	2017-02-23 12:27:23.55465	0	\N	\N	\N	\N	1
12026	3110	Чизкейк "Шоколадный"	1917.80	 16 порций; 1,93 кг,1 шт.	2016-06-07 08:48:01.538484	2017-02-23 12:27:23.561893	0	\N	\N	\N	\N	1
12055	3339	«Французская»	6.20	40 г; 120 шт в 1 уп.	2016-06-07 08:48:03.167919	2017-07-05 15:05:02.874499	0	foto-459.jpg		kilo		1
13145	3345	Креветка северная, 90/120 	600	пивная, без глазировки, Гренландия	2017-02-10 15:24:56.9335	2017-07-17 12:42:29.179979	0	foto-383.jpg	aymwjZVxNAnWpOLTKJcCdtvBXMbQkHfFReuIgirhsElSqGDUYz	kilo	00000002799	1
13148	3222	Форель речная	384.40	потрошёная, зачищенная, 240-280 г	2017-02-10 15:34:16.475739	2017-06-26 11:13:53.369949	0	foto-237.jpg	mRJZFhiXUojrOeGdHLNcnCAfBPaYgTqsuQxMWkySlpvbEIKwDz	kilo		1
13130	3338	Филе трески на шкуре	546.27	сухая (бортовая) заморозка	2017-02-10 13:06:12.894101	2017-06-07 07:18:47.519844	0	\N	ZrJzwqVBpCFRMHYnQbfDTNSxljyXiOPoELdAveGuKghkIsUmac	piece		1
12893	3468	Профитроль в шоколадном соусе	881.48	1.1 кг, 24 шарика	2017-01-29 10:23:13.362782	2017-06-07 08:12:53.344814	0	\N	kqtWfbszJAPXBpvidghTnrcaFmIexYNDyRKVHEwolLuGCUOZMj	piece		1
12892	3468	Профитроль классический	881.48	 в белом соусе, 1,1 кг, 24 шарика	2017-01-29 10:23:13.359967	2017-06-07 08:12:53.347266	0	\N	FuZTIdAiNpkDeJPQsKljbCocmxBngOSEYwzftLXWUqRrMVhyav	piece		1
12070	3115	Карамельное	2634.50	4,8 л (2,822 кг),1 уп.	2016-06-07 08:48:06.181536	2016-06-07 08:48:06.181536	0	\N	\N	\N	\N	1
12071	3115	Клубничное	2610.90	4,8 л (2,730 кг),1 уп.	2016-06-07 08:48:06.193091	2016-06-07 08:48:06.193091	0	\N	\N	\N	\N	1
12072	3115	Крем Брюле	2634.50	4,8 л,1 уп.	2016-06-07 08:48:06.207702	2016-06-07 08:48:06.207702	0	\N	\N	\N	\N	1
12073	3115	Лимонно-лаймовый сорбет	3174.40	4,8 л (2,890 кг),1 уп.	2016-06-07 08:48:06.219846	2016-06-07 08:48:06.219846	0	\N	\N	\N	\N	1
12074	3115	Малиново-клубничный сорбет	3174.40	4,8 л (2,398 кг),1 уп.	2016-06-07 08:48:06.231452	2016-06-07 08:48:06.231452	0	\N	\N	\N	\N	1
12075	3115	Мятное Шоколадное 4,8л (2,610кг)	2634.50	 4,8 л (2,610 кг),1 уп.	2016-06-07 08:48:06.243652	2016-06-07 08:48:06.243652	0	\N	\N	\N	\N	1
12076	3115	Сорбет Манго	2824.05	4,8 л (3,060 кг),1 уп.	2016-06-07 08:48:06.255137	2016-06-07 08:48:06.255137	0	\N	\N	\N	\N	1
12077	3115	Тирамису 4,8 л (2,568 кг)	2973.90	0,175 л,1 уп.	2016-06-07 08:48:06.267295	2016-06-07 08:48:06.267295	0	\N	\N	\N	\N	1
12078	3115	Фисташковое	2973.90	4,8 л (2,930 кг),1 уп.	2016-06-07 08:48:06.278022	2016-06-07 08:48:06.278022	0	\N	\N	\N	\N	1
12079	3115	Чёрная Смородина	2824.05	4,8 л (2,930 кг),1 уп.	2016-06-07 08:48:06.28941	2016-06-07 08:48:06.28941	0	\N	\N	\N	\N	1
12080	3115	Шоколадное	2491.95	4,8 л (2,730 кг),1 уп.	2016-06-07 08:48:06.30158	2016-06-07 08:48:06.30158	0	\N	\N	\N	\N	1
12081	3115	Шоколад Кокос	2708.55	4,8 л,1 уп.	2016-06-07 08:48:06.31456	2016-06-07 08:48:06.31456	0	\N	\N	\N	\N	1
12082	3116	Банановое	1117.80	5 л,1 уп.	2016-06-07 08:48:07.181114	2016-06-07 08:48:07.181114	0	\N	\N	\N	\N	1
12083	3116	Вишнёвый щербет	560.20	3,5 л,1 уп.	2016-06-07 08:48:07.197369	2016-06-07 08:48:07.197369	0	\N	\N	\N	\N	1
12084	3116	Клубничное	1118	5 л,1 уп.	2016-06-07 08:48:07.214359	2016-06-07 08:48:07.214359	0	\N	\N	\N	\N	1
12085	3116	Лесной Орех	1412	5 л,1 уп.	2016-06-07 08:48:07.227648	2016-06-07 08:48:07.227648	0	\N	\N	\N	\N	1
12086	3116	Фисташковое	577.50	3,5 л,1 уп.	2016-06-07 08:48:07.238069	2016-06-07 08:48:07.238069	0	\N	\N	\N	\N	1
12087	3116	Шоколадное	450.05	3,5 л,1 уп.	2016-06-07 08:48:07.252762	2016-06-07 08:48:07.252762	0	\N	\N	\N	\N	1
12088	3117	Вафельный рожок	1218.85	1 кор * 340 шт,1 уп.	2016-06-07 08:48:08.118584	2016-06-07 08:48:08.118584	0	\N	\N	\N	\N	1
12090	3117	Пломбир «48 копеек»	56.95	420 мл брикет,1 шт.	2016-06-07 08:48:08.144388	2016-06-07 08:48:08.144388	0	\N	\N	\N	\N	1
12098	3120	Ананас	237.45	1 кг	2016-06-07 08:48:09.40575	2016-06-07 08:48:09.40575	0	\N	\N	\N	\N	1
12099	3120	Апельсин	237.45	1 кг	2016-06-07 08:48:09.417511	2016-06-07 08:48:09.417511	0	\N	\N	\N	\N	1
12100	3120	Вишня	237.45	1 кг	2016-06-07 08:48:09.429083	2016-06-07 08:48:09.429083	0	\N	\N	\N	\N	1
12101	3120	Дыня	237.45	1 кг	2016-06-07 08:48:09.441455	2016-06-07 08:48:09.441455	0	\N	\N	\N	\N	1
12102	3120	Малина	237.45	1 кг	2016-06-07 08:48:09.452471	2016-06-07 08:48:09.452471	0	\N	\N	\N	\N	1
12103	3120	Тропические фрукты	237.45	1 кг	2016-06-07 08:48:09.465271	2016-06-07 08:48:09.465271	0	\N	\N	\N	\N	1
12104	3120	Лайм	237.45	1 кг	2016-06-07 08:48:09.479823	2016-06-07 08:48:09.479823	0	\N	\N	\N	\N	1
12105	3120	Коктейль клубничный	346.40	1,250 кг	2016-06-07 08:48:09.492071	2016-06-07 08:48:09.492071	0	\N	\N	\N	\N	1
12106	3120	Коктейль Советский	419	1,5 кг/8 шт	2016-06-07 08:48:09.503145	2016-06-07 08:48:09.503145	0	\N	\N	\N	\N	1
12139	3125	«Пуриссими Эспрессо Арабика» 	522.50	1 кг	2016-06-07 08:48:10.758026	2017-02-22 07:14:59.723807	0	\N		piece		1
12097	3119	«Клубничное»	420.35	молокосодержащая; 2,170 кг,1 уп.	2016-06-07 08:48:09.209678	2017-02-23 12:27:23.392146	0	\N	\N	\N	\N	1
12096	3119	«Советское»	605.20	молокосодержащая; 2,170 кг,1 уп.	2016-06-07 08:48:09.197085	2017-02-23 12:27:23.395975	0	\N	\N	\N	\N	1
12129	3122	Малиновый соус	411.15	кули, 0,5 л, «Буаро»	2016-06-07 08:48:10.078791	2017-06-05 11:12:46.355686	0	\N		piece		1
12128	3122	Клубничный соус 	388.60	0,5 л, «Буаро»	2016-06-07 08:48:10.06689	2017-06-05 11:12:46.366416	0	\N		piece		1
12897	3469	Чизкейк «Монтероза»	1306.24		2017-01-29 10:23:13.372534	2017-06-07 08:09:13.070033	0	\N	kqtWfbszJAPXBpvidghTnrcaFmIexYNDyRKVHEwolLuGCUOZMj	piece		1
12126	3122	Соус «Тропические фрукты»	405.25	кули, 0,5 л	2016-06-07 08:48:10.045382	2017-06-05 11:12:46.369663	0	\N		piece		1
12125	3122	Абрикосовый соус	264.75	кули, 0,5 л, «Буаро»	2016-06-07 08:48:10.033257	2017-06-05 11:12:46.371187	0	\N		piece		1
12127	3122	Соус «Чёрная смородина»	300.15	0,5 л, «Буаро»	2016-06-07 08:48:10.05651	2017-06-05 11:19:45.698718	0	\N		piece		1
12898	3469	Чизкейк «New York классический»	2070.52	2,13 кг, 16 порций	2017-01-29 10:23:13.374976	2017-06-07 08:09:13.067437	0	\N	kqtWfbszJAPXBpvidghTnrcaFmIexYNDyRKVHEwolLuGCUOZMj	piece		1
12896	3273	Торт «Тирамису»	1092.02	1,26 кг, 12 порций	2017-01-29 10:23:13.370125	2017-06-07 08:11:33.618594	0	\N	kqtWfbszJAPXBpvidghTnrcaFmIexYNDyRKVHEwolLuGCUOZMj	piece		1
12895	3273	Торт «Сельва Нера»	1381.24	1,15 кг, 12 порций	2017-01-29 10:23:13.367678	2017-06-07 08:11:33.620536	0	\N	kqtWfbszJAPXBpvidghTnrcaFmIexYNDyRKVHEwolLuGCUOZMj	piece		1
12894	3273	Торт «Рикотта» творожный	1126.95	1 кг, 12 порций	2017-01-29 10:23:13.365237	2017-06-07 08:11:33.622235	0	\N	kqtWfbszJAPXBpvidghTnrcaFmIexYNDyRKVHEwolLuGCUOZMj	piece		1
12015	3465	Макарони «Ассорти»	25.82	0,250 г, 25 шт	2016-06-07 08:48:01.200066	2017-06-07 08:16:28.35561	0	Макарони_ассорти_9.jpg		piece		1
12095	3119	«Элитное»	284.40	молокосодержащая; 1,666 кг,1 уп.	2016-06-07 08:48:09.183632	2017-02-23 12:27:23.399495	0	\N	\N	\N	\N	1
12094	3119	«Сливочная мечта»	439.60	молокосодержащая; 1,666 кг,1 уп.	2016-06-07 08:48:09.168021	2017-02-23 12:27:23.403765	0	\N	\N	\N	\N	1
12093	3119	«Молочная мечта»	420.35	молокосодержащая; 1,666 кг,1 уп.	2016-06-07 08:48:09.157284	2017-02-23 12:27:23.407099	0	\N	\N	\N	\N	1
12092	3119	«Шоколадное»	420.35	молокосодержащая; 1,666 кг,1 уп.	2016-06-07 08:48:09.148619	2017-02-23 12:27:23.410561	0	\N	\N	\N	\N	1
12204	3143	Турне	731.95	выдержанный сыр (50%) из коровьего молока,1 кг	2016-06-07 08:48:14.117664	2017-02-10 18:45:26.037218	0	\N		\N	\N	1
12203	3143	Арабеск	1564.20	Козье и коровье молоко,1 кг	2016-06-07 08:48:14.10566	2017-02-10 18:45:26.040624	0	\N		\N	\N	1
12202	3143	Арабеск	1681.10	(100% козье молоко) ~1 кг,1 кг	2016-06-07 08:48:14.093245	2017-02-10 18:45:26.042512	0	\N		\N	\N	1
12163	3135	Вермишель	33.35	фасованная; 0,450 г,1 уп.	2016-06-07 08:48:12.32054	2017-02-23 12:27:23.257862	0	\N	\N	\N	\N	1
12161	3133	"American Sandwich"	65.10	1 уп.	2016-06-07 08:48:12.035905	2016-06-07 08:48:12.035905	0	\N	\N	\N	\N	1
12162	3134	"Forno Bonomi"	174.65	 сахарное (0,400 кг) *15шт,1 уп.	2016-06-07 08:48:12.166301	2016-06-07 08:48:12.166301	0	\N	\N	\N	\N	1
12164	3135	Перья рифлёные "Знатные"	33.35	\n\t\t\t\t\t\t\t\t\t\t1 уп.\n\t\t\t\t\t\t\t\t\t,1 уп.	2016-06-07 08:48:12.333126	2016-06-07 08:48:12.333126	0	\N	\N	\N	\N	1
12165	3135	Рожки витые "Знатные"	33.35	\n\t\t\t\t\t\t\t\t\t\t1 уп.\n\t\t\t\t\t\t\t\t\t,1 уп.	2016-06-07 08:48:12.344269	2016-06-07 08:48:12.344269	0	\N	\N	\N	\N	1
13315	3417	Гикори	819.71	с копчёным вкусом; 2,360 кг	2017-02-21 16:25:38.565667	2017-02-23 12:27:21.072189	0	\N	TFVyLofHqJzcXmhdDWunpOIRkSCQKgUPEMvjiwtxbaGZerYBNl	piece		1
12187	3137	Кубанский элитный 	74.28	Экстра; 900 г; 1*12	2016-06-07 08:48:12.909954	2017-02-23 12:27:23.23385	0	\N		piece		1
12186	3137	Супер БАСМАТИ	79.86	Экстра; 500 г	2016-06-07 08:48:12.901352	2017-02-23 12:27:23.236683	0	\N		piece		1
12160	3132	Тип 00	1834.20	из мягких сортов пшеницы; 25 кг,1 уп.	2016-06-07 08:48:11.910297	2017-02-23 12:27:23.264957	0	\N	\N	\N	\N	1
12159	3131	«Печагин»	436.44	5 л; 1*2	2016-06-07 08:48:11.758948	2017-02-23 12:27:23.27027	0	\N		piece		1
12158	3131	«Печагин»	95.18	1 л; 1*15	2016-06-07 08:48:11.744132	2017-02-23 12:27:23.275224	0	\N		piece		1
12157	3130	«Де Чекко»	537.87	первый холодный отжим; 1 л; ст/б	2016-06-07 08:48:11.595622	2017-02-23 12:27:23.28003	0	\N		piece		1
12155	3130	«Cinquina»	332.97	0,25 л; с ароматами (Розмарин; Перец; Орегано); 1*12	2016-06-07 08:48:11.572474	2017-02-23 12:27:23.284664	0	\N		piece		1
12149	3129	Real Tang	1417.81	1,8 л; пл/бут*6	2016-06-07 08:48:11.355616	2017-02-23 12:27:23.295886	0	\N		piece		1
12148	3128	Масло для фритюра	505.29	в канистре; 5 л.	2016-06-07 08:48:11.20999	2017-02-23 12:27:23.300667	0	\N		\N	\N	1
12147	3127	Крахмал картофельный	75.72	500 г; 1/10	2016-06-07 08:48:11.081258	2017-02-23 12:27:23.305118	0	\N		piece		1
12188	3138	"Ризо Галло"	284.50	1 кг	2016-06-07 08:48:13.060711	2016-06-07 08:48:13.060711	0	\N	\N	\N	\N	1
12189	3138	"Галло"	92.10	0,5 кг,1 уп.	2016-06-07 08:48:13.072189	2016-06-07 08:48:13.072189	0	\N	\N	\N	\N	1
12143	3125	«Ароматико Корпосо»	596.26	250 г	2016-06-07 08:48:10.80667	2017-02-22 07:14:59.717858	0	\N		piece		1
12141	3125	«Кения АА»	551.19	250 г	2016-06-07 08:48:10.779889	2017-02-22 07:14:59.719892	0	\N		piece		1
12142	3125	«Биолоджика»	670.03	250 г	2016-06-07 08:48:10.792221	2017-02-22 07:15:21.36106	0	\N		piece		1
12205	3144	С белой плесенью	191.40	125 г,1 шт	2016-06-07 08:48:14.268898	2016-06-07 08:48:14.268898	0	\N	\N	\N	\N	1
12206	3144	Грана Падано	1655.20	безлактозный 1/32,1 кг	2016-06-07 08:48:14.280212	2016-06-07 08:48:14.280212	0	\N	\N	\N	\N	1
12207	3144	Джугас	833.85	безлактозный 40% 1/4,5 круг,1 кг	2016-06-07 08:48:14.294141	2016-06-07 08:48:14.294141	0	\N	\N	\N	\N	1
12208	3145	Маскарпоне	191.40	0,4 кг,1 шт	2016-06-07 08:48:14.448361	2016-06-07 08:48:14.448361	0	\N	\N	\N	\N	1
12209	3145	Маскарпоне	469.40	0,5 кг "Galbani",1 шт	2016-06-07 08:48:14.460812	2016-06-07 08:48:14.460812	0	\N	\N	\N	\N	1
12210	3146	в рассоле "Galbani"	67.80	0,125 г,1 шт	2016-06-07 08:48:14.621387	2016-06-07 08:48:14.621387	0	\N	\N	\N	\N	1
12211	3146	Мини в рассоле	86.40	0,150 г "Galbani",1 шт	2016-06-07 08:48:14.633448	2016-06-07 08:48:14.633448	0	\N	\N	\N	\N	1
12212	3146	Для пиццы	288.85/кг	2,2 кг,1 уп.	2016-06-07 08:48:14.647627	2016-06-07 08:48:14.647627	0	\N	\N	\N	\N	1
12213	3147	Гауда "Биберталлер"	351.70	1 кг	2016-06-07 08:48:14.794504	2016-06-07 08:48:14.794504	0	\N	\N	\N	\N	1
12214	3147	Голландский	365	шар 50% Бобровский СЗ,1 кг	2016-06-07 08:48:14.805232	2016-06-07 08:48:14.805232	0	\N	\N	\N	\N	1
12215	3147	Маасдам	526.50	"Сернурский сырзавод",1 кг	2016-06-07 08:48:14.818064	2016-06-07 08:48:14.818064	0	\N	\N	\N	\N	1
12216	3147	Парижская бурёнка (Фета)	129.35	 500 г,1 шт	2016-06-07 08:48:14.827557	2016-06-07 08:48:14.827557	0	\N	\N	\N	\N	1
12217	3147	Российский Сливочный	365	50%,1 кг	2016-06-07 08:48:14.840201	2016-06-07 08:48:14.840201	0	\N	\N	\N	\N	1
13008	3286	Диафрагма толстая 	520.96		2017-02-01 08:07:29.943812	2017-06-02 10:38:14.905397	0	\N	vXoSqIWcBTdVFOymegsRxlMjapHLPZtiDwGKEzYubkUCrJAQNn	kilo		1
13006	3286	Blade	1143.35		2017-02-01 08:07:29.938533	2017-06-02 10:38:14.908429	0	Блейд.jpg	AaqlKWrvfcixJoeCmZIHNVGgBDkFjLzXpYRSwdUPMhEsQbynOT	kilo		1
13157	3349	«Маасдам Flaman»	598.06	45%, круг	2017-02-10 18:48:37.182762	2017-06-07 09:15:38.499465	0	\N	JucpksZKSoODBVmaxwqhCvFRULMHgfrlbQTjEiydnAGXtzeNIP	kilo		1
13156	3349	«Маасдам Джагибо»	582.84		2017-02-10 18:48:37.179693	2017-06-07 09:15:38.502476	0	\N	PksMwuDQlYGqheHmbBLUXoZdajKWxtNCFifgvpTVzJOcSIynER	kilo		1
12201	3142	Сметана	89.55	30%, 350 г	2016-06-07 08:48:13.943724	2017-06-07 09:17:58.18801	0	\N		piece		1
12197	3141	Сливки 33%	153.67	0,5 л	2016-06-07 08:48:13.754968	2017-06-07 09:18:47.308728	0	\N		piece		1
12195	3140	Молоко	65.57	3,5%, 1 л	2016-06-07 08:48:13.59341	2017-06-07 09:19:11.581428	0	\N		piece		1
12192	3139	Масло сливочное	283.38	 82%, 500 г, Новая Зеландия	2016-06-07 08:48:13.424144	2017-06-07 09:20:19.848255	0	\N		piece		1
12191	3139	Масло сладко-сливочное	112.70	несолёное, 82,5%, 180 г	2016-06-07 08:48:13.41173	2017-06-07 09:20:19.850699	0	\N		piece		1
13358	3439	Грузди	117.82	отборные, маринованные; 580 г	2017-02-21 20:08:11.810628	2017-06-08 13:15:09.849868	0	\N	UixfRmNcIleuvrzQMgWPkohtpHGYjOnyJKXdZsqAbEwaTVLCSD	piece		1
12218	3147	Gold Blu	886.40	с плесенью,1 кг	2016-06-07 08:48:14.857164	2016-06-07 08:48:14.857164	0	\N	\N	\N	\N	1
12219	3147	Салакис (Фета)	200.70	 0,5 кг "President",1 шт	2016-06-07 08:48:14.86916	2016-06-07 08:48:14.86916	0	\N	\N	\N	\N	1
12220	3147	Скаморца	187.60	копчёный (0,235кг),1 шт	2016-06-07 08:48:14.882423	2016-06-07 08:48:14.882423	0	\N	\N	\N	\N	1
12221	3147	Творожный	129.35	45% из козьего молока 150 г,1 шт	2016-06-07 08:48:14.894529	2016-06-07 08:48:14.894529	0	\N	\N	\N	\N	1
12222	3147	Халумис	508.40	из коровьего молока,1 кг	2016-06-07 08:48:14.906409	2016-06-07 08:48:14.906409	0	\N	\N	\N	\N	1
12224	3147	Чеддер Красный	450.85	безлактозный,1 кг	2016-06-07 08:48:14.930455	2016-06-07 08:48:14.930455	0	\N	\N	\N	\N	1
12225	3147	Чеддер оранжевый	517.05	1 кг	2016-06-07 08:48:14.941784	2016-06-07 08:48:14.941784	0	\N	\N	\N	\N	1
12226	3148	Бешамель	1111.70	1,5 кг,1 уп.	2016-06-07 08:48:15.267341	2016-06-07 08:48:15.267341	0	\N	\N	\N	\N	1
12227	3148	Деми Гласс	1249.70	1,5 кг,1 уп.	2016-06-07 08:48:15.280965	2016-06-07 08:48:15.280965	0	\N	\N	\N	\N	1
12228	3148	Говяжий бульон	565.40	2 кг,1 уп.	2016-06-07 08:48:15.292079	2016-06-07 08:48:15.292079	0	\N	\N	\N	\N	1
12229	3148	Грибной бульон	690.05	2 кг,1 уп.	2016-06-07 08:48:15.302983	2016-06-07 08:48:15.302983	0	\N	\N	\N	\N	1
12230	3148	Куриный бульон	565.40	2 кг,1 уп.	2016-06-07 08:48:15.315987	2016-06-07 08:48:15.315987	0	\N	\N	\N	\N	1
12231	3148	Овощной бульон	592.30	2 кг,1 уп.	2016-06-07 08:48:15.328655	2016-06-07 08:48:15.328655	0	\N	\N	\N	\N	1
12232	3148	Рыбный бульон	368.20	0,925 кг,1 уп.	2016-06-07 08:48:15.340922	2016-06-07 08:48:15.340922	0	\N	\N	\N	\N	1
12233	3148	Приправа универсальная Деликат	283.15	1 кг,1 уп.	2016-06-07 08:48:15.352171	2016-06-07 08:48:15.352171	0	\N	\N	\N	\N	1
12234	3148	Светлая Пассеровка	682.05	1 кг,1 уп.	2016-06-07 08:48:15.362284	2016-06-07 08:48:15.362284	0	\N	\N	\N	\N	1
12235	3148	Тёмная Пассеровка	441.65	1 кг,1 уп.	2016-06-07 08:48:15.373714	2016-06-07 08:48:15.373714	0	\N	\N	\N	\N	1
12236	3148	Соус из чёрного перца	1127.65	2,3 кг,1 уп.	2016-06-07 08:48:15.384434	2016-06-07 08:48:15.384434	0	\N	\N	\N	\N	1
12237	3148	Суп-пюре из белых грибов	1615.2	1,6 кг,1 уп.	2016-06-07 08:48:15.395165	2016-06-07 08:48:15.395165	0	\N	\N	\N	\N	1
12238	3149	«Дижонская»	277.70	1 л,1 шт	2016-06-07 08:48:15.531638	2016-06-07 08:48:15.531638	0	\N	\N	\N	\N	1
12239	3149	«Дижонская»	137.60	370 г,1 шт	2016-06-07 08:48:15.54247	2016-06-07 08:48:15.54247	0	\N	\N	\N	\N	1
12240	3149	«Дижонская» зернистая	277.70	1 л,1 шт	2016-06-07 08:48:15.553753	2016-06-07 08:48:15.553753	0	\N	\N	\N	\N	1
12241	3149	острая "Дижон" зернистая	183.15	0.250 мл,1 шт	2016-06-07 08:48:15.565515	2016-06-07 08:48:15.565515	0	\N	\N	\N	\N	1
12242	3149	средне-острая	191.20	"Дижон" 250 мл,1 шт	2016-06-07 08:48:15.576396	2016-06-07 08:48:15.576396	0	\N	\N	\N	\N	1
12243	3150	томатный "Хайнс"	141.05	1 л,1 шт	2016-06-07 08:48:15.709383	2016-06-07 08:48:15.709383	0	\N	\N	\N	\N	1
12244	3150	томатный	126.15	18 * 25 г,1 шт	2016-06-07 08:48:15.723682	2016-06-07 08:48:15.723682	0	\N	\N	\N	\N	1
12246	3151	Mr Ricco 	108.45	850 мл,1 шт	2016-06-07 08:48:15.862794	2016-06-07 08:48:15.862794	0	\N	\N	\N	\N	1
12247	3151	Золотой	293.80	1 л,1 шт	2016-06-07 08:48:15.875362	2016-06-07 08:48:15.875362	0	\N	\N	\N	\N	1
12248	3151	"Домашний"	544.15	Хеллманс 5 л,1 шт	2016-06-07 08:48:15.88424	2016-06-07 08:48:15.88424	0	\N	\N	\N	\N	1
12249	3151	"Настоящий" 	1112.10	Хеллманс 5,3 л,1 шт	2016-06-07 08:48:15.893426	2016-06-07 08:48:15.893426	0	\N	\N	\N	\N	1
12250	3152	"Uncle Ben`s"	818.50	2,5 кг,1 шт	2016-06-07 08:48:16.023606	2016-06-07 08:48:16.023606	0	\N	\N	\N	\N	1
12251	3152	Соус барбекю	121.95	18 * 25 г,1 шт	2016-06-07 08:48:16.037623	2016-06-07 08:48:16.037623	0	\N	\N	\N	\N	1
12252	3152	с копчёным вкусом	971.30	Гикори 2360 г,1 шт	2016-06-07 08:48:16.049929	2016-06-07 08:48:16.049929	0	\N	\N	\N	\N	1
12245	3151	Mr Ricco 	108.45	800 мл; дой-пак,1 шт	2016-06-07 08:48:15.852506	2017-02-23 12:27:23.142011	0	\N	\N	\N	\N	1
12223	3147	"Cremette"	677.85	Хохланд; 65% 2 кг,1 уп.	2016-06-07 08:48:14.9187	2017-02-23 12:27:23.192354	0	\N	\N	\N	\N	1
12269	3153	Соус Табаско с красным перцем	375.05	150 мл,1 шт	2016-06-07 08:48:16.400085	2017-02-15 14:30:51.603609	0	\N		kilo		1
12268	3153	Соус Рыбный	206.90	700 мл 1/12 Тайланд,1 шт	2016-06-07 08:48:16.388219	2017-02-15 14:30:51.605557	0	\N		kilo		1
13159	3350	Сыр Маскарпоне	300.49	0,5 кг	2017-02-10 18:50:29.532309	2017-02-23 04:17:16.252402	0	\N	AtyTFaDMJcBChPnkjIEqZQWNOUmSHzYlowgLRKdGfeubprXVvs	piece		1
12276	3154	"Синг Сонг"	77.60	1 л,1 шт	2016-06-07 08:48:16.602626	2016-06-07 08:48:16.602626	0	\N	\N	\N	\N	1
12277	3154	Киккоман	139.70	150 мл диспенсер,1 шт	2016-06-07 08:48:16.615453	2016-06-07 08:48:16.615453	0	\N	\N	\N	\N	1
12278	3154	Киккоман	510.85	1 л,1 шт	2016-06-07 08:48:16.625774	2016-06-07 08:48:16.625774	0	\N	\N	\N	\N	1
12279	3154	Киккоман	1694.55	5 л,1 шт	2016-06-07 08:48:16.637808	2016-06-07 08:48:16.637808	0	\N	\N	\N	\N	1
12280	3154	Киккоман	2715.15	19 л,1 шт	2016-06-07 08:48:16.652462	2016-06-07 08:48:16.652462	0	\N	\N	\N	\N	1
12281	3154	Ямаса	2650.95	18 л,1 шт	2016-06-07 08:48:16.662886	2016-06-07 08:48:16.662886	0	\N	\N	\N	\N	1
12282	3155	бальз.белый 	431.40	Карандини 500 мл,1 шт	2016-06-07 08:48:16.79919	2016-06-07 08:48:16.79919	0	\N	\N	\N	\N	1
12283	3155	бальзамический "IL TORRIONE"	274.25	Карандини 500 мл,1 шт	2016-06-07 08:48:16.810861	2016-06-07 08:48:16.810861	0	\N	\N	\N	\N	1
12284	3155	Со вкусом белого трюфеля "IL TORRIONE"	377.60	Карандини 500 мл,1 шт	2016-06-07 08:48:16.821519	2016-06-07 08:48:16.821519	0	\N	\N	\N	\N	1
12285	3155	ЯГОДНЫЙ бальзамический "IL TORRIONE"	362.10	Карандини 250 мл,1 шт	2016-06-07 08:48:16.835262	2016-06-07 08:48:16.835262	0	\N	\N	\N	\N	1
12286	3156	белый	237.95	Карандини 0,500,1 шт	2016-06-07 08:48:16.978474	2016-06-07 08:48:16.978474	0	\N	\N	\N	\N	1
12287	3156	"IL TORRIONE" 1 год	147.35	Карандини 0,500,1 шт	2016-06-07 08:48:16.991203	2016-06-07 08:48:16.991203	0	\N	\N	\N	\N	1
12288	3156	"IL TORRIONE"	1109.55	Карандини 5 л,1 шт	2016-06-07 08:48:17.000112	2016-06-07 08:48:17.000112	0	\N	\N	\N	\N	1
12289	3157	сливочный	167.10	250 мл,1 шт	2016-06-07 08:48:17.148512	2016-06-07 08:48:17.148512	0	\N	\N	\N	\N	1
13168	3353	Сыр с белой плесенью	183.39	125 г	2017-02-10 19:08:01.4593	2017-06-07 09:07:48.706349	0	\N	OyQcXeAtnzvFWrbYdJVTgHfKZhsPMLUlNiaExkBCqSGjRowDuI	piece		1
13165	3352	Творожный сыр 45% 	307.35	из козьего молока, 150 г	2017-02-10 18:59:51.538683	2017-06-07 09:11:43.725329	0	\N	ISxaDhebsdGtTgFlcQkHZmRJoULyKupPvqjCXnrzANYVfwOBWE	piece		1
13163	3351	«Моцарелла» мини	101.07	в рассоле, 0,150 г	2017-02-10 18:56:22.499639	2017-06-07 09:12:56.987423	0	\N	BziAQpsUJahSvRykeoljLTVEZCrOfwmDPxIKYFqWtcdgnMGbNH	piece		1
12293	3159	томатный "Хайнс"	141.05	1 л,1 шт	2016-06-07 08:48:17.426237	2016-06-07 08:48:17.426237	0	\N	\N	\N	\N	1
12929	3276	Вафельный рожок (маленький)	1206.97	10 гр; 340 шт	2017-01-29 11:43:21.520358	2017-02-23 12:27:22.058357	0	\N	bUDaVPSuGCXmrBLeRAQcnkOpjwziyNYJtTZlMgIvqoKWhHdFsx	\N	\N	1
12091	3118	Посыпка декоративная	162.05	фигурная; ведро п/э; 600 г,1 уп.	2016-06-07 08:48:08.329379	2017-02-23 12:27:23.414108	0	\N	\N	\N	\N	1
12951	3472	Шоколадное	2500.60	4,8 л; 2,730 кг	2017-01-29 12:05:05.688624	2017-03-01 18:36:20.733182	0	\N	lidHUjeNKIuxagpzPhBDGfcQJEnFvwYZAyTCMXmLkoqWRVrtbS	piece		1
12275	3153	соус для пиццы	586.60	Томатный классический "Мутти" (4,100 кг) ж/б*3 шт,1 шт	2016-06-07 08:48:16.473583	2017-02-15 14:30:51.590515	0	\N		kilo		1
12274	3153	Соус Халапеньо	879.35	980 г,1 шт	2016-06-07 08:48:16.462025	2017-02-15 14:30:51.594482	0	\N		kilo		1
12292	3158	малиновый	402.45	1 шт	2016-06-07 08:48:17.302615	2016-12-28 10:16:46.645992	0	IMG_0483.jpg		\N	\N	1
13251	3381	Перечный	553.54	с чесноком; 150 мл.	2017-02-17 19:16:07.528299	2017-02-23 12:27:21.276004	0	\N	dHVaxtYkbesMGXhuilfJKroTqgcjvWZUIDLNQPEpnFAzOwmCyR	piece		1
12959	3471	Пломбир "ГЮВ"	430.29	3 кг	2017-01-29 12:05:05.705802	2017-02-28 18:52:51.226554	0	\N	lidHUjeNKIuxagpzPhBDGfcQJEnFvwYZAyTCMXmLkoqWRVrtbS	piece		1
13166	3352	Творожный сыр 70%	407.24	1,5 кг	2017-02-10 19:03:19.138527	2017-06-07 09:11:43.723489	0	\N	mqtgwWcaifJuoOEMVZSvxLyFlRKBCADhTzQNdpbGjIsXkrPUne	kilo		1
12376	3163	Топинг «Экзотика»	272	1 л, "Dolce Rosa"	2016-06-07 08:48:19.107048	2017-06-05 11:57:02.483242	0	\N		piece		1
13169	3353	Сыр с плесенью "Gold Blu"	883.64		2017-02-10 19:08:01.462755	2017-06-07 09:07:48.702487	0	\N	VLIOAPXTjfEqhMDSdYptRzZuHikbxoWnrmQFCGKNlseUcvJgBw	kilo		1
13177	3354	Камамбер «Де Фамиль»	192.10	50%, 130 г	2017-02-11 11:34:17.644302	2017-06-07 09:10:19.595663	0	\N	PvEKamRwYSdxZDHolWctNsGUIOfjVyQTnLMAkFqrgiCpBheJub	piece		1
13175	3354	Сыр «Гран Лео»	1475.28	1-1,5 кг	2017-02-11 11:34:17.638625	2017-06-07 09:10:19.597181	0	\N	PvEKamRwYSdxZDHolWctNsGUIOfjVyQTnLMAkFqrgiCpBheJub	kilo		1
13164	3351	«Моцарелла» тёртый	314.73	1*10	2017-02-10 18:56:22.502005	2017-06-07 09:12:56.985722	0	\N	BziAQpsUJahSvRykeoljLTVEZCrOfwmDPxIKYFqWtcdgnMGbNH	piece		1
13162	3351	«Моцарелла» для пиццы	292.19	2,2 кг	2017-02-10 18:56:22.497311	2017-06-07 09:12:56.988944	0	\N	BziAQpsUJahSvRykeoljLTVEZCrOfwmDPxIKYFqWtcdgnMGbNH	kilo		1
12425	3165	Луковый порошок	567.55	570 г	2016-06-07 08:48:20.132083	2017-07-05 12:10:33.197748	0	\N		piece		1
12424	3165	Лавровый лист	187.15	45 г	2016-06-07 08:48:20.12149	2017-07-05 12:10:33.199685	0	\N		piece		1
12423	3165	Куркума	408.75	500 г	2016-06-07 08:48:20.111674	2017-07-05 12:10:33.201477	0	\N		piece		1
12422	3165	Кумин	564.35	молотый, 430 г	2016-06-07 08:48:20.099728	2017-07-05 12:10:33.203385	0	\N		piece		1
12419	3165	Карри	444.35	молотый, 480 г	2016-06-07 08:48:20.067576	2017-07-05 12:10:33.205221	0	\N		piece		1
12398	3164	Сироп «Лайм Джюс»	488.85	1 л, Monin	2016-06-07 08:48:19.500679	2017-06-05 11:27:40.586672	0	\N		piece		1
12397	3164	Сироп «Кокос»	616.90	1 л, Monin	2016-06-07 08:48:19.48866	2017-06-05 11:27:40.589145	0	\N		piece		1
12310	3161	Наполнитель «Шоколад»	272.10	2 л, "Miller&Miller"	2016-06-07 08:48:18.03242	2017-06-05 11:40:13.23052	0	\N		piece		1
12309	3161	Наполнитель «Малина»	258.65	2 л, "Miller&Miller"	2016-06-07 08:48:18.021498	2017-06-05 11:40:13.233524	0	\N		piece		1
12308	3161	Наполнитель «Лесная ягода»	258.65	2 л, "Miller&Miller"	2016-06-07 08:48:18.009671	2017-06-05 11:40:13.235884	0	\N		piece		1
12307	3161	Наполнитель «Клубника»	258.65	2 л, "Miller&Miller"	2016-06-07 08:48:17.998401	2017-06-05 11:40:13.237831	0	\N		piece		1
12306	3161	Наполнитель «Ваниль»	258.65	2 л, "Miller&Miller"	2016-06-07 08:48:17.987851	2017-06-05 11:40:13.239961	0	\N		piece		1
12305	3161	Наполнитель «Банан»	258.65	2 л, "Miller&Miller"	2016-06-07 08:48:17.974225	2017-06-05 11:40:13.241829	0	\N		piece		1
12304	3160	Барный сироп «Тархун»	224.60	0,75 л, "Miller&Miller"	2016-06-07 08:48:17.849077	2017-06-05 11:48:59.261487	0	\N		piece		1
12303	3160	Барный сироп «Огуречный»	226.60	0,75 л, "Miller&Miller"	2016-06-07 08:48:17.838279	2017-06-05 11:48:59.263611	0	\N		piece		1
12302	3160	Барный сироп «Миндаль»	226.60	0,75 л, "Miller&Miller"	2016-06-07 08:48:17.826487	2017-06-05 11:48:59.265088	0	\N		piece		1
12301	3160	Барный сироп «Маракуйя»	226.60	0,75 л, "Miller&Miller"	2016-06-07 08:48:17.812768	2017-06-05 11:48:59.266304	0	\N		piece		1
12300	3160	Барный сироп «Клубника»	224.60	0,75 л, "Miller&Miller"	2016-06-07 08:48:17.802411	2017-06-05 11:48:59.267455	0	\N		piece		1
12299	3160	Барный сироп «Карамель»	224.60	0,75 л, "Miller&Miller"	2016-06-07 08:48:17.791779	2017-06-05 11:48:59.268636	0	\N		piece		1
12298	3160	Барный сироп «Имбирь»	226.60	0,75 л, "Miller&Miller"	2016-06-07 08:48:17.777402	2017-06-05 11:48:59.269794	0	\N		piece		1
12297	3160	Барный сироп «Дыня»	226.35	0,75 л, "Miller&Miller"	2016-06-07 08:48:17.764984	2017-06-05 11:48:59.270873	0	\N		piece		1
12296	3160	Барный сироп «Груша»	226.60	0,75 л, "Miller&Miller"	2016-06-07 08:48:17.753576	2017-06-05 11:48:59.272139	0	\N		piece		1
12949	3277	Фисташковое	2984.32	4,8 л; 2,930 кг	2017-01-29 12:05:05.684182	2017-03-01 18:36:48.965518	0	\N	lidHUjeNKIuxagpzPhBDGfcQJEnFvwYZAyTCMXmLkoqWRVrtbS	piece		1
12948	3472	Тирамису 	2984.12	4,8 л; 2,568 кг	2017-01-29 12:05:05.681799	2017-03-01 18:36:20.735168	0	\N	lidHUjeNKIuxagpzPhBDGfcQJEnFvwYZAyTCMXmLkoqWRVrtbS	piece		1
12492	3179	По-римски	404.40	 в масле "И.П.О.С.Е.А." (580 мл) ст/б*12 шт.,1 уп.	2016-06-07 08:48:22.903038	2016-06-07 08:48:22.903038	0	\N	\N	\N	\N	1
12495	3181	Луковички	196.60	красные марин. по-деревенски 520г.1/6,1 уп.	2016-06-07 08:48:23.166894	2016-06-07 08:48:23.166894	0	\N	\N	\N	\N	1
12437	3165	Чесночный порошок	616.50	700 г	2016-06-07 08:48:20.283749	2017-07-05 12:10:33.185647	0	\N		piece		1
12455	3166	«Чили Чипотль»	791	перец, 550 г, 1 уп.	2016-06-07 08:48:20.616787	2017-07-05 11:38:30.085008	0	\N		piece		1
12454	3166	«Чили Гуахийо»	719.10	перец, 500 г, 1 уп.	2016-06-07 08:48:20.603983	2017-07-05 11:38:30.088699	0	\N		piece		1
12453	3166	«Чили Анчо»	889.50	перец, 50 г, 1 уп.	2016-06-07 08:48:20.593148	2017-07-05 11:38:30.092113	0	\N		piece		1
12452	3166	«Смесь пяти перцев»	1117.30	дроблёная, 500 г, 1 уп.	2016-06-07 08:48:20.581866	2017-07-05 11:38:30.100397	0	\N		piece		1
12439	3165	Эстрагон	647.55	105 г	2016-06-07 08:48:20.305684	2017-07-05 12:10:33.183132	0	\N		piece		1
12438	3165	Шафран	262.25	0,25 г	2016-06-07 08:48:20.294957	2017-07-05 12:10:33.184621	0	\N		piece		1
12436	3165	Фенхель	347.95	300 г	2016-06-07 08:48:20.272078	2017-07-05 12:10:33.186761	0	\N		piece		1
12434	3165	Тимьян	400.15	170 г	2016-06-07 08:48:20.249525	2017-07-05 12:10:33.187834	0	\N		piece		1
12433	3165	Орегано	153.95	90 г	2016-06-07 08:48:20.235353	2017-07-05 12:10:33.18888	0	\N		piece		1
12469	3168	Паста «Базилик»	281.30	250 г	2016-06-07 08:48:21.03443	2017-07-05 14:25:23.654631	0	\N		piece		1
12468	3168	Чиполетте «Чили»	833.65	750 г	2016-06-07 08:48:21.022895	2017-07-05 14:25:23.658268	0	\N		piece		1
12467	3168	Карри, зелёная	452	470 г	2016-06-07 08:48:21.01108	2017-07-05 14:25:23.660382	0	\N		piece		1
12472	3169	Хмели-сунели	173.20	400 г	2016-06-07 08:48:21.216442	2017-07-05 14:25:44.003538	0	\N		piece		1
12515	3187	Филе тунца в соку	706.40	1,7 кг, (продукт 1,2 кг), ж/б	2016-06-07 08:48:24.106076	2017-07-17 11:20:53.606529	0	\N		piece	00000000	1
12471	3169	Розмарин	173.90	250 г	2016-06-07 08:48:21.204266	2017-07-05 14:25:44.00533	0	\N		piece		1
12470	3169	Базилик	102.45	125 г	2016-06-07 08:48:21.189018	2017-07-05 14:25:44.006773	0	\N		piece		1
12475	3170	Прованские травы	781.80	в масле, 580 мл, стекло	2016-06-07 08:48:21.374821	2017-07-05 14:26:55.157861	0	\N		piece		1
12474	3170	Итальянские травы	781.80	в масле	2016-06-07 08:48:21.364644	2017-07-05 14:26:55.159162	0	\N		piece		1
12473	3170	Базилик	706.70	в масле	2016-06-07 08:48:21.3528	2017-07-05 14:26:55.1605	0	\N		piece		1
12431	3165	Паприка молотая	501.15	480 г	2016-06-07 08:48:20.211681	2017-07-05 12:10:33.190954	0	\N		piece		1
12513	3186	Персик в сиропе	173.14	Половинки, ж/б, 850 г	2016-06-07 08:48:23.96271	2017-06-09 12:05:20.064103	0	\N		piece		1
12531	3195	Имбирь белый	193.50	маринованный, фасованный	2016-06-07 08:48:25.526342	2017-06-07 08:47:41.439284	0	\N		kilo		1
13179	3354	Сыр «Бридель»	1497.82	рассольный, 3,4 кг	2017-02-11 11:34:17.65049	2017-06-07 09:10:19.593501	0	\N	PvEKamRwYSdxZDHolWctNsGUIOfjVyQTnLMAkFqrgiCpBheJub	piece		1
12511	3185	Томаты сушёные, в масле	770.94	1,6 кг, стекло	2016-06-07 08:48:23.820524	2017-06-09 11:50:16.714713	0	\N		piece		1
12505	3183	Оливки таджарские	1338.41	б/к, 0,95 л	2016-06-07 08:48:23.510591	2017-06-09 11:53:08.326995	0	\N		piece		1
12430	3165	Паприка копчёная	506.95	230 г	2016-06-07 08:48:20.198247	2017-07-05 12:10:33.192009	0	\N		piece		1
12516	3188	Фасоль красная, в соку	92.20	400 г, ж/б	2016-06-07 08:48:24.25828	2017-06-09 12:05:46.799902	0	\N		piece		1
12457	3167	«К грилю»	484.15	специи, 710 г, 1 уп.	2016-06-07 08:48:20.76653	2017-07-05 11:29:53.265547	0	\N		piece		1
12427	3165	Можжевеловая ягода	562.70	340 г	2016-06-07 08:48:20.159433	2017-07-05 12:10:33.194075	0	\N		piece		1
12451	3166	Перец чёрный молотый	721.05	470 г, 1 уп.	2016-06-07 08:48:20.571436	2017-07-05 11:34:00.46001	0	\N		piece		1
12450	3166	Перец чёрный дроблёный	721.05	450 г, 1 уп.	2016-06-07 08:48:20.560005	2017-07-05 11:34:00.461667	0	\N		piece		1
12449	3166	Перец чёрный, горошек	721.05	530 г, 1 уп.	2016-06-07 08:48:20.548344	2017-07-05 11:34:00.463334	0	\N		piece		1
12448	3166	Перец с лимонным ароматом	413.20	720 г, 1 уп.	2016-06-07 08:48:20.536312	2017-07-05 11:34:00.464997	0	\N		piece		1
12447	3166	Перец белый целый	1661.65	640 г, 1 уп.	2016-06-07 08:48:20.524385	2017-07-05 11:34:00.466654	0	\N		piece		1
12944	3277	Крем Брюле	2645.06	4,8 л	2017-01-29 12:05:05.672477	2017-03-01 18:36:48.967663	0	\N	lidHUjeNKIuxagpzPhBDGfcQJEnFvwYZAyTCMXmLkoqWRVrtbS	piece		1
12943	3473	Клубничное	2620.37	4,8 л; 2,730 кг	2017-01-29 12:05:05.669985	2017-03-01 17:51:22.95197	0	\N	lidHUjeNKIuxagpzPhBDGfcQJEnFvwYZAyTCMXmLkoqWRVrtbS	piece		1
12945	3473	Лимонно-лаймовый сорбет	3185.12	4,8 л; 2,890 кг	2017-01-29 12:05:05.674867	2017-03-01 18:36:00.920017	0	\N	lidHUjeNKIuxagpzPhBDGfcQJEnFvwYZAyTCMXmLkoqWRVrtbS	piece		1
12942	3277	Карамельное 	2645.06	4,8 л; 2,822 кг	2017-01-29 12:05:05.66755	2017-02-28 18:54:26.56944	0	\N	lidHUjeNKIuxagpzPhBDGfcQJEnFvwYZAyTCMXmLkoqWRVrtbS	piece		1
12941	3277	Грецкий орех	2645.06	4,8 л	2017-01-29 12:05:05.6651	2017-02-28 18:54:26.570773	0	\N	lidHUjeNKIuxagpzPhBDGfcQJEnFvwYZAyTCMXmLkoqWRVrtbS	piece		1
12940	3277	Ванильное	2510.85	4,8 л; 2,410 кг	2017-01-29 12:05:05.662686	2017-02-28 18:54:26.572217	0	\N	lidHUjeNKIuxagpzPhBDGfcQJEnFvwYZAyTCMXmLkoqWRVrtbS	piece		1
12552	3200	Тальерини №105	350	20 мл	2016-06-07 08:48:27.699093	2016-06-07 08:48:27.699093	0	\N	\N	\N	\N	1
12553	3200	Канелоне	365		2016-06-07 08:48:27.70935	2016-06-07 08:48:27.70935	0	\N	\N	\N	\N	1
12554	3200	Оливки «Maestro de Oliva»	1850	1 кг	2016-06-07 08:48:27.722846	2016-06-07 08:48:27.722846	0	\N	\N	\N	\N	1
12555	3200	Карандини «Il Torrione»	762	200 мл	2016-06-07 08:48:27.734964	2016-06-07 08:48:27.734964	0	\N	\N	\N	\N	1
12541	3198	Фунчоза	126.75	0,5 кг	2016-06-07 08:48:26.051677	2017-06-07 08:44:39.240115	0	\N		piece		1
12540	3198	Удон	39.35	0,3 кг	2016-06-07 08:48:26.040346	2017-06-07 08:44:39.242301	0	\N		piece		1
12539	3198	Соба	39.35	0,3 кг	2016-06-07 08:48:26.027259	2017-06-07 08:44:39.244076	0	\N		piece		1
12538	3198	Харусаме	129.35	Рисовая, 0,5 кг	2016-06-07 08:48:26.015961	2017-06-07 08:44:39.245836	0	\N		piece		1
12537	3198	Рамен	212.10	в/м, 1 кг	2016-06-07 08:48:26.005389	2017-06-07 08:44:39.247576	0	\N		piece		1
12536	3197	Паста соевая тёмная	155.20		2016-06-07 08:48:25.856949	2017-06-07 08:45:20.2434	0	\N		kilo		1
12535	3197	Паста соевая белая	155.20		2016-06-07 08:48:25.843333	2017-06-07 08:45:20.245116	0	\N		piece		1
12534	3196	Кунжут чёрный	237.95	0,5 кг	2016-06-07 08:48:25.695404	2017-06-07 08:46:10.531589	0	\N		piece		1
12530	3194	Икра масаго оранжевая	486.25	0,5 кг	2016-06-07 08:48:25.3749	2017-06-07 08:48:40.541735	0	\N		piece		1
12529	3194	Икра масаго красная	429.35	0,5 кг	2016-06-07 08:48:25.361167	2017-06-07 08:48:40.543909	0	\N		piece		1
12532	3195	Имбирь розовый	164.80	маринованный, фасованный	2016-06-07 08:48:25.540394	2017-06-07 08:47:41.437012	0	\N		kilo		1
12533	3196	Кунжут белый	300.05	фасованный	2016-06-07 08:48:25.68181	2017-06-07 08:47:58.198622	0	\N		kilo		1
12528	3194	Икра масаго чёрная	382.50	0,5 кг	2016-06-07 08:48:25.349315	2017-06-07 08:48:40.545956	0	\N		piece		1
12527	3194	Икра масаго зелёная	522.15	0,5 кг	2016-06-07 08:48:25.337963	2017-06-07 08:48:40.547957	0	\N		piece		1
12526	3193	Тобико чёрная	537.95	0,5 кг	2016-06-07 08:48:25.210503	2017-06-07 08:49:29.429496	0	\N		piece		1
12525	3193	Тобико оранжевая	584.50	0,5 кг	2016-06-07 08:48:25.199095	2017-06-07 08:49:29.431602	0	\N		piece		1
12524	3193	Тобико малиновая	569	0,5 кг	2016-06-07 08:48:25.185385	2017-06-07 08:49:29.433546	0	\N		piece		1
12523	3193	Тобико зелёная	481.05	0,5 кг	2016-06-07 08:48:25.168023	2017-06-07 08:49:29.435418	0	\N		piece		1
12522	3192	Шиитаки сушёные	452.60	 0,25 кг	2016-06-07 08:48:25.01796	2017-06-07 08:49:56.343395	0	\N		piece		1
12521	3192	Древесные грибы	785.20	чёрные	2016-06-07 08:48:25.005755	2017-06-07 08:49:56.345214	0	\N		kilo		1
12520	3191	Вакаме сушёные 	386.80	 0,5 кг	2016-06-07 08:48:24.84488	2017-06-07 08:50:27.387177	0	\N		piece		1
12519	3191	Нори «Золотые»	235.90	50 листов	2016-06-07 08:48:24.83301	2017-06-07 08:50:27.389124	0	\N		piece		1
12518	3190	Васаби в порошке	234.65		2016-06-07 08:48:24.685153	2017-06-07 08:50:47.290849	0	\N		kilo		1
12510	3185	Томаты сушёные, в масле	537.72	0,9 кг, стекло	2016-06-07 08:48:23.810086	2017-06-09 11:50:16.719608	0	\N		piece		1
12504	3183	Оливки сицилийские 	454.68	с/к, пластиковое ведро, 930 г	2016-06-07 08:48:23.500624	2017-06-09 11:53:08.328934	0	\N		piece		1
12491	3178	Корнишоны	98.61	3-6 см, стекло, 720 мл	2016-06-07 08:48:22.773109	2017-06-09 11:55:17.855491	0	\N		piece		1
12498	3182	Маслины без косточек	79.91	314 мл	2016-06-07 08:48:23.321902	2017-06-09 11:53:57.594255	0	\N		piece		1
12501	3183	Оливки без косточек	91.19	350 г	2016-06-07 08:48:23.466555	2017-06-09 11:54:07.451297	0	\N		piece		1
12490	3177	Каперсы в уксусе	391.88	«Пунтина Лакримелла»; 720 мл, стекло	2016-06-07 08:48:22.620342	2017-06-09 11:57:59.764761	0	\N		piece		1
12489	3177	Каперсы с ножкой	458.26	«Лакримелла» 0,580 г	2016-06-07 08:48:22.608259	2017-06-09 11:57:11.675112	0	\N		piece		1
12488	3176	Груши в сиропе 	126.02	половинки, 850 мл	2016-06-07 08:48:22.459203	2017-06-09 11:58:41.8346	0	\N		piece		1
12486	3175	Шампиньоны резаные	409.70	«Бояринъ», ж/б, 310 мл	2016-06-07 08:48:22.316109	2017-06-09 12:01:37.888047	0	\N		piece		1
12485	3175	Опята маринованные	126.12	«Бояринъ», стекло, 580 мл	2016-06-07 08:48:22.304004	2017-06-09 12:01:37.889912	0	\N		piece		1
12484	3175	Маслята	126.53	«Бояринъ», 580 г	2016-06-07 08:48:22.291312	2017-06-09 12:01:37.891704	0	\N		piece		1
12482	3174	Горошек консервированный	79.92	425 мл, ж/б	2016-06-07 08:48:22.145785	2017-06-09 12:02:20.238293	0	\N		piece		1
12480	3173	Артишоки по-римски	402.12	в масле, 580 мл, стекло	2016-06-07 08:48:22.006313	2017-06-09 12:02:44.153548	0	\N		piece		1
12477	3171	Ананасы кусочками 	164.54	в сиропе, ж/б, 850 мл	2016-06-07 08:48:21.692929	2017-06-09 12:04:13.134657	0	\N		piece		1
12479	3172	Филе анчоуса	1102.37	в подсолнечном масле, «Манчин»; ж/б, 400/600 г	2016-06-07 08:48:21.855633	2017-06-09 12:03:19.52267	0	\N		piece		1
12506	3184	Молоко кокосовое	121.00	55%, 400 мл	2016-06-07 08:48:23.638998	2017-06-09 12:04:34.45939	0	\N		piece		1
12556	3201	Тальерини №105	350	20 мл	2016-06-07 08:48:28.46063	2016-06-07 08:48:28.46063	0	\N	\N	\N	\N	1
12557	3201	Канелоне	365		2016-06-07 08:48:28.472905	2016-06-07 08:48:28.472905	0	\N	\N	\N	\N	1
12558	3201	Карандини «Il Torrione»	762	200 мл	2016-06-07 08:48:28.484868	2016-06-07 08:48:28.484868	0	\N	\N	\N	\N	1
12559	3202	Тальерини №105	350	20 мл	2016-06-07 08:48:29.657478	2016-06-07 08:48:29.657478	0	\N	\N	\N	\N	1
12560	3202	Канелоне	365		2016-06-07 08:48:29.670202	2016-06-07 08:48:29.670202	0	\N	\N	\N	\N	1
12561	3202	Оливки «Maestro de Oliva»	1850	1 кг	2016-06-07 08:48:29.680914	2016-06-07 08:48:29.680914	0	\N	\N	\N	\N	1
12562	3202	Карандини «Il Torrione»	762	200 мл	2016-06-07 08:48:29.692687	2016-06-07 08:48:29.692687	0	\N	\N	\N	\N	1
12563	3203	Тальерини №105	350	20 мл	2016-06-07 08:48:30.840723	2016-06-07 08:48:30.840723	0	\N	\N	\N	\N	1
12564	3204	Тальерини №105	350	20 мл	2016-06-07 08:48:31.584609	2016-06-07 08:48:31.584609	0	\N	\N	\N	\N	1
12565	3204	Канелоне	365		2016-06-07 08:48:31.594339	2016-06-07 08:48:31.594339	0	\N	\N	\N	\N	1
12566	3204	Карандини «Il Torrione»	762	200 мл	2016-06-07 08:48:31.605862	2016-06-07 08:48:31.605862	0	\N	\N	\N	\N	1
12567	3205	Тальерини №105	350	20 мл	2016-06-07 08:48:31.782054	2016-06-07 08:48:31.782054	0	\N	\N	\N	\N	1
12568	3205	Канелоне	365		2016-06-07 08:48:31.792264	2016-06-07 08:48:31.792264	0	\N	\N	\N	\N	1
12569	3205	Оливки «Maestro de Oliva»	1850	1 кг	2016-06-07 08:48:31.801516	2016-06-07 08:48:31.801516	0	\N	\N	\N	\N	1
12570	3205	Карандини «Il Torrione»	762	200 мл	2016-06-07 08:48:31.814067	2016-06-07 08:48:31.814067	0	\N	\N	\N	\N	1
12571	3206	Тальерини №105	350	20 мл	2016-06-07 08:48:32.744867	2016-06-07 08:48:32.744867	0	\N	\N	\N	\N	1
12572	3206	Канелоне	365		2016-06-07 08:48:32.758931	2016-06-07 08:48:32.758931	0	\N	\N	\N	\N	1
12573	3206	Оливки «Maestro de Oliva»	1850	1 кг	2016-06-07 08:48:32.771265	2016-06-07 08:48:32.771265	0	\N	\N	\N	\N	1
12574	3206	Карандини «Il Torrione»	762	200 мл	2016-06-07 08:48:32.78297	2016-06-07 08:48:32.78297	0	\N	\N	\N	\N	1
12575	3207	Тальерини №105	350	20 мл	2016-06-07 08:48:33.489336	2016-06-07 08:48:33.489336	0	\N	\N	\N	\N	1
12576	3207	Канелоне	365		2016-06-07 08:48:33.502227	2016-06-07 08:48:33.502227	0	\N	\N	\N	\N	1
12577	3207	Карандини «Il Torrione»	762	200 мл	2016-06-07 08:48:33.514128	2016-06-07 08:48:33.514128	0	\N	\N	\N	\N	1
12578	3208	Тальерини №105	350	20 мл	2016-06-07 08:48:34.568198	2016-06-07 08:48:34.568198	0	\N	\N	\N	\N	1
12579	3208	Канелоне	365		2016-06-07 08:48:34.579162	2016-06-07 08:48:34.579162	0	\N	\N	\N	\N	1
12580	3208	Оливки «Maestro de Oliva»	1850	1 кг	2016-06-07 08:48:34.59802	2016-06-07 08:48:34.59802	0	\N	\N	\N	\N	1
12581	3208	Карандини «Il Torrione»	762	200 мл	2016-06-07 08:48:34.615068	2016-06-07 08:48:34.615068	0	\N	\N	\N	\N	1
12582	3209	Тальерини №105	350	20 мл	2016-06-07 08:48:35.760331	2016-06-07 08:48:35.760331	0	\N	\N	\N	\N	1
12583	3210	Тальерини №105	350	20 мл	2016-06-07 08:48:36.458282	2016-06-07 08:48:36.458282	0	\N	\N	\N	\N	1
12584	3210	Канелоне	365		2016-06-07 08:48:36.470962	2016-06-07 08:48:36.470962	0	\N	\N	\N	\N	1
12585	3210	Карандини «Il Torrione»	762	200 мл	2016-06-07 08:48:36.485301	2016-06-07 08:48:36.485301	0	\N	\N	\N	\N	1
12586	3211	Тальерини №105	350	20 мл	2016-06-07 08:48:36.66421	2016-06-07 08:48:36.66421	0	\N	\N	\N	\N	1
12587	3211	Канелоне	365		2016-06-07 08:48:36.676604	2016-06-07 08:48:36.676604	0	\N	\N	\N	\N	1
12588	3211	Оливки «Maestro de Oliva»	1850	1 кг	2016-06-07 08:48:36.688669	2016-06-07 08:48:36.688669	0	\N	\N	\N	\N	1
12589	3211	Карандини «Il Torrione»	762	200 мл	2016-06-07 08:48:36.698791	2016-06-07 08:48:36.698791	0	\N	\N	\N	\N	1
12590	3212	Кальмар дальневосточный	115.75	1 кг	2016-06-08 10:53:30.128115	2016-06-08 10:53:30.128115	0	\N	\N	\N	\N	1
12591	3212	Кальмар, кольца в кляре	325.90	1 кг	2016-06-08 10:53:30.156396	2016-06-08 10:53:30.156396	0	\N	\N	\N	\N	1
12592	3212	Кальмар свежемороженный,	469.15	10-15 см (Испания),1 кг	2016-06-08 10:53:30.177984	2016-06-08 10:53:30.177984	0	\N	\N	\N	\N	1
12593	3212	Трубки кальмара	199.50	U10,1 кг	2016-06-08 10:53:30.193117	2016-06-08 10:53:30.193117	0	\N	\N	\N	\N	1
12602	3216	Креветка	1077.95	с/м б/г; блок 21/25; Индия,1 кг	2016-06-08 10:53:32.765141	2017-02-23 12:27:22.735106	0	\N		\N	\N	1
12600	3216	Креветка аргентинская	607.80	б/г; 30/40; весовая,1 кг	2016-06-08 10:53:32.677777	2017-02-23 12:27:22.740002	0	\N		\N	\N	1
12185	3137	Пропаренный	74.28	Экстра; 900 г; 1*12	2016-06-07 08:48:12.889646	2017-02-23 12:27:23.239458	0	\N		piece		1
12177	3136	Феделини	119.80	№10; 0,5 кг	2016-06-07 08:48:12.642093	2017-02-23 12:27:23.242466	0	\N		piece		1
13285	3391				2017-02-20 07:47:26.000516	2017-02-20 07:47:26.000516	0	\N	lbPtpgmAuOGvHDWYRkjJwTdSVFzLsQIiXEnfcZKorqyUxCNBMa	piece		1
13286	3392				2017-02-20 12:19:07.475182	2017-02-20 12:19:07.475182	0	\N	urIFEybqpXGLdVUjJMSfvHYalCOTBDAehWgxRZiozwtcPKnskN	piece		1
12176	3136	Фарфалле	119.70	№93; 0,5 кг	2016-06-07 08:48:12.629125	2017-02-23 12:27:23.245254	0	\N		piece		1
12939	3277	В ассортименте	685.19	0,9 л	2017-01-29 12:05:05.660037	2017-02-28 18:54:26.580046	0	\N	lidHUjeNKIuxagpzPhBDGfcQJEnFvwYZAyTCMXmLkoqWRVrtbS	piece		1
12938	3277	В ассортименте	532.74	0,5 л	2017-01-29 12:05:05.657446	2017-02-28 18:54:26.581534	0	\N	lidHUjeNKIuxagpzPhBDGfcQJEnFvwYZAyTCMXmLkoqWRVrtbS	piece		1
12937	3277	В ассортименте 	191.59	0,175 л	2017-01-29 12:05:05.65456	2017-02-28 18:54:26.582847	0	\N	xwYjdeERMguSoqOkvGWlUPDmQHaZsFLIJKVzphCtAfTryNXbin	piece		1
12601	3216	Креветка коктейльная	456.25	200/300,1 кг	2016-06-08 10:53:32.754285	2017-01-19 19:54:52.035134	0	\N		\N	\N	1
12599	3216	Креветка «Ботан»	1613.85	1 кг	2016-06-08 10:53:32.649854	2017-01-19 19:54:52.03869	0	\N		\N	\N	1
12651	3240	Абрикосовый	264.75	кули; 0,5 л,1 уп.	2016-09-15 09:41:39.278465	2017-02-23 12:27:22.640875	0	\N		\N	\N	1
12043	3111	Шоколад чёрный	1572.45	72%; таблетки; 2.5 кг; 1 уп.	2016-06-07 08:48:01.878613	2017-02-23 12:27:23.500385	0	\N		\N	\N	1
12656	3246	Чёрная смородина	300.15	кули; 0,5 л,1 уп.	2016-09-15 10:07:47.116779	2017-02-23 12:27:22.623799	0	\N		\N	\N	1
12620	3219	Осьминог «премиум»	331.05	61+; 0,820 г,1 уп.	2016-06-08 10:53:34.487881	2017-02-23 12:27:22.69851	0	\N		\N	\N	1
12614	3218	Мясо краба 2-я фаланга	3239.47	Мурманск; 1*5	2016-06-08 10:53:33.425332	2017-02-23 12:27:22.707988	0	\N		\N	\N	1
12630	3222	Корюшка	325.08		2016-06-08 10:53:36.834563	2017-06-06 12:17:24.264028	0	\N		kilo		1
12595	3213	Каракатица "Baby Cuttlefish"	760	очищенная, 61+	2016-06-08 10:53:30.593261	2017-07-17 11:37:16.178677	0	foto-217.jpg		kilo	00000000554	1
13192	3355	Сыр безлактозный «Джугас»	805.77	40%, 1/4,5, круг	2017-02-11 11:36:41.891349	2017-06-07 09:08:48.892354	0	\N	IUqFsSecPYMhRbnVdijpuyaTAlrkGBKEWofCzJXtvDHxmLZwOQ	kilo		1
13186	3354	Тофу	166.49	твердый, тетрапак, Япония, 297 г	2017-02-11 11:34:17.669168	2017-06-07 09:10:19.591918	0	\N	PvEKamRwYSdxZDHolWctNsGUIOfjVyQTnLMAkFqrgiCpBheJub	piece		1
12655	3245	Тропические фрукты	405.25	кули; 0,5 л,1 уп.	2016-09-15 10:06:06.219783	2017-02-23 12:27:22.627213	0	\N		\N	\N	1
12645	3224	12			2016-08-15 11:20:55.343143	2016-08-15 11:20:55.343143	0	\N	\N	\N	\N	1
12654	3243	Тропические фрукты		кули; 0,5 л,1 уп.	2016-09-15 10:02:30.793519	2017-02-23 12:27:22.630486	0	\N	\N	\N	\N	1
12653	3242			кули; 0,5 л,1 уп.	2016-09-15 09:59:49.843447	2017-02-23 12:27:22.634263	0	\N	\N	\N	\N	1
12652	3241			кули; 0,5 л,1 уп.	2016-09-15 09:54:51.862252	2017-02-23 12:27:22.637489	0	\N	\N	\N	\N	1
12650	3237	Тропические фрукты	405.25 	кули; 0,5 л,1 уп.	2016-09-15 09:24:14.188844	2017-02-23 12:27:22.644046	0	\N	\N	\N	\N	1
12646	3228	Клубничный	388.60	0,5 л,1 уп.	2016-09-12 19:09:51.736398	2016-09-12 19:12:59.048542	0	\N	\N	\N	\N	1
12647	3231	Клубничный	388.60	0,5 л,1 уп.	2016-09-12 19:24:26.284576	2016-09-12 19:24:58.773207	0	\N	\N	\N	\N	1
12648	3235	Тропические фрукты			2016-09-15 09:03:19.838404	2016-09-15 09:03:19.838404	0	\N	\N	\N	\N	1
12649	3236	Тропические фрукты			2016-09-15 09:22:33.754904	2016-09-15 09:22:33.754904	0	\N	\N	\N	\N	1
12613	3218	Мидии в голубых раковинах (аналог черноморских)	321.49	Новая Зеландия; 1*10	2016-06-08 10:53:33.398678	2017-02-23 12:27:22.711439	0	\N		\N	\N	1
12291	3158	винный натуральный	130.35	6% кра,сный; "Конди" 1 л*12 шт,1 шт	2016-06-07 08:48:17.28994	2017-02-23 12:27:23.093647	0	\N		\N	\N	1
12042	3111	Шоколад чёрный	1655.20	55% таблетки 2,5 кг; 1 уп.	2016-06-07 08:48:01.867083	2017-02-23 12:27:23.504393	0	\N		\N	\N	1
12290	3158	винный натуральный	119.30	6% белый "Конди" 1 л,1 шт	2016-06-07 08:48:17.278961	2016-12-28 10:16:46.651285	0	\N		\N	\N	1
12657	3247	test			2016-12-28 14:22:33.60392	2016-12-28 14:22:33.60392	0	13298139_614374652059244_1853346754_n.jpg	eSYCsbgiNzmBPwfqJULtxruRWdHvaITVhXyAcFDMZjpOnQlGKk	\N	\N	1
12658	3247				2016-12-28 14:22:33.84907	2016-12-28 14:22:33.84907	0	\N	bArghkXyVRtjPUWGonlzmFJCKDHfvTINQupeqsidBcLaEMSYwO	\N	\N	1
12954	3470	Ванильное	460.01	3,5 л	2017-01-29 12:05:05.695306	2017-03-01 18:33:39.323641	0	\N	lidHUjeNKIuxagpzPhBDGfcQJEnFvwYZAyTCMXmLkoqWRVrtbS	piece		1
12953	3470	Банановый	1126.95	5 л	2017-01-29 12:05:05.692986	2017-03-01 18:33:39.326206	0	\N	lidHUjeNKIuxagpzPhBDGfcQJEnFvwYZAyTCMXmLkoqWRVrtbS	piece		1
12946	3473	Малиново-клубничный сорбет	3185.12	4,8 л; 2,398 кг	2017-01-29 12:05:05.677129	2017-03-01 18:36:00.918077	0	\N	lidHUjeNKIuxagpzPhBDGfcQJEnFvwYZAyTCMXmLkoqWRVrtbS	piece		1
12947	3472	Мятно-Шоколадное	2645.06	4,8 л; 2,610 кг	2017-01-29 12:05:05.679473	2017-03-01 18:36:20.737256	0	\N	lidHUjeNKIuxagpzPhBDGfcQJEnFvwYZAyTCMXmLkoqWRVrtbS	piece		1
12617	3218	Мясо камчатского краба 1-я фаланга в хитине	4403.31	Мурманск	2016-06-08 10:53:33.49788	2017-01-19 19:47:00.629757	0	\N		\N	\N	1
12616	3218	Мясо камчатского краба «Роза»	3074.32		2016-06-08 10:53:33.474902	2017-01-19 19:47:00.631958	0	\N		\N	\N	1
12615	3218	Мясо камчатского краба 1-я фаланга, ЭКСТРА	3841.88	Мурманск	2016-06-08 10:53:33.445868	2017-01-19 19:47:00.634355	0	\N		\N	\N	1
12612	3218	Мидии КИВИ в зеленых раковинах, половинки	681.30	1 кг	2016-06-08 10:53:33.376083	2017-01-19 19:47:00.640463	0	\N		\N	\N	1
12611	3218	Мидии без раковины	229.70	200-300г,1*10	2016-06-08 10:53:33.356252	2017-01-19 19:47:00.642389	0	\N		\N	\N	1
12619	3219	Осьминог	401.40	41/60 (Вьетнам),1 кг	2016-06-08 10:53:34.459263	2017-01-19 19:54:40.424031	0	\N		\N	\N	1
11929	3065	«Прошутто крудо»	414.93	Окорок сыровяленый; в нарезке; 250 г	2016-06-07 08:47:27.716321	2017-03-03 09:06:44.700217	0	Прошутто_2.jpg		piece		1
12667	3253	Голень	167.82	пакет 2,5 кг; «ЕС АГРО»  1*15 кг	2017-01-13 08:10:22.861522	2017-02-23 12:27:22.598862	0	\N	OvUIApmrzlQjyYBCLoacDEhWKxMtZwXfuPiSqekNTFRsnVHdgb	\N	\N	1
12663	3249	лордло.	234е5н4н3	11лдо; 22лоп; 11.	2017-01-09 11:33:14.53157	2017-02-23 12:27:22.609996	0	foto-492.jpg	YpMWweZKbXRDvPJQfrhqycUoagiCzNtFxdlAVOsGHkImELjTSB	\N	\N	1
12041	3111	Шоколад молочный	1655.20	35% таблетки 2,5 кг; 1 уп.	2016-06-07 08:48:01.855196	2017-02-23 12:27:23.508547	0	\N		\N	\N	1
12040	3111	Шоколад белый	1681.10	30% таблетки 2,5 кг; 1 уп.	2016-06-07 08:48:01.841746	2017-02-23 12:27:23.51274	0	\N		\N	\N	1
12039	3111	Разрыхлитель теста	444.45	915 г; 1 уп.	2016-06-07 08:48:01.830555	2017-02-23 12:27:23.51697	0	\N		\N	\N	1
12038	3111	Миндальная мука	1370.75	1 кг; 1 уп.	2016-06-07 08:48:01.815289	2017-02-23 12:27:23.521333	0	\N		\N	\N	1
12036	3111	Кукурузный крахмал	1009.50	"Mondamin" 2,5 кг; 1 уп.	2016-06-07 08:48:01.791312	2017-02-23 12:27:23.527767	0	\N		\N	\N	1
12035	3111	Какао-порошок	654.35	тёмн/красн 1 кг Франция; 1 уп.	2016-06-07 08:48:01.770506	2017-02-23 12:27:23.532254	0	\N		\N	\N	1
12034	3111	Желатин	1745.75	листовой; 1 кг; 1 уп.	2016-06-07 08:48:01.757362	2017-02-23 12:27:23.536797	0	\N		\N	\N	1
12033	3111	Дрожжи сухие	39.75	Pakmaya 100 г; 1 уп.	2016-06-07 08:48:01.747521	2017-02-23 12:27:23.541082	0	\N		\N	\N	1
12032	3111	Джем порционный	99.85	в ассортименте 20 г * 20 шт; 1 уп.	2016-06-07 08:48:01.734743	2017-02-23 12:27:23.545412	0	\N		\N	\N	1
11957	3079	Филе грудки без кожи	220.50	лоток; «Аркадиа»,1 кг	2016-06-07 08:47:32.46836	2017-02-23 12:27:23.716799	0	\N		\N	\N	1
12610	3217	Креветка тигровая, 16/20	700	«Frozen Shrimp», Без головы, с/м	2016-06-08 10:53:33.114437	2017-07-17 13:10:39.426847	0	foto-203.jpg		kilo	00000001673	1
12623	3221	Икра красная (горбуша)	575	 «Макаров», ж/б, 140 г	2016-06-08 10:53:35.776048	2017-07-17 11:20:33.13231	0	foto-158.jpg		piece	00000000855	1
12642	3223	Филе тилапии	584.38	б/к, 0% глазировки, Izumidai	2016-06-08 10:53:37.380414	2017-06-26 12:41:42.688073	0	foto-289.jpg		kilo		1
12633	3222	Дорадо	730.58	потрошёный, очищенный; 300-400 г	2016-06-08 10:53:36.903218	2017-06-26 11:13:53.379007	0	foto-5.jpg		kilo		1
12622	3220	Осьминог крупный	1000	Китай, 3-5 кг	2016-06-08 10:53:35.444258	2017-07-18 13:11:46.545086	0	foto-228.jpg		kilo	00000001848	1
13191	3355	Сыр безлактозный «Чеддер Красный»	746.66		2017-02-11 11:36:41.88725	2017-06-07 09:08:48.895165	0	\N	wKTGZBkxpjciJVoaDNXdUfFgPRAvHeyCrbLYhmOSutMQzEWnqs	kilo		1
12621	3220	Осьминог крупный	2200	Марокко, 7% глазировки, 2-3 кг	2016-06-08 10:53:35.431273	2017-07-18 13:11:46.546801	0	foto-226.jpg		kilo	00000001037	1
13187	3354	«Фетакса»	205.11	400 г	2017-02-11 11:34:17.671733	2017-06-07 09:10:19.589799	0	\N	PvEKamRwYSdxZDHolWctNsGUIOfjVyQTnLMAkFqrgiCpBheJub	piece		1
12627	3222	Окунь морской	341.06	с/м, б/г, 200-300 г	2016-06-08 10:53:36.764065	2017-06-26 11:10:54.896277	0	foto-250.jpg		kilo		1
11956	3079	Филе бедра без кожи	231.85	лоток; «Аркадиа»,1 кг	2016-06-07 08:47:32.457576	2017-02-23 12:27:23.720698	0	\N		\N	\N	1
11955	3079	Бедро с кожей без кости	217.05	лоток или пакет; «Аркадиа»,1 кг	2016-06-07 08:47:32.445607	2017-02-23 12:27:23.724394	0	\N		\N	\N	1
11953	3079	Голень	142.90	лоток; «Аркадиа»,1 кг	2016-06-07 08:47:32.421931	2017-02-23 12:27:23.729832	0	\N		\N	\N	1
11952	3079	Бедро с кожей на кости	145.30	лоток; «Аркадиа»,1 кг	2016-06-07 08:47:32.408209	2017-02-23 12:27:23.734213	0	Bedro.jpg		\N	\N	1
12668	3254	Бедро с кожей на кости	153.67	цыплята, лоток	2017-01-13 08:13:22.530731	2017-03-03 08:43:32.525208	0	\N	WZRXOwbfplNihuLykrKAxdoHYecSjMJtznGvQDBsgFamUEITqV	kilo		1
12672	3254	Крылышки, 3 фаланги	152.45	цыплята, лоток	2017-01-13 08:24:30.306059	2017-03-03 08:45:48.450883	0	\N	DjfGOiQHePFxLgBrvztAnyIJbhoupSYRlVqkNWKcECdZUwmMsX	kilo		1
12659	3248	1	11	11	2017-01-09 11:11:09.331164	2017-01-09 11:11:09.331164	0	IMG_7196.jpg	dvZmpFUcYiOjSBheuAzCGQNkogJrKXflEysawxqtPRnDMHTbLI	\N	\N	1
11954	3079	Окорочка	119.50	1 кг	2016-06-07 08:47:32.432847	2017-01-12 13:26:34.156463	0	\N		\N	\N	1
12662	3249	Ведро	9060960	112	2017-01-09 11:26:25.590938	2017-01-09 15:02:32.291454	0	foto-451.jpg	QCSMIWNvPjyAEdHYuKXLfUJimDwntklabpzxogTGcRVFZqrBOs	\N	\N	1
12661	3249	Соус			2017-01-09 11:26:25.287168	2017-01-09 15:02:32.293884	0	foto-346.jpg	jUxGoTbQOvhtBckWiyKDqRHgZYnVEPfMXrJewuSdNlCzaAspmL	\N	\N	1
12664	3250				2017-01-12 09:09:27.497167	2017-01-12 09:09:27.497167	0	\N	rcEpUXGkOCRANDKmuQnLFsbHSygTitxYowWahjJdfIlMBVzPZv	\N	\N	1
12665	3251				2017-01-12 10:12:46.323693	2017-01-12 10:12:46.323693	0	15871639_1304252656280187_8977622654139991355_n.jpg	fmPYevOpBWArZwouxNFTDkMdKIbHgQiGhLtqczRlXyjsnEVJCa	\N	\N	1
12666	3252				2017-01-13 08:08:29.999887	2017-01-13 08:08:29.999887	0	\N	tDXdSkExVMPwQjvAyahfJZGusizLerNcFnRYpmOoUgTlICHbqK	\N	\N	1
12152	3130	«Olitalia»	204.90	0,25 л	2016-06-07 08:48:11.534539	2017-02-22 15:12:16.1211	0	\N		piece		1
12675	3253	Окорочок 	114.34	монолит; «ЕС АГРО»; ТУ 1*15 кг	2017-01-13 08:29:32.85056	2017-02-23 12:27:22.583643	0	\N	ArBZhSuKGjTsPneEzMvyoVfRxdCkUipWXmOlgFtDHwNbQYLJaq	\N	\N	1
12151	3130	«Olea Moresca»	269.96	0,5 л; 1*12	2016-06-07 08:48:11.521516	2017-02-23 12:27:23.29108	0	\N		piece		1
12683	3081	Цыпленок-корнишон кукурузного откорма (жёлтый)	156.24	400-450 г; 1*16; 1 шт	2017-01-13 08:40:36.304754	2017-02-23 12:27:22.574795	0	\N	NIYkmhsOwzSMiCxETfevHXujtLJlWrRnqbyDpZBgQFAKcGVdPU	\N	\N	1
13199	3359	Горох колотый	46.41	450 г; 1*8	2017-02-11 12:57:44.143157	2017-02-23 12:27:21.394615	0	\N	FoebVcGTlMzWyAkaESRBZnhNtsvriPdCmqLDjgwxXUKIJOHQfp	piece		1
13195	3357	Масло фундука (лесной орех)	1074.50	1 л; ж/б; 1*6	2017-02-11 12:39:47.555706	2017-02-23 12:27:21.397895	0	\N	ndHsCPhfLZDKRIVzWlSbxvXOeFEoBQNaMpAriYJGctqwUTgumj	piece		1
13203	3362	Крупа пшеничная Кускус	111.47	450 г 	2017-02-11 13:05:45.089824	2017-02-23 04:12:39.152795	0	\N	caomhEyOzSdACpJxNrtwGUkIPnuTqisLHfveDljVBZgMbKXYWQ	piece		1
13193	3129	Real Tang	570.55	0,5 л; Китай	2017-02-11 12:32:56.552903	2017-02-23 12:27:21.404662	0	\N	yIoWafJGkUjCprYtPwXhLqVegFxHDlBzQZsiRMbndmuANvETKO	piece		1
13204	3358	Тайский рис жасмин	201.32		2017-02-11 13:06:45.795123	2017-02-23 04:14:21.279677	0	\N	yznRlxZDcmXdbTpqSvKQhVCsAfEtLrkUHauGJwIBMgeiPWjFNo	piece		1
13090	3318	«По-венски»	660.71	64% свинины и 20% говядины; 4*50 г	2017-02-08 14:30:36.795956	2017-02-23 12:27:21.65285	0	\N	lbofhPCqQXAZVempRnrMLgyOuYHtkJEcGvKSBiUzNIxWTwDFda	piece		1
13089	3318	Мини-сосиски «По-венски»	660.71	64% свинины и 20% говядины; 10*20 г	2017-02-08 14:30:36.793058	2017-02-23 12:27:21.655954	0	\N	YuaAjbvJfHtgsIEPONMUpdVcTrXZLzSleoGwDBFinWQRxmhyCq	piece		1
13205	3363	Чечевица элитная 	80.02	зеленая; 450 г; 1*8	2017-02-11 13:08:00.138548	2017-02-23 12:27:21.381182	0	\N	EBARCMNtqXWbfnTjhUvlKuwxQaIpJFiGogSLcmDkryHzYVdOZs	piece		1
13202	3361		190.36	смесь; 0,5 кг	2017-02-11 13:04:26.222818	2017-02-23 12:27:21.386641	0	\N	xZtSWJVnwicUkDdFfQEzrRNvpOmGoebglsXTKjuMYhaCLAIByP	piece		1
13200	3360	Гречневая крупа	110.96	900 г; 1*12	2017-02-11 12:59:03.824448	2017-02-23 12:27:21.39095	0	\N	GCkwvUiTfgBhqMAEszWoyIpduRtrHQcLKSZnYaVJPDNFbOxjXe	piece		1
12708	3256				2017-01-14 12:04:12.03338	2017-01-14 12:04:12.03338	0	DeCecco_Spaghetti_1kg.jpg	kSjpwBhTgQFWuUrKLNqcOaZPeGzmyHEdnoIsfYbRJtxXiVlMCA	\N	\N	1
12709	3256				2017-01-14 12:04:12.275463	2017-01-14 12:04:12.750468	0	Jasmine_rice.jpg	mnWjAVEFZfpRTyeIJDGcLboukKSwOhBHXlaPUvsrMdigzqNQCt	\N	\N	1
12710	3256				2017-01-14 12:04:12.75387	2017-01-14 12:04:12.75387	0	\N	mnWjAVEFZfpRTyeIJDGcLboukKSwOhBHXlaPUvsrMdigzqNQCt	\N	\N	1
12711	3256				2017-01-14 12:05:24.135351	2017-01-14 12:05:24.135351	0	Nuggets-2.jpg	yIqvpmFriYolcTCOfgZeUjubhVNxGPBDzaWsQKXAtwRHLMSkdE	\N	\N	1
12712	3257	Кот домашний	500.50	10 кг  	2017-01-14 12:16:30.699908	2017-01-14 12:18:59.262477	0	\N	kasnUgyuDKfcOBlZEhzmJANqxQdHwMtvpSPVbICriFXWRjeTLo	\N	\N	1
12682	3081	Цыпленок-корнишон 	145.48	500+ г; 1 шт	2017-01-13 08:40:36.301602	2017-02-23 12:27:22.579314	0	\N	NIYkmhsOwzSMiCxETfevHXujtLJlWrRnqbyDpZBgQFAKcGVdPU	\N	\N	1
12674	3253	Крыло	147.43	двухфаланговое; пакеты 2,5 кг «ЕС АГРО» 1*15	2017-01-13 08:29:32.846334	2017-02-23 12:27:22.588369	0	Krylia.jpg	ArBZhSuKGjTsPneEzMvyoVfRxdCkUipWXmOlgFtDHwNbQYLJaq	\N	\N	1
13224	3372				2017-02-15 14:06:08.788607	2017-02-17 10:47:47.853359	0	\N	OpUBkqTQcRPXAHNFIhEljvWeLVgdzyxoMZtiGCwufsnmbaDrSK	piece		1
12685	3255	Яйцо куриное 	6.92	С1; 1 шт	2017-01-13 08:54:59.680031	2017-02-23 12:27:22.57114	0	\N	xbfpjVnlNFoUewXtLRGQuDysmzZWcErahJTMCvHAIqdSOKYBgi	\N	\N	1
11937	3068	Бекон	345.45	нарезка; 0,5 кг,1 уп.	2016-06-07 08:47:28.883371	2017-02-23 12:27:23.773493	0	\N		\N	\N	1
11981	3093	Шпинат рубленый 	174.68	порционный	2016-06-07 08:47:45.575737	2017-06-08 08:31:35.377559	0	spinach.png		kilo		1
12701	3086	Чипсы 	518.50	2,5 кг 	2017-01-13 11:39:39.110295	2017-06-08 08:24:30.068146	0	\N	xFiZuyscmKGoRpWYMdzDbrVIUkHaLtEhPBqCgJNjfwTlOeSvAX	piece		1
12704	3094	Морошка	416.26	0,3 кг	2017-01-13 13:40:40.291078	2017-06-08 08:34:41.608181	0	\N	BvgUWpChPJEMwFHOrLoNyRTVmkXzntsibeAYuQxDjZIalGfqcd	piece		1
13009	3286	Диафрагма тонкая 	755.57	наружная	2017-02-01 08:07:29.946263	2017-06-02 10:38:14.904059	0	диафрагма_тонкая_untitled-1769.jpg	vXoSqIWcBTdVFOymegsRxlMjapHLPZtiDwGKEzYubkUCrJAQNn	kilo		1
11991	3094	Клюква дикая	221.09		2016-06-07 08:47:45.826011	2017-06-08 08:34:41.61679	0	foto-494.jpg		kilo		1
11928	3065	Окорок сыровяленый 	2908	кусковой; Granduca 2,5 кг; 1*6	2016-06-07 08:47:27.703943	2017-03-03 09:06:44.703125	0	\N		piece		1
13212	3366	Бальзамный темный	201.42	6%; 500 мл; 1*12 ст/бут; Prezioso	2017-02-11 19:21:37.292092	2017-02-23 12:27:21.373655	0	\N	fhuTtvMGZScpINHDJQRUXEdynCxbraoeLAsjqmiKPzOVgWYklF	piece		1
13215	3367	Дижонская	108.60	370 г	2017-02-11 19:26:30.315245	2017-02-23 04:40:34.547676	0	\N	ltscIkdqGOUJTubxjMZYnyaPzQDweXVgFSKrWNvEBfCAhHiLpR	piece		1
13214	3367	Дижонская	234.62	1 л	2017-02-11 19:26:30.312155	2017-02-23 04:40:34.549349	0	\N	WIfsLogPSpyCDbZkGeJKnwzrQNBiMxuHhFaYRcdEtlXqTjvVUO	piece		1
13207	3365	Хрен	199.78	сливочный; 250 мл	2017-02-11 14:14:56.41973	2017-02-23 12:27:21.376826	0	\N	xNAfUgnQLopDRChOiKEdqwjFMrTkaPbzZIWVXGSmyteJYvuHsl	piece		1
13229	3374	Грудка без кости, без кожи	278	лоток	2017-02-17 12:32:11.785394	2017-03-01 11:50:06.014216	0	\N	PLMbjvTSOdFrXRygiWwaAVcQehGZufnYoDNEItCkmBJzqlspUx	kilo		1
13206	3364	Чернила каракатицы	2353.79	0,5 л	2017-02-11 14:13:56.29506	2017-02-23 04:40:05.651227	0	\N	fkRoqslCULZbuwADmOhrdMGeatIWQBPTcJVnKijvXExHNzSpYy	piece		1
13217	3368		137.08	1 л	2017-02-11 19:29:43.095559	2017-02-23 04:41:05.617568	0	\N	RkKDcdxXVBvhsfWnAapMEyuwFgSUbPOlerGTYIjNHZtoJQqCzi	piece		1
13220	3370	на перепелином яйце	135.14	900 мл; ведро; 1*8	2017-02-11 19:36:06.89025	2017-02-23 12:27:21.353789	0	\N	osNxmTzPhZLJOSqaWetgvyAkXQuVIjMwKBdbcRFpfirEnGYHUC	piece		1
13218	3369	Личи	184.41	в сиропе; 565 г; ж/б	2017-02-11 19:30:48.632716	2017-02-23 12:27:21.360539	0	\N	cglDRBuZiCKdwHmeoLWQOFrJqtvSnGNjMabUXhYfVITExkAsyp	piece		1
13216	3367	Дижонская	108.60	зернистая; 370 г	2017-02-11 19:26:30.318001	2017-02-23 12:27:21.364884	0	\N	ltscIkdqGOUJTubxjMZYnyaPzQDweXVgFSKrWNvEBfCAhHiLpR	piece		1
13213	3366	Бальзамный белый	192.56	5,4%; 500 мл; 1*12	2017-02-11 19:21:37.29396	2017-02-23 12:27:21.370427	0	\N	fhuTtvMGZScpINHDJQRUXEdynCxbraoeLAsjqmiKPzOVgWYklF	piece		1
13230	3375	Окорочок без кости, с кожей 	233.69	цыплята	2017-02-17 12:37:39.488242	2017-03-03 09:23:26.816152	0	\N	qFmLdgtNDezpQXjURYclIZAbOHEnJhfPMVviWoySaurkwBTKxC	kilo		1
13014	3287	Ribeye, 7 рёбер 	2540.76	б/к, зачищенный, Prime 	2017-02-01 10:45:31.329168	2017-06-01 08:06:26.248203	0	Rib-eye-7-ribs-choice.jpg	pwbumWeIEPnUyVAHkDfRSFjsBrxNilaqMYXvZTzOhdogJKctGQ	piece		1
13013	3287	Толстый край 	2346.11	зачищенный, Choice, с/м, б/к, в/у, 5 кг	2017-02-01 10:45:31.325803	2017-06-01 08:06:50.211776	0	\N	YGWXjEUmsLbfKiTDPdeqvyCuOSQgHZwkphcnJtINlAazMVoBFx	piece		1
13083	3314	Бекон говяжий 	1014.26	сырокопчёный в нарезке, замороженный, 1*10	2017-02-08 13:50:06.061234	2017-06-01 08:10:15.147812	0	\N	KrCmxVSIhQFBAokavtHsReqLJznEcpfwUDMWZPTliuGXdyOjYN	kilo		1
12702	3096	Лисички 	148.56	0,3 кг	2017-01-13 11:48:27.98957	2017-06-08 08:28:35.497837	0	\N	CWcEKrsOdhpBRfYejNaDiUQxkAVHyStLXZbIgznGvTJqouPFwM	piece		1
12740	3222	Треска	226.77	потрошёная, без головы; с/м, 1+ кг	2017-01-19 16:19:51.226394	2017-06-26 11:13:53.372951	0	foto-209.jpg	JHRDUayixSmOjKXYPWtZTLEbplqzFuBwgCAGohsrnQcMNIdvek	kilo		1
12705	3094	Облепиха дикая	254.18		2017-01-13 13:40:40.293989	2017-06-08 08:34:41.605483	0	Seabucktorn.png	BvgUWpChPJEMwFHOrLoNyRTVmkXzntsibeAYuQxDjZIalGfqcd	kilo		1
11982	3093	Шпинат листовой	72.33	порционный, 0,4 кг	2016-06-07 08:47:45.587672	2017-06-08 08:31:35.375153	0	\N		piece		1
12794	3262	Гуляш	548.32	"Stew"	2017-01-25 18:07:14.026532	2017-06-02 10:26:00.204017	0	\N	bNKiWXkOuplzEtcdMYIABVgywLDmUsFhQRoJTnCePjfqvGaHSx	kilo		1
12793	3262	Подлопаточный отруб	698.71	Chuck eye roll	2017-01-25 18:07:14.022636	2017-06-02 10:26:00.20573	0	foto-284.jpg	bNKiWXkOuplzEtcdMYIABVgywLDmUsFhQRoJTnCePjfqvGaHSx	kilo		1
12719	3215	Креветка аргентинская целая	675	L2, с/м	2017-01-19 13:32:10.803057	2017-07-17 12:28:08.797374	0	foto-338.jpg	ZTLaqWDiyHojMgKdYtrzlFJGsxIUXVCuhnmEQbevNfSBRwAPpk	kilo	00000002266	1
12607	3217	Креветка тигровая, 16/20	720	Индия, Без головы, с/м	2016-06-08 10:53:33.059587	2017-07-17 13:10:39.428863	0	foto-359.jpg		kilo	00000002610	1
12718	3258	Кальмар с щупальцами	610	40/60	2017-01-19 11:35:49.276301	2017-07-17 11:30:13.622174	0	\N	dCnXxHoAugGriSkYwjDpaIJRmZFyevLEqPKcNltVUTBhWOQbMz	kilo	00000002338	1
12725	3214	Краб «Опилио», конечности 	925	Мурманск, 900+, без глазировки	2017-01-19 13:51:32.083795	2017-07-17 11:56:14.645975	0	foto-442.jpg	SQqDIJANZVxHmzkRhcMoKUFyedgrPpBLiXuwnECvjsaWGTOtYb	kilo	00000002565	1
12605	3217	Креветка тигровая, 13/15	999	Без головы, с/м	2016-06-08 10:53:33.014265	2017-07-18 14:22:17.762054	0	\N		kilo	00000000033	1
12723	3215	Креветка «Ваннамэй», 16/20	268	в панировке, с/м, 0,227 г	2017-01-19 13:32:10.814566	2017-07-17 12:43:40.507338	0	foto-236.jpg	ZTLaqWDiyHojMgKdYtrzlFJGsxIUXVCuhnmEQbevNfSBRwAPpk	kilo	00000000782	1
12596	3214	Краб камчатский, крупная конечность	2376	Мурманск, без глазировки	2016-06-08 10:53:30.82015	2017-07-17 11:56:14.650282	0	foto-444.jpg		kilo	00000001931	1
12743	3222	Угорь 	1185.35	жареный в соусе «Унаги»	2017-01-19 16:19:51.232938	2017-06-26 11:14:17.678636	0	\N	JHRDUayixSmOjKXYPWtZTLEbplqzFuBwgCAGohsrnQcMNIdvek	kilo		1
12724	3215	Креветка коктейльная, 200/300	440	очищенная	2017-01-19 13:32:10.817198	2017-07-17 12:44:40.209344	0	\N	ZTLaqWDiyHojMgKdYtrzlFJGsxIUXVCuhnmEQbevNfSBRwAPpk	kilo	00000000023	1
12732	3220	Осьминог  "Baby Octopus"	550	41/60	2017-01-19 14:35:06.393734	2017-07-18 13:14:06.627408	0	foto-207.jpg	mgyLUGVsSeDZKvIBxkaRlYpTuNAHXnJociPFbhqWdOfQjrtwCM	kilo	00000000450	1
12735	3221	Икра щуки	293	112 г, стекло	2017-01-19 14:47:35.278105	2017-07-17 11:25:36.946919	0	\N	WwSaoxDOHFKfiMQhPZdzjnrleCTIBNAVsGytvupRYkLqmXJbUE	piece	00000000417	1
12734	3221	Икра минтая	59		2017-01-19 14:47:35.274657	2017-07-17 11:25:36.949595	0	\N	WwSaoxDOHFKfiMQhPZdzjnrleCTIBNAVsGytvupRYkLqmXJbUE	piece	00000000947	1
12703	3096	Опята	78.48	0,3 кг	2017-01-13 11:48:27.992905	2017-06-08 08:27:31.675183	0	\N	CWcEKrsOdhpBRfYejNaDiUQxkAVHyStLXZbIgznGvTJqouPFwM	piece		1
12738	3222	Сибас 	749.53	потрошёный, очищенный; 300-400 г	2017-01-19 16:19:51.221791	2017-06-26 11:13:53.377064	0	foto-237.jpg	JHRDUayixSmOjKXYPWtZTLEbplqzFuBwgCAGohsrnQcMNIdvek	kilo		1
13028	3295	Желудки куриные	100.41		2017-02-01 12:57:22.12795	2017-02-17 12:34:48.004653	0	\N	zmpqObZGFWXYjQtuvirnRfHVLISMDKPNoUTlBCceAEgkxsaydh	piece		1
13223	3371	kutdity	233	reer	2017-02-13 12:15:53.119812	2017-02-13 12:15:53.119812	0	\N	kiaoIgyJTXtDeRdAujVCGlHKQzSmMLEqZbnrxcWYOPhsUwNpfF	kilo		1
12759	3218	Филе гребешка с/м М РФ	1975.45	1*12	2017-01-19 19:47:00.644275	2017-01-19 19:47:00.644275	0	\N	YXvQgjCIbzJDlfWHFUmxtKRoEMhuanPGiyqLTZdekcrSwBVsAp	\N	\N	1
12761	3218	Лапки лягушачьи	781.29	5 кг	2017-01-19 19:47:00.649662	2017-01-19 19:47:00.649662	0	\N	YXvQgjCIbzJDlfWHFUmxtKRoEMhuanPGiyqLTZdekcrSwBVsAp	\N	\N	1
12787	3261	Толстый край	2858.36	Ribeye TOP CHOICE; б/к; зачищенный; ~5ru	2017-01-25 17:26:46.549278	2017-02-23 12:27:22.377359	0	\N	sXAnFGRYcdtMTvBybSzlmPhQHjfUwKEqpNrWaJgCexuZIDkoVi	\N	\N	1
12785	3261	Толстый край	2346.11	CHOICE; б/к; зачищенный; с/м; в/у; 5кг	2017-01-25 17:26:46.54481	2017-02-23 12:27:22.383334	0	\N	sXAnFGRYcdtMTvBybSzlmPhQHjfUwKEqpNrWaJgCexuZIDkoVi	\N	\N	1
12783	3261	Лопатка говяжья	445.05	Брянск; н/к; с/м; 1,5кг*4	2017-01-25 17:26:46.540262	2017-02-23 12:27:22.387809	0	\N	sXAnFGRYcdtMTvBybSzlmPhQHjfUwKEqpNrWaJgCexuZIDkoVi	\N	\N	1
12770	3062	Вырезка	822.68		2017-01-25 16:08:49.4542	2017-02-17 16:09:55.757917	0	\N	lOfLJviVEGoAxXyYjHZNPSqctBMWhTdsCubraUzDgmRQFkwIKp	piece		1
11878	3043	Нога без кости	621.88		2016-06-07 08:47:07.72888	2017-02-17 16:11:37.80384	0	\N		piece		1
12782	3261	Кисть говяжья трубчатая	104.61	Брянск; в/у; с/м; 3 кг	2017-01-25 17:26:46.537859	2017-02-23 12:27:22.390921	0	\N	sXAnFGRYcdtMTvBybSzlmPhQHjfUwKEqpNrWaJgCexuZIDkoVi	\N	\N	1
12781	3261	Грудинка говяжья	463.59	Брянск; б/к; охлажденная; 800 г.	2017-01-25 17:26:46.535514	2017-02-23 12:27:22.393616	0	\N	sXAnFGRYcdtMTvBybSzlmPhQHjfUwKEqpNrWaJgCexuZIDkoVi	\N	\N	1
12780	3261	Говядина мраморная, грудинка б/к, охл., зерновой откорм	448.22	Брянск; 1*7	2017-01-25 17:26:46.53314	2017-02-23 12:27:22.39625	0	\N	sXAnFGRYcdtMTvBybSzlmPhQHjfUwKEqpNrWaJgCexuZIDkoVi	\N	\N	1
12779	3261	Говядина мраморная, грудинка на кости, зеровой откорм	???	Брянск; 1*7	2017-01-25 17:26:46.530721	2017-02-23 12:27:22.399024	0	\N	sXAnFGRYcdtMTvBybSzlmPhQHjfUwKEqpNrWaJgCexuZIDkoVi	\N	\N	1
12775	3261	Говядина, зерновой откорм	637.10	Глазной мускул б/к; ~2 кг	2017-01-25 17:26:46.520861	2017-02-23 12:27:22.404399	0	\N	sXAnFGRYcdtMTvBybSzlmPhQHjfUwKEqpNrWaJgCexuZIDkoVi	\N	\N	1
12763	3218	Улитки по Бургундски	375.48	125г; 12шт	2017-01-19 19:47:00.654827	2017-02-23 12:27:22.424814	0	\N	YXvQgjCIbzJDlfWHFUmxtKRoEMhuanPGiyqLTZdekcrSwBVsAp	\N	\N	1
12771	3261	Вырезка говяжья зачищенная	3649.79	Tenderloin CHOICE ~2 кг	2017-01-25 17:26:46.51022	2017-01-25 17:26:46.51022	0	\N	fyIzgKTJaAkoHElWxbsVSNmtuYQPeOrBiLcGFjDMvXnwhZUqdR	\N	\N	1
12772	3261	Вырезка говяжья зачищенная	3302.99	Tenderloin TOP CHOICE ~2 кг	2017-01-25 17:26:46.513133	2017-01-25 17:26:46.513133	0	\N	sXAnFGRYcdtMTvBybSzlmPhQHjfUwKEqpNrWaJgCexuZIDkoVi	\N	\N	1
12773	3261	Вырезка говяжья зачищенная	3302.99	Tenderloin c/м CHOICE ~2 кг	2017-01-25 17:26:46.515798	2017-01-25 17:26:46.515798	0	\N	sXAnFGRYcdtMTvBybSzlmPhQHjfUwKEqpNrWaJgCexuZIDkoVi	\N	\N	1
12774	3261	Говядина, зерновой откорм	1143.35	BLADE	2017-01-25 17:26:46.518369	2017-01-25 17:26:46.518369	0	\N	sXAnFGRYcdtMTvBybSzlmPhQHjfUwKEqpNrWaJgCexuZIDkoVi	\N	\N	1
12776	3261	Говядина, зерновой откорм	520.96	Диафрагма толстая	2017-01-25 17:26:46.523319	2017-01-25 17:26:46.523319	0	\N	sXAnFGRYcdtMTvBybSzlmPhQHjfUwKEqpNrWaJgCexuZIDkoVi	\N	\N	1
12777	3261	Говядина, зерновой откорм	755.57	Диафрагма тонкая (наружная)	2017-01-25 17:26:46.525857	2017-01-25 17:26:46.525857	0	\N	sXAnFGRYcdtMTvBybSzlmPhQHjfUwKEqpNrWaJgCexuZIDkoVi	\N	\N	1
12778	3261	Говядина, зерновой откорм	1293.44	Покромка	2017-01-25 17:26:46.528352	2017-01-25 17:26:46.528352	0	\N	sXAnFGRYcdtMTvBybSzlmPhQHjfUwKEqpNrWaJgCexuZIDkoVi	\N	\N	1
12784	3261	Ростбиф (сирлойн)	755.57		2017-01-25 17:26:46.542549	2017-01-25 17:26:46.542549	0	\N	sXAnFGRYcdtMTvBybSzlmPhQHjfUwKEqpNrWaJgCexuZIDkoVi	\N	\N	1
12762	3218	Улитки в чесночном соусе	374.77	173г; 12 шт	2017-01-19 19:47:00.652275	2017-02-23 12:27:22.428426	0	\N	YXvQgjCIbzJDlfWHFUmxtKRoEMhuanPGiyqLTZdekcrSwBVsAp	\N	\N	1
12760	3218	Филе гребешка отбеленный	1334.01	Китай; 10/20; 1*10	2017-01-19 19:47:00.647061	2017-02-23 12:27:22.433463	0	\N	YXvQgjCIbzJDlfWHFUmxtKRoEMhuanPGiyqLTZdekcrSwBVsAp	\N	\N	1
12758	3259	Мидии без раковины	229.70	200-300г; 1*10	2017-01-19 19:23:00.339088	2017-02-23 12:27:22.438499	0	\N	iUBMHoIpzuKNCYFZJlrsDQPREeqXygVWnbTktahGvwOLcjfASm	\N	\N	1
12757	3259	Мидии КИВИ в зеленых раковинах, половинки	681.30	Новая Зеландия; 1*10	2017-01-19 19:23:00.336129	2017-02-23 12:27:22.442023	0	\N	iUBMHoIpzuKNCYFZJlrsDQPREeqXygVWnbTktahGvwOLcjfASm	\N	\N	1
12756	3259	Мидии в голубых раковинах (аналог черноморских)	321.49	40/60; 1*5	2017-01-19 19:23:00.33273	2017-02-23 12:27:22.445253	0	\N	yqFxrMgWQJiCfGzuIjAXORhvepobcYlasVBPLnEHDkdTNStZwK	\N	\N	1
12406	3164	Сироп «Огурец»	632.20	1 л, Monin	2016-06-07 08:48:19.597692	2017-06-05 11:27:40.577916	0	\N		piece		1
12405	3164	Сироп «Мохито ментол»	660.05	1 л, Monin	2016-06-07 08:48:19.586091	2017-06-05 11:27:40.580606	0	\N		piece		1
12404	3164	Сироп «Миндальный»	462.45	0,7 л, Monin	2016-06-07 08:48:19.575911	2017-06-05 11:27:40.582647	0	\N		piece		1
12399	3164	Сироп «Лесной Орех»	579.35	1 л, Monin	2016-06-07 08:48:19.513591	2017-06-05 11:27:40.584686	0	\N		piece		1
12765	3260	Морской коктейль "Premium" 	550	Кальмар 35%, мидии 25%; моллюски 20%, осьминоги 10%; креветки 10% 	2017-01-24 16:19:27.583442	2017-07-17 11:29:11.042949	0	foto-378.jpg	KnMOxaelDHdkTSoZBGCmQRIYghwuLAvUpJbFsPXErztWqciyNf	kilo	5678954	1
11880	3043	Корейка	1079.83	Дагестан, с/м, 8 ребер, 1*10	2016-06-07 08:47:07.746317	2017-06-01 08:12:28.762444	0	\N		piece		1
12402	3474	Сироп «Мандарин»	632.20	1 л	2016-06-07 08:48:19.551549	2017-06-05 11:08:16.741423	0	\N		piece		1
12764	3258	Трубки кальмара 	399	отбеленные, очищенные, U10	2017-01-19 19:52:25.126908	2017-07-18 13:06:55.849092	0	foto-476.jpg	dSVIOUyEhXpqCJrsPDicTKlfBZgLFtWGxuvYMjNzQeAbaRwomk	kilo	00000000454	1
12768	3223	Филе лакедры желтохвостой 	4573.37	«Hamachi Loin»	2017-01-24 17:38:35.97414	2017-06-06 12:05:18.570449	0	\N	woPWNXOmpuFLQeBiRxShzTjsUatbZMnHdYlkrVIEyfKcqADJCg	kilo		1
13231	3376	Крыло двухфаланговое 	147.43	замороженное, цыплята, «ЕС Агро»; пакет 2,5 кг	2017-02-17 12:42:04.364982	2017-03-03 10:19:06.338756	0	Крылокур_1.jpg	cvNCSOFtIjgLYblPxXuoRETkGQeKZBzHdqAnimDJahrVywfUps	kilo		1
13232	3376	Голень 	167.82	цыплята, «ЕС Агро»; пакет 2,5 кг	2017-02-17 12:44:00.723538	2017-03-03 10:18:52.406717	0	\N	hlRrdTHKkpDtPQcVsxBAGwyMqNiLJUOnYFSWbvEaufmzCIogZj	kilo		1
12769	3062	Оковалок	457.65		2017-01-25 16:08:49.449757	2017-02-17 16:09:55.760328	0	foto-321.jpg	lOfLJviVEGoAxXyYjHZNPSqctBMWhTdsCubraUzDgmRQFkwIKp	piece		1
11927	3065	«Брезаола»	2068.37	Говядина сыровяленая	2016-06-07 08:47:27.69212	2017-03-03 09:06:44.705254	0	\N		piece		1
12805	3262	Стейк «Нью-Йорк»	2581.74		2017-01-25 18:07:14.064742	2017-03-03 14:26:58.414934	0	\N	bNKiWXkOuplzEtcdMYIABVgywLDmUsFhQRoJTnCePjfqvGaHSx	kilo		1
12828	3265	Филе 	849.73	замороженное; лоток; 1*8	2017-01-25 20:01:21.835009	2017-02-23 12:27:22.318012	0	\N	qVFkSTpnObvtfZdQlaPLIYHcUeRCmwMisJgErojyzXAKhGuBWD	piece		1
12822	3264	Ветчина из индейки	151.84	0,4 кг	2017-01-25 19:58:01.245024	2017-02-17 15:40:16.548944	0	Ветчинка__.jpg	tedRDBcYpTsqMxZwunIrOmViWHvkJGaUPbCKhlzLSfFgXQAjNy	piece		1
12826	3265	Печень 	311.97	замороженная; лоток; 1*8	2017-01-25 20:01:21.829924	2017-02-23 12:27:22.325074	0	\N	qVFkSTpnObvtfZdQlaPLIYHcUeRCmwMisJgErojyzXAKhGuBWD	piece		1
12791	3261	Хвост говяжий	368.42	Брянск; с/м; 1кг	2017-01-25 17:37:22.849365	2017-02-23 12:27:22.365307	0	\N	yNWacdXVHeztnBQJgMvhpDlomfqLSiCYAwOjRbPxsUuKZrTIEG	\N	\N	1
12825	3265	Ножки 	698.71	замороженные; лоток; 1*8	2017-01-25 20:01:21.826623	2017-02-23 12:27:22.329603	0	\N	BeyHrOLSkIowumvTNhpKGXJZjARdWlFMgCsfVcntiqDaEzQYxP	piece		1
12790	3261	Тонкий край	1472.72	Striploin CHOICE; б/к; С/М; ~3кг	2017-01-25 17:37:22.846738	2017-02-23 12:27:22.367934	0	\N	yNWacdXVHeztnBQJgMvhpDlomfqLSiCYAwOjRbPxsUuKZrTIEG	\N	\N	1
12789	3261	Тонкий край	2032.61	Striploin PRIME; б/к; зачищенный; ~3кг	2017-01-25 17:37:22.844045	2017-02-23 12:27:22.37069	0	\N	yNWacdXVHeztnBQJgMvhpDlomfqLSiCYAwOjRbPxsUuKZrTIEG	\N	\N	1
12788	3261	Толстый край	2433.19	Ribeye TOP CHOICE; б/к; зачищенный; С/М; ~5кг	2017-01-25 17:37:22.841134	2017-02-23 12:27:22.374262	0	\N	yNWacdXVHeztnBQJgMvhpDlomfqLSiCYAwOjRbPxsUuKZrTIEG	\N	\N	1
12786	3261	Толстый край	2540.76	Ribeye PRIME; б/к; зачищенный; 7 ребер	2017-01-25 17:26:46.547062	2017-02-23 12:27:22.380744	0	\N	sXAnFGRYcdtMTvBybSzlmPhQHjfUwKEqpNrWaJgCexuZIDkoVi	\N	\N	1
12875	3330	Для гамбургеров с кунжутом	11.63	100 мм	2017-01-29 08:32:23.09792	2017-02-15 11:45:06.273976	0	\N	ewZWFvoNLxqIuGhcJRjtiaApMlCKYVQDBXydmSOkfnPgHzUrTs	kilo		1
12839	3269	Крыло цыпленка двух-фаланговое замороженное	147.43	"ЕС АГРО"; пакеты 2,5 кг; 1*15 	2017-01-25 20:20:39.927976	2017-02-23 12:27:22.286149	0	\N	vkpMWcUFTlwEobAYmyLrzVdxQjNBtufOGHSRhZXKIgsiCDqJan	\N	\N	1
12835	3269	Голень куриная замороженная	167.82	"ЕС АГРО"; пакет 2,5 кг; 1*15 кг	2017-01-25 20:20:39.91668	2017-02-23 12:27:22.295451	0	\N	vkpMWcUFTlwEobAYmyLrzVdxQjNBtufOGHSRhZXKIgsiCDqJan	\N	\N	1
12833	3268	Яйцо куриное 	6.92	1С	2017-01-25 20:07:34.196596	2017-01-25 20:07:34.196596	0	\N	PUTwbYgHmEXtaWchFejQRdfCKOvyunNkrZJpDMLsGoVzqxAISB	\N	\N	1
12834	3269	Бедро с кожей на кости (лоток)	153.67		2017-01-25 20:20:39.91343	2017-01-25 20:20:39.91343	0	\N	DeLJMEKCfRmYghoQTqHtSVnAlXdrusvyxFbBkjzwaiZIGWpOUN	\N	\N	1
12392	3164	Сироп «Имбирный пряник»	488.85	0,7 л, Monin	2016-06-07 08:48:19.428646	2017-06-05 11:27:40.594827	0	\N		piece		1
12390	3164	Сироп «Зелёная мята»	465.55	1 л, Monin	2016-06-07 08:48:19.405292	2017-06-05 11:27:40.597167	0	\N		piece		1
12388	3164	Сироп «Амаретто»	569	1 л, Monin	2016-06-07 08:48:19.382394	2017-06-05 11:27:40.598987	0	\N		piece		1
12384	3164	Сироп «Гренадин»	413.80	1 л, Monin	2016-06-07 08:48:19.336703	2017-06-05 11:27:40.60087	0	\N		piece		1
12387	3474	Сироп «Ежевика»	569	1 л	2016-06-07 08:48:19.369605	2017-06-05 11:08:16.750363	0	\N		piece		1
12386	3474	Сироп «Дыня»	645.25	1 л	2016-06-07 08:48:19.359519	2017-06-05 11:08:16.752344	0	\N		piece		1
12383	3474	Сироп «Вишня»	632.10	1 л	2016-06-07 08:48:19.326633	2017-06-05 11:08:16.754051	0	\N		piece		1
12803	3262	Спинной отруб 	2433.19	на кости, "extra choice" 	2017-01-25 18:07:14.058548	2017-06-01 08:05:10.061787	0	\N	bNKiWXkOuplzEtcdMYIABVgywLDmUsFhQRoJTnCePjfqvGaHSx	piece		1
11879	3043	Рёберная часть	403.45	«Лента», Дагестан, с/м, 1*10	2016-06-07 08:47:07.737382	2017-06-01 08:12:28.76526	0	foto-336.jpg		piece		1
11885	3044	Седло на кости	655.58	2-3 кг, 1*18	2016-06-07 08:47:07.896808	2017-06-01 08:13:12.010713	0	foto-324.jpg		piece		1
12815	3263	«Мексиканские»	679.76	85% говядины, 5*60 г	2017-01-25 19:51:29.381553	2017-06-01 08:13:44.225033	0	\N	nGhbucOMfgINpslTBXWUkYdyxAVJHjFQRzZEetLSvPmCwoiDra	piece		1
12813	3263	«Баварская улитка»	686.11	93% свинины, 2*100 г	2017-01-25 19:51:29.376973	2017-06-01 08:13:44.227616	0	\N	nGhbucOMfgINpslTBXWUkYdyxAVJHjFQRzZEetLSvPmCwoiDra	piece		1
12810	3263	«Баварские»	648.00	93% свинины, 10*100 г	2017-01-25 19:51:29.36964	2017-06-01 08:13:44.2296	0	\N	nGhbucOMfgINpslTBXWUkYdyxAVJHjFQRzZEetLSvPmCwoiDra	piece		1
12801	3262	Пашина 	1171.83	"Flank Steak" 	2017-01-25 18:07:14.052045	2017-06-02 10:26:00.199556	0	\N	bNKiWXkOuplzEtcdMYIABVgywLDmUsFhQRoJTnCePjfqvGaHSx	kilo		1
12907	3274	Дыня пюре	659.58	1 кг	2017-01-29 10:50:07.434666	2017-06-02 10:42:01.957224	0	\N	XlPuACBYNLstcwWQOmvgDoTGEIRbnVSkqfeadzZxrUMpiJFKHy	piece		1
12389	3475	Сироп «Жёлтый банан»	556.05	0,7 л	2016-06-07 08:48:19.394252	2017-06-05 11:02:23.038847	0	\N		piece		1
12385	3475	Сироп «Груша»	563.85	0,7 л	2016-06-07 08:48:19.349212	2017-06-05 11:02:23.041135	0	\N		piece		1
12636	3223	Филе горбуши	288.91	7% глазировки, на шкуре	2016-06-08 10:53:37.259553	2017-06-29 14:16:12.210682	0	\N		kilo		1
12379	3474	Сироп «Арбуз»	622.80	1 л	2016-06-07 08:48:19.277397	2017-06-05 11:08:16.755767	0	\N		piece		1
12378	3475	Сироп «Апельсин»	455.20	0,7 л	2016-06-07 08:48:19.26701	2017-06-05 11:57:42.887735	0	\N		piece		1
12836	3269	Филе куриной грудки на кости с кожей	213.41	ГОСТ "Аркадия" (лоток) 1*8	2017-01-25 20:20:39.919698	2017-01-25 20:20:39.919698	0	\N	vkpMWcUFTlwEobAYmyLrzVdxQjNBtufOGHSRhZXKIgsiCDqJan	\N	\N	1
12837	3269	Желудки кур	100.41		2017-01-25 20:20:39.922474	2017-01-25 20:20:39.922474	0	\N	vkpMWcUFTlwEobAYmyLrzVdxQjNBtufOGHSRhZXKIgsiCDqJan	\N	\N	1
12838	3269	Крыло 3 фаланги (лоток)	152.45		2017-01-25 20:20:39.925248	2017-01-25 20:20:39.925248	0	\N	vkpMWcUFTlwEobAYmyLrzVdxQjNBtufOGHSRhZXKIgsiCDqJan	\N	\N	1
12840	3269	Окорочка цыпленка бройлера	114.34	"ЕС АГРО" монолит ТУ 1*15 кг	2017-01-25 20:20:39.930655	2017-01-25 20:20:39.930655	0	\N	vkpMWcUFTlwEobAYmyLrzVdxQjNBtufOGHSRhZXKIgsiCDqJan	\N	\N	1
12841	3269	Окорочок(бедро) без кости с кожей	233.69	"Аркадиа"	2017-01-25 20:20:39.933247	2017-01-25 20:20:39.933247	0	\N	vkpMWcUFTlwEobAYmyLrzVdxQjNBtufOGHSRhZXKIgsiCDqJan	\N	\N	1
12842	3269	Печень кур	146.30		2017-01-25 20:20:39.93596	2017-01-25 20:20:39.93596	0	\N	vkpMWcUFTlwEobAYmyLrzVdxQjNBtufOGHSRhZXKIgsiCDqJan	\N	\N	1
12843	3269	Сердце кур	214.23		2017-01-25 20:20:39.938612	2017-01-25 20:20:39.938612	0	\N	vkpMWcUFTlwEobAYmyLrzVdxQjNBtufOGHSRhZXKIgsiCDqJan	\N	\N	1
12844	3269	Филе бедра без кожи, без кости подложка	259.19	"ЕС АГРО" 1*15	2017-01-25 20:20:39.941255	2017-01-25 20:20:39.941255	0	\N	vkpMWcUFTlwEobAYmyLrzVdxQjNBtufOGHSRhZXKIgsiCDqJan	\N	\N	1
12845	3269	Филе грудки цыпленка без кости, без кожи (лоток)	278.00	"Аркадиа" 1*8	2017-01-25 20:20:39.943946	2017-01-25 20:20:39.943946	0	\N	vkpMWcUFTlwEobAYmyLrzVdxQjNBtufOGHSRhZXKIgsiCDqJan	\N	\N	1
12846	3269	Филе грудной части цыпленка без кости, без кожи, подложка	271.91	"ЕС АГРО" 1*15	2017-01-25 20:20:39.946562	2017-01-25 20:20:39.946562	0	\N	vkpMWcUFTlwEobAYmyLrzVdxQjNBtufOGHSRhZXKIgsiCDqJan	\N	\N	1
12848	3269	Цыпленок корнишон	145.48	500+г	2017-01-25 20:20:39.957549	2017-01-25 20:20:39.957549	0	\N	vkpMWcUFTlwEobAYmyLrzVdxQjNBtufOGHSRhZXKIgsiCDqJan	\N	\N	1
12849	3269	Цыплята	148.76	1 кат.	2017-01-25 20:20:39.959928	2017-01-25 20:20:39.959928	0	\N	vkpMWcUFTlwEobAYmyLrzVdxQjNBtufOGHSRhZXKIgsiCDqJan	\N	\N	1
12832	3267	Утка пекинская	292.19	"Рамаевская" (Халяль); 2.1-2.4 кг; 1*2	2017-01-25 20:06:47.709139	2017-02-23 12:27:22.303007	0	\N	fdckrNAXIepjFCDhHzgOQTmEtLGbRnBqUlaJsYZyKuwxSvMVWo	\N	\N	1
12831	3267	Утиные окорочка подложка	368.42	Ростов-на-Дону; 250-280 г.; 1*4	2017-01-25 20:06:47.705969	2017-02-23 12:27:22.306959	0	\N	fdckrNAXIepjFCDhHzgOQTmEtLGbRnBqUlaJsYZyKuwxSvMVWo	\N	\N	1
12830	3267	Утиная грудка подложка	444.64	Ростов-на-Дону; 250-280 г.; 1*4	2017-01-25 20:06:47.701999	2017-02-23 12:27:22.310727	0	\N	wzdVblDiCxprRAoOgPLuHnvksMcjBGQXYhUISyEfeqJKFmNZat	\N	\N	1
12829	3266	Тушка первой категории	533.56	"Углич" Премиум; 200+г.; 1*10	2017-01-25 20:03:09.338799	2017-02-23 12:27:22.314305	0	\N	byeMLvTAzkZqVHUFnhasjYENxdogBCwGJputXWKfmSOIcRiQlP	\N	\N	1
12865	3329	Классический	38.94	Бельгия; 70 г; 1*64	2017-01-27 17:20:40.16291	2017-02-23 12:27:22.202994	0	Croissant.jpg	uvCRYmQzwcSKgpOGeDntHMNWkyPxFLhUdoarTZbXisqfjABIJE	kilo		1
12847	3269	Цыпленок корнишон кукурузного откорма (жёлтый)	156.24	400-450 г.; 1*16	2017-01-25 20:20:39.949232	2017-02-23 12:27:22.272679	0	\N	vkpMWcUFTlwEobAYmyLrzVdxQjNBtufOGHSRhZXKIgsiCDqJan	\N	\N	1
12864	3270	Вафли Венские "McCain" с карамелизированным сахаром	40.88	90 гр	2017-01-27 17:20:40.159713	2017-01-27 17:20:40.159713	0	\N	CMEtejaTxygrKVmuFnzwoBhlJGIOQsHiYUAvWcZNkdpLSPDRfX	\N	\N	1
12827	3265	Тушка 	443.51		2017-01-25 20:01:21.832456	2017-02-17 15:46:14.363798	0	ЗАйчик-2.jpg	qVFkSTpnObvtfZdQlaPLIYHcUeRCmwMisJgErojyzXAKhGuBWD	piece		1
12870	3270	Улитка с корицей	39.45	Бельгия	2017-01-27 17:20:40.176828	2017-01-27 17:20:40.176828	0	\N	uvCRYmQzwcSKgpOGeDntHMNWkyPxFLhUdoarTZbXisqfjABIJE	\N	\N	1
12871	3271	Багет "Мини" 	24.38	140 гр	2017-01-29 08:32:23.088354	2017-01-29 08:32:23.088354	0	\N	FjheuUYgORfsaocCqXSInMJArixPWQdDpkLTZvGtKyBmlNzEwH	\N	\N	1
12872	3271	Батон "Французский" с чесночной начинкой	82.94	220 гр	2017-01-29 08:32:23.091932	2017-01-29 08:32:23.091932	0	\N	ewZWFvoNLxqIuGhcJRjtiaApMlCKYVQDBXydmSOkfnPgHzUrTs	\N	\N	1
12867	3329	С сыром и ветчиной	51.13	Бельгия; 95 г; 1*48	2017-01-27 17:20:40.168666	2017-02-23 12:27:22.19274	0	Cheese_croissant.jpg	uvCRYmQzwcSKgpOGeDntHMNWkyPxFLhUdoarTZbXisqfjABIJE	kilo		1
12883	3271	Чиабатта "Итальянская" (классическая светлая)	54.02	250 гр; 1*15	2017-01-29 08:32:23.113392	2017-02-23 12:27:22.161672	0	\N	ewZWFvoNLxqIuGhcJRjtiaApMlCKYVQDBXydmSOkfnPgHzUrTs	\N	\N	1
12879	3271	Мексиканские специи Кармашки Тако кукурузные	203.37	2,2 к	2017-01-29 08:32:23.105644	2017-01-29 08:32:23.105644	0	\N	ewZWFvoNLxqIuGhcJRjtiaApMlCKYVQDBXydmSOkfnPgHzUrTs	\N	\N	1
12880	3271	Мексиканские специи Чипсы Начос Тортилья	326.77	500 гр	2017-01-29 08:32:23.107567	2017-01-29 08:32:23.107567	0	\N	ewZWFvoNLxqIuGhcJRjtiaApMlCKYVQDBXydmSOkfnPgHzUrTs	\N	\N	1
12884	3271	Чиабатта классическая	19.57	80 гр	2017-01-29 08:32:23.115283	2017-01-29 08:32:23.115283	0	\N	ewZWFvoNLxqIuGhcJRjtiaApMlCKYVQDBXydmSOkfnPgHzUrTs	\N	\N	1
12866	3329	С миндалём	50.21	Бельгия; 95 г; 1*48	2017-01-27 17:20:40.165697	2017-02-23 12:27:22.19734	0	Almond_Croissant.jpg	uvCRYmQzwcSKgpOGeDntHMNWkyPxFLhUdoarTZbXisqfjABIJE	kilo		1
12877	3330	Для французского хот-дога	14.61		2017-01-29 08:32:23.101871	2017-02-15 11:45:06.270763	0	\N	ewZWFvoNLxqIuGhcJRjtiaApMlCKYVQDBXydmSOkfnPgHzUrTs	kilo		1
12862	3466	Торт «Шоколадный»	973.28	1,1 кг, 10 порций	2017-01-27 16:54:26.369678	2017-06-07 08:06:40.142744	0	Шоколадный.jpg	IZqMglnKeoNYrjpTXEtQxhdUciOuCmGzwHAJVDPksBbLvFRfWa	piece		1
12863	3109	Торт «Трюфель»	1157.69	1,1 кг, 10 порций	2017-01-27 16:54:26.371997	2017-06-07 08:07:54.385819	0	Трюфель1_400x400.jpg	IZqMglnKeoNYrjpTXEtQxhdUciOuCmGzwHAJVDPksBbLvFRfWa	piece		1
12886	3272	«New York с вишней»	1131.98	1,66 кг, 16 порций, 1*4	2017-01-29 10:08:04.627421	2017-06-07 08:10:39.064641	0	\N	MgNBwnHODeLdkEjaSyiCzRxKfFcZoUATVQhPXGvqmsuYWrJplI	piece		1
12885	3272	«New York классический»	963.03	1,66 кг, 16 порций, 1*4	2017-01-29 10:08:04.624258	2017-06-07 08:10:39.066099	0	\N	fOGNbRkEmMQxCcazpynBdSYlZqtIvuerDFLoTUsXVJKgwjhHiP	piece		1
12882	3271	Тортийяс пшеничный 6	140.46	15 см; 330 гр; 12 шт	2017-01-29 08:32:23.111441	2017-02-23 12:27:22.165062	0	\N	ewZWFvoNLxqIuGhcJRjtiaApMlCKYVQDBXydmSOkfnPgHzUrTs	\N	\N	1
12053	3113	Батон «Французский» с чесночной начинкой 	83.75	220 г	2016-06-07 08:48:03.142219	2017-02-15 11:49:20.173951	0	Baguette.jpg		kilo		1
12052	3113	Багет «Мини»	23.30	140 г	2016-06-07 08:48:03.130702	2017-02-15 11:49:20.176332	0	\N		kilo		1
11877	3043	Язык 	482.85	1*13	2016-06-07 08:47:07.721106	2017-02-17 16:11:37.804785	0	\N		piece		1
12881	3271	Тортийяс пшеничный 10	268.76	25 см; 12 шт	2017-01-29 08:32:23.109509	2017-02-23 12:27:22.168269	0	\N	ewZWFvoNLxqIuGhcJRjtiaApMlCKYVQDBXydmSOkfnPgHzUrTs	\N	\N	1
12878	3330	Для хот-дога с кунжутом	11.99	160 мм; 1*60	2017-01-29 08:32:23.10376	2017-02-23 12:27:22.175161	0	Hotdog_buns.jpg	ewZWFvoNLxqIuGhcJRjtiaApMlCKYVQDBXydmSOkfnPgHzUrTs	kilo		1
12049	3112	Пекан кленовый	45.10	Бельгия; 1 шт.	2016-06-07 08:48:02.932237	2017-02-23 12:27:23.487907	0	Pecan.jpg		kilo		1
12869	3270	Пекан кленовый	45.19	Дания; 97гр; 48 шт	2017-01-27 17:20:40.174107	2017-02-23 12:27:22.188746	0	\N	uvCRYmQzwcSKgpOGeDntHMNWkyPxFLhUdoarTZbXisqfjABIJE	\N	\N	1
12901	3467	Штрудель яблочный	1006.27	1,1 кг	2017-01-29 10:23:13.382359	2017-02-23 07:03:49.389505	0	\N	kqtWfbszJAPXBpvidghTnrcaFmIexYNDyRKVHEwolLuGCUOZMj	piece		1
12048	3329	С шоколадом и фундуком	50.70	Бельгия; 1 шт	2016-06-07 08:48:02.923436	2017-02-23 12:27:23.492636	0	Choco_croissant.jpg		kilo		1
12044	3112	Вафли «Брюссельские»	313.80	720 г; 1 уп.	2016-06-07 08:48:02.87101	2017-02-23 12:27:23.496735	0	\N		kilo		1
12928	3276	Вафельный рожок "Факел"	1328.78	528 шт	2017-01-29 11:43:21.517361	2017-01-29 11:43:21.517361	0	\N	mEejhnXUKLBvtdJMWDsiNrROTPCfgYHQbkSyZIpqwxuVcalFoG	\N	\N	1
12930	3276	Креманка	2782.14	170 гр	2017-01-29 11:43:21.522989	2017-01-29 11:43:21.522989	0	\N	bUDaVPSuGCXmrBLeRAQcnkOpjwziyNYJtTZlMgIvqoKWhHdFsx	\N	\N	1
12931	3276	Смесь сухая молокосодержащая для мягкого мороженого "Шоколадное"	485.62	1,666 кг	2017-01-29 11:43:21.52568	2017-01-29 11:43:21.52568	0	\N	bUDaVPSuGCXmrBLeRAQcnkOpjwziyNYJtTZlMgIvqoKWhHdFsx	\N	\N	1
12932	3276	Смесь сухая молокосодержащая для десертов охлажденных "Йогуртино"	700.05	1,250 кг	2017-01-29 11:43:21.528313	2017-01-29 11:43:21.528313	0	\N	bUDaVPSuGCXmrBLeRAQcnkOpjwziyNYJtTZlMgIvqoKWhHdFsx	\N	\N	1
12933	3276	Смесь сухая молокосодержащая для мягкого мороженого "Молочная мечта"	485.62	1,666 кг	2017-01-29 11:43:21.530801	2017-01-29 11:43:21.530801	0	\N	bUDaVPSuGCXmrBLeRAQcnkOpjwziyNYJtTZlMgIvqoKWhHdFsx	\N	\N	1
12934	3276	Смесь сухая молокосодержащая для мягкого мороженого "Советское"	633.15	2,170 кг	2017-01-29 11:43:21.533359	2017-01-29 11:43:21.533359	0	\N	bUDaVPSuGCXmrBLeRAQcnkOpjwziyNYJtTZlMgIvqoKWhHdFsx	\N	\N	1
12935	3276	Смесь сухая молокосодержащая для мягкого мороженого "Клубничная"	448.48	2,170 кг	2017-01-29 11:43:21.535825	2017-01-29 11:43:21.535825	0	\N	bUDaVPSuGCXmrBLeRAQcnkOpjwziyNYJtTZlMgIvqoKWhHdFsx	\N	\N	1
12936	3276	Смесь сухая коктейль клубничный	343.01	1,250 кг	2017-01-29 11:43:21.53825	2017-01-29 11:43:21.53825	0	\N	bUDaVPSuGCXmrBLeRAQcnkOpjwziyNYJtTZlMgIvqoKWhHdFsx	\N	\N	1
12927	3275	Соус малиновый (кули)	407.14	"Буарон" 0,5 л	2017-01-29 11:27:49.235422	2017-06-05 11:21:26.478537	0	\N	cVvKYxazLimfkqSQXsPMAUywoJtTRBHIZCnjFuEGgDOhpdebWl	piece		1
12926	3275	Соус клубничный	384.81	"Буарон" 0,5 л	2017-01-29 11:27:49.23268	2017-06-05 11:21:26.482161	0	\N	cVvKYxazLimfkqSQXsPMAUywoJtTRBHIZCnjFuEGgDOhpdebWl	piece		1
12925	3275	Соус из черной смородины	297.21	"Буарон" 0,5 л	2017-01-29 11:27:49.229869	2017-06-05 11:21:26.483832	0	\N	cVvKYxazLimfkqSQXsPMAUywoJtTRBHIZCnjFuEGgDOhpdebWl	piece		1
12924	3275	Соус из тропических фруктов (кули)	401.30	"Буаро" 0,5 л	2017-01-29 11:27:49.227051	2017-06-05 11:21:26.485451	0	\N	cVvKYxazLimfkqSQXsPMAUywoJtTRBHIZCnjFuEGgDOhpdebWl	piece		1
12923	3275	Соус абрикосовый (кули)	262.17	"Буаро"0,5 л	2017-01-29 11:27:49.223858	2017-06-05 11:21:26.486996	0	\N	tqyJsrHMiLjScNxlephbkRmZFwIvTfGnEKOaPVgCoDXdQuAWzY	piece		1
12891	3272	«New York яблочный»	1030.29	1,66 кг, 16 порций, 1*4	2017-01-29 10:08:04.641056	2017-06-07 08:10:39.056628	0	\N	MgNBwnHODeLdkEjaSyiCzRxKfFcZoUATVQhPXGvqmsuYWrJplI	piece		1
12890	3272	«New York шоколадный»	1129.52	1,66 кг, 16 порций, 1*4	2017-01-29 10:08:04.638435	2017-06-07 08:10:39.058109	0	\N	MgNBwnHODeLdkEjaSyiCzRxKfFcZoUATVQhPXGvqmsuYWrJplI	piece		1
12889	3272	«New York черничный»	1199.28	1,66 кг, 16 порций, 1*4	2017-01-29 10:08:04.635783	2017-06-07 08:10:39.060145	0	\N	MgNBwnHODeLdkEjaSyiCzRxKfFcZoUATVQhPXGvqmsuYWrJplI	piece		1
12888	3272	«New York с лесными ягодами»	1110.46	1,66 кг, 16 порций, 1*4	2017-01-29 10:08:04.632997	2017-06-07 08:10:39.06161	0	\N	MgNBwnHODeLdkEjaSyiCzRxKfFcZoUATVQhPXGvqmsuYWrJplI	piece		1
12887	3272	«New York c клубникой»	1058.83	1,66 кг, 16 порций	2017-01-29 10:08:04.630173	2017-06-07 08:10:39.063133	0	\N	MgNBwnHODeLdkEjaSyiCzRxKfFcZoUATVQhPXGvqmsuYWrJplI	piece		1
12950	3473	Черная смородина	2835.92	4,8 л; 2,930 кг	2017-01-29 12:05:05.686476	2017-03-01 18:36:00.914157	0	\N	lidHUjeNKIuxagpzPhBDGfcQJEnFvwYZAyTCMXmLkoqWRVrtbS	piece		1
12952	3472	Шоколад-кокос	2714.42	4,8 л	2017-01-29 12:05:05.690819	2017-03-01 18:36:20.730965	0	\N	lidHUjeNKIuxagpzPhBDGfcQJEnFvwYZAyTCMXmLkoqWRVrtbS	piece		1
12962	3278	Масло виноградных косточек	355.40	"La Tourangelle" 0,5л ж/б 1*12	2017-01-29 14:29:30.463363	2017-01-29 14:29:30.463363	0	\N	YUTkQiKJLqRvcEPjSGZAVwsnhfMzIrNbXgxpDeatWolBHdOuFC	\N	\N	1
12963	3278	Масло для фритюра в канистре	505.29	5л	2017-01-29 14:29:30.4665	2017-01-29 14:29:30.4665	0	\N	eQDmEjJcTbKnlBroPkygxFzSOXiAUdYqvsIRGZauWHwVCMthfp	\N	\N	1
12964	3278	Масло кунжутное Real Tang	570.55	Китай 0,5л	2017-01-29 14:29:30.468927	2017-01-29 14:29:30.468927	0	\N	eQDmEjJcTbKnlBroPkygxFzSOXiAUdYqvsIRGZauWHwVCMthfp	\N	\N	1
12967	3278	Масло оливковое EXTRA VERGIN	2355.33	5л (1-й отжим) Luglio 1*4	2017-01-29 14:29:30.476308	2017-01-29 14:29:30.476308	0	\N	eQDmEjJcTbKnlBroPkygxFzSOXiAUdYqvsIRGZauWHwVCMthfp	\N	\N	1
12972	3278	Масло оливковое EXTRA VERGIN	204.90	"Olitalia" 0,25л	2017-01-30 07:58:55.673365	2017-01-30 07:58:55.673365	0	\N	XIJGDHitBjfoZlRCAePYNgQwzOurSmndVqMaxyULchbsEpFTKk	\N	\N	1
12979	3279	Киноа "Магистраль"	387.78	450гр	2017-01-30 08:20:18.21908	2017-01-30 08:20:18.21908	0	\N	oYKCqHREvrUfTiFdIXgMZLexsnywSlzAbOPuDmVhBJpckaNWGt	\N	\N	1
12980	3279	Киноа смесь "Магистраль"	190.36	450гр	2017-01-30 08:20:18.221588	2017-01-30 08:20:18.221588	0	\N	oYKCqHREvrUfTiFdIXgMZLexsnywSlzAbOPuDmVhBJpckaNWGt	\N	\N	1
12981	3279	Крупа пшеничная Кускус "Магистраль"	111.47	450гр	2017-01-30 08:20:18.224167	2017-01-30 08:20:18.224167	0	\N	oYKCqHREvrUfTiFdIXgMZLexsnywSlzAbOPuDmVhBJpckaNWGt	\N	\N	1
12983	3279	Рис Супер БАСАМАТИ "Агро-Альянс Экстра"	79.86	500гр	2017-01-30 08:20:18.229558	2017-01-30 08:20:18.229558	0	\N	oYKCqHREvrUfTiFdIXgMZLexsnywSlzAbOPuDmVhBJpckaNWGt	\N	\N	1
12985	3279	Рис Арборио "Ризо Галло"	276.11	1кг	2017-01-30 08:20:18.23459	2017-01-30 08:20:18.23459	0	\N	oYKCqHREvrUfTiFdIXgMZLexsnywSlzAbOPuDmVhBJpckaNWGt	\N	\N	1
12986	3279	Рис Арборио Гало	91.19	0,5 кг	2017-01-30 08:20:18.237176	2017-01-30 08:20:18.237176	0	\N	oYKCqHREvrUfTiFdIXgMZLexsnywSlzAbOPuDmVhBJpckaNWGt	\N	\N	1
12988	3279	Тайский рис жасмин	201.32	AROY-D	2017-01-30 08:20:18.241953	2017-01-30 08:20:18.241953	0	\N	oYKCqHREvrUfTiFdIXgMZLexsnywSlzAbOPuDmVhBJpckaNWGt	\N	\N	1
12989	3279	Чечевица Элитная зеленая "Агро-Альянс Экстра"	80.02	450гр; 1*8	2017-01-30 08:20:18.244179	2017-02-23 12:27:21.923478	0	\N	oYKCqHREvrUfTiFdIXgMZLexsnywSlzAbOPuDmVhBJpckaNWGt	\N	\N	1
12987	3279	Рис Венере "Ризо Галло"	449.25	0,5кг; 1*10	2017-01-30 08:20:18.239579	2017-02-23 12:27:21.927566	0	\N	oYKCqHREvrUfTiFdIXgMZLexsnywSlzAbOPuDmVhBJpckaNWGt	\N	\N	1
12984	3279	Рис Элитный пропаренный "Агро-Альянс Экстра"	74.28	900гр; 1*12	2017-01-30 08:20:18.232253	2017-02-23 12:27:21.933641	0	\N	oYKCqHREvrUfTiFdIXgMZLexsnywSlzAbOPuDmVhBJpckaNWGt	\N	\N	1
12982	3279	Рис Кубанский Элитный "Агро-Альянс Экстра"	74.28	900гр; 1*12	2017-01-30 08:20:18.226762	2017-02-23 12:27:21.937872	0	\N	oYKCqHREvrUfTiFdIXgMZLexsnywSlzAbOPuDmVhBJpckaNWGt	\N	\N	1
12273	3153	Соус устричный	354.35	0,907 л,1 шт	2016-06-07 08:48:16.449629	2017-02-15 14:30:51.596173	0	\N		kilo		1
12272	3153	Тонкацу	482.30	1,8 л,1 шт	2016-06-07 08:48:16.437795	2017-02-15 14:30:51.598645	0	\N		kilo		1
12271	3153	Spicy World	1303.50	1 шт	2016-06-07 08:48:16.4219	2017-02-15 14:30:51.600387	0	\N		kilo		1
12978	3279	Гречневая крупа "Агро-Альянс Экстра"	110.96	900гр; 1*12	2017-01-30 08:20:18.216502	2017-02-23 12:27:21.943849	0	\N	oYKCqHREvrUfTiFdIXgMZLexsnywSlzAbOPuDmVhBJpckaNWGt	\N	\N	1
12270	3153	Терияки	460.40	1,8 л,1 шт	2016-06-07 08:48:16.410292	2017-02-15 14:30:51.602033	0	\N		kilo		1
12977	3279	Горох колотый "Агро-Альянс Экстра" 	46.41	450гр; 1*8	2017-01-30 08:20:18.213518	2017-02-23 12:27:21.94697	0	\N	sfVJCiUxMnIPuWDORgbmBoGFaqcrkvLSdEtKQTHXAwepyhzjZN	\N	\N	1
12976	3278	Масло из фундука (лесной орех)	1074.50	"La Tourangelle" 1л; ж/б 1*6	2017-01-30 07:58:55.685305	2017-02-23 12:27:21.95065	0	\N	XIJGDHitBjfoZlRCAePYNgQwzOurSmndVqMaxyULchbsEpFTKk	\N	\N	1
12975	3278	Масло подсолнечное 	95.18	"Печагин" 1л; 1*15	2017-01-30 07:58:55.682412	2017-02-23 12:27:21.953607	0	\N	XIJGDHitBjfoZlRCAePYNgQwzOurSmndVqMaxyULchbsEpFTKk	\N	\N	1
12974	3278	Масло подсолнечное	436.44	"Печагин" 5л; 1*2	2017-01-30 07:58:55.679565	2017-02-23 12:27:21.956539	0	\N	XIJGDHitBjfoZlRCAePYNgQwzOurSmndVqMaxyULchbsEpFTKk	\N	\N	1
12973	3278	Масло оливковое EXTRA VERGIN	269.96	"Olea Moresca" 0,5л; 1*12	2017-01-30 07:58:55.676504	2017-02-23 12:27:21.95952	0	\N	XIJGDHitBjfoZlRCAePYNgQwzOurSmndVqMaxyULchbsEpFTKk	\N	\N	1
12971	3278	Масло оливковое с ароматом Трюфеля	352.95	0,25л; ст/б Lulglio 1*12	2017-01-30 07:58:55.670385	2017-02-23 12:27:21.963313	0	\N	XIJGDHitBjfoZlRCAePYNgQwzOurSmndVqMaxyULchbsEpFTKk	\N	\N	1
12970	3278	Масло оливковое рафинированное Pomace 	255.00	"CINQUINA" пластиковая бутылка; 1л; 1*12	2017-01-30 07:58:55.667333	2017-02-23 12:27:21.966329	0	\N	XIJGDHitBjfoZlRCAePYNgQwzOurSmndVqMaxyULchbsEpFTKk	\N	\N	1
12969	3278	Масло оливковое EXTRA VERGIN c ароматами	332.97	"CINQUINA" (Розмарин; перец; орегано) 0,25л; 1*12	2017-01-30 07:58:55.661251	2017-02-23 12:27:21.969154	0	\N	XIJGDHitBjfoZlRCAePYNgQwzOurSmndVqMaxyULchbsEpFTKk	\N	\N	1
12966	3278	Масло оливковое первого холодного прессования EXTRA VERGIN	537.87	"Де Чекко" 1л; ст/б	2017-01-29 14:29:30.474063	2017-02-23 12:27:21.972994	0	\N	eQDmEjJcTbKnlBroPkygxFzSOXiAUdYqvsIRGZauWHwVCMthfp	\N	\N	1
12965	3278	Масло кунжутное Real Tang	1417.81	1,8л; пл/бут*6	2017-01-29 14:29:30.471837	2017-02-23 12:27:21.975898	0	\N	eQDmEjJcTbKnlBroPkygxFzSOXiAUdYqvsIRGZauWHwVCMthfp	\N	\N	1
12958	3470	Страчателла	1126.95	5 л	2017-01-29 12:05:05.703739	2017-03-01 18:33:39.315692	0	\N	lidHUjeNKIuxagpzPhBDGfcQJEnFvwYZAyTCMXmLkoqWRVrtbS	piece		1
12957	3470	Лесной орех	1229.40	5 л	2017-01-29 12:05:05.701656	2017-03-01 18:33:39.317906	0	\N	lidHUjeNKIuxagpzPhBDGfcQJEnFvwYZAyTCMXmLkoqWRVrtbS	piece		1
12956	3470	Клубничное	504.16	3,5 л	2017-01-29 12:05:05.699626	2017-03-01 18:33:39.319834	0	\N	lidHUjeNKIuxagpzPhBDGfcQJEnFvwYZAyTCMXmLkoqWRVrtbS	piece		1
12961	3471	Экстрэм рожок клубника	50.21	120 мл	2017-01-29 12:05:05.709959	2017-02-28 18:52:51.222784	0	\N	lidHUjeNKIuxagpzPhBDGfcQJEnFvwYZAyTCMXmLkoqWRVrtbS	piece		1
12960	3471	Пломбир "ГЮВ" Шоколад	430.29	3 кг	2017-01-29 12:05:05.707915	2017-02-28 18:52:51.224898	0	\N	lidHUjeNKIuxagpzPhBDGfcQJEnFvwYZAyTCMXmLkoqWRVrtbS	piece		1
12955	3470	Вишневый щербет	464.54	3,5 л	2017-01-29 12:05:05.697442	2017-03-01 18:33:39.321724	0	\N	lidHUjeNKIuxagpzPhBDGfcQJEnFvwYZAyTCMXmLkoqWRVrtbS	piece		1
12767	3223	Дорадо, филе 	883.12	с/м, 100-200 г	2017-01-24 17:38:35.971642	2017-06-29 14:15:37.901324	0	\N	woPWNXOmpuFLQeBiRxShzTjsUatbZMnHdYlkrVIEyfKcqADJCg	kilo		1
13021	3291	Кость говяжья 	104.61	трубчатая; с/м; вакуум	2017-02-01 11:10:59.220137	2017-02-23 12:27:21.841177	0	\N	jtBKroxXUsRghaVukeNSOJTqdbLZEfpAQYvDHiywIPzlGMcWmF	\N	\N	1
12267	3153	Соус песто	156.15	De Cecco 200 г,1 шт	2016-06-07 08:48:16.376739	2017-02-15 14:30:51.607103	0	\N		kilo		1
12266	3153	Соус острый "Красный дьявол"	1257.55	3,8 л США,1 шт	2016-06-07 08:48:16.365022	2017-02-15 14:30:51.608752	0	\N		kilo		1
13004	3285	Tenderloin Top Choice	3302.99	~2кг	2017-02-01 08:03:22.10515	2017-02-17 16:01:05.176335	0	foto-280.jpg	TxPOMKenBorJASyXfpsdEWgzINQcklaZRvLtmhjqUCuDiVbwHY	piece		1
12994	3281	«Снежный краб»	177.76	0,5 кг; 1*12	2017-02-01 07:18:52.023962	2017-02-23 12:27:21.914931	0	\N	acHbIWZwKpUARkyTYdlGtmjXJrniCzPhgeFDLSoxVBMsvfEquO	\N	\N	1
12265	3153	Соус Мицукан	1624.20	20 л,1 шт	2016-06-07 08:48:16.351474	2017-02-15 14:30:51.610234	0	\N		kilo		1
12264	3153	Соус Мицукан	171.75	0,5 л,1 шт	2016-06-07 08:48:16.339279	2017-02-15 14:30:51.611772	0	\N		kilo		1
12263	3153	Соус кунжутный	475.80	1 л,1 шт	2016-06-07 08:48:16.324423	2017-02-15 14:30:51.61325	0	\N		kilo		1
12262	3153	Соус КимЧи	123.85	 0,3 л,1 шт	2016-06-07 08:48:16.308779	2017-02-15 14:30:51.614692	0	\N		kilo		1
12261	3153	Соус для фастфуда	5.65 р/шт	распак 25 г/72 шт,1 шт	2016-06-07 08:48:16.289237	2017-02-15 14:30:51.616115	0	\N		kilo		1
12260	3153	"Унаги Терияки"	430.15	Соус для угря 1,8 л,1 шт	2016-06-07 08:48:16.278028	2017-02-15 14:30:51.617585	0	\N		kilo		1
12259	3153	Наршараб	203.40	Соус гранатовый,1 шт	2016-06-07 08:48:16.266535	2017-02-15 14:30:51.619054	0	\N		kilo		1
12258	3153	Соус "Чили сладкий" д/курицы	226.35	AROY-D 850 мл ст/б,1 шт	2016-06-07 08:48:16.254392	2017-02-15 14:30:51.620579	0	\N		kilo		1
12257	3153	Соус "Ворчестер"	228.45	140 г,1 шт	2016-06-07 08:48:16.243273	2017-02-15 14:30:51.622	0	\N		kilo		1
12256	3153	Паста Том Ям	340.05	454 г * 24 шт,1 шт	2016-06-07 08:48:16.226294	2017-02-15 14:30:51.623486	0	\N		kilo		1
12255	3153	Паста Тобан Джан	891.65	1 кг острая,1 шт	2016-06-07 08:48:16.214784	2017-02-15 14:30:51.624955	0	\N		kilo		1
12254	3153	Мерим соус для суши	2769.15	 19 л,1 шт	2016-06-07 08:48:16.202332	2017-02-15 14:30:51.62637	0	\N		kilo		1
13054	3304	Фисташки зелёные	5406.80	очищенные 1*10; 1 кг	2017-02-05 18:59:12.66988	2017-02-23 12:27:21.742826	0	\N	tndOyGqIHEpPYDBZJCavbAgTKFxUwjLosieRhkfuVWmcMSlrNz	piece		1
13053	3304	Грецкий орех	1016.21	очищенный 1*10; 1 кг	2017-02-05 18:59:12.665794	2017-02-23 12:27:21.747433	0	\N	fdQtHCXGuyvWZTLkNxicUmIBqDzYPFlwjpMJshOVSKbgReEnAr	piece		1
13042	3126	«Экстра»	19.51	1 кг; 1*20	2017-02-05 11:59:42.330271	2017-02-23 12:27:21.780379	0	\N	HJkmqyPhnvKYesIbLxOdSgwMztUVoTpuAcRGDNjECZXfrBFiWa	piece		1
13052	3303	Мёд цветочный	231.23	натуральный; 1 кг; пл/б; 1*6	2017-02-05 14:40:17.586388	2017-02-23 12:27:21.751266	0	\N	bCnYdjBDaJQXUSzVIfiteWAEoPwMNquhFmKvxykHgpOrcLRlGZ	piece		1
13039	3298	Утка пекинская	292.19	халяль; 2,1-2,4 кг; 1*12	2017-02-02 08:25:49.313549	2017-02-23 12:27:21.790588	0	\N	EAlUibodfuzBFPCHIjgXGrDLanQNOswJcmykZTveRMSpVKtqYW	kilo		1
13018	3288	Striploin 	1472.72	зачищенный, Choice, с/м, б/к, ~3 кг	2017-02-01 10:52:50.374784	2017-06-01 08:07:17.941246	0	1-rib-top-choice-striploin.jpg	HXUYNzAoQePlatdhVsGifFCkDmgMcwqIEOZxLSBbvWyrnTjpuR	piece		1
13005	3285	Tenderloin Choice	3302.99	очищенная, с/м, ~2 кг	2017-02-01 08:03:22.108071	2017-06-01 08:09:29.94535	0	\N	TxPOMKenBorJASyXfpsdEWgzINQcklaZRvLtmhjqUCuDiVbwHY	piece		1
13003	3285	Tenderloin Choice	3649.79	очищенная, ~2 кг	2017-02-01 08:03:22.101938	2017-06-01 08:09:29.948391	0	\N	BZLNYOyEnlsmXzrutUHQaThPFvAqjxciJoKdSMgRkpDeWbIwGf	piece		1
13020	3290	Грудинка говяжья 	463.59	охлажденная, без кости, 800 г	2017-02-01 11:09:52.365207	2017-06-01 08:09:42.261525	0	\N	KJUVYBEkDoPpzrAhIZeHtnmuivLNaTbfMGQXcWRyxdqgwCsOSl	piece		1
13019	3289	Хвост говяжий 	368.42	с/м, 1 кг	2017-02-01 10:54:11.373338	2017-06-01 08:09:50.461792	0	\N	gnmpDuEVhiwSlOxUfFjGYBZqTAPkQNWyctdzMrebHvRsoXLIKC	piece		1
13010	3286	Покромка	1293.44		2017-02-01 08:07:29.948772	2017-06-02 10:38:14.902059	0	Покромка.jpg	vXoSqIWcBTdVFOymegsRxlMjapHLPZtiDwGKEzYubkUCrJAQNn	kilo		1
13036	3296	Картофель фри с кожурой	353.97	11 мм, 2.5 кг	2017-02-01 13:55:23.525873	2017-06-08 08:22:03.703775	0	foto-407.jpg	chfWQlqxNitMnCDomBsObSPaGETwgVZYUXRkrKdJuzepjLIvAH	piece		1
13002	3284	Лягушачьи лапки	818	5 кг	2017-02-01 07:35:55.448727	2017-07-18 13:54:41.184184	0	\N	uYpJdheBwVCAZKlPyXnfTmjbgtvaqoHUcOxirGDQzLWIkFMSEN	kilo	00000000527	1
12997	3282	Мидии без раковины	238	200/300	2017-02-01 07:24:10.508695	2017-07-18 13:44:27.896961	0	foto-479.jpg	VyXDlFrhBncxHUsoTNjmdtJpCbaegkWuGLRfAZviEqPYIMwOSK	kilo	00000000787	1
12993	3280	Улитки «по-бургундски»	390	125 г	2017-01-30 16:03:23.406207	2017-07-18 13:05:53.597089	0	\N	aDWQNtqhvnYHFkEJriwBPTcyVlxKIzpGjRZAMCUfdSLeXosmbu	piece	00000001956	1
12715	3258	Кальмар очищенный	100.0	дальневосточный, «Командорский»	2017-01-19 11:28:54.131066	2017-07-17 11:51:08.685274	0	\N	DNsjrHBkedXtQYLimTpvCVZUFuIqyxKobOhGfRlMJAPzWwnaSg	kilo	00000001220	1
12604	3217	Креветка тигровая, 16/20	1040	 Бангладеш, без головы, с/м	2016-06-08 10:53:32.996586	2017-07-18 14:22:17.778675	0	foto-360.jpg		kilo	00000002457	1
12626	3222	Палтус синекорый	716.44	потрошёный, без головы; с/м, 2-3 кг	2016-06-08 10:53:36.734516	2017-06-26 11:13:53.382952	0	foto-224.jpg		kilo		1
12714	3258	Кальмар неочищенный	200.0	дальневосточный, «Командорский»	2017-01-19 11:28:54.122296	2017-07-17 11:51:08.688548	0	\N	AINTHClYukQKVcFzhjqrEdsgZwxtBLnOpfUXaSDGimPbMyRoWe	kilo	00000000207	1
12995	3282	Мидии в голубых раковинах	310	40/60	2017-02-01 07:24:10.503805	2017-07-18 13:44:27.899336	0	foto-214.jpg	cgHoYTrFtnBIJPGqMsdKfAyZLVijbNQkeUlmxRvphuzCDXWaSw	kilo	00000002254	1
13001	3283	Мясо краба, 2-я фаланга	3300	Мурманск	2017-02-01 07:34:40.756283	2017-07-18 13:35:11.746822	0	\N	jJzbuYvUKhiMcyWLAdPFGVnRtDHEsNxeOfkmrgaBTqpSwQIXCZ	kilo	00000001961	1
12998	3283	Мясо краба «Роза»	2640		2017-02-01 07:34:40.746356	2017-07-18 13:35:11.761471	0	foto-439.jpg	ZtpYQFLkuvhTmEioIUVbPrqcyCfXARzsHnJSeOlWxdDjKBMGwN	kilo	00000000850	1
12253	3153	Соус сырный	140.40	18 * 25 г,1 шт	2016-06-07 08:48:16.187927	2017-02-15 14:30:51.62781	0	\N		kilo		1
13051	3302	Пенне Ригате №73	74.59	500 г; 1 уп.	2017-02-05 14:38:09.60476	2017-02-23 12:27:21.755425	0	\N	lvwboUqePNImdukLBVSWsDRTiahxjrOFtyJcfYnQMzGgKEpZAX	\N	\N	1
13049	3300	Чёрный	338.50	пакетированный; 100*2 г	2017-02-05 13:25:57.47708	2017-02-23 12:27:21.76051	0	\N	dpvzAKJNRxVBcPtrqTimfUQbWOYjHMuElXweLygoCShnsGFZkI	piece		1
13040	3254	Грудка на кости с кожей	213.41	цыплята, лоток	2017-02-02 13:50:45.779306	2017-03-03 08:43:32.519433	0	\N	EduBYWmTspHlkQLnjUIbvtyZONaCJMzoGKrRDFAhxwqefSXgiP	kilo		1
13043	3125	«Эль Сальвадор» 	667.26	250 г	2017-02-05 12:17:13.068438	2017-02-22 07:14:59.715494	0	\N	PByZgArutFWJlYOsVRIhiKcdkvmSbxDqEjTNaGQMnfUHXozewp	piece		1
12140	3125	«Колумбия Меделлин Супремо» 	595.75	250 г	2016-06-07 08:48:10.76957	2017-02-22 07:14:59.721814	0	\N		piece		1
13048	3300	Зелёный	338.50	пакетированный; 100*2 г	2017-02-05 13:25:57.473349	2017-02-23 12:27:21.768792	0	\N	bPEZscAeqgITkJrMWhdaYBXzHvRQyiwCltmOSNDxnujKLVUGof	piece		1
13041	3123	Пудра	199.07	нетающая; 600 г; 4 шт	2017-02-05 11:55:08.31342	2017-02-23 12:27:21.783898	0	\N	vckBdYGzFqQMofEjSCtUIRZHxawypKNeibhDTlmVrunJWOgXsA	piece		1
13029	3295	Печень куриная	146.3		2017-02-01 12:57:22.131229	2017-02-17 12:34:53.948754	0	Chicken-liver.jpg	dhTLsFcAymtSBixYWrpCVzXgfaGMNoRbPnKeuDkQwZIqJHOUEl	piece		1
12145	3126	Морская крупная	72.33	К.И.С. 1 кг; 12 шт 0012	2016-06-07 08:48:10.949563	2017-02-23 12:27:23.311249	0	\N		piece		1
13026	3294	Грудка без кости, без кожи	271.91	Куриное филе, «ЕС АГРО», подложка	2017-02-01 12:52:19.302573	2017-03-03 09:23:48.625254	0	\N	mLEftWJnwpqehAdGNUHxTvsFrVoDSIcyXOaRlBjCbKgZMPQzkY	kilo		1
13023	3293	Сирлойн	755.57		2017-02-01 11:13:02.692368	2017-02-17 14:36:28.890387	0	Roastbeef.jpg	wLavVDZOfQUnMkRdYrIbmFAsGcqzNpXxeToiSPKjCyWglEuhtJ	piece		1
13030	3295	Сердце куриное	214.23		2017-02-01 12:57:22.133947	2017-02-17 12:34:15.943665	0	\N	dhTLsFcAymtSBixYWrpCVzXgfaGMNoRbPnKeuDkQwZIqJHOUEl	piece		1
12133	3123	Тростниковый коричневый	222.45	кусковой; 0,5 кг	2016-06-07 08:48:10.458147	2017-02-23 12:27:23.335209	0	\N		piece		1
12134	3123	Тростниковый белый 	184.31	кусковой; 0,5 кг	2016-06-07 08:48:10.468263	2017-02-23 12:27:23.330857	0	\N		piece		1
13046	3124	«Коста Рика»	1255.02	500 г	2017-02-05 13:15:36.594089	2017-02-22 07:16:12.284867	0	\N	CrNWpVYtMlTaePcqkgDzwSiUdBvLFHXGIsEjOymARZubJhKfQx	piece		1
13045	3124	«Бразил Сантос»	1142.32	500 г	2017-02-05 13:15:36.591034	2017-02-22 07:16:12.286727	0	\N	CrNWpVYtMlTaePcqkgDzwSiUdBvLFHXGIsEjOymARZubJhKfQx	piece		1
12135	3123	Ванильный	169.25	1 кг	2016-06-07 08:48:10.47985	2017-02-22 07:08:28.348487	0	\N		piece		1
12131	3123	Песок	54.87	фасовка по 5 или 10 кг	2016-06-07 08:48:10.435455	2017-02-22 07:08:28.355615	0	\N		piece		1
12146	3126	Морская мелкая	72.33	 К.И.С. 1 кг,*12 шт 0012	2016-06-07 08:48:10.960892	2017-02-22 07:10:22.470191	0	\N		piece		1
12144	3126	Крупная	17.11	1 кг*20 шт	2016-06-07 08:48:10.937174	2017-02-22 07:10:22.47484	0	\N		piece		1
13050	3301	Чайный напиток каркадэ «Суданская роза»	70.59	70 г	2017-02-05 13:27:45.485986	2017-02-22 07:12:54.009469	0	\N	CTmRLBnbuYfQWEiwqPcFdOHlzXgAGNSsJhrMjIKvxeZVkoUpyD	piece		1
13047	3299	«Колумбия Супремо»	335.53	500 г	2017-02-05 13:20:49.95316	2017-02-22 07:13:15.040767	0	\N	JieNSMokOXfHgburycUasdQIZjtnmVFAzpEDWvCLYqPxThKlGB	piece		1
13044	3124	«Кения»	1142.32	500 г	2017-02-05 13:15:36.58725	2017-02-22 07:16:12.288303	0	\N	CrNWpVYtMlTaePcqkgDzwSiUdBvLFHXGIsEjOymARZubJhKfQx	piece		1
13071	3307	Мякоть помидора	224.17		2017-02-06 10:46:41.357881	2017-02-23 04:30:13.744329	0	\N	LOzJBMFkIUiRvrDtpQECKXscmhVHdxjTaGlgZYNSwAWnqoPufe	piece		1
13056	3304	Миндальная мука 	1310.85	белая (мелкий помол); 1 кг; 1*10	2017-02-05 19:07:27.921574	2017-02-23 12:27:21.737876	0	\N	FCTamqEGprHfXSLYAUPIuJzVtxjhidDKZgOocByWRnsewQvNbk	piece		1
13088	3317	«Люкс»	603.44	85% куриного мяса; подкопчёные; 10*20 г	2017-02-08 14:30:34.456673	2017-02-23 12:27:21.675391	0	\N	lbofhPCqQXAZVempRnrMLgyOuYHtkJEcGvKSBiUzNIxWTwDFda	\N	\N	1
13087	3317	«По-венски»	660.71	64% свинины и 20% говядины; 4*50 г	2017-02-08 14:30:34.453755	2017-02-23 12:27:21.678858	0	\N	lbofhPCqQXAZVempRnrMLgyOuYHtkJEcGvKSBiUzNIxWTwDFda	\N	\N	1
13086	3317	Мини-сосиски «По-венски»	660.71	64% свинины и 20% говядины; 10*20 г	2017-02-08 14:30:34.449763	2017-02-23 12:27:21.681912	0	\N	YuaAjbvJfHtgsIEPONMUpdVcTrXZLzSleoGwDBFinWQRxmhyCq	\N	\N	1
13085	3316	Бекон	292.19	сырокопчёный, нарезка, «Эко», 500 г, 1*30	2017-02-08 14:05:51.819592	2017-06-01 08:09:05.897529	0	\N	NTrvuJkHoKmznRgwfslDAVqGibxjaPYtSZUXECMpLcWIQyOehB	piece		1
13093	3319	«Европейские»	737.13	85% говядины, 10*20 г	2017-02-08 14:36:38.083278	2017-06-01 08:13:22.644027	0	\N	sXjLvqDYpUMPgJNHmrVOwkxZldGtzuIeoicnyCKfRTbBaSQWhA	piece		1
13092	3319	«Баварские» 	698.71	93% свинины, 10*20 г	2017-02-08 14:36:38.079697	2017-06-01 08:13:22.646896	0	\N	qzDkPhsnJECUjHaQRdTSyFBcgeXZtWIulAbrKfYGVvpwLomNxi	piece		1
13094	3320	«Мюнхенские»	573.32	 80 г, 1*5	2017-02-08 14:40:16.943279	2017-06-01 08:13:31.250438	0	\N	IdsWMlPGqJbjiHAgNOzXxZnpcmvfCLkDtVReQKohSBEwyUaTFr	piece		1
13060	3305	Томатная паста «Пая»	102.44	800 г	2017-02-05 19:38:10.617752	2017-06-09 12:06:12.126135	0	\N	HRUJtxCEnbemOakoMIGTlgpFiYrqVBLvQsyzdSWjAPDhNcZuKX	piece		1
13035	3296	Картофель фри "French fries"	256.03	9 мм, премиум, 2.5 кг	2017-02-01 13:55:23.522686	2017-06-08 08:22:03.706731	0	foto-408.jpg	chfWQlqxNitMnCDomBsObSPaGETwgVZYUXRkrKdJuzepjLIvAH	piece		1
13034	3296	Картофель фри «Экспресс»	287.99	Быстрого приготовления; 9 мм, 2,5 кг	2017-02-01 13:55:23.520251	2017-06-08 08:22:23.794715	0	foto-389.jpg	chfWQlqxNitMnCDomBsObSPaGETwgVZYUXRkrKdJuzepjLIvAH	piece		1
13038	3297	Картофельные дольки со специями 	328.97	2,5 кг	2017-02-01 14:04:18.306404	2017-06-08 08:25:23.852359	0	foto-397.jpg	YDwIVasMEJguvmlyhdrCAojQWqUFpBRzGxiOceHkPLftnbXZKN	piece		1
13037	3297	Картофельные дольки "Spicy XL" 	443.2	2,5 кг	2017-02-01 14:04:18.303068	2017-06-08 08:25:23.853909	0	foto-401.jpg	eudQKJUqiVvbWCfonRDPXlgrBypYzsFOhAIxHcNkamELwSMtjT	piece		1
13081	3313	Сосиски «Фермерские»	182.88		2017-02-08 13:35:16.184241	2017-02-17 15:52:27.496923	0	\N	oZORBaUKCGkYmSsITMFhcwqAXPefiEjWLtlJHzdxNVurbyDnpg	piece		1
13225	3373	меня			2017-02-15 14:40:12.593419	2017-02-15 14:43:46.02848	0	tumblr_inline_nq8u3gTEVK1qau50i_540.jpg	mkIbyYXdcsGJLRrupAgWTqDtejONvwoaKBlFQxHUPfSVEChnZz	kilo		1
13082	3065	«Пепперони» 	626.08	колбаса в нарезке; 1 кг/уп.	2017-02-08 13:36:46.734335	2017-03-03 09:06:44.693819	0	\N	ZplkMCWaHvuEFcQKqGYPXjONRtJdATBSbLeDifwsrgUmVxhyon	piece		1
13063	3308	Початки кукурузы 	107.58	«Делькофф» 370мл; ст/б*12 шт; 1 уп.	2017-02-05 19:43:29.303893	2017-02-23 12:27:21.725448	0	\N	ZkQrLVTdDxRXgbsohSAFqtieHzuMwmElOINanpcKfvYBjUGPWC	\N	\N	1
13074	3311	Маргарин	48.77	250 г	2017-02-06 11:16:43.095745	2017-02-21 16:54:42.353751	0	\N	YaMyixQTlKLRuOStXIsoCgUbZpkzVAJrNWdDcqnwFBmvEjefhP	piece		1
13091	3318	«Люкс»	603.44	85% куриного мяса; подкопчёные; 10*20 г	2017-02-08 14:30:36.798633	2017-02-21 19:14:00.092763	0	\N	lbofhPCqQXAZVempRnrMLgyOuYHtkJEcGvKSBiUzNIxWTwDFda	piece		1
13055	3304	Лепестки миндаля	1449.67	1 кг	2017-02-05 19:04:53.447811	2017-02-22 07:11:48.9239	0	\N	lPRofuXiUWTnmGvDEqLOktKVNxseFQAbzjgMcJYhSdyIHCarZp	piece		1
13102	3323	Рёбра 	406.53	Short Ribs	2017-02-08 16:28:21.681333	2017-02-08 16:28:21.681333	0	\N	SFRrDpgavOmInJKYjluANxdycTHXzZMfsLhCeBbVWiQwoEtGqU	\N	\N	1
13104	3323	Котлетное мясо	319.65		2017-02-08 16:28:21.687095	2017-02-08 16:28:21.687095	0	\N	asBeWnLMEwSTkvXqQyUcjCzpuRGhDrilxAYZOPHfbdKVmFJNtg	\N	\N	1
13105	3323	Лопатка 	627	Under blade	2017-02-08 16:28:21.689803	2017-02-08 16:28:21.689803	0	\N	asBeWnLMEwSTkvXqQyUcjCzpuRGhDrilxAYZOPHfbdKVmFJNtg	\N	\N	1
13106	3323	Щёки 	473.84	с/м; ~1 кг; 1*30	2017-02-08 16:28:21.692552	2017-02-23 12:27:21.611029	0	\N	asBeWnLMEwSTkvXqQyUcjCzpuRGhDrilxAYZOPHfbdKVmFJNtg	\N	\N	1
13228	3373	Растащило			2017-02-15 14:43:46.030686	2017-02-15 14:43:46.030686	0	tumblr_n268h0yUtP1sib0ygo1_500.jpg	ASNPYkqyuCKLOVdFxXbTWZJrfBcmRQeUMsnotwzGEiHpvhaIgj	kilo		1
13080	3331	«Итальянская»	54.02	250 г 	2017-02-08 09:52:01.078914	2017-02-09 15:31:53.291413	0	\N	mNitScGhMfoKsADOLgHJRQCUyjTZIeBzExqkwpYFbPXundaWvV	\N	\N	1
13109	3326	«Снежный краб»	177.76	0,5 кг; 1*12	2017-02-09 09:07:38.416266	2017-02-23 12:27:21.599076	0	Crab_hor.jpg	ubmhXZIGByfLzdjPsglHEVcYxUWAOewaNkKvTSpqniDFJMCrRQ	\N	\N	1
13108	3325	«Снежный краб»	177.76	0,5 кг; 1*12	2017-02-09 08:57:09.224874	2017-02-23 12:27:21.602523	0	\N	OyWezbgnJLHhaRxMjGDSduIoACUZYPsmfVkqwBXtQpKTFiEcrl	\N	\N	1
13239	3378	Том Ям	217.71	400 г*24 шт	2017-02-17 18:33:07.888174	2017-02-23 04:42:12.364378	0	\N	mKrPjYHhdtwCgpNZRcbLITlnuXAOSiJvqzyfsVeGDUaQMEoFWx	piece		1
13227	3373				2017-02-15 14:40:12.87573	2017-02-15 14:40:40.180918	0	tumblr_mave2k74h01qza249o1_500.jpg	NekFGrILgKYAEmCljczZuwaUBWvSspiDHPhnJtxdoqMQXVTyRf	kilo		1
13226	3373	не по-детски			2017-02-15 14:40:12.735203	2017-02-15 14:43:46.025589	0	tumblr_kzof0rkkBj1qzaeyyo1_400.jpg	NekFGrILgKYAEmCljczZuwaUBWvSspiDHPhnJtxdoqMQXVTyRf	kilo		1
13107	3324	Стейк из тонкой диафрагмы	1098.27	б/к, замороженный, «Мачете»	2017-02-08 16:35:48.06259	2017-06-01 08:08:21.252388	0	\N	IamCNRvDlkpqZQxthKfsGboMJjBdXOirwPnyFuUVcHgYzALSTW	piece		1
13084	3315	Бекон 	278.57	"Alfoldi", нарезка 500 г, замороженный, 1*24	2017-02-08 14:02:41.130673	2017-06-01 08:11:27.784747	0	\N	iKRELDqwgXzOVlImBkeGJsFSbQTjoacHpyxhnWtudCfAvYUrMN	kilo		1
13117	3332	Черника	321.49		2017-02-10 12:27:20.393512	2017-06-08 08:33:39.570387	0	\N	bNfMgAuHoLitakjGTDdzOlsFERIqPWZmUVBhwvYCcXxJKnyerS	kilo		1
13119	3334	Кукуруза в початках	139.95	Венгрия, 1/12 кг	2017-02-10 12:35:32.213241	2017-06-01 10:57:30.061735	0	\N	ZbwxoLOvMGSKRIQAzCJFuHBjlXcyUmYertknpTDPVgsNqhaEdW	piece		1
13057	3187	Тунец в масле	101.99	кусочками, 185 г	2017-02-05 19:18:31.737948	2017-07-17 11:20:53.604042	0	\N	mduPebjcHnCUGKFiykoZgpMqwTNszItAXlBvDWfYEJxRSahLrQ	piece	00000000	1
13124	3335	Нерка дикая экспортная	634.07	потрошёная, без головы, 1-2 кг, с/м	2017-02-10 12:40:28.776719	2017-06-29 14:12:16.698611	0	\N	EiplqduWTKRcgIwGDhSzNtnsFajkOXMrLYvoVUHmBbAefQZPCy	kilo		1
13079	3312	Творог рассыпчатый 	288.61	0,2%, 900 г	2017-02-06 11:27:02.884692	2017-06-07 09:17:22.571814	0	\N	wsTvcDemhnjKpPgJIzFLqyMbQGxfZlWrdNtVSRUXYAEHiaCoBO	piece		1
12514	3187	Тунец в собственном соку	101.99	кусочками, 185 г, с ключом, Тайланд	2016-06-07 08:48:24.094879	2017-07-17 11:20:53.60792	0	\N		piece	00000000	1
13118	3333	Картофель молодой без кожуры	422.46	2,5 кг	2017-02-10 12:31:59.931812	2017-06-08 08:14:08.860092	0	foto-371.jpg	hClOxFUBfyGAXRiEunJtkTZzHKsvNPVjedmoDQYpbwSrIaLcgM	piece		1
13116	3332	Чёрная смородина	141.49		2017-02-10 12:22:33.084473	2017-06-08 08:33:39.572674	0	\N	eJHdqmxRCpnZIOfNSVwFtsgKGUzWcQYXMakhbulrDLyoEBivAP	kilo		1
13064	3183	Оливки «супергигант»	295.68	Греция, б/к, 850 мл	2017-02-05 19:51:34.047487	2017-06-09 11:53:08.323967	0	\N	kFNdHiMyUWncGgusDvRXxeSBrVtJqCYQLAzIOhaEfwpTbZKPjl	piece		1
13070	3182	Маслины с косточкой	78.17	314 мл, ж/б	2017-02-06 10:45:39.693199	2017-06-09 11:53:57.592378	0	\N	OYnRruvwCfJqPbiUlcWpANMXyVLdaHtZIDogxSsGEjKkBTQemz	piece		1
13068	3180	Кукуруза сладкая	51.46	425 мл, ж/б	2017-02-06 10:37:00.673278	2017-06-09 11:54:54.363941	0	\N	CUHAgbuStTcZYpolrWnRJjDIOXwaMhGimEfFQzksNLKePqBdVv	piece		1
13115	3332	Малина	439.52		2017-02-10 12:22:33.081539	2017-06-08 08:33:39.574609	0	foto-500.jpg	eJHdqmxRCpnZIOfNSVwFtsgKGUzWcQYXMakhbulrDLyoEBivAP	kilo		1
12517	3189	Перец халапенью	624.85	кольца, 3 кг, ж/б	2016-06-07 08:48:24.385822	2017-06-09 12:06:02.529691	0	\N		piece		1
13061	3306	Тамаринд	223.34	без косточек, 454 г	2017-02-05 19:39:34.249319	2017-06-09 12:06:25.807669	0	\N	rxIfhHXlCjkNvGZKPMWsQJFmVdiYBtUeRpbzSLwyAcoTEDqguO	piece		1
13069	3310	Манго в сиропе	122.94	дольки, 425 г, ж/б	2017-02-06 10:38:48.658688	2017-06-09 12:06:50.272336	0	\N	WfhmwgRInTkODeUoZPJMqXcxQEvrtGSsVlFdjyHziYANKLubpB	piece		1
13065	3309	Абрикос в сиропе	80.84	половинки, ж/б, 425 мл	2017-02-06 09:54:05.320516	2017-06-09 12:07:19.157194	0	\N	OJIwGorWxtdBZbKcmMinFljRTuLqHvVhkDpSgafENzyPQAUseC	piece		1
13111	3339	«Валуа»	7.89	45 г; 120 шт	2017-02-09 15:23:02.025742	2017-07-05 15:04:39.949923	0	foto-457.jpg		kilo		1
13096	3321	«Пряные с сыром»	724.12	полукопчёные; 58% свинины; 18% говядины; 14% сыра; 4*50 г	2017-02-08 14:45:20.814737	2017-02-23 12:27:21.635664	0	\N	ENeKnijLDrbqOzCFslkIZvAVtPYawMxTmRpuhogXGfQJdycUHB	\N	\N	1
13240	3379	«Синг Сонг»	243.73	1 л; 1*6	2017-02-17 18:49:17.857085	2017-02-23 12:27:21.307171	0	\N	PkTUQHlApYbBONEWgfwdxrhMyqvIzeDtZnCujGJKVcFmoSLais	piece		1
13095	3321	«Франконские»	698.82	 копчёные 93% свинины; 4*50 г	2017-02-08 14:45:20.812001	2017-02-23 12:27:21.639319	0	\N	rUiuavEyqPCLhxdSIocJDFWslVQemXwGnzMYgftKAbjRkZHpTO	\N	\N	1
13103	3323	Толстый край 	999.92	Ribeye; с/м; 1*16	2017-02-08 16:28:21.684304	2017-02-23 12:27:21.617672	0	\N	asBeWnLMEwSTkvXqQyUcjCzpuRGhDrilxAYZOPHfbdKVmFJNtg	\N	\N	1
13237	3378	Тобан Джан	522.50	острая; 1 кг	2017-02-17 18:33:07.88239	2017-02-23 12:27:21.313895	0	\N	mKrPjYHhdtwCgpNZRcbLITlnuXAOSiJvqzyfsVeGDUaQMEoFWx	piece		1
13233	3377	Мерим	2008.13	 соус для суши; 19 л	2017-02-17 18:26:43.401348	2017-02-23 12:27:21.327266	0	\N	IDUHYKgWpxLPhtilAvCBoTFnRdMNJyzjkfbXaemsZrOuGqwcES	piece		1
13219	3370		128.78	800 мл; (дой-пак)	2017-02-11 19:36:06.886935	2017-02-23 12:27:21.35745	0	\N	kMcyGVvPgphWrlUzfHmITRwuxaQYneLJqdXtAbiDFNCsEZKSoO	piece		1
13261	3383		136.26	0.3 л.	2017-02-17 19:21:17.47492	2017-02-17 19:21:17.47492	0	\N	zvpmfDSGLXMyVPAuNJZkjRWdiltqeCbonKBTahOQcrFHsYEUIg	piece		1
13262	3383		571.68	1,8 л.	2017-02-17 19:21:17.47855	2017-02-17 19:21:17.47855	0	\N	rsebdulOaiSvghZncPTyGmHXxVFKwMNWJRtqDYQUzpLCAoBjIk	piece		1
13263	3383	Овощной	294.55	пл/б.; 1 л.; 1*20	2017-02-17 19:21:17.481663	2017-02-23 12:27:21.231327	0	\N	rsebdulOaiSvghZncPTyGmHXxVFKwMNWJRtqDYQUzpLCAoBjIk	piece		1
13259	3382		550.67	 1,8 л.; 1*6	2017-02-17 19:18:41.584668	2017-02-23 12:27:21.239686	0	\N	sgxQwpjXeNKynCkbRJqMLFlPhSfrudUHWEVZmTOGaBAcoYvtIi	piece		1
13284	3390	Base	691.64	1,8 л; пл/б	2017-02-17 20:02:15.744096	2017-02-23 12:27:21.17917	0	\N	NxTQdtcXCoBPSgAfuULRWbpDZhYIsrMvOGaKqyFmzjHwVeiEkJ	piece		1
13280	3389	Гикори	819.71	с копчёным вкусом; 2,360 кг.	2017-02-17 19:58:28.100815	2017-02-23 12:27:21.185775	0	\N	xIBhiujJnmtTMeRcFqZNGwPCHdKgbfayDkSpVUEoYOslAWQzLr	piece		1
13279	3389		839.63	американский; 2,360 кг.; 1*3	2017-02-17 19:58:28.097235	2017-02-23 12:27:21.189585	0	\N	GBtpokciKElWZwbCUqDSVdzPeJYuxhIOLTvraHQNsgXFRnmjAf	piece		1
13278	3367	Рум-сервис	3328.61	33 мл; ст/б; 1*80	2017-02-17 19:52:59.556979	2017-02-23 12:27:21.194075	0	\N	MrHAyaqKTNcpuzsolktLbRmgjBWJSwDfFeXYCVPIUZxiQnOdhG	piece		1
13277	3368	Рум-сервис	3328.61	33 мл; ст/б; 1*80	2017-02-17 19:51:50.427159	2017-02-23 12:27:21.19913	0	\N	VYQfJjHlIavWypXbLnhgSxFieMsqCmBzTGrAZEouRdODKktwPU	piece		1
13276	3384	для курицы	222.22	сладкий; 920 мл; ст/б	2017-02-17 19:48:22.558505	2017-02-23 12:27:21.203372	0	\N	DVdveCItAsrigqmNbRUhnoxGBFwyQWLacHuJPXYEOMZklTjSzK	piece		1
13258	3381	Хабанеро	553.54	испепеляюще-острый; 150 мл	2017-02-17 19:16:07.547612	2017-02-23 12:27:21.243907	0	\N	wQlBgcOKPLXrnjYpAFbsGhMdEIeCUfzokvxHSqiWTNytamDVRu	piece		1
13257	3381		243.73	с красным перцем; 60 мл	2017-02-17 19:16:07.545034	2017-02-23 12:27:21.248915	0	\N	wQlBgcOKPLXrnjYpAFbsGhMdEIeCUfzokvxHSqiWTNytamDVRu	piece		1
13271	3387	Томатный соус	121.82	с острым перцем; 400 г; 1*6	2017-02-17 19:38:20.954063	2017-02-23 12:27:21.209118	0	\N	lFArSQDHkhWaZIoXidqejNcpLYfKOGPzVbEUCTtmwgMsnRyBux	piece		1
13256	3381		60.96	с красным перцем; 3,8 мл	2017-02-17 19:16:07.542454	2017-02-23 12:27:21.253735	0	\N	wQlBgcOKPLXrnjYpAFbsGhMdEIeCUfzokvxHSqiWTNytamDVRu	piece		1
13283	3390		138.31	0,215 л	2017-02-17 20:02:15.740686	2017-02-21 15:18:17.212996	0	\N	LXFfMIUbVSHqiwNEZBongYaDPOJsWdRKQzucTjAlkxrhCvpemy	piece		1
13255	3381		509.18	с красным перцем; 150 мл	2017-02-17 19:16:07.539911	2017-02-23 12:27:21.259781	0	\N	wQlBgcOKPLXrnjYpAFbsGhMdEIeCUfzokvxHSqiWTNytamDVRu	piece		1
13275	3388	Салатная заправка соево-кунжутная	741.23	1 л.	2017-02-17 19:45:31.453964	2017-02-21 15:18:53.717909	0	\N	cYKnoNQkOXmEvGTpibwHlUMIWfPDSZJteBqyCLjxgVzAsdhRFr	piece		1
13270	3386	Томатное рагу	180.83	болоньез с говядиной и свининой; 400 г; 1*6	2017-02-17 19:36:01.009587	2017-02-23 12:27:21.212779	0	\N	vrFufRHlJSByonqdzQtmTKPZaWwDxeMIhLOVYkCgjiXNbUpsEA	piece		1
13254	3381		553.54	с зеленым перцем; 150 мл	2017-02-17 19:16:07.537244	2017-02-23 12:27:21.264733	0	\N	wQlBgcOKPLXrnjYpAFbsGhMdEIeCUfzokvxHSqiWTNytamDVRu	piece		1
13268	3385		373.95	бальзамный; со вкусом белого трюфеля; 250 мл; 1*6	2017-02-17 19:33:37.445049	2017-02-23 12:27:21.216347	0	\N	xzsWrNKtRDclHeUvGYVnkiuhyLoQOpfTgbdMJaAIwjCBSEqXZP	piece		1
13266	3385		366.67	бальзамный; белый; 500 мл	2017-02-17 19:33:37.437488	2017-02-23 12:27:21.219893	0	\N	qVjKuFnewcAvCfbhasBQzHXLPUGEoTdZgMptWYkSDNJOxRImyl	piece		1
13253	3381	Перечный	243.73	чипотле; 60 мл	2017-02-17 19:16:07.534392	2017-02-23 12:27:21.268481	0	\N	wQlBgcOKPLXrnjYpAFbsGhMdEIeCUfzokvxHSqiWTNytamDVRu	piece		1
13252	3381	Перечный	553.54	сладко-острый; "Asia"; 150 мл	2017-02-17 19:16:07.531469	2017-02-23 12:27:21.272215	0	\N	wQlBgcOKPLXrnjYpAFbsGhMdEIeCUfzokvxHSqiWTNytamDVRu	piece		1
13265	3384	Chili Garlic	138.31	с чесноком; 0,240 г; стекло	2017-02-17 19:24:15.267121	2017-02-23 12:27:21.223244	0	\N	LVhBDORrtImKwxNUAblvPJFEdTenCZGSHqszakugMoyYQiWpjX	piece		1
13260	3382		877.08	2,160 кг. 	2017-02-17 19:18:41.588355	2017-02-21 16:06:40.687436	0	\N	nyrCFBQIXhYWZcemTLSslDkxpJRqaztdbjiNPEGwoKvUgVAHuM	piece		1
13264	3384	Для курицы	1292.00	6.3 кг; пл/канистра	2017-02-17 19:24:15.265177	2017-02-23 12:27:21.227226	0	\N	PcNOHwCxjGbdTgulmLpEFvzVsnrqIeWRUSYMZtBaoikADQKyhJ	piece		1
13300	3404	Соус рыбный	232.36	700 мл; 1*12; Тайланд	2017-02-21 12:06:31.054463	2017-02-23 12:27:21.127074	0	\N	TJYDdfgteBPizFjQoZCsAOHkNhSUXRMWvrnLqGymwIVlxubEpK	piece		1
13201	3361		387.78	450 г	2017-02-11 13:04:26.219757	2017-02-23 04:13:08.987787	0	\N	fTxAqWrymSjtBgInHXVzQUJKOkPNMabwRYGFCLlsihZpeovEDd	piece		1
13298	3402	Соус острый «Красный дьявол»	1459.92	3,8 л; США	2017-02-21 12:04:44.254665	2017-02-23 12:27:21.133213	0	\N	dzoNftWvJrgShjYqZaOGsuRQHcUPxCbiwTKnkXMBEmDyVLpFlI	piece		1
13152	3348	Сыр «Арабеск»	762.03	смесь козьего и коровьего молока	2017-02-10 18:44:29.972737	2017-06-07 09:16:45.707016	0	\N	PwDZNxVyzsXHOavUpEgYkFKRGLiqrQfdbAhCjctulMnJoeWSIm	kilo		1
13100	3322	Лопатка 	627	Under blade	2017-02-08 16:28:21.554244	2017-06-02 10:24:53.981277	0	foto-264.jpg	asBeWnLMEwSTkvXqQyUcjCzpuRGhDrilxAYZOPHfbdKVmFJNtg	kilo		1
13099	3322	Котлетное мясо	319.65	1 кг	2017-02-08 16:28:21.552538	2017-06-02 10:24:53.9841	0	foto-303.jpg	asBeWnLMEwSTkvXqQyUcjCzpuRGhDrilxAYZOPHfbdKVmFJNtg	kilo		1
13110	3328	«Снежный краб»	189	0,5 кг	2017-02-09 09:15:27.501326	2017-07-17 11:57:03.50333	0	foto-95.jpg	KnrDzFHRSaYuwJltOyVpIsNAMGbjeUokdExQmZgcXfiPqCLTvB	kilo	00000001866	1
13297	3401	Соус Кунжутный	768.38	Dressing; 1,5 л	2017-02-21 12:03:46.008913	2017-02-23 12:27:21.137226	0	\N	mKgBxvbIwGjJPkiWSOrEzUNXFtRDpolHQsTMcqyfeVZhYLCaAu	piece		1
13290	3394	Соус «Ворчестер»	194.66	140 г	2017-02-21 11:54:39.159469	2017-02-21 15:17:17.332368	0	\N	bReMzkniayAIqYoOTrKBtNXWQwSJLZjVucdvPsUlDpEhxmgFHf	piece		1
13287	3393	Барбекю	5.94	 25 мл; 1*125	2017-02-21 11:51:07.159629	2017-02-23 12:27:21.172414	0	\N	BfEkDhZqcFPepyAIwMJWbmdxtzuRCYrVgSsNGOXQLHolaKijUn	piece		1
13243	3379	Кетьяп Манис	424.05	сладкий; 0,5 л; 1*12	2017-02-17 18:49:17.866127	2017-02-23 12:27:21.303207	0	\N	ApqKOmBhEbXaQPRUvrJcdSljYGDekoIwfWLnNHMVZuzFsTCyxg	piece		1
13302	3405	«Домашний»	601.08	5 л	2017-02-21 14:45:33.919127	2017-02-23 04:37:47.870759	0	\N	DTrBcvabtIxkGijNoLSRhHECgfeWUXKJulFOMnsYyzAdmVwqQP	piece		1
13301	3405	«Настоящий»	1176.49	5,3 л	2017-02-21 14:45:33.911925	2017-02-23 04:37:47.872756	0	\N	JUFmcXqRGwMZHCNhASyfrLTBDgknsojWYeQdIVulazKpEtivbx	piece		1
13236	3378	Карри	255.62	красная; 400 г; пл/б	2017-02-17 18:33:07.879485	2017-02-23 12:27:21.317351	0	\N	mKrPjYHhdtwCgpNZRcbLITlnuXAOSiJvqzyfsVeGDUaQMEoFWx	piece		1
13299	3403	Соус песто	224.47	200 г	2017-02-21 12:05:34.43158	2017-02-23 04:38:16.364338	0	\N	qQGpXlJRkjCznUcAIKFPEwWTmfdMtvDbSyYNshHOLraeguoixZ	piece		1
13235	3378	Карри	234.72	зелёная; 400 г; пл/б	2017-02-17 18:33:07.876342	2017-02-23 12:27:21.320726	0	\N	mKrPjYHhdtwCgpNZRcbLITlnuXAOSiJvqzyfsVeGDUaQMEoFWx	piece		1
13194	3356	Масло виноградных косточек	355.40	0,5 л; ж/б; 1*12	2017-02-11 12:36:28.504831	2017-02-23 12:27:21.401569	0	\N	kPuYgtiqfbwaKecUjIDBGrWdnJTzxZOFyMSCERVphHQmoXlsvA	piece		1
13295	3399	Соус Томатино	864.43	3 кг	2017-02-21 12:01:09.700435	2017-02-23 04:39:08.545702	0	\N	BRlazrCHegQLAsXNpItmnFMufYwEdiWGVPkvcDbOSqZhxjTUKy	piece		1
13296	3400	Соус крабовый «Донгу»	193.38	ст/б; 0,5 л; 1*12	2017-02-21 12:02:39.254248	2017-02-23 12:27:21.141145	0	\N	YGWRbNvACacSTfjlhKwyxEFPVozpJLkUZXsmHutMeQOgIDrBdq	piece		1
12137	3124	«Эль Сальвадор»	1395.89	500 г	2016-06-07 08:48:10.621523	2017-02-22 07:16:12.289792	0	\N		piece		1
13294	3398	Соус для угря Унаги	495.76	1,8 л; 1*6	2017-02-21 12:00:11.864765	2017-02-23 12:27:21.146823	0	\N	YZltfxVDzWvGBTOnKEAeNyUaRMrPCgqscJdSjobFLXuQmHIpik	piece		1
13293	3397	Соус Гуакамоле	244.76	250 г; 1*6	2017-02-21 11:59:18.817004	2017-02-23 12:27:21.151479	0	\N	ziJjkOsxYQZGtguvdlLqKHMrXhpeAVNWUEPnbocTfRFDywaCBI	piece		1
13292	3396	Соус гранатовый	237.18	Наршараб; 400 г; 1*20	2017-02-21 11:58:06.51719	2017-02-23 12:27:21.155642	0	\N	bxsDvSIfNCuWQZdVMHzmJYoqOhKkwTigrtLeBjnlaUypRXcFAG	piece		1
13291	3395	Соус Chilli Garlic	1472.72	2,13 кг; ж/б; 1*6 	2017-02-21 11:57:03.111184	2017-02-23 12:27:21.159495	0	\N	cJRKMobkYQwdaWjsxUFnDXmpuSiAZhfLeItyVzOrlEqgPHvGNB	piece		1
13289	3393	Сырный	6.51	25 мл; 1*125	2017-02-21 11:51:07.169575	2017-02-23 12:27:21.16482	0	\N	mVgCnkuxXfdbQKFyURwzNphtLaorOvAJeEjlYsWMBSPiIGqHDc	piece		1
12167	3136	Лингуине	103.05	 №7; 0,5 кг	2016-06-07 08:48:12.518796	2017-02-23 12:27:23.250708	0	\N		piece		1
13238	3378	Том Кха	234.72	400 г*24 шт	2017-02-17 18:33:07.885346	2017-02-23 04:42:12.366368	0	\N	mKrPjYHhdtwCgpNZRcbLITlnuXAOSiJvqzyfsVeGDUaQMEoFWx	piece		1
13288	3393	Кетчуп	6.51	25 мл; 1*125	2017-02-21 11:51:07.166215	2017-02-23 12:27:21.168503	0	\N	mVgCnkuxXfdbQKFyURwzNphtLaorOvAJeEjlYsWMBSPiIGqHDc	piece		1
13303	3406	Томатный соус для пиццы	636.53	классический; 4,1 кг; ж/б *3 шт	2017-02-21 15:22:11.706487	2017-02-23 12:27:21.119498	0	\N	dEWMYBkxbGohyCzUPcfAauXeZOsrlwQSKmjVHgNqpniDTJtLIv	piece		1
12136	3124	«Колумбия Меделлин Супремо»	1142.32	500 г	2016-06-07 08:48:10.609277	2017-02-22 07:54:07.676977	0	\N		piece		1
12166	3136	Канеллони	151.25	 №100; 0,250 кг	2016-06-07 08:48:12.506438	2017-02-23 12:27:23.25334	0	\N		piece		1
13316	3417		839.63	американский; 2,360 кг; 1*3	2017-02-21 16:25:38.571867	2017-02-23 12:27:21.068111	0	\N	IzTpeFXaPdKQjcWZgoBxOutYvDMsyVUCnNiSRALklHJEmhfrGq	piece		1
13319	3420	«Ассорти»	41.96 руб.	5 видов; 0,5 кг; 25 шт	2017-02-21 16:43:08.826746	2017-02-23 12:27:21.053746	0	\N	pvbysAIDoBgOJQPxauVdFUZlMmNnTqCrjEhkcGtYHLwSKRzieW	piece		1
13323	3420	«Новогоднее ассорти»	51.23	25 шт	2017-02-21 16:46:16.645412	2017-02-21 16:46:16.645412	0	\N	kzbJKajrtFYovxOpqHMWNcEsnPimRufDwVBeIdCQhZUTygSLAX	piece		1
13313	3415	Уксус Карандини	979.94	бальзамный; 5 л	2017-02-21 16:15:36.343299	2017-02-23 12:27:21.081104	0	\N	kJvRrLtscOpadAlPTneojuNqMKfYyEBGSUiwgzWhZCbXVFmIQH	piece		1
13314	3416	Уксус малиновый	394.44	на красном вине; 7%; 1 л*6	2017-02-21 16:18:43.707739	2017-02-23 12:27:21.076167	0	\N	icWOJrDsCkyPtIeqXvwYaKRmlpMQTHVuESfhdbGgFLNAZxjzno	piece		1
13311	3413	натуральный	128.17	красный; 6%; 1 л*12 шт; «Конди»	2017-02-21 16:11:05.24435	2017-02-23 12:27:21.088389	0	\N	rZHEpasYSiJefxGltcDudPVFzkBOgoWUTIAhLNbwXyjKRvmQqn	piece		1
13312	3414	Мицукан	1057.80	19 л	2017-02-21 16:12:42.978767	2017-02-23 04:36:00.051678	0	\N	flVEAydmetaxSBniPZqYGKHDUJkurLTbRsWFNcvoQgjChwXMIp	piece		1
13308	3411	Соус-крем Glaze Prezioso	296.60	бальзамный; тёмный; 6%; 500 мл; 1*12	2017-02-21 15:57:39.259718	2017-02-23 12:27:21.104124	0	\N	XeHIcVZhKBxotFaQUGdfWSMNjPEOkYpgTsqDRuiAJvbLmCynlw	piece		1
13310	3413	натуральный	152.04	белый; 6%; 1 л; 1*12	2017-02-21 16:11:05.24028	2017-02-23 12:27:21.09359	0	\N	sVTMoPtDCknQAwmGjhURdirNcOzSyWKuJfZqXlpBgbLveaxYIF	piece		1
13309	3412	Соус чили	695.54	сладкий; 1,950 л	2017-02-21 16:01:08.49063	2017-02-23 12:27:21.098974	0	\N	vhpPnZeEcGtyXgSdiAmCbLNFqlIOQTzKfksWoruxJMRjHVwBYU	piece		1
13329	3426	Сыр «Турне»	1781.41	100% козье молоко, ~ 1 кг (ТА)	2017-02-21 17:14:22.118518	2017-06-07 09:02:46.216	0	\N	rxnsjdOIhugXVSDHmBJyFvUYCLEMwbZoPQfKltTiqNcaRkpzWG	piece		1
13328	3425	Сыр «Моцарелла»	84.53	0,125 г	2017-02-21 17:09:27.958073	2017-06-07 09:03:40.30949	0	\N	eFiOfaDXYSkxqsHWvwgZGruNpJUERKdIPhCzylMnbQLcTAtVmo	piece		1
13326	3423	Сливки «Шантипак»	158.90	растительные, жидкие	2017-02-21 16:53:18.555606	2017-06-07 09:04:41.897312	0	\N	hGfECQBjrDoiAkXwdJHsLmcIqWugpvxTVSMKbUFYayzRlNPetZ	piece		1
13151	3348	Сыр «Арабеск»	2342.11	100% ОВЕЧЬЕ молоко, ~ 1 кг	2017-02-10 18:44:29.969673	2017-06-07 09:16:45.709039	0	\N	PwDZNxVyzsXHOavUpEgYkFKRGLiqrQfdbAhCjctulMnJoeWSIm	kilo		1
13322	3421	Сливки 22%	256.13	1 л	2017-02-21 16:45:38.662133	2017-06-07 09:06:14.935297	0	\N	dvepQVkZFNRfJAaqISXnOjbCmGolwiTBuyEhtUrKHLMWYczDPs	piece		1
13075	3141	Сливки 33%	381.63	1 л	2017-02-06 11:24:49.414788	2017-06-07 09:18:47.304827	0	\N	OZHYnLhXWafCbeocMzNKmDuJBrxQGpvPqdRTjlIVAFitgwkESU	piece		1
13318	3419	Молоко	73.56	3,5%, 1 л	2017-02-21 16:40:14.686196	2017-06-07 09:20:37.47528	0	\N	bKjpwIqCdZzchLBmGNsnrkiTRvtxgafJlUDeoyMuWXSHYPVOQF	piece		1
12476	3171	Ананасы «Шайба»	174.12	в сиропе, ж/б, 850 мл	2016-06-07 08:48:21.682061	2017-06-09 12:04:13.137138	0	\N		piece		1
13307	3410	Соус-крем Кардини	303.15	бальзамный; ягодный; 250 мл	2017-02-21 15:51:46.40397	2017-02-23 12:27:21.108833	0	\N	SCoEvANsQwufJpgexrbFZzGhKWkByPXTjIlcdODYqRUHmMaLnt	piece		1
13305	3408	Соевый соус	122.94	550 мл; пл/б; 1*24	2017-02-21 15:37:50.146228	2017-02-23 12:27:21.114151	0	\N	zeVabfumogTqKjBEDCIkdyhRHlXvFMrUPiNZYsJpGxwQAWStLO	piece		1
13250	3380	Супериор	185.95	темный; 0,5 л; ст/б.	2017-02-17 18:56:54.140259	2017-02-23 12:27:21.280067	0	\N	XYPuqpOlgxyZNnIBwHAjMasfvUVdkecFWKhCrbQEiDGRotTzJm	piece		1
13306	3409	Соус соевый	3072.48	18 л	2017-02-21 15:42:54.517598	2017-02-23 04:37:19.014281	0	\N	EymTjWbnMQoKHrGfZDdROelCiLhqxVsPUwuSpFINgzaBAYvtkJ	piece		1
13304	3407	Соевый соус	2172.97	5 л	2017-02-21 15:35:54.786272	2017-02-23 04:37:32.014868	0	\N	WIuPAdjzNEgvLVcsRfXCSDlGTMHJoUahZbkqKxwYeimOtpyrBF	piece		1
13340	3427	«Антуаз»	901.56	1.1 кг; 10 порций	2017-02-21 17:24:06.241999	2017-02-23 12:27:20.995083	0	\N	OpUNliEucTFJxArgXhwdanGbtkDzeHPKmBvCfSqyojRYIsZWVM	piece		1
13339	3427	«Птичье молоко»	763.26	0.9 кг; 10 порций	2017-02-21 17:24:06.238853	2017-02-23 12:27:20.999025	0	\N	OpUNliEucTFJxArgXhwdanGbtkDzeHPKmBvCfSqyojRYIsZWVM	piece		1
13338	3427	«Паризьен»	865.71	1.1 кг; 10 порций	2017-02-21 17:24:06.234804	2017-02-23 12:27:21.002407	0	\N	OpUNliEucTFJxArgXhwdanGbtkDzeHPKmBvCfSqyojRYIsZWVM	piece		1
13335	3427	«Медово-сливочный»	818.58	1.1 кг; 10 порций	2017-02-21 17:20:14.619526	2017-02-23 12:27:21.011921	0	\N	TsVwdcYQXKSNCkOUtxZjypLnBDoamAGMJIhvEreFbqHRWPgifu	piece		1
13334	3427	«Маково-черничный»	1147.44	1.3 кг; 10 порций	2017-02-21 17:20:14.614673	2017-02-23 12:27:21.015402	0	\N	TsVwdcYQXKSNCkOUtxZjypLnBDoamAGMJIhvEreFbqHRWPgifu	piece		1
13331	3427	«Захер»	965.60	1.1 кг; 10 порций	2017-02-21 17:15:01.19888	2017-02-23 12:27:21.025165	0	\N	pxiKBObkjaRfVgFLYGoNqrdnZczWUXehICSAmTJvPtuDsMHyQE	piece		1
13320	3420	«Ассорти» мини	25.82	250 г; 25 шт	2017-02-21 16:44:34.108326	2017-02-23 12:27:21.049459	0	\N	BmSlQLtInXseNPVqcTrFDgdHvYECROpyajGZWwoxzbuMhkAKUi	piece		1
13249	3380	Супериор	1823.61	темный; 8 л; 1*2; пл/канистра	2017-02-17 18:56:54.136981	2017-02-23 12:27:21.283807	0	\N	XYPuqpOlgxyZNnIBwHAjMasfvUVdkecFWKhCrbQEiDGRotTzJm	piece		1
13248	3380	Супериор	495.76	темный; 1,8 л*12; пл/канистра	2017-02-17 18:56:54.133633	2017-02-23 12:27:21.287485	0	\N	XYPuqpOlgxyZNnIBwHAjMasfvUVdkecFWKhCrbQEiDGRotTzJm	piece		1
13247	3380	Супериор	166.49	светлый; 0,5 л; ст/б.	2017-02-17 18:56:54.130124	2017-02-23 12:27:21.291093	0	\N	XYPuqpOlgxyZNnIBwHAjMasfvUVdkecFWKhCrbQEiDGRotTzJm	piece		1
13246	3380	Супериор	1516.26	светлый; 8 л; 1*2; пл/канистра	2017-02-17 18:56:54.126679	2017-02-23 12:27:21.295368	0	\N	XYPuqpOlgxyZNnIBwHAjMasfvUVdkecFWKhCrbQEiDGRotTzJm	piece		1
13245	3380	Супериор	382.66	светлый; 1,8 л*12; пл/канистра	2017-02-17 18:56:54.122442	2017-02-23 12:27:21.299165	0	\N	WwKMoBvrPYHQUCXaGcgObAsVyDLfxdzjnEZIkmtqeJThpFRuSN	piece		1
13234	3378	Карри	229.29	жёлтая; 400 г; пл/б	2017-02-17 18:33:07.8729	2017-02-23 12:27:21.324035	0	\N	bpahkmHNZrcCQMsSolDzPUAvYVKLBJFGRXudOnEyeiTgwtfxqW	piece		1
13346	3427	«Три шоколада»			2017-02-21 17:28:23.549397	2017-02-21 17:28:23.549397	0	\N	MbCVOclqgrWUaGEhFdzSfTyHxPLXuoYiJQsmjeBZnDpkRIvAwt	piece		1
13369	3449	Оливковое масло Pomace	255.00	рафинированное; 1 л; пл/б; 1*12	2017-02-22 06:43:48.533444	2017-02-23 12:27:20.898762	0	\N	XdDSrnLjANlxbGvafwqTVWEeyMuzgJZYpPCIKUthRimFcHBkOo	piece		1
13373	3451	Арборио	276.11	1 кг	2017-02-22 06:59:31.862844	2017-02-22 06:59:31.862844	0	\N	yabtWQTsHrDJfYLOpdeGEPVNuzKiXmhqcBjgoIkvlwnRUMCSAF	piece		1
13374	3451	Арборио	91.19	0,5 кг	2017-02-22 06:59:31.867031	2017-02-22 06:59:31.867031	0	\N	yabtWQTsHrDJfYLOpdeGEPVNuzKiXmhqcBjgoIkvlwnRUMCSAF	piece		1
13377	3454	Barrilla Пенне Ригате №73	74.59	500г	2017-02-22 07:18:11.081794	2017-02-22 07:18:11.081794	0	\N	THisfgZxdKlGPkwLNOhtjEoAUnIBXWuyaDczFmYbpJVqeSrCRM	piece		1
13336	3429	Сыр «Салаксис»	190.97	0,250 г	2017-02-21 17:23:42.679892	2017-06-07 08:55:22.811677	0	\N	odfvrLPgtQKCXWSHNOnTlFYMDsZVkbBJhyEzqARGIxmjawuipU	piece		1
13333	3428	Сыр «Фуэте» выдержанный	2688.09	100% овечье молоко, ~ 1 кг	2017-02-21 17:15:58.836124	2017-06-07 08:57:30.453528	0	\N	jLrtqeXWGCNIskSgpZdUmAJMKixvcOnlBhbPEwHuDzaYyfFQTR	piece		1
13330	3426	Сыр «Турне» выдержанный	1803.12	100% козье молоко, ~ 1 кг	2017-02-21 17:14:22.1227	2017-06-07 09:02:46.213029	0	\N	LzfvKMngsSIbitlUahJkrXVBdHCoAYDxmPQOcRqWejupyTFNwG	piece		1
13327	3424	Сыр «Маскарпоне»	313.66	80%, 0,5 кг	2017-02-21 17:02:36.186371	2017-06-07 09:04:20.165589	0	\N	jOBDMbxSuoIPqLCEGfzYnKTrVmvwkgJcWyNRiHQpahZXtlFUsA	piece		1
13174	3354	Горгонзола	1075.73	48%	2017-02-11 11:34:17.635638	2017-06-07 09:10:19.599004	0	\N	PvEKamRwYSdxZDHolWctNsGUIOfjVyQTnLMAkFqrgiCpBheJub	kilo		1
13172	3354	Гауда «Биберталлер»	420.25		2017-02-11 11:34:17.629462	2017-06-07 09:10:19.601202	0	\N	PvEKamRwYSdxZDHolWctNsGUIOfjVyQTnLMAkFqrgiCpBheJub	kilo		1
13321	3421	Сливки 33%	345.77	1 л	2017-02-21 16:45:38.658606	2017-06-07 09:06:03.52442	0	\N	LwiWjKsvErCGIMeoXuSRaJNzghUYqnBxdkZDVfbmTOptQPylcF	piece		1
13325	3422	Сливки 35% 	174.58	0,5 л	2017-02-21 16:47:18.689482	2017-06-07 09:06:41.389162	0	\N	ZQIEYiTfRHGgocqLaDhVemyMFnwKlUvtdruSzJsbOWkXCxAjBN	piece		1
13324	3422	Сливки 35%	386.75	1 л	2017-02-21 16:47:18.686104	2017-06-07 09:06:41.39129	0	\N	XsFYSUEPOmjwBJZabWAIzgKrCxuNqfivLDQdlTpRkHchMVeytn	piece		1
13317	3418	Масло сливочное «Тысяча Озёр»	311.35	82%, 450 г	2017-02-21 16:38:35.637887	2017-06-07 09:21:07.422838	0	\N	SJPYNDeBGwfcuMxQHEqZlUghorbKtApCdsVOyknIiaTRzFWvXj	piece		1
13368	3448	Оливки «гигант»	1562.37	4,25 л	2017-02-21 20:31:52.303137	2017-06-08 13:11:35.818555	0	\N	vKjYeFAyqHhVdSQkwEBMOWsgtPGaxrJRZNucmLbIiCUlnDzXTp	piece		1
13367	3447	Оливки «гигант»	1512.58	 с/к, 3 кг, ж/б	2017-02-21 20:31:17.953078	2017-06-08 13:11:59.43267	0	\N	qkgAfuHsKowxXdRnUrpIehQaTEcNLtFJPYDGzCOjVZlmvbyBMW	piece		1
13366	3446	Маслины чёрные	286.76	резаные, 1,8 кг	2017-02-21 20:29:11.48876	2017-06-08 13:12:10.467169	0	\N	GRFesNUaiCwOxAYJykHnWzcDMtuPTQIbLpoXhEZmBrSlqdVjgv	piece		1
13361	3441	Кукуруза сладкая	79.92	консервированная	2017-02-21 20:17:27.68935	2017-06-08 13:12:55.563504	0	\N	ynvSzOZCGFPoiYrxTbVgBRfjuNltDwcHEIqeQdULhasKMpmXAJ	piece		1
13365	3445	Маслины гигант	1441.37	с/к, 4,25 л	2017-02-21 20:27:46.054626	2017-06-08 13:13:28.444366	0	\N	TCmdLQiIxpPlBsfhrczZoVESHMvKWFJeRyqAYXGaUNbOuDgtnw	piece		1
13364	3444	Маслины «супергигант»	1249.59	3,15 кг, ж/б	2017-02-21 20:26:23.971272	2017-06-08 13:13:48.344921	0	\N	azqxJHODUWVGdfliLMQZSRokmbFeIgcrKtsEpvYwXjhPnTABCN	piece		1
13362	3442	Кукуруза, початки	107.58	«Delcoff», 370 мл, ст/б	2017-02-21 20:20:50.502052	2017-06-08 13:14:40.324382	0	\N	vkOHGbsPxoIeVfMEZBwjcFNqQmdXSLJKiCphrDYaWuRytTzgAn	piece		1
13359	3439	Шампиньоны	181.75	580 г	2017-02-21 20:08:11.814246	2017-02-23 04:28:54.915087	0	\N	oOsYbrhQSTPkZUxDmdMGewWugvJjiftIlNCcanXqzpVKFyAHLB	piece		1
13354	3435	Томатная паста	250.09	800 г; 1*12	2017-02-21 18:59:43.016146	2017-02-23 12:27:20.948189	0	\N	WnLGqNgoCmSIYdyXPxzEjbUrpAHcOZMeaKviRusJlwtBfFQhDV	piece		1
13352	3433	Сыр Русский пармезан	709.98	30%; ~2 кг	2017-02-21 17:41:13.036739	2017-02-23 12:27:20.955772	0	\N	dTKZisxhYmvwgRGIzljkpPBDFUcCSnVoyNrJAtaXqWefubHELO	piece		1
13353	3434	Черри с зеленью и чесноком	99.48	«Бояринъ» ст/б; 700 мл; 1*12	2017-02-21 18:40:56.573829	2017-02-23 12:27:20.951856	0	\N	GiokXlvzMbdNeJngOhEDHyxtVZQSsFwjUqYAIKcufrmRapWPCB	piece		1
13380	3455		112.90	№103; 0,25 кг	2017-02-22 07:22:45.83106	2017-02-23 12:27:20.862738	0	\N	HilCmZPqcKVfWdksTYUeQhpBXJIOoAytGENSxjMRuvDwgbLzFa	piece		1
13379	3455		75.65	№2233; 0,25 кг	2017-02-22 07:22:45.828425	2017-02-23 12:27:20.866776	0	\N	HilCmZPqcKVfWdksTYUeQhpBXJIOoAytGENSxjMRuvDwgbLzFa	piece		1
13378	3455	со шпинатом	126.55	 №310; 0,250 кг	2017-02-22 07:22:45.824873	2017-02-23 12:27:20.870794	0	\N	YCaSXBuchlwEFGRDTdnspVUIHfOmeJrKAqMgxvQyWLojkitzPZ	piece		1
13376	3453	Сахарная пудра	43.34	300 г; 1/22	2017-02-22 07:06:34.732658	2017-02-23 12:27:20.876215	0	\N	GXHyZcwlQtVLvOJbzgRDIFdohkrfSCiKEUBqsTxYMeNpmAnjau	piece		1
13375	3452	Сахар тростниковый	202.25	белый; кусковой; 0,5 кг	2017-02-22 07:04:00.101048	2017-02-23 12:27:20.880157	0	\N	HiYWBdJptgNIEknScsRLjerfGuhPAOFbqCZKyVxvalUozMwTXD	piece		1
13372	3451	Венере	449.25	0,5 кг; 1*10	2017-02-22 06:59:31.857731	2017-02-23 12:27:20.887204	0	\N	DujmbeUXftzKYMiwWLFaslBIEpdOGPRTCJvqHghAnkrNZoQxcy	piece		1
13371	3450		2355.33	5 л; ж/б; 1-й отжим; 1*4	2017-02-22 06:46:27.989847	2017-02-23 12:27:20.891272	0	\N	tavZqUVgoBnDXbwdFOuHkTGczImeLNyMpxSRJhjAsWiflrQEKY	piece		1
13345	3427	«Страджетелли»	860.58	1,100 кг; 1*10 шт	2017-02-21 17:28:23.546637	2017-02-23 12:27:20.976981	0	\N	MbCVOclqgrWUaGEhFdzSfTyHxPLXuoYiJQsmjeBZnDpkRIvAwt	piece		1
13344	3427	«Наполеон»	1009.14	1.4 кг; 9 порций	2017-02-21 17:28:23.540022	2017-02-23 12:27:20.980237	0	\N	MbCVOclqgrWUaGEhFdzSfTyHxPLXuoYiJQsmjeBZnDpkRIvAwt	piece		1
13341	3427	«Грушевый»	1022.46	1.3 кг; 10 порций	2017-02-21 17:24:06.244997	2017-02-23 12:27:20.991423	0	\N	OpUNliEucTFJxArgXhwdanGbtkDzeHPKmBvCfSqyojRYIsZWVM	piece		1
13381	3456	Нерри	295.90	с чернилами каракатицы 0,5 кг	2017-02-22 07:25:43.604921	2017-02-22 07:25:43.604921	0	\N	wDXckafelJxdAvbnLgzOHENmKyYBqPSVGiphCWUZrTIojutQMF	piece		1
13384	3457				2017-02-22 07:26:21.706876	2017-02-22 07:26:21.706876	0	\N	CSHvldLkYNOjWAGpqhiMoXfPQxaRsmbTcDFUwKyeEBtVzrJIug	piece		1
12876	3330	Для гамбургеров с кунжутом	16.02	Финляндия; 125 мм; 60 шт	2017-01-29 08:32:23.099937	2017-02-23 12:27:22.180586	0	Гамбулочки_1.jpg	ewZWFvoNLxqIuGhcJRjtiaApMlCKYVQDBXydmSOkfnPgHzUrTs	kilo		1
13392	3462	Соус Устричный	353.36	0,907 кг; ст/б; 1*12	2017-02-22 07:47:05.258316	2017-02-23 12:27:20.817643	0	\N	dULqHaoYKJyciQSVTBeshkxMZPOrwfXCbNmRApWvgFDtuGnlIz	piece		1
13391	3461	Соус Шрирача	211.56	510 г; пл/б	2017-02-22 07:45:33.90742	2017-02-23 12:27:20.82169	0	\N	PXVQHnRozWsdUZOYCmTIfDiFkepKlqxrvNGbaEhgAytuwLjMBS	piece		1
13390	3460		119.00	 №101; 0,25 кг	2017-02-22 07:32:17.582046	2017-02-23 12:27:20.825691	0	\N	qvQZnlIpcuGJrtdWgjhPHEmRboTOUMYCyVazsXSkewKFixLfNB	piece		1
13389	3460		145.35	№201; 0,5 кг	2017-02-22 07:32:17.57754	2017-02-23 12:27:20.832719	0	\N	fSnByUxIscQHrRZFJqhAVoWdKOwamTDkCjivPMGXtupeYlNEgz	piece		1
13388	3459		104.30	№34; 0,5 кг	2017-02-22 07:30:11.619898	2017-02-23 12:27:20.836521	0	\N	sJdEaLzIBkFuKUCtpmRAxbvyiHqPwXjWMQSnYgorhDTGVclZeO	piece		1
13394	3464	Соус Чеддер 	1459.92	Мексиканские специи; 3 кг	2017-02-22 07:49:57.346835	2017-02-23 12:27:20.802202	0	\N	QASnKPZosbqEWmJRGfaeOBpitTXDkzHVjycudYhNlFCUMgLwvr	piece		1
12859	3109	Торт «Наполеон» 	1009.14	1,4 кг, 9 порций	2017-01-27 16:54:26.363005	2017-06-07 08:07:54.389393	0	Наполеон.jpg	IZqMglnKeoNYrjpTXEtQxhdUciOuCmGzwHAJVDPksBbLvFRfWa	piece		1
12858	3109	Торт «Грушевый»	1022.46	1,3 кг, 10 порций	2017-01-27 16:54:26.360655	2017-06-07 08:07:54.392257	0	Грушевый1_400x400.jpg	IZqMglnKeoNYrjpTXEtQxhdUciOuCmGzwHAJVDPksBbLvFRfWa	piece		1
12850	3109	Торт «Захер»	965.60	1,1 кг, 10 порций	2017-01-27 16:54:26.333917	2017-06-07 08:07:54.404205	0	Захер1_400x400.jpg	IZqMglnKeoNYrjpTXEtQxhdUciOuCmGzwHAJVDPksBbLvFRfWa	piece		1
13350	3431	Сыр «Легенда» мини	573.11	50% жирности; из коровьего молока	2017-02-21 17:33:03.041168	2017-06-07 08:53:52.692778	0	\N	nWMGCkySROHhtQVDwsjpfPXZFqmudBKxUAiLcoJzNeIEvTYgrb	kilo		1
13348	3431	Сыр «Российский Император»	509.28	50% жирности, ~5,5 кг	2017-02-21 17:29:20.620816	2017-06-07 08:53:52.702093	0	\N	HNWmxsVJrzBcOLFlnKUDYRigkPZvQhtGdjEpwIoqSCXuaAMTyf	kilo		1
13343	3430	Рикотта	131.81	45%, 0,25 кг	2017-02-21 17:26:42.435875	2017-06-07 08:54:43.338351	0	\N	isGlMwpckVjTJbCeKWUQtyXofHYhNgdOPxIAvzZRmraqunDBLF	piece		1
13342	3430	Creamcheese	128.02	сливочный, 0,180 г	2017-02-21 17:26:42.432072	2017-06-07 08:54:43.34031	0	\N	btCfDBeTZAIcNUMpwkvPymGXlJOKHLSshnuVodYEFzgxRQjaiq	piece		1
13351	3432	Сыр «Cremette»	883.64	65%, 2 кг	2017-02-21 17:36:17.318993	2017-06-07 08:55:53.301251	0	\N	BHsxUWjGRArwbzDMnqdgEZciPLlCImYoyXVTvptKfhJQukFaNS	piece		1
13171	3354	Буррата «Гальбани»	197.84	мини, 125 г	2017-02-11 11:34:17.626449	2017-06-07 09:10:19.60286	0	\N	PvEKamRwYSdxZDHolWctNsGUIOfjVyQTnLMAkFqrgiCpBheJub	piece		1
13360	3440	Грибы белые	890.30	резаные, в масле, 0,8	2017-02-21 20:10:41.921226	2017-06-08 13:12:22.64459	0	\N	KaqnCwByiuTfJzEHVUjeShrMdFoRPbOAxYIWpQsXZLtlGgvckN	piece		1
13363	3443	Кукуруза	52.97	«Сто рецептов», 425 г	2017-02-21 20:22:42.606387	2017-06-08 13:14:07.967037	0	\N	INthjikDqJASKpZMHOnwUlmreERLGxzfuFTVbXWQcCBgPyodYs	piece		1
13357	3438	Артишоки	918.47	целые, натуральные, 2,65 кг, ж/б	2017-02-21 20:02:59.537346	2017-06-08 13:17:12.167426	0	\N	wvDgFZyNIeKocfVLCTkurdJYWOUjsHxAtSiGXPMbBhzEalmQnR	piece		1
13356	3437	Томаты	345.77	очищенные целые в томатном соку; 2,5 кг, ж/б	2017-02-21 19:11:45.29121	2017-06-08 13:18:37.116954	0	\N	LSTlKVDHqjcktIhfOYNwWEBzsboenuGRrQAvMXaPyidmUFZgxJ	piece		1
13355	3436	Томаты	535.77	вяленые, в масле, 750 г	2017-02-21 19:08:08.961087	2017-06-08 13:19:27.305391	0	\N	EkrmVdKJaZHTtcPUoBgWqFjLzSfbRxplIyvnXYANiCeDhuMwsO	piece		1
13393	3463	Соус Хойсин	100.0	2,20 кг; пл/б	2017-02-22 07:48:48.030478	2017-11-02 10:54:21.151362	0	\N	gFeHapoDhkOwzsIUYrGQSBNJCdcqtxnWjlEbLTAXyMPivRKmZu	piece	00000000007	1
13387	3459		129.75	триколор; №34; 0,5 кг	2017-02-22 07:30:11.6166	2017-02-23 12:27:20.840316	0	\N	FqNmnHcPyfOgBuJkZaKWzoXhrtCETUQipsvexRIbAjGMdLlVYD	piece		1
13386	3458		103.05	 №41; 0,5 кг	2017-02-22 07:28:31.671471	2017-02-23 12:27:20.844171	0	\N	RkCYalOmWrdZFuhcgGTUxENeDLzSMfvotqPnbJBQXiIHyKpAws	piece		1
13385	3458		141.70	ТРИКОЛОР; 0,5 кг	2017-02-22 07:28:31.667787	2017-02-23 12:27:20.848037	0	\N	buUdmLvoJhPyiFrwxNZegtsSYXBHpDzOnGVRCQlEjITfaAqcMk	piece		1
13383	3456	Спагетти	195.55	№12; 1 кг	2017-02-22 07:25:43.615953	2017-02-23 12:27:20.853523	0	\N	TnKvAFUeldOyESHmcpPMJiWVwBtGxuqaZzshCQXNIYbgLfjkoD	piece		1
13382	3456		193.35	№11; 1 кг	2017-02-22 07:25:43.613217	2017-02-23 12:27:20.857383	0	\N	TnKvAFUeldOyESHmcpPMJiWVwBtGxuqaZzshCQXNIYbgLfjkoD	piece		1
13370	3450		352.95	с ароматом трюфеля 0,25 л; ст/б; 1*12	2017-02-22 06:45:01.8132	2017-02-23 12:27:20.895109	0	\N	XewSsurJKzbBohtxTWFqVvODEgkGyZUQpLYPaMAfRjCmcnHNid	piece		1
12802	3262	Ребра для гриля 	736.83	"Short Ribs", с/м, 0,9-1 кг, 1*16	2017-01-25 18:07:14.055581	2017-06-01 08:05:10.07519	0	foto-305.jpg	bNKiWXkOuplzEtcdMYIABVgywLDmUsFhQRoJTnCePjfqvGaHSx	piece		1
13016	3287	Ribeye	2433.19	зачищенный, с/м, б/к, Top Choice, ~5 кг	2017-02-01 10:45:31.335095	2017-06-01 08:06:26.242982	0	foto-265.jpg	pwbumWeIEPnUyVAHkDfRSFjsBrxNilaqMYXvZTzOhdogJKctGQ	kilo		1
13015	3287	Ribeye 	2858.36	зачищенный, Top Choice, ~5 кг	2017-02-01 10:45:31.332178	2017-06-01 08:06:26.246289	0	\N	pwbumWeIEPnUyVAHkDfRSFjsBrxNilaqMYXvZTzOhdogJKctGQ	piece		1
13017	3288	Striploin 	2032.61	зачищенный, Prime, ~3 кг	2017-02-01 10:52:50.371685	2017-06-01 08:07:17.944031	0	foto-423.jpg	FpGouJSVLeRtaCDiWHQEnlAPKTXygmskjrBbfMNhdYqzvxIZcw	piece		1
13022	3292	Лопатка говяжья	445.05	на кости, с/м, 1,5 кг*4	2017-02-01 11:12:17.1285	2017-06-01 08:10:04.493552	0	\N	yXADoUkqcYQFhPpwnsuJxbKTLWRvMZmCgadilfVBzHeEGrtINO	piece		1
11916	3063	Корейка молочного телёнка	1792.88	7/8 рёбер, с/м, 1*12	2016-06-07 08:47:27.318246	2017-06-01 08:10:41.632809	0	\N		piece		1
11919	3064	Вырезка	367.80	вакуумная упаковка	2016-06-07 08:47:27.474649	2017-06-01 08:11:03.244123	0	\N		piece		1
11886	3044	Корейка 7-8 рёбер	2047.98	Исландия, 400-500 г	2016-06-07 08:47:07.905964	2017-06-01 08:13:12.009363	0	\N		piece		1
11997	3094	Брусника 	215.56		2016-06-07 08:47:45.913159	2017-06-08 08:34:41.610558	0	\N		kilo		1
11984	3093	Фасоль стручковая 	145.07	Резанная	2016-06-07 08:47:45.616803	2017-06-08 08:31:35.369884	0	foto-10.jpg		kilo		1
11995	3094	Голубика	119.56	0,3 кг	2016-06-07 08:47:45.883837	2017-06-08 08:34:41.612477	0	\N		piece		1
13098	3322	Толстый край 	999.92	Ribeye, с/м, 1*16	2017-02-08 16:28:21.550806	2017-06-02 10:24:53.985951	0	foto-275.jpg	asBeWnLMEwSTkvXqQyUcjCzpuRGhDrilxAYZOPHfbdKVmFJNtg	kilo		1
12796	3262	Тонкий край	2554.90	охлаждённый, Striploin choice	2017-01-25 18:07:14.034097	2017-06-02 10:26:00.201705	0	Говяжий-стриплойн__.jpg	bNKiWXkOuplzEtcdMYIABVgywLDmUsFhQRoJTnCePjfqvGaHSx	kilo		1
12795	3262	Грудной отруб	494.22	"Brisket Select", без кости	2017-01-25 18:07:14.030232	2017-06-02 10:26:00.202971	0	\N	bNKiWXkOuplzEtcdMYIABVgywLDmUsFhQRoJTnCePjfqvGaHSx	kilo		1
13012	3286	Грудинка без кости	448.22	охлаждённая, мраморная, 1*7 кг	2017-02-01 10:36:02.961441	2017-06-02 10:38:14.895664	0	\N	OVmtjGsiyJIoQDZcbeuXYhALzWRqFSNwgrBxaEUldKHCTvMPnp	kilo		1
13011	3286	Грудинка на кости 	??	охлаждённая	2017-02-01 10:36:02.958412	2017-06-02 10:38:14.900359	0	Grudinka_na_Kosti_2_Etic.jpg	OVmtjGsiyJIoQDZcbeuXYhALzWRqFSNwgrBxaEUldKHCTvMPnp	kilo		1
13007	3286	Глазной мускул	673.10	б/к, ~2кг	2017-02-01 08:07:29.941314	2017-06-02 10:38:14.907213	0	Глазной-мускул.jpg	vXoSqIWcBTdVFOymegsRxlMjapHLPZtiDwGKEzYubkUCrJAQNn	kilo		1
12109	3121	Вишня	565.90	1 кг	2016-06-07 08:48:09.688814	2017-06-02 10:42:10.946259	0	\N		piece		1
12108	3121	Ананас	575.70	1 кг	2016-06-07 08:48:09.675392	2017-06-02 10:42:10.94851	0	\N		piece		1
12107	3121	Абрикос	523.45	1 кг	2016-06-07 08:48:09.65959	2017-06-02 10:42:10.949568	0	\N		piece		1
12410	3474	Сироп «Черника»	489.30	0,7 л	2016-06-07 08:48:19.645823	2017-06-05 11:08:16.732006	0	\N		piece		1
12551	3199	Палочки бамбуковые	4497	3000 шт, 1 уп.	2016-06-07 08:48:26.300503	2017-06-07 08:43:37.294866	0	\N		piece		1
12856	3466	Торт «Птичье молоко» 	763.26	0,9 кг, 10 порций	2017-01-27 16:54:26.355937	2017-06-07 08:06:40.146812	0	Птичье_молоко.jpg	IZqMglnKeoNYrjpTXEtQxhdUciOuCmGzwHAJVDPksBbLvFRfWa	piece		1
12857	3109	Торт «Антуаз»	901.56	1,1 кг, 10 порций	2017-01-27 16:54:26.358275	2017-06-07 08:07:54.396518	0	Антуаз_1.jpg	IZqMglnKeoNYrjpTXEtQxhdUciOuCmGzwHAJVDPksBbLvFRfWa	piece		1
12854	3109	Торт «Медово-Сливочный»	818.58	1,1 кг, 10 порций	2017-01-27 16:54:26.350831	2017-06-07 08:07:54.398922	0	Медово-сливочный1_400x400.jpg	IZqMglnKeoNYrjpTXEtQxhdUciOuCmGzwHAJVDPksBbLvFRfWa	piece		1
12851	3465	Макарони «Ассорти» мини 	41.96	5 видов, 0,55 кг, 25 шт	2017-01-27 16:54:26.342974	2017-06-07 08:16:28.352619	0	Макарони_ассорти_мини.jpg	IZqMglnKeoNYrjpTXEtQxhdUciOuCmGzwHAJVDPksBbLvFRfWa	piece		1
12550	3199	Стружка тунца	703.50	сушёная, 0,5 кг, 1 уп.	2016-06-07 08:48:26.290313	2017-06-07 08:43:37.298863	0	\N		piece		1
12549	3199	Салат «Чука»	508.50	2 кг, 1 уп.	2016-06-07 08:48:26.278276	2017-06-07 08:43:37.300729	0	\N		piece		1
11994	3094	Ежевика 	223.34		2016-06-07 08:47:45.869705	2017-06-08 08:34:41.613867	0	foto-487.jpg		kilo		1
13101	3322	Щёки 	473.84	с/м, ~1 кг, 1*30	2017-02-08 16:28:21.555966	2017-07-12 11:11:06.804448	0	\N	asBeWnLMEwSTkvXqQyUcjCzpuRGhDrilxAYZOPHfbdKVmFJNtg	piece	3434	1
12699	3086	Картофельные оладьи	351.97	1,5 кг	2017-01-13 11:39:39.10526	2017-06-08 08:24:30.073819	0	foto-375.jpg	xFiZuyscmKGoRpWYMdzDbrVIUkHaLtEhPBqCgJNjfwTlOeSvAX	piece		1
12698	3086	Картофельные котлеты с луком	330.2	1,5 кг	2017-01-13 11:39:39.102685	2017-06-08 08:24:30.07638	0	\N	xFiZuyscmKGoRpWYMdzDbrVIUkHaLtEhPBqCgJNjfwTlOeSvAX	piece		1
12697	3086	Картофельные «Улыбки»	106.76	450 г	2017-01-13 11:39:39.100066	2017-06-08 08:24:30.078503	0	\N	xFiZuyscmKGoRpWYMdzDbrVIUkHaLtEhPBqCgJNjfwTlOeSvAX	piece		1
11993	3094	Калина	111.78		2016-06-07 08:47:45.856479	2017-06-08 08:34:41.615408	0	\N		kilo		1
13097	3322	Рёбра 	406.53	Short Ribs	2017-02-08 16:28:21.548823	2017-06-02 10:24:53.988702	0	foto-304.jpg	SFRrDpgavOmInJKYjluANxdycTHXzZMfsLhCeBbVWiQwoEtGqU	kilo		1
12922	3274	Черника пюре	961.50	1 кг	2017-01-29 10:50:07.466513	2017-06-02 10:42:01.930314	0	\N	XlPuACBYNLstcwWQOmvgDoTGEIRbnVSkqfeadzZxrUMpiJFKHy	piece		1
12921	3274	Черная смородина пюре	597.60	1 кг	2017-01-29 10:50:07.46448	2017-06-02 10:42:01.934503	0	\N	XlPuACBYNLstcwWQOmvgDoTGEIRbnVSkqfeadzZxrUMpiJFKHy	piece		1
12920	3274	Персик пюре	562.97	1 кг	2017-01-29 10:50:07.461984	2017-06-02 10:42:01.936608	0	\N	XlPuACBYNLstcwWQOmvgDoTGEIRbnVSkqfeadzZxrUMpiJFKHy	piece		1
12919	3274	Маракуйя пюре	781.70	1 кг	2017-01-29 10:50:07.460028	2017-06-02 10:42:01.938302	0	\N	XlPuACBYNLstcwWQOmvgDoTGEIRbnVSkqfeadzZxrUMpiJFKHy	piece		1
12918	3274	Мандарин пюре	667.47	1 кг	2017-01-29 10:50:07.458095	2017-06-02 10:42:01.939898	0	\N	XlPuACBYNLstcwWQOmvgDoTGEIRbnVSkqfeadzZxrUMpiJFKHy	piece		1
12917	3274	Манго пюре	611.12	1 кг	2017-01-29 10:50:07.456186	2017-06-02 10:42:01.941552	0	\N	XlPuACBYNLstcwWQOmvgDoTGEIRbnVSkqfeadzZxrUMpiJFKHy	piece		1
12916	3274	Малина пюре	777.60	1 кг	2017-01-29 10:50:07.454265	2017-06-02 10:42:01.943045	0	\N	XlPuACBYNLstcwWQOmvgDoTGEIRbnVSkqfeadzZxrUMpiJFKHy	piece		1
12915	3274	Личи пюре	796.04	1 кг	2017-01-29 10:50:07.452267	2017-06-02 10:42:01.944511	0	\N	XlPuACBYNLstcwWQOmvgDoTGEIRbnVSkqfeadzZxrUMpiJFKHy	piece		1
12914	3274	Красная смородина пюре	578.75	1 кг	2017-01-29 10:50:07.450263	2017-06-02 10:42:01.946039	0	\N	XlPuACBYNLstcwWQOmvgDoTGEIRbnVSkqfeadzZxrUMpiJFKHy	piece		1
12913	3274	Кокос пюре	829.75	1 кг	2017-01-29 10:50:07.448279	2017-06-02 10:42:01.94892	0	\N	XlPuACBYNLstcwWQOmvgDoTGEIRbnVSkqfeadzZxrUMpiJFKHy	piece		1
12912	3274	Клубника пюре	583.97	1 кг	2017-01-29 10:50:07.446251	2017-06-02 10:42:01.950177	0	\N	XlPuACBYNLstcwWQOmvgDoTGEIRbnVSkqfeadzZxrUMpiJFKHy	piece		1
12911	3274	Киви пюре	660.71	1 кг	2017-01-29 10:50:07.444219	2017-06-02 10:42:01.951451	0	\N	XlPuACBYNLstcwWQOmvgDoTGEIRbnVSkqfeadzZxrUMpiJFKHy	piece		1
12910	3274	Земляника пюре	915.91	1 кг	2017-01-29 10:50:07.442178	2017-06-02 10:42:01.952664	0	\N	XlPuACBYNLstcwWQOmvgDoTGEIRbnVSkqfeadzZxrUMpiJFKHy	piece		1
12909	3274	Зелёный лимон пюре	508.67	1 кг	2017-01-29 10:50:07.439977	2017-06-02 10:42:01.953837	0	\N	XlPuACBYNLstcwWQOmvgDoTGEIRbnVSkqfeadzZxrUMpiJFKHy	piece		1
12908	3274	Ежевика пюре	717.15	1 кг	2017-01-29 10:50:07.437774	2017-06-02 10:42:01.955034	0	\N	XlPuACBYNLstcwWQOmvgDoTGEIRbnVSkqfeadzZxrUMpiJFKHy	piece		1
12906	3274	Груша пюре	454.58	1 кг	2017-01-29 10:50:07.43236	2017-06-02 10:42:01.958685	0	\N	XlPuACBYNLstcwWQOmvgDoTGEIRbnVSkqfeadzZxrUMpiJFKHy	piece		1
12905	3274	Вишня пюре	684.17	1 кг	2017-01-29 10:50:07.430205	2017-06-02 10:42:01.959782	0	\N	XlPuACBYNLstcwWQOmvgDoTGEIRbnVSkqfeadzZxrUMpiJFKHy	piece		1
12904	3274	Апельсин пюре	555.90	1 кг	2017-01-29 10:50:07.427937	2017-06-02 10:42:01.960896	0	\N	XlPuACBYNLstcwWQOmvgDoTGEIRbnVSkqfeadzZxrUMpiJFKHy	piece		1
12903	3274	Ананас пюре	570.14	1 кг	2017-01-29 10:50:07.425558	2017-06-02 10:42:01.962237	0	\N	XlPuACBYNLstcwWQOmvgDoTGEIRbnVSkqfeadzZxrUMpiJFKHy	piece		1
12902	3274	Абрикос пюре	487.67	1 кг	2017-01-29 10:50:07.422037	2017-06-02 10:42:01.963408	0	\N	oVpTfWuFBbkwgcMrPLeCJsSYzGiDXjtZxaOAEIKlvHRQdNyqhU	piece		1
12124	3121	Чёрная смородина	565.15	1 кг	2016-06-07 08:48:09.871157	2017-06-02 10:42:10.918294	0	\N		piece		1
12123	3121	Персик	484.15	1 кг	2016-06-07 08:48:09.858426	2017-06-02 10:42:10.92212	0	\N		piece		1
12122	3121	Манго	555.05	1 кг	2016-06-07 08:48:09.846504	2017-06-02 10:42:10.923878	0	\N		piece		1
12121	3121	Малина	677.60	1 кг	2016-06-07 08:48:09.834727	2017-06-02 10:42:10.925506	0	\N		piece		1
12120	3121	Личи	673.50	1 кг	2016-06-07 08:48:09.821748	2017-06-02 10:42:10.927737	0	\N		piece		1
12119	3121	Красная смородина	584.40	1 кг	2016-06-07 08:48:09.80899	2017-06-02 10:42:10.929328	0	\N		piece		1
12118	3121	Кокос	883.50	1 кг	2016-06-07 08:48:09.797328	2017-06-02 10:42:10.930787	0	\N		piece		1
12117	3121	Клубника	543.15	1 кг	2016-06-07 08:48:09.785785	2017-06-02 10:42:10.932347	0	\N		piece		1
12116	3121	Киви	667.15	1 кг	2016-06-07 08:48:09.774132	2017-06-02 10:42:10.933938	0	\N		piece		1
12115	3121	Земляника	924.85	1 кг	2016-06-07 08:48:09.76132	2017-06-02 10:42:10.935492	0	\N		piece		1
12114	3121	Зелёный лимон	513.65	1 кг	2016-06-07 08:48:09.750367	2017-06-02 10:42:10.937081	0	\N		piece		1
12113	3121	Жёлтый лимон	476.95	1 кг	2016-06-07 08:48:09.739502	2017-06-02 10:42:10.938675	0	\N		piece		1
12112	3121	Ежевика	667.30	1 кг	2016-06-07 08:48:09.725345	2017-06-02 10:42:10.940354	0	\N		piece		1
12111	3121	Дыня	484.50	1 кг	2016-06-07 08:48:09.712382	2017-06-02 10:42:10.942479	0	\N		piece		1
12110	3121	Груша	530.70	1 кг	2016-06-07 08:48:09.700153	2017-06-02 10:42:10.944144	0	\N		piece		1
12700	3086	Картофельные котлеты	456.42	треугольные, 2,5 кг	2017-01-13 11:39:39.107761	2017-06-08 08:24:30.070125	0	foto-382.jpg	xFiZuyscmKGoRpWYMdzDbrVIUkHaLtEhPBqCgJNjfwTlOeSvAX	piece		1
11983	3093	Цветная капуста 	161.36		2016-06-07 08:47:45.602849	2017-06-08 08:31:35.372665	0	foto-519.jpg		kilo		1
12694	3086	Картофельное пюре	516.33	3 кг	2017-01-13 11:27:21.652209	2017-06-08 08:24:51.132454	0	foto-412.jpg	PbIqApjhwyEsrouxXJMFWvazHcKfTmegZVONLCYtQGUBDilRnS	piece		1
13033	3296	Картофель фри «Жульен»	287.79	6 мм, премиум, 2.5 кг	2017-02-01 13:55:23.517418	2017-06-08 08:22:03.70794	0	foto-404.jpg	chfWQlqxNitMnCDomBsObSPaGETwgVZYUXRkrKdJuzepjLIvAH	piece		1
13032	3296	Картофель фри «Стейкхаус»	286.86	9*18 мм, 2.5 кг	2017-02-01 13:55:23.514882	2017-06-08 08:22:03.709448	0	foto-402.jpg	chfWQlqxNitMnCDomBsObSPaGETwgVZYUXRkrKdJuzepjLIvAH	piece		1
13031	3296	Картофель фри "Valley Farm" 	220.48	9 мм, 2,5 кг	2017-02-01 13:55:23.51174	2017-06-08 08:22:03.710592	0	foto-408.jpg	lGhiwdXpgsSqFJfbyUOtTouWZvVDAHMrQaRmIPKENzkLxceBnY	piece		1
12391	3475	Сироп «Зелёное Яблоко»	671.30	1 л	2016-06-07 08:48:19.417056	2017-06-05 11:02:23.034221	0	\N		piece		1
12407	3474	Сироп «Персик»	575.20	1 л	2016-06-07 08:48:19.610338	2017-06-05 11:08:16.734596	0	\N		piece		1
12403	3474	Сироп «Маракуйя»	580.05	1 л	2016-06-07 08:48:19.56452	2017-06-05 11:08:16.739591	0	\N		piece		1
12401	3474	Сироп «Манго»	536.70	1 л	2016-06-07 08:48:19.53996	2017-06-05 11:08:16.74368	0	\N		piece		1
12400	3474	Сироп «Малина»	606.15	1 л	2016-06-07 08:48:19.525412	2017-06-05 11:08:16.745499	0	\N		piece		1
12396	3474	Сироп «Клюква»	491.40	1 л	2016-06-07 08:48:19.477597	2017-06-05 11:08:16.747122	0	\N		piece		1
12395	3474	Сироп «Киви»	465.55	1 л	2016-06-07 08:48:19.465924	2017-06-05 11:08:16.74881	0	\N		piece		1
12409	3164	Сироп «Фисташка»	469.40	1 л, Monin	2016-06-07 08:48:19.633481	2017-06-05 11:27:40.571722	0	\N		piece		1
12408	3164	Сироп «Сахарный тростник»	401.40	1 л, Monin	2016-06-07 08:48:19.622999	2017-06-05 11:27:40.575498	0	\N		piece		1
12394	3164	Сироп «Карамель»	543.15	1 л, Monin	2016-06-07 08:48:19.453243	2017-06-05 11:27:40.591046	0	\N		piece		1
12393	3164	Сироп «Имбирный»	585.85	0,7 л, Monin	2016-06-07 08:48:19.441657	2017-06-05 11:27:40.592921	0	\N		piece		1
12382	3164	Сироп «Ваниль»	670.40	1 л, Monin	2016-06-07 08:48:19.313543	2017-06-05 11:27:40.602669	0	\N		piece		1
12381	3164	Сироп «Бузина»	627.95	1 л, Monin	2016-06-07 08:48:19.300494	2017-06-05 11:27:40.604443	0	\N		piece		1
12380	3164	Сироп «Блю Курасао»	481.05	1 л, Monin	2016-06-07 08:48:19.290314	2017-06-05 11:27:40.606169	0	\N		piece		1
12377	3164	Сироп «Амаретто»	569	1 л, Monin	2016-06-07 08:48:19.254506	2017-06-05 11:27:40.607979	0	\N		piece		1
12295	3160	Барный сироп «Гренадин»	226.60	0,75 л, "Miller&Miller"	2016-06-07 08:48:17.741854	2017-06-05 11:48:59.273256	0	\N		piece		1
12294	3160	Барный сироп «Арбуз»	224.60	0,75 л, "Miller&Miller"	2016-06-07 08:48:17.730035	2017-06-05 11:48:59.275083	0	\N		piece		1
12375	3163	Топинг «Шоколадный»	288.15	1 л, "Dolce Rosa"	2016-06-07 08:48:19.095156	2017-06-05 11:57:02.486429	0	\N		piece		1
12374	3163	Топинг «Вишнёвый»	282.45	1 л, "Dolce Rosa"	2016-06-07 08:48:19.083694	2017-06-05 11:57:02.489157	0	\N		piece		1
12373	3163	Топинг «Орех»	271.60	1 л, "Dolce Rosa"	2016-06-07 08:48:19.073527	2017-06-05 11:57:02.490825	0	\N		piece		1
12372	3163	Топинг «Малиновый»	282.45	1 л, "Dolce Rosa"	2016-06-07 08:48:19.061248	2017-06-05 11:57:02.492643	0	\N		piece		1
12371	3163	Топинг «Лесная Ягода»	282.45	1 л, "Dolce Rosa"	2016-06-07 08:48:19.049476	2017-06-05 11:57:02.494323	0	\N		piece		1
12370	3163	Топинг «Клубничный»	282.45	1 л, "Dolce Rosa"	2016-06-07 08:48:19.037683	2017-06-05 11:57:02.496086	0	\N		piece		1
12369	3163	Топинг «Киви»	282.45	1 л, "Dolce Rosa"	2016-06-07 08:48:19.026591	2017-06-05 11:57:02.497792	0	\N		piece		1
12368	3163	Топинг «Карамельный»	282.45	1 л, "Dolce Rosa"	2016-06-07 08:48:19.01743	2017-06-05 11:57:02.499565	0	\N		piece		1
12367	3163	Топинг «Вишнёвый»	282.45	1 л, "Dolce Rosa"	2016-06-07 08:48:19.006806	2017-06-05 11:57:02.501292	0	\N		piece		1
12366	3163	Топинг «Ванильный»	272	1 л, "Dolce Rosa"	2016-06-07 08:48:18.993534	2017-06-05 11:57:02.502928	0	\N		piece		1
12365	3163	Топинг «Банановый»	270.95	1 л, "Dolce Rosa"	2016-06-07 08:48:18.982066	2017-06-05 11:57:02.504575	0	\N		piece		1
12548	3199	Рыбный бульон «Хондаши»	449.70	1 уп.	2016-06-07 08:48:26.265646	2017-06-07 08:43:37.302343	0	\N		kilo		1
12465	3167	Мексиканские специи «Фахито»	1225.10	1200 г, 1 уп.	2016-06-07 08:48:20.87234	2017-07-05 11:29:53.241425	0	\N		piece		1
12462	3167	«Чили и Манго»	1166.85	специи, 850 г, 1 уп.	2016-06-07 08:48:20.832608	2017-07-05 11:29:53.248597	0	\N		piece		1
12464	3167	«Средиземноморская смесь»	636.35	специи, 350 г, 1 уп.	2016-06-07 08:48:20.861126	2017-07-05 11:29:53.250809	0	\N		piece		1
12463	3167	«Прованские травы»	330.05	специи, 150 г, 1 уп.	2016-06-07 08:48:20.847501	2017-07-05 11:29:53.252837	0	\N		piece		1
12461	3167	«Стейкхаус Барбекю»	584.50	специи, 700 г, 1 уп.	2016-06-07 08:48:20.815556	2017-07-05 11:29:53.254712	0	\N		piece		1
12460	3167	«К рыбе с лимоном»	1098.65	специи, 580 г, 1 уп.	2016-06-07 08:48:20.801287	2017-07-05 11:29:53.256671	0	\N		piece		1
12459	3167	«Гирос, к мясу»	542.95	специи, 600 г, 1 уп.	2016-06-07 08:48:20.788374	2017-07-05 11:29:53.258544	0	\N		piece		1
12446	3166	Перец розовый, горошек	1002.45	260 г, 1 уп.	2016-06-07 08:48:20.509563	2017-07-05 11:34:00.468302	0	\N		piece		1
12445	3166	Перец кайенский	601.60	молотый 470 г, 1 уп.	2016-06-07 08:48:20.49822	2017-07-05 11:34:00.470116	0	\N		piece		1
12444	3166	Перец зелёный	1314.80	160 г, 1 уп.	2016-06-07 08:48:20.489032	2017-07-05 11:34:00.471785	0	\N		piece		1
12443	3166	Перец душистый горошек	510.95	380 г, 1 уп.	2016-06-07 08:48:20.476407	2017-07-05 11:34:00.473346	0	\N		piece		1
12442	3166	Перец белый целый	1661.65	640 г, 1 уп.	2016-06-07 08:48:20.465353	2017-07-05 11:34:00.474882	0	\N		piece		1
12441	3166	Перец белый 	1333.10	молотый, 510 г, 1 уп.	2016-06-07 08:48:20.453283	2017-07-05 11:34:00.476427	0	\N		piece		1
12440	3166	Перец апельсиновый	371.70	325 г, 1 уп.	2016-06-07 08:48:20.440875	2017-07-05 11:34:00.477965	0	\N		piece		1
12638	3223	Филе палтуса	1130.64		2016-06-08 10:53:37.302392	2017-06-06 12:05:18.579154	0	\N		kilo		1
13000	3283	Мясо краба, 1-я фаланга 	3600	в хитине, Мурманск	2017-02-01 07:34:40.753619	2017-07-18 13:35:11.753926	0	\N	jJzbuYvUKhiMcyWLAdPFGVnRtDHEsNxeOfkmrgaBTqpSwQIXCZ	kilo	00000002046	1
13122	3335	Сёмга (Лосось) 	884.15	потрошёная, с головой, 6-7 кг	2017-02-10 12:40:28.772348	2017-06-29 14:12:29.633714	0	foto-448.jpg	EiplqduWTKRcgIwGDhSzNtnsFajkOXMrLYvoVUHmBbAefQZPCy	kilo		1
12641	3223	Филе судака на шкуре	488.90	300-400, 7% глазировки	2016-06-08 10:53:37.362189	2017-06-29 14:15:37.904491	0	\N		kilo		1
12999	3283	Мясо краба, 1-я фаланга 	3700	«экстра», Мурманск	2017-02-01 07:34:40.750817	2017-07-18 13:35:11.759549	0	\N	jJzbuYvUKhiMcyWLAdPFGVnRtDHEsNxeOfkmrgaBTqpSwQIXCZ	kilo	00000000577	1
13125	3336	Филе сёмги "Super"	1253.07	с/м, вакуум, 2-3 кг	2017-02-10 12:57:18.326441	2017-06-07 07:26:58.312309	0	\N	LzluYgTMaqVWNXrBohSsKciAjkFvIfdQZGpxeJHRnmOECDtPbw	kilo		1
12852	3465	Макарони «Новогоднее ассорти»	51.23	25 шт	2017-01-27 16:54:26.345605	2017-06-07 08:05:15.406802	0	\N	IZqMglnKeoNYrjpTXEtQxhdUciOuCmGzwHAJVDPksBbLvFRfWa	piece		1
12861	3466	Торт «Три Шоколада»	1398.45	1,1 кг, 10 порций	2017-01-27 16:54:26.367483	2017-06-07 08:06:40.144294	0	Три_шоколада1_400x400.jpg	IZqMglnKeoNYrjpTXEtQxhdUciOuCmGzwHAJVDPksBbLvFRfWa	piece		1
12860	3466	Торт «Страджетелли»	860.58	1,1 кг, 10 порций	2017-01-27 16:54:26.365277	2017-06-07 08:06:40.145498	0	Страджетелли1_400x400.jpg	IZqMglnKeoNYrjpTXEtQxhdUciOuCmGzwHAJVDPksBbLvFRfWa	piece		1
12855	3466	Торт «Паризьен»	865.71	1,1 кг, 10 порций	2017-01-27 16:54:26.353384	2017-06-07 08:06:40.148108	0	Парижский4.jpg	IZqMglnKeoNYrjpTXEtQxhdUciOuCmGzwHAJVDPksBbLvFRfWa	piece		1
12853	3109	Торт «Маково-Черничный»	1147.44	1,3 кг, 10 порций	2017-01-27 16:54:26.348232	2017-06-07 08:07:54.400981	0	Маково-черничный1_400x400.jpg	IZqMglnKeoNYrjpTXEtQxhdUciOuCmGzwHAJVDPksBbLvFRfWa	piece		1
12900	3469	Чизкейк «Шоколадная трилогия»	1205.74	1,2 кг, 12 порций	2017-01-29 10:23:13.37986	2017-06-07 08:09:13.062417	0	\N	kqtWfbszJAPXBpvidghTnrcaFmIexYNDyRKVHEwolLuGCUOZMj	piece		1
12547	3199	Рис «Нишики»	1505.55	20 кг, 1 уп.	2016-06-07 08:48:26.254754	2017-06-07 08:43:37.305478	0	\N		piece		1
12546	3199	Редька маринованная	97	0,5 кг, 1 уп.	2016-06-07 08:48:26.242662	2017-06-07 08:43:37.307589	0	\N		piece		1
12545	3199	Приправа «Аджи-но-мото»	167.10	454 г, 1 уп.	2016-06-07 08:48:26.230971	2017-06-07 08:43:37.309251	0	\N		piece		1
12544	3199	Панировочные сухари «Панко»	159.15		2016-06-07 08:48:26.219778	2017-06-07 08:43:37.310848	0	\N		kilo		1
12543	3199	«Темпура»	133.25	Смесь для обжаривания, 1 уп.	2016-06-07 08:48:26.208102	2017-06-07 08:43:37.312552	0	\N		piece		1
12542	3199	Листья бамбука	193.25	солёные, 100 листов, 1 уп.	2016-06-07 08:48:26.191951	2017-06-07 08:43:37.314134	0	\N		piece		1
13347	3431	Сыр «Голландский»	482.34	45% жирности, 1*5,5	2017-02-21 17:29:20.616838	2017-06-07 08:53:52.703261	0	\N	AoNsbVkWOxEmBICrXLUvJZafiSQqlehcpjFdtDPRyTKMwHzYnu	kilo		1
13349	3431	Мягкий сыр «Лукоз»	1173.67	из козьего молока; «козья моцарелла»	2017-02-21 17:30:59.869857	2017-06-07 08:54:07.992249	0	\N	YrPFQqleDHXNnugIChJBEwoWcmsxKRAMyaSkLtzUbpGZiVTOjf	kilo		1
13337	3429	Фета «Салаксис»	270.47	0,5 кг	2017-02-21 17:23:42.684995	2017-06-07 08:55:22.808731	0	\N	LabeuKsycRzADiSMIkNEjUFdvCQYomlrXZJxPWBgVfwthnHTqG	piece		1
13332	3428	Сыр Фуэте зрелый	1888.98	100% козье молоко, ~ 1 кг	2017-02-21 17:15:58.83131	2017-06-07 08:57:30.456237	0	\N	jOtruXdZGbwhskWSAUTqQLagNeRxoHymFBczCMlKPDVpEifvYJ	piece		1
13161	3351	«Моцарелла» в рассоле	74.28	0,125 г	2017-02-10 18:56:22.494921	2017-06-07 09:12:56.990592	0	\N	BziAQpsUJahSvRykeoljLTVEZCrOfwmDPxIKYFqWtcdgnMGbNH	piece		1
13150	3348	Сыр «Арабеск»	1471.19	100% козье молоко, ~1 кг	2017-02-10 18:44:29.96532	2017-06-07 09:17:02.015388	0	\N	gQiINcfkveCAtMUKnwmJZYuBlzrsXEVbdjPxqypSahWFGLRODo	kilo		1
12625	3222	Рыба ледяная 	1121.83	250+	2016-06-08 10:53:36.707719	2017-06-26 11:10:47.673797	0	foto-247.jpg		kilo		1
13137	3343	Креветки тигровые целые, 4/6	1822	с/м	2017-02-10 15:09:04.685546	2017-07-18 14:29:11.914756	0	\N	tMFWZLSbQemRHNBsxcoTviOIrqyCfnlKkpwXVdzGJguEPhjYAa	kilo	00000000034	1
13144	3345	Креветка северная, 70/90 	660	пивная, без глазировки	2017-02-10 15:24:56.930608	2017-07-17 12:42:29.181638	0	foto-394.jpg	aymwjZVxNAnWpOLTKJcCdtvBXMbQkHfFReuIgirhsElSqGDUYz	kilo	00000000945	1
12631	3222	Камбала 	349.46	потрошёная, без головы; Мурманск, с/м	2016-06-08 10:53:36.859477	2017-06-26 11:13:53.381068	0	foto-211.jpg		kilo		1
12624	3222	Сельдь 	158.70	ОКРФ Олюторская, L1, т/о	2016-06-08 10:53:36.681786	2017-06-26 11:13:53.384906	0	foto-446.jpg		kilo		1
13123	3335	Горбуша 	251.21	потрошёная, Дальний Восток; б/г, с/м	2017-02-10 12:40:28.774503	2017-06-29 14:12:00.352516	0	\N	EiplqduWTKRcgIwGDhSzNtnsFajkOXMrLYvoVUHmBbAefQZPCy	kilo		1
12716	3258	Кольца кальмара в кляре 	100.0	«Nordic Seafood»	2017-01-19 11:28:54.134482	2017-07-17 11:34:17.525973	0	foto-191.jpg	DNsjrHBkedXtQYLimTpvCVZUFuIqyxKobOhGfRlMJAPzWwnaSg	piece	00000000016	1
12637	3223	Филе пангасиуса	182.88		2016-06-08 10:53:37.286195	2017-06-29 14:15:55.320097	0	\N		kilo		1
12739	3222	Скумбрия 	165.81	непотрошёная, с головой, 300-500 г	2017-01-19 16:19:51.224121	2017-06-29 14:17:53.815777	0	\N	JHRDUayixSmOjKXYPWtZTLEbplqzFuBwgCAGohsrnQcMNIdvek	piece		1
12717	3258	Кальмар-мини 	100.0	очищенный, «ARTI Seafood», 61+	2017-01-19 11:35:49.273144	2017-07-17 11:34:17.52296	0	\N	dCnXxHoAugGriSkYwjDpaIJRmZFyevLEqPKcNltVUTBhWOQbMz	kilo	00000001385	1
13149	3347	Печень трески	119	высший сорт, 120 г, ж/б с ключом	2017-02-10 15:50:31.248145	2017-07-17 11:25:56.798427	0	\N	VIsWAZtwqaXPGpSzuyfCUBvbnTglNdYrkJihOFjxcHKeMoEQRm	piece	00000000494	1
13114	3332	Красная смородина	190.56		2017-02-10 12:22:33.07864	2017-06-08 08:33:39.576504	0	\N	eJHdqmxRCpnZIOfNSVwFtsgKGUzWcQYXMakhbulrDLyoEBivAP	kilo		1
13112	3332	Вишня 	219.76	без косточек	2017-02-10 12:22:33.069834	2017-06-08 08:33:39.582873	0	\N	ThzuXYfPeROntZwELNKVQUIsAdDSqpCryBWGMJoabgiHvmkcFx	kilo		1
13113	3332	Клубника	177.86		2017-02-10 12:22:33.075715	2017-06-08 08:33:51.54038	0	foto-496.jpg	eJHdqmxRCpnZIOfNSVwFtsgKGUzWcQYXMakhbulrDLyoEBivAP	kilo		1
13414	3258	Кольца кальмара в панировке	669	ARTI Seafood	2017-07-17 11:42:13.112799	2017-07-17 11:42:13.112799	0	\N	WZtJisQBICKNeUoHdApDmrVayMEYcLqjzbhxukGOfSTlwFRXvP	kilo	00000001849	1
12458	3167	«К картофелю» 	369.35	специи, 850 г, 1 уп.	2016-06-07 08:48:20.778227	2017-07-05 11:29:53.260668	0	\N		piece		1
12456	3167	«Персиллад»	490.05	специи, 760 г, 1 уп.	2016-06-07 08:48:20.752801	2017-07-05 11:29:53.267904	0	\N		piece		1
13412	3258	Кальмар гигантский	215	филе, 2-4 кг, Перу	2017-07-17 11:31:15.431067	2017-07-17 11:51:08.678979	0	\N	zNcpAiugkMKhZntIPCfEDRmaFqVbeXHTSwlrovxWJyYQBUjOdL	kilo	00000002465	1
13407	3476	Имбирь	755	430 г	2017-07-05 11:55:55.895839	2017-07-05 12:06:24.311765	0	\N	OeRBapcjMfIziNPQoxDsrUbXTukYmvWAZgCHLthGFwnqVSEJdl	piece		1
13406	3476	Кардамон	2332	молотый, 520 г	2017-07-05 11:55:38.33313	2017-07-05 12:06:24.314553	0	\N	ZhoQXlAWVzOBKertYcPFEusbqvMpSiTDHaRdmyjkxgUGCLnNfJ	piece		1
13405	3476	Мускатный орех	2330	молотый, 550 г	2017-07-05 11:55:38.325177	2017-07-05 12:06:24.316414	0	\N	ZhoQXlAWVzOBKertYcPFEusbqvMpSiTDHaRdmyjkxgUGCLnNfJ	piece		1
13404	3476	Ванильный стручок	656	20 г	2017-07-05 11:42:37.359155	2017-07-05 12:06:24.318297	0	\N	DbjEkuFOAYohZMKmBxdfwVQgPqzJlStaITUXesWvGyHRrLNCni	piece		1
13403	3476	Анис	555	200 г	2017-07-05 11:42:37.35667	2017-07-05 12:06:24.320345	0	\N	DbjEkuFOAYohZMKmBxdfwVQgPqzJlStaITUXesWvGyHRrLNCni	piece		1
13402	3476	Гвоздика целая	1100	350 г	2017-07-05 11:42:37.35418	2017-07-05 12:06:24.33252	0	\N	DbjEkuFOAYohZMKmBxdfwVQgPqzJlStaITUXesWvGyHRrLNCni	piece		1
13401	3476	Какао Чили	473	290 г	2017-07-05 11:42:37.351534	2017-07-05 12:06:24.334195	0	\N	DbjEkuFOAYohZMKmBxdfwVQgPqzJlStaITUXesWvGyHRrLNCni	piece		1
13400	3476	Тмин	385	470 г	2017-07-05 11:42:37.348936	2017-07-05 12:06:24.336014	0	\N	DbjEkuFOAYohZMKmBxdfwVQgPqzJlStaITUXesWvGyHRrLNCni	piece		1
13399	3476	Корица в палочках	385	300 г	2017-07-05 11:42:37.345833	2017-07-05 12:06:24.337729	0	\N	DbjEkuFOAYohZMKmBxdfwVQgPqzJlStaITUXesWvGyHRrLNCni	piece		1
13398	3476	Корица молотая	413	500 г	2017-07-05 11:39:23.142556	2017-07-05 12:06:24.339558	0	\N	XNAsKSdanHibZPopWtOImEzjBDgGelcMyVLqFvkhTfrRwQYxUu	piece		1
13408	3477	Маринад сухой с травами	584.5	670 г	2017-07-05 12:12:12.442452	2017-07-05 14:05:35.326901	0	\N	ToYpXtHhDKJrOxIFluaSQMZgjkRByVcALnNbGweqidmWzUvECf	piece		1
13410	3477	Маринад сухой «Чили»	584.5	700 г	2017-07-05 14:05:35.330118	2017-07-05 14:05:35.330118	0	\N	qbnOctxDmuKANfjElwoGzeshBVdyvrHpWLikXYQRgMPJSZCUIa	piece		1
13409	3167	«Охотничья» смесь	584.5	450 г	2017-07-05 12:13:55.81326	2017-07-05 14:06:12.880417	0	\N	LHJRQMEPtSfhuNUyxjpaeoZTKWOvVwcmDAzIldCFgYkGiXBsqn	piece		1
13411	3221	Икра лососевая 	904	зернистая, ст/б, 230г	2017-07-17 11:14:27.994434	2017-07-17 11:20:33.125217	0	\N	SkCcueNMrVfyIzwGxvEWKgdQlnOFsAamhUPRqbijtLpToYDBHJ	piece	00000002575	1
12603	3217	Креветка тигровая, 8/12	1255	Без головы, с/м	2016-06-08 10:53:32.979537	2017-07-18 14:22:17.787482	0	foto-367.jpg		kilo	00000000030	1
13413	3213	Каракатица целая	390	очищенная, с/м, 11/20, Тайланд	2017-07-17 11:37:16.181675	2017-07-17 11:37:16.181675	0	\N	BAMQrRiGebsKPCIHjcmYqlFnSpwXukaTydOzWENLUgVoZJvDtx	kilo	00000001720	1
13415	3214	Краб в панцире 	1715	с/м, 70/100, Тайланд	2017-07-17 11:53:42.84398	2017-07-17 11:53:42.84398	0	\N	gLmzIBsOeCTDFkoJPpyVZQqvjNuEcSlRiXHwfYKGWMbAahdtUn	kilo	00000002797	1
13416	3215	Креветка в панировке «бабочка»	1185	21/25	2017-07-17 12:25:10.041614	2017-07-17 12:25:10.041614	0	\N	rCNyEKQduJSiMLhcvfXPFpjZOowgAYTWRlskznxbeHDGUmVBqa	kilo	00000002434	1
13417	3345	Креветка северная  50/60	850	пивная, 0% глазировки, Магадан	2017-07-17 12:42:29.184332	2017-07-17 12:42:29.184332	0	\N	iBFQndlmDHWxUeCcZRtSvYGjPrOITLbgqpsaAkwKVNoyMuXzhf	kilo	00000002406	1
13418	3345	Креветка северная 50/70   	930	пивная, 0% глазировки, Магадан	2017-07-17 12:42:29.187804	2017-07-17 12:42:29.187804	0	\N	iBFQndlmDHWxUeCcZRtSvYGjPrOITLbgqpsaAkwKVNoyMuXzhf	kilo	00000002070	1
13419	3345	Креветка северная 60/80   	838	Гренландия	2017-07-17 12:42:29.189749	2017-07-17 12:42:29.189749	0	\N	iBFQndlmDHWxUeCcZRtSvYGjPrOITLbgqpsaAkwKVNoyMuXzhf	kilo	00000001751	1
13420	3346	Креветки тигровые очищенные 26/30	720	вареные	2017-07-17 13:22:03.553771	2017-07-17 13:22:03.553771	0	\N	tnpeSZAmqEMJVwoauyXThcGNdsRiBHjzxYlLfQgUFPbkIODWvr	kilo	00000000041	1
13421	3346	Креветки тигровые с хвостиком, 16/20	1150	очищенные, сырые 	2017-07-17 13:22:03.557377	2017-07-17 13:22:03.557377	0	\N	tnpeSZAmqEMJVwoauyXThcGNdsRiBHjzxYlLfQgUFPbkIODWvr	kilo	00000002491	1
13422	3346	Креветки тигровые с хвостиком, 21/25	699	очищенные, сырые 	2017-07-17 13:22:03.560525	2017-07-17 13:22:03.560525	0	\N	tnpeSZAmqEMJVwoauyXThcGNdsRiBHjzxYlLfQgUFPbkIODWvr	kilo	00000000597	1
13423	3217	Креветки тигровые, 21/25	650	Без головы, Индия	2017-07-17 13:27:16.747521	2017-07-17 13:27:16.747521	0	\N	GmBRkNKvwCFjMHioOTVgJphPqctsIrAyWSnfYlaxEdzDLXUebu	piece	00000002772	1
13424	3478	Шашлык из морепродуктов	456		2017-07-18 12:57:25.622701	2017-07-18 12:57:25.622701	0	\N	pvlwcanHBZFWsMYxIgCUPziuOXkomtySANrLhEbeqGRTKJVfDj	kilo	00000002051	1
13425	3479	Филе гребешка крупного	2058	с/м, РФ 	2017-07-18 13:04:38.732772	2017-07-18 13:04:38.732772	0	\N	VSQDNJqrUPBcljnGYztCbMvKEFofdpAuyZakWegHXIsORiwhmL	kilo	00000002808	1
13426	3479	Филе гребешка	1960	с/м, РФ 	2017-07-18 13:04:38.736102	2017-07-18 13:04:38.736102	0	\N	LXAcCJpOqWSbQliszvjuaZfePmNHDVodUxKRrEtknTFgYhwMIG	piece	00000000042	1
13427	3479	Филе гребешка 10/20	1396	отбеленное, Китай	2017-07-18 13:04:38.737798	2017-07-18 13:04:38.737798	0	\N	LXAcCJpOqWSbQliszvjuaZfePmNHDVodUxKRrEtknTFgYhwMIG	piece	000000001628	1
13428	3220	Осьминог крупный	640	1-3 кг, Китай	2017-07-18 13:09:54.694019	2017-07-18 13:11:46.536507	0	\N	NwDISOiKHMvmXhWlztVgFEZUfPdaCeyYQjJAoxBcGTbpRqkrsL	kilo	00000002378	1
13429	3280	Мясо улитки	1090	90/100, 0,5 кг, Франция	2017-07-18 13:25:11.435258	2017-07-18 13:25:11.435258	0	\N	OKuHYADXFwMmrfZUlWbctpGqBTLdIPJRNziVSegQCyaojvksnh	piece	00000001867	1
13397	3282	Мидии «Киви» в зелёных раковинах	795	половинки, Новая Зеландия 	2017-06-26 12:53:49.742322	2017-07-18 13:44:27.894682	0	foto-468.jpg	hHxlypCzPviFeXUwaRSZbNWcoLqAJjfmguVdtOKkrYsBDEMnGI	kilo	00000001639	1
13431	3283	Мясо краба, 1-я фаланга	3800	 в вакууме, Мурманск	2017-07-18 13:35:11.772562	2017-07-18 13:35:11.772562	0	\N	KGHqEOpjJmziLNvslVRfWUaMhFbxntuXPYoCeZgyIcdBATDSkQ	kilo	00000002278	1
13430	3283	Мясо Опилио (краб-стригун)	2178	крупная 1-я фаланга, глазурь 20%; Мурманск	2017-07-18 13:35:11.763268	2017-07-18 13:35:46.095052	0	\N	KGHqEOpjJmziLNvslVRfWUaMhFbxntuXPYoCeZgyIcdBATDSkQ	kilo	00000002711	1
13432	3282	Мидии варёно-мороженные 	360	очищенные, 200/300, Чили, весовые	2017-07-18 13:44:27.903066	2017-07-18 13:44:41.201017	0	\N	SXHJUoxCALYVdzgMGIfErakenjpvOtFBPlsKuhiTbcyZqmRWND	kilo	00000002783	1
13433	3217	Креветки тигровые, 16/20, эконом	500	Без головы, Китай	2017-07-18 14:13:32.028879	2017-07-18 14:13:32.028879	0	\N	tMOZAfHLnyVGKXzeRcjUIqQFpruDkYhldwgsCEaWPbTSoBJvmi	kilo	00000002773	1
13434	3217	Креветки тигровые, 16/20, эконом 	500	с/м, б/г, Китай	2017-07-18 14:22:17.793366	2017-07-18 14:22:17.793366	0	\N	hmlBEZLykRYUzetiOboxTWJgnIFdKVaDjcGHSPACMfrvXuwNqp	piece	00000002773	1
13435	3343	Креветки тигровые целые, 6/8	1687	с/м	2017-07-18 14:31:37.49379	2017-07-18 14:31:37.49379	0	\N	fTkltKnGUQZzcjwmEHieJBrpoXYhvCAaSsgIROPbDFVuNWqLxM	kilo	00000000289	1
13436	3343	Креветки тигровые целые, 8/12	1550	с/м	2017-07-18 14:31:37.498486	2017-07-18 14:31:37.498486	0	\N	fTkltKnGUQZzcjwmEHieJBrpoXYhvCAaSsgIROPbDFVuNWqLxM	kilo	00000000035	1
13437	3480	Item1	500		2017-11-01 14:21:34.938209	2017-11-01 14:21:34.938209	0	\N	AjNuZizdWvPSRJUYrmykVqFbhIHMotLgpBQxKCaTlwcDfsnGEO	piece	test_code1	1
13438	3480	Item2	600		2017-11-01 14:21:34.949943	2017-11-01 14:21:34.949943	0	\N	LptaWynBRqQMJgGfsYFimbroUkIvPKzxSdOAXwchjlNVZDHeCE	piece	test_code2	1
13440	3480	Item4	800		2017-11-01 14:21:34.956414	2017-11-01 14:21:34.956414	0	\N	LptaWynBRqQMJgGfsYFimbroUkIvPKzxSdOAXwchjlNVZDHeCE	piece	test_code4	1
13442	3322				2017-11-21 12:52:19.740419	2017-11-21 12:52:19.740419	0	\N	kiqveCBcPFDjSHWEXzxVnArIKwhaJRMTYpZgNotOLfdUsGmbQu	piece		1
13443	3481	Булочки «Валуа»	7.89	45 г, 1 шт	2017-11-21 13:04:24.891771	2017-11-21 13:04:24.891771	0	\N	IqapNQSrnvZFwARtTEjoKgkJeibULsHBYhfXVMWxuOdGPCzDyl	piece	0000000000	1
\.


--
-- Name: card_items_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.card_items_id_seq', 13443, true);


--
-- Data for Name: cards; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.cards (id, category_id, created_at, updated_at, image, title, card_logo, temp_key, priority, image_style, size_cd) FROM stdin;
3329	1066	2017-02-09 14:24:36.443342	2017-03-02 15:14:11.5682	Croissant.jpg	Круассаны	\N		2	f	0
3298	1063	2017-02-02 08:25:49.307454	2017-02-17 13:15:31.017834	001.083.jpg		f9f2059ca9cf15189f02a9082b1dd9c1.png	ATerrZsqfmlJNeFDneqjvvAYejlYOarhNPkhgFgVCpzNIukzSo	10	f	0
3295	1063	2017-02-01 12:57:22.124951	2017-02-17 13:15:30.986656	Stomaches.jpg	Субпродукты	\N	qcuLTlZXnKwadazWXXiyrUKVFjrqfttxZGXjDrlARZpqsETAaD	6	f	0
3109	1065	2016-06-07 08:47:59.977554	2017-06-01 12:20:46.218079	Трюфель1_400x400.jpg	Торты	Seleznev.jpeg		0	f	0
3316	1062	2017-02-08 14:05:51.816539	2017-02-17 16:22:08.235549	foto-426.jpg		\N	dMZfeVDnAycrvROITGKxDsBqLnUjDRMklDaioTNavkoPxAxjJH	6	f	0
3093	1064	2016-06-07 08:47:45.541921	2017-06-01 10:44:01.618947	Broccoli_1.png	Овощи весовые свежезамороженные	0281.jpg		5	f	0
3113	1066	2016-06-07 08:48:03.094582	2017-03-02 15:14:39.881868	Baguette.jpg	Хлеб	\N		7	f	0
3112	1066	2016-06-07 08:48:02.166591	2017-03-02 15:14:39.898466	Pecan.jpg	Сладкая выпечка	\N		9	f	0
3315	1062	2017-02-08 14:02:41.127329	2017-02-17 16:26:36.844798	\N		\N	AVpChzODnxCyyGkuUcUGWOiBDuGliVODajEjEcgvDXQKpYWmZc	18	f	0
3318	1062	2017-02-08 14:30:36.790936	2017-02-17 16:27:31.794591	\N	Сосиски для гриля	\N	DVruPpAEjLbojjKVoWTTfnZEawYDbhtAYPZTSFmyzGhussWOtC	23	f	0
3321	1062	2017-02-08 14:45:20.809438	2017-02-17 16:27:09.823192	\N	Колбаски для гриля замороженные	logo_copy_2.png	fcFsUfOqlkORxoQcygZWPjszVjblHsvhqKGmPustaeLKuTsHUV	24	f	0
3124	1068	2016-06-07 08:48:10.573152	2017-02-21 08:22:25.599321	\N	Кофе в зёрнах	286289013_w640_h2048_compagina_dellarabica_logo.png		15	f	0
3063	1062	2016-06-07 08:47:27.284347	2017-02-17 16:26:39.420307	\N	Телятина	\N		16	f	0
3043	1062	2016-06-07 08:47:07.684103	2017-02-17 16:27:16.978415	foto-337.jpg	Баранина	\N		19	f	0
3082	1063	2016-06-07 08:47:34.179302	2017-02-17 13:15:30.995201	Duck.jpg	Утка	logo.png		7	f	0
3288	1062	2017-02-01 10:52:50.368676	2017-02-17 16:22:08.248915	стриплойн-тонкий-1.jpg	Говядина, тонкий край	мираторг_1_article.jpg	ebCWpbTiWDZIiDDemoVIvXTMlgGVXeYZSxvlhKAljzqxXBuQJg	4	f	0
3313	1062	2017-02-08 13:35:16.180686	2017-02-21 14:13:29.346412	foto-149.jpg		logo-75.png	NdgLRHJUMMeHLQAxEExuCJnCQDXVFNrszIqTZRAmSvKEBlVYxu	26	f	0
3304	1068	2017-02-05 18:59:12.660625	2017-02-22 07:58:53.642395	\N	Орехи	\N	kCeXmWXQYvEBJmyAoTHeMFlqAjBciXzcoaZYJEzNmEzwAlYVZf	0	f	0
3126	1068	2016-06-07 08:48:10.901763	2017-02-05 11:59:42.318593	\N	Соль	\N		13	f	0
3299	1068	2017-02-05 13:20:49.947434	2017-02-21 08:23:03.264925	\N	Кофе в капсулах	286289013_w640_h2048_compagina_dellarabica_logo.png	ZBSgoBnWOccuMhblEHIBoStdJpraPSRiMDiwCbzpgDGiyocqiy	0	f	0
3125	1068	2016-06-07 08:48:10.726069	2017-02-21 08:22:37.470023	\N	Кофе молотый	286289013_w640_h2048_compagina_dellarabica_logo.png		14	f	0
3061	1062	2016-06-07 08:47:26.928476	2017-02-17 16:23:05.468268	foto-307.jpg		Primebeef_Logo_1.png		9	f	0
3081	1063	2016-06-07 08:47:33.407304	2017-02-17 13:15:30.978011	Frozen_chick.jpg	Тушки	\N		5	f	0
3083	1063	2016-06-07 08:47:34.363454	2017-02-17 13:15:31.0031	Perepela.jpg	Перепёлка	\N		8	f	0
3065	1062	2016-06-07 08:47:27.640597	2017-02-17 16:27:19.010108	Сырокопч-2.jpg	Мясные деликатесы	\N		21	f	0
3064	1062	2016-06-07 08:47:27.438767	2017-02-17 16:26:39.412908	foto-363.jpg	Свинина	мираторг_1_article.jpg		17	f	0
3114	1066	2016-06-07 08:48:03.360208	2017-03-02 15:14:22.93545	Talosto_red.jpg	Тесто слоёное	Talosto.png		4	f	0
3310	1073	2017-02-06 10:38:48.656154	2017-02-23 04:29:57.077207	\N		Aroy-d-Logo.png	jJKtPSjbCXdGbOrjvRLrQqwYmWHbkKePBVBOCMYoKZMgBjkgvs	15	f	0
3044	1062	2016-06-07 08:47:07.851429	2017-02-17 16:27:19.003726	\N	Ягнятина	\N		20	f	0
3314	1062	2017-02-08 13:50:06.057968	2017-02-17 16:26:02.80127	\N		logo_copy.png	wXBxffLUmooAqkOaphbxNvtWVVQUZkAWJFWgKGgCKbcVnSsvHD	15	f	0
3317	1062	2017-02-08 14:30:34.446628	2017-02-17 16:27:09.82838	\N	Сосиски для гриля замороженные	logo_copy_2.png	DVruPpAEjLbojjKVoWTTfnZEawYDbhtAYPZTSFmyzGhussWOtC	25	f	0
3324	1062	2017-02-08 16:35:48.059735	2017-06-01 08:07:49.541722	foto-420.jpg	Стейк из тонкой диафрагмы	\N	YMfYTSSEFtaweAhWRTclcgjYTKPofIGiYgfoNqvADNyTlMaUtM	5	f	0
3311	1069	2017-02-06 11:16:43.093527	2017-02-21 17:50:58.093212	\N		пышка.jpg	hAHegMgppIJMrNOOuNVGWGzsZjzIqiZMStxkLTktLMqXyROgdQ	9	f	0
3062	1062	2016-06-07 08:47:27.103889	2017-02-17 16:23:11.664425	\N	Говядина	\N		10	f	0
3306	1073	2017-02-05 19:39:34.247026	2017-02-23 04:30:31.836227	\N		Aroy-d-Logo.png	zZrprMxDnQCLNZTxBGStNLLBtGLfiVjnKbiZZOLEaIVyPdDtCM	18	f	0
3183	1073	2016-06-07 08:48:23.436383	2017-02-22 11:59:14.117933	\N	Оливки	\N		26	f	0
3312	1069	2017-02-06 11:27:02.881915	2017-02-21 17:50:34.307625	\N		image001.png	GFbEtsWfNmTmsTkAvHRhkJZKLWkGrjuEtAWKvDwAwPfgtSHHPW	8	f	0
3305	1073	2017-02-05 19:38:10.613105	2017-02-23 04:30:46.009637	\N		\N	YUzfhOIUKwhUOXUFIQlqgRkRcuYrTLmcKbAPfGyEAsdLQoWDjC	19	f	0
3123	1068	2016-06-07 08:48:10.393754	2017-02-22 07:07:23.427304	\N	Сахар	logo_mistral.png		16	f	0
3303	1068	2017-02-05 14:40:17.583521	2017-02-22 07:58:32.378112	\N		\N	GUPIHTwFSkZrEdPxMKyLqeOqBKmREkWmuwjsNGOsNfQvwIrWty	0	f	0
3301	1068	2017-02-05 13:27:45.483279	2017-02-22 07:58:13.43231	\N		\N	FtBBmIYPCVpIDxIxmSFHlBdbHZIiERSDTyguIKweaqBRFcPgon	0	f	0
3307	1073	2017-02-05 19:41:38.081539	2017-02-23 04:30:13.74201	\N		logo-parmalat.png	WfpYUTcPcNFIrPuPiukePVBExqgAKvaqznYJblLSpgDBXhgLQq	17	f	0
3186	1073	2016-06-07 08:48:23.926344	2017-02-22 11:59:14.103021	\N		LORADOaktuell-1.jpg		23	f	0
3141	1069	2016-06-07 08:48:13.709264	2017-02-21 16:50:03.342895	\N	Сливки	белый_город.png		11	f	0
3185	1073	2016-06-07 08:48:23.729554	2017-02-22 11:59:14.108296	\N	Томаты	И.П.О.С.Е.А.png		24	f	0
3320	1062	2017-02-08 14:40:16.939401	2017-06-05 07:19:33.02859	foto-433.jpg	Колбаски-гриль свиные	catalog7.jpg	TUuXPtXKxuqzFwRhNbtwToiNlFWJbxfaiBmPkQMRNFRjkPSJng	28	t	0
3474	1071	2017-03-09 14:45:13.513383	2017-06-05 11:08:16.728472	Screenshot_2017-05-02_21.08.00.png	Ягодные сиропы	\N		1	f	1
3094	1064	2016-06-07 08:47:45.757808	2017-06-08 08:19:26.953356	foto-487.jpg	Ягоды лесные свежезамороженные	0281.jpg		3	f	0
3475	1071	2017-03-09 14:46:49.899768	2017-06-05 11:08:44.729924	\N	Фруктовые сиропы	\N		0	f	0
3280	1076	2017-01-30 16:03:23.397185	2017-07-18 14:38:46.401763	\N	Улитки	\N	jKaDefCMxJAvsEheyMOICRbuzCDNqpDdlLsfgUPOefCzxYTMJV	21	f	0
3336	1076	2017-02-10 12:57:18.320469	2017-07-18 14:36:18.677096	foto-219.jpg	Филе сёмги	\N	mDPWbCZiceFUfnWkrgWRInVERSzFHcYcGrEcWWGtYfNXPAczrS	2	f	0
3086	1064	2016-06-07 08:47:37.007472	2017-06-08 08:12:07.597511	foto-375.jpg	Картофельные полуфабрикаты	McCain_logo.svg.png		1	f	0
3332	1064	2017-02-10 12:22:33.061645	2017-06-08 08:20:00.239313	foto-496.jpg	Ягоды садовые свежезамороженные	0281.jpg	VWxMkaUCxpEDxfqlRMXKgdujDRJANbkBKUQAmfgLyGUzeXcSXz	4	f	0
3096	1064	2016-06-07 08:47:46.141004	2017-06-08 08:27:31.67232	Mushrooms.png	Грибы свежезамороженные	0281.jpg		7	f	0
3309	1073	2017-02-06 09:54:05.312067	2017-06-09 12:07:19.154945	\N		LORADOaktuell-1.jpg	eMWqHqOGytVMejcMcoVcqovGgrpYFkncQYrAkpWVFQpagfmPCg	16	f	0
3337	1076	2017-02-10 13:01:18.909469	2017-07-18 14:36:28.280486	foto-199.jpg	Филе тунца	\N	anuSKVKUfMFFAKUtLOxaCNZVqsCWMRpInRfFFuhYATowGYaBwu	4	f	0
3331	1066	2017-02-09 15:30:26.618215	2017-03-02 15:14:39.890048	\N	Чиабатта	\N		8	f	0
3200	1075	2016-06-07 08:48:26.667417	2016-06-07 08:48:27.814848	kalm9.jpg	Кальмар	catalog9.jpg	\N	0	f	0
3201	1075	2016-06-07 08:48:27.937315	2016-06-07 08:48:28.542884	karakat2.jpg	Каракатица	\N	\N	0	f	0
3202	1075	2016-06-07 08:48:28.634446	2016-06-07 08:48:29.773906	krab1.jpg	Краб	catalog7.jpg	\N	0	f	0
3203	1075	2016-06-07 08:48:29.880299	2016-06-07 08:48:30.925125	krev14.jpg	Креветки	catalog10.jpg	\N	0	f	0
3184	1073	2016-06-07 08:48:23.604316	2017-02-22 11:59:14.113027	\N		\N		25	f	0
3142	1069	2016-06-07 08:48:13.903356	2017-02-21 16:54:59.410491	\N		image001.png		10	f	0
3176	1073	2016-06-07 08:48:22.424452	2017-02-22 11:59:14.144188	\N		LORADOaktuell-1.jpg		31	f	0
3129	1068	2016-06-07 08:48:11.320508	2017-02-11 12:32:56.548238	\N	Кунжутное масло	\N		10	f	0
3131	1068	2016-06-07 08:48:11.708235	2017-02-11 12:30:38.203687	\N	Подсолнечное масло	\N		8	f	0
3128	1068	2016-06-07 08:48:11.173617	2017-02-11 12:34:27.509115	\N		\N		11	f	0
3119	1067	2016-06-07 08:48:08.434889	2017-02-28 18:46:50.121885	\N	Сухие смеси для мягкого мороженого	McCain_logo.png	\N	12	f	0
3182	1073	2016-06-07 08:48:23.26265	2017-02-22 11:59:14.12303	\N	Маслины	лорадо.jpg		27	f	0
3175	1073	2016-06-07 08:48:22.243388	2017-02-22 11:59:14.149096	\N	Грибы	\N		32	f	0
3180	1073	2016-06-07 08:48:22.993966	2017-02-22 11:59:14.128281	\N		лорадо.jpg		28	f	0
3130	1068	2016-06-07 08:48:11.473705	2017-02-22 06:46:55.192468	\N	Оливковое масло	EXTRA_VERGIN.jpg		9	f	0
3174	1073	2016-06-07 08:48:22.112568	2017-02-22 11:59:14.154018	\N		logo-bonduelle.png		33	f	0
3178	1073	2016-06-07 08:48:22.737864	2017-02-22 11:59:14.133697	\N		LORADOaktuell-1.jpg		29	f	0
3177	1073	2016-06-07 08:48:22.573921	2017-02-22 11:59:14.138914	\N	Каперсы	И.П.О.С.Е.А.png		30	f	0
3173	1073	2016-06-07 08:48:21.972069	2017-02-22 11:59:14.158988	\N		И.П.О.С.Е.А.png		34	f	0
3140	1069	2016-06-07 08:48:13.550026	2017-02-21 16:43:08.297185	\N		логотип-Чудское-озеро.jpg		12	f	0
3139	1069	2016-06-07 08:48:13.363335	2017-02-21 16:43:08.304385	\N	Масло	logo__1_.png		13	f	0
3172	1073	2016-06-07 08:48:21.808111	2017-02-22 11:59:14.163887	\N		\N		35	f	0
3171	1073	2016-06-07 08:48:21.648095	2017-02-22 11:59:14.168825	\N	Ананасы	LORADOaktuell-1.jpg		36	f	0
3120	1067	2016-06-07 08:48:09.370803	2017-02-28 18:46:50.114752	\N	Сухие смеси для десертов	\N	\N	11	f	0
3118	1067	2016-06-07 08:48:08.286086	2017-02-28 18:46:50.128348	\N		\N	\N	13	f	0
3127	1068	2016-06-07 08:48:11.050327	2017-02-22 07:11:00.719177	\N		\N		12	f	0
3189	1073	2016-06-07 08:48:24.351221	2017-02-22 11:59:14.087943	\N		\N		20	f	0
3188	1073	2016-06-07 08:48:24.221941	2017-02-22 11:59:14.092945	\N		logo-heinz.jpg		21	f	0
3393	1070	2017-02-21 11:51:07.153292	2017-02-23 04:36:14.914926	\N	Соус дип-пот	Kraft-Heinz-Company-Logo-Design-3.png	WJClkmZtRbbVsZvyBTjGZSBGmPzuzKhQsrKWIsfigYHNyysUkL	27	f	0
3136	1068	2016-06-07 08:48:12.466753	2017-02-21 13:32:16.041696	\N	Паста	38747.png		3	f	0
3204	1075	2016-06-07 08:48:31.043328	2016-06-07 08:48:31.664464	lobst5.jpg	Лобстер	\N	\N	0	f	0
3205	1075	2016-06-07 08:48:31.748671	2016-06-07 08:48:31.761465	\N	Мидии	\N	\N	0	f	0
3206	1075	2016-06-07 08:48:31.85428	2016-06-07 08:48:32.865148	catalog1.jpg	Кальмар	catalog9.jpg	\N	0	f	0
3207	1075	2016-06-07 08:48:33.004763	2016-06-07 08:48:33.570795	catalog4.jpg	Кальмар	\N	\N	0	f	0
3208	1075	2016-06-07 08:48:33.667642	2016-06-07 08:48:34.727501	catalog2.jpg	Краб	catalog7.jpg	\N	0	f	0
3209	1075	2016-06-07 08:48:34.859211	2016-06-07 08:48:35.857685	catalog6.jpg	Креветки	catalog10.jpg	\N	0	f	0
3210	1075	2016-06-07 08:48:35.99079	2016-06-07 08:48:36.543451	catalog8.jpg	Лобстер	\N	\N	0	f	0
3211	1075	2016-06-07 08:48:36.62878	2016-06-07 08:48:36.641728	\N	Мидии	\N	\N	0	f	0
3290	1062	2017-02-01 11:09:52.361877	2017-02-17 16:25:51.144787	\N		мираторг_1_article.jpg	EvnXLojLkFmZQBSGGTDqmXvxyoqYzaQxRUDNKrNTHdxzhwRoMx	14	f	0
3121	1067	2016-06-07 08:48:09.623825	2017-06-02 10:42:10.913855	\N	Ягодные пюре	\N		10	f	1
3161	1071	2016-06-07 08:48:17.939176	2017-06-05 11:45:53.395914	\N	Десертные наполнители "Miller&Miller"	Miller_logo.png		4	f	0
3122	1071	2016-06-07 08:48:09.994456	2017-06-05 11:23:03.470329	\N	Ягодные соусы «Буаро»	boiron-spec.jpg		2	f	0
3160	1071	2016-06-07 08:48:17.694058	2017-06-05 11:48:56.988395	\N	Барные сиропы "Miller&Miller"	Miller_logo.png		6	f	1
3187	1076	2016-06-07 08:48:24.057672	2017-07-18 14:38:21.29784	\N	Тунец консервированный	\N		25	f	0
3163	1071	2016-06-07 08:48:18.944593	2017-06-05 11:57:02.480365	\N	Топинги "Dolce Rosa"	DOLCE_ROSA_logo-210x210-1.jpg		8	f	1
3199	1074	2016-06-07 08:48:26.149537	2017-06-02 10:44:03.500558	\N	Разное	\N		0	f	0
3166	1072	2016-06-07 08:48:20.402527	2017-07-05 14:04:06.730382	\N	Перец Santa Maria	logo.png		4	f	1
3282	1076	2017-02-01 07:24:10.501832	2017-07-18 14:38:25.174464	foto-468.jpg	Мидии	\N	VUsNHalBOFswqwFsWDFfsvyGcATuUXTqSVdoMPCLCAFxkftiMH	19	f	0
3168	1072	2016-06-07 08:48:20.964556	2017-07-05 14:25:23.651417	\N	Паста	\N		2	f	0
3164	1071	2016-06-07 08:48:19.209591	2017-06-05 11:23:31.579415	\N	Сиропы Monin	logo-gourmet-panache-full-coloured-pantone.jpg		3	f	1
3169	1072	2016-06-07 08:48:21.154798	2017-07-05 11:24:38.811233	\N	Masterfoods	5c73c9_b5a34263757849b2acccdf45efb5cd0b-mv2.jpg		1	f	0
3465	1065	2017-02-22 17:39:29.251964	2017-06-07 08:05:15.402128	Макарони_ассорти_мини.jpg	Макарони	300-300р.jpg		2	f	0
3466	1065	2017-02-22 17:42:39.313123	2017-06-07 08:06:40.140682	Шоколадный.jpg	Шоколадные торты	300-300р.jpg		1	f	0
3272	1065	2017-01-29 10:08:04.620498	2017-06-07 08:10:39.054399	\N	Чизкейки «New York»	\N	WfMcvoxAwXtjMDftISYThBsoLJRUJjCjEUOMwzlrVZnsMpoktw	4	f	0
3273	1065	2017-01-29 10:23:13.356527	2017-06-07 08:11:33.615195	\N	Торты	ac7f4eb5df90dbbf69c055fe714c92e6.jpg	JeytSmQyjdEBCCNLTOQwePZRqHhXMCzDUSkyCuCYlUBifFQqTc	5	f	0
3198	1074	2016-06-07 08:48:25.96913	2017-06-07 08:44:39.237031	\N	Лапша	\N		0	f	0
3197	1074	2016-06-07 08:48:25.807098	2017-06-07 08:45:20.241058	\N	Паста соевая	\N		0	f	0
3196	1074	2016-06-07 08:48:25.649042	2017-06-07 08:46:10.529107	\N	Кунжут	\N		0	f	0
3195	1074	2016-06-07 08:48:25.4883	2017-06-07 08:47:23.295788	\N	Имбирь	\N		0	f	0
3194	1074	2016-06-07 08:48:25.303918	2017-06-07 08:48:40.539169	\N	Икра масаго	\N		0	f	0
3193	1074	2016-06-07 08:48:25.134324	2017-06-07 08:49:29.426979	\N	Икра летучей рыбы Тобико	\N		0	f	0
3192	1074	2016-06-07 08:48:24.967831	2017-06-07 08:49:56.341238	\N	Грибы	\N		0	f	0
3191	1074	2016-06-07 08:48:24.800137	2017-06-07 08:50:27.385044	\N	Водоросли	\N		0	f	0
3190	1074	2016-06-07 08:48:24.653617	2017-06-07 08:50:47.288715	\N	Васаби	\N		0	f	0
3297	1064	2017-02-01 14:04:18.300068	2017-06-08 08:12:39.650034	foto-397.jpg	Картофельные дольки	McCain_logo.svg.png	bPGtdJWUTCVtSYsIIoeVwZYlvOmQVhAZtlXKqznWpdIDzUmxGO	2	f	0
3283	1076	2017-02-01 07:34:40.743855	2017-07-18 14:38:30.262956	foto-439.jpg	Мясо краба	\N	ORmglhlyKLeZeHdYeSKSxnXsfzkqnsETtfRimzwRYmtnZfUYpe	13	f	1
3165	1072	2016-06-07 08:48:19.960069	2017-07-05 12:12:25.886584	\N	Специи Santa Maria	logo.png		7	f	1
3167	1072	2016-06-07 08:48:20.713324	2017-07-05 11:39:30.625283	\N	Приправы Santa Maria	logo.png		3	f	1
3170	1072	2016-06-07 08:48:21.317306	2017-07-05 14:27:11.74099	\N	Свежие специи в масле, Primerda	\N		0	f	0
3285	1062	2017-02-01 08:03:22.098894	2017-02-17 16:22:36.453716	foto-280.jpg	Вырезка говяжья	мираторг_1_article.jpg	wfnSRWFlVrkoxMgLRuREtZQMeTFGrtjUReMyJordpglfkFubsZ	8	f	0
3277	1067	2017-01-29 12:05:05.650871	2017-03-02 13:42:52.593988	\N	Мороженое «Movenpick»	movenpick-logo.jpg	KczcRenBnXxaEHamRaeRfZWFstSJGjopbXsUhYscHkcoABBrqo	1	f	0
3254	1063	2017-01-13 08:13:22.528221	2017-02-17 13:15:34.325827	chicken_wings.jpg	Цыплята разделанные	\N	NSEwQYpwwEtQMxJJJehbaehKGFVrjNHzijaeptZbcOgNPUiSBl	1	f	0
3300	1068	2017-02-05 13:25:57.470384	2017-02-20 13:34:02.173929	\N	Чай 	1477-909959-footer-logo.png	MWlkVLNmcjvbrqhiqjDEhPeBuWNBHbDePvLlKIQOJnUqzawKZj	0	f	0
3255	1063	2017-01-13 08:54:59.676714	2017-02-17 13:15:31.01086	Eggs.jpg	Яйцо	\N	zvAgumsAreNOzXYcRyKaaslrVNoVPeiBzUTMVIeUVKuFJBPJyE	9	f	0
3293	1062	2017-02-01 11:13:02.689996	2017-02-17 16:22:19.213495	Roastbeef.jpg	Ростбиф	мираторг_1_article.jpg	IJQuPRJINKFCOPFifKUOxkbOctsXSLDOPFORlKZFMrJMaaUQhh	7	f	0
3286	1062	2017-02-01 08:07:29.935706	2017-02-17 16:21:59.823058	Блейд.jpg	Говядина зернового откорма	мираторг_1_article.jpg	vKFUmxXqcfgrQSJPAHwICqNqtrHPUZqbxpIWQqkvSsMCjoBkFF	2	f	0
3276	1067	2017-01-29 11:43:21.514415	2017-02-28 18:46:50.062172	\N	Смеси для мороженого, рожки	\N	pNeWTGVaucEKFBdCDqcQINbYDfDiVxpmhtolnFCPioOXissCpi	3	f	0
3292	1062	2017-02-01 11:12:17.125932	2017-02-17 16:25:45.603069	\N		мираторг_1_article.jpg	BDyVWjEvzZGJTZqjyogtLqZdxYKJthnpyfuVxPDWCcNrEboWqi	12	f	0
3289	1062	2017-02-01 10:54:11.371036	2017-02-17 16:25:48.528087	\N		мираторг_1_article.jpg	eYnmHUiaCuzUsftzxuVhEzyIncZGxVkeleIVnKdtLOqPEeWFnJ	13	f	0
3291	1062	2017-02-01 11:10:59.217709	2017-02-17 16:23:20.259155	\N		мираторг_1_article.jpg	wIzKmPaVckIseJzMqbQPEGcysbbSvzHMeIEYLWmhLAKAiTaKWF	11	f	0
3265	1062	2017-01-25 20:01:21.823705	2017-02-17 16:27:31.801452	Зайчик.jpg	Кролик	313226.jpg	uYuYAjRhvTPkkQEMjZaRzWjSSTswihnpBIgldrYPuQMUraWNjT	22	f	0
3287	1062	2017-02-01 10:45:31.321615	2017-02-17 16:22:08.24216	\N	Говядина, толстый край	мираторг_1_article.jpg	TqOcLgGMSKpWbsHlGxMIZGpSDgqxtNFgKBGqLJoiRWkFIwvaHb	3	f	0
3263	1062	2017-01-25 19:51:29.353817	2017-02-17 16:27:36.589597	\N	Колбаски для жарки и гриля, замороженные 	logo_copy_2.png	XqIAZfGiLXRZPLhGAmJSqthJvCdsbwlNbdDONkmPElIFNreUep	29	f	0
3137	1068	2016-06-07 08:48:12.817969	2017-02-22 06:48:13.432839	\N	Рис	imgres.png		2	f	0
3334	1064	2017-02-10 12:35:32.209691	2017-02-10 12:36:06.897653	\N	Кукуруза	\N	RdwkiyZgMdJNcVdfuiLRryxfmHIrhUcuAymUGJLxOAvOYbADIn	8	f	0
3340	1066	2017-02-10 13:26:31.009256	2017-03-02 15:14:29.981565	\N		7f76a2d1ac991697f8e41d84b7c1e317.jpeg	QrvHwcYzYaBnmBmrTYDmkNGGYKDtAHNyCcdACycAhegMsMtXjZ	5	f	0
3330	1066	2017-02-09 14:28:13.984353	2017-03-02 15:14:39.907253	Hotdog_buns.jpg	Булочки для бургеров	\N		6	f	0
3341	1066	2017-02-10 13:28:03.61461	2017-03-02 15:14:15.816695	Springroll.jpg		\N	OsPdYANpGuoXMUxhynGQxCgwNYsdPFeQIdvtYgnECUmmokIqJU	3	f	0
3294	1063	2017-02-01 12:43:29.165598	2017-02-10 17:17:15.833845	Filet_woskin.jpg	Филе куриное	\N	ebCsbsMFdyHdyfkDedZCGRuLAeQhaRvmmgDLCbjyHCCkeiggdi	0	f	0
3362	1068	2017-02-11 13:05:45.087223	2017-02-23 04:12:39.150778	\N		logo_mistral.png	aAbWsFkNqOBDlqkiJvwAjyhmhcMpwoGgbMEKYywSQTQDVJLRiH	0	f	0
3367	1070	2017-02-11 19:26:30.309711	2017-02-23 04:40:34.541444	\N	Горчица	Kraft-Heinz-Company-Logo-Design-3.png	FUzAltfNGTduPkZLEwQLxlTidiUQWsryXtwupOuVRtHyFHwppU	44	f	0
3342	1066	2017-02-10 13:36:13.661403	2017-03-02 15:14:11.538794	Waffels.jpg		McCain_logo.png	FmcSJweIfBBfBZMSwtxJYRGZKHSORhAAXzpLWxXbUfftCXyyLt	0	f	0
3339	1066	2017-02-10 13:13:50.150824	2017-03-02 15:14:11.547542	Buns.jpg	Булочки	logo.png		1	f	0
3363	1068	2017-02-11 13:08:00.136075	2017-02-23 04:11:57.793743	\N		b3668ee63809b7beb08a40d1254c759d.jpg	oRDLOKyCTsvsjLMYHqNkLvnGtqMjcKmJLHhKjYHGXPdZKSuCQs	0	f	0
3335	1076	2017-02-10 12:40:28.762498	2017-07-18 14:36:11.617553	foto-448.jpg	Красная рыба	\N	UMSqdhjWfjzZeFRmzsuMurbWCJbiAGAqqOUywkJhLalKBPWAdV	1	f	0
3262	1062	2017-01-25 18:07:13.996933	2018-02-05 10:50:40.926602	foto-421.jpg	Мраморная говядина	Primebeef_Logo_1.png	CZOVCkBnLEfFihalGfxTTwozlfyRyxxfxgWVLrVSBvtCTmQAXD	1	f	1
3258	1076	2017-01-19 11:28:54.116439	2017-07-18 14:38:46.409325	foto-476.jpg	Кальмар	\N	KgVrYeoodpjGSdhmxLHiavQdqmNPtYJmAiadnqrhkSaqjrneYY	20	f	1
3274	1067	2017-01-29 10:50:07.419724	2017-06-02 10:42:01.926807	\N	Ягодные пюре 	\N	aRISSVuxMpuUFtHtbYkjAVbGSEZuTIYKKmqQNPCUkaOVtdAJmf	7	f	1
3338	1076	2017-02-10 13:06:12.890234	2017-07-18 14:36:28.288282	\N	Филе трески	\N	aBlEmJrgBWpplFyoMCLzteCZekdqVtXbfqTvJnxuBaljAUJYOh	5	f	0
3264	1062	2017-01-25 19:58:01.24175	2017-06-05 07:19:46.444282	Ветчинка.jpg		logo-75.png	OlUVkeEzQuCdTrjyQBgcXXoIfxNzWzmYVAKupFOJtLjBudoZXX	30	t	0
3214	1076	2016-06-08 10:53:30.756637	2017-07-18 14:38:21.292269	foto-444.jpg	Краб	\N		24	f	0
3343	1076	2017-02-10 15:09:04.675668	2017-07-18 14:38:07.022949	foto-204.jpg	Креветки тигровые целые	\N	jNRgWqPVtJhxVMvbdXFnMIfLccIQxXhWujajBSNADyRsEKaXRY	7	f	0
3217	1076	2016-06-08 10:53:32.912998	2017-07-18 14:38:07.048587	foto-367.jpg	Креветки тигровые без головы	\N		10	f	1
3221	1076	2016-06-08 10:53:35.701649	2017-07-18 14:38:21.281194	foto-158.jpg	Икра	\N		22	f	0
3284	1076	2017-02-01 07:35:55.446391	2017-07-18 14:38:28.063047	\N		\N	LYpLQXtjMgmSXWduDICuPrqfbvVVvgMgcSIQWuQrkGUwsAoIhm	16	f	0
3215	1076	2016-06-08 10:53:31.015644	2017-07-18 14:38:07.041308	foto-236.jpg	Креветка	\N		9	f	0
3345	1076	2017-02-10 15:22:12.63259	2017-07-18 14:36:30.898332	foto-395.jpg	Креветки без глазировки	\N	HtNotdtvPjMGEkbSdgKZVEWbJfdKEbuLXLnpgoOUxaADisCMui	6	f	1
3467	1065	2017-02-23 06:58:38.461829	2017-06-07 08:13:12.927131	\N	 	\N		7	f	0
3348	1069	2017-02-10 18:44:29.957161	2017-06-07 09:16:45.703917	\N	Сыр «Арабеск»	a_a7d46763.jpg	JAqEGfeVPahWHqmyOsXNeZHuzuEUQcIHXqGHdhXXIwjwxrzzPp	7	f	0
3333	1064	2017-02-10 12:31:59.929317	2017-06-08 08:14:02.40708	foto-371.jpg	Картофель молодой 	McCain_logo.svg.png	lwycGgldBtsiVTPuKjSNDRcUJUwwnFalNZgaeiTvNVMuKQryQd	6	f	0
3213	1076	2016-06-08 10:53:30.516831	2017-07-18 14:38:21.303458	foto-217.jpg	Каракатица	\N		26	f	0
3223	1076	2016-06-08 10:53:37.174843	2017-07-18 14:36:03.050772	foto-241.jpg	Рыба, филе 	\N		0	f	1
3220	1076	2016-06-08 10:53:34.729167	2017-07-18 14:38:21.286998	foto-226.jpg	Осьминог	\N		23	f	0
3222	1076	2016-06-08 10:53:36.008678	2017-07-18 14:36:18.669535	foto-3.jpg	Рыба	\N		3	f	1
3346	1076	2017-02-10 15:29:35.708814	2017-07-18 14:38:07.032845	foto-178.jpg	Креветки тигровые очищенные	\N	WCDujxnbstRGDhNesBZCcWeGHDPAqnynlQfAtYEDqmxVktaIvC	8	f	0
3344	1076	2017-02-10 15:16:08.969379	2017-07-18 14:38:32.861761	\N	Креветки в блоке льда	\N	EdzzAOIkgtpGSDbyTmqOojjDBATVcdSMKZfdpAzcYBQAsoJlIj	11	f	0
3260	1076	2017-01-24 16:19:27.576819	2017-07-18 14:38:28.054998	foto-378.jpg	Морской коктейль	nordic65.png	ApdOnhOsKyYVpZdXRcWbmULXNosXUVNXFzqXZKZydfYpdypANG	15	f	0
3347	1076	2017-02-10 15:50:31.244748	2017-07-18 14:38:25.168402	\N		\N	osOspCIuBYayoQmthiznxwSqdxtokvWQskaWUmgmmMkGjTydyH	18	f	0
3375	1063	2017-02-17 12:37:39.485204	2017-02-17 13:15:34.316347	\N		logo.jpg	PGNISfgofMxphsxlUwdocCoYoaiXeVBwOzIdgkeNAThRgOHsMi	3	f	0
3374	1063	2017-02-17 12:32:11.77662	2017-02-17 13:15:34.321308	\N		logo.jpg	nmyLZOaDdMmEiiQhpjSvCfewNWFYSaJLjXDUlhVkDogRVIijxN	4	f	0
3369	1070	2017-02-11 19:30:48.629488	2017-02-23 04:41:23.595597	\N		aroy-d.jpg	OkCosAnnWMKbGcBTEsOzGKanIIeJZlFSkYgkWViPzFmriIHeUO	42	f	0
3350	1069	2017-02-10 18:50:29.523179	2017-02-23 04:17:16.250042	\N		logo.png	iniLBdQYhVigxbQWVsxioZuGyRXSpeqGBHoGrpKowocyOLcWTe	5	f	0
3356	1068	2017-02-11 12:36:28.501177	2017-02-23 04:15:19.780567	\N		logo-la-tourangelle_0.png	ZxrcXEZDYjFNDCAHQmlprneKnkUotzfUciBZtilQzLHczSPock	0	f	0
3358	1068	2017-02-11 12:55:46.061338	2017-02-23 04:14:21.277125	\N		aroy-d.jpg	hXrgmVsbTwpofYdEGsxofZjnVmOrYwRLORXcNwSNtQwZedRYRH	0	f	0
3360	1068	2017-02-11 12:59:03.822019	2017-02-23 04:14:00.880133	\N		imgres.png	IvCaplypzMvLUxLnXNLRFzqUCvRtBuzpghePprvsGAfWzqgYmf	0	f	0
3361	1068	2017-02-11 13:04:26.216479	2017-02-23 04:13:08.982237	\N	Киноа	logo_mistral.png	KwDztpjqOzpwtDfhtXsqzDYyrXSczBgeabrTTFlJQGZkosEkjv	0	f	0
3359	1068	2017-02-11 12:57:44.14068	2017-02-23 04:13:31.425505	\N		imgres.png	aDgULkSAAjGrXZoNRXVMGjjdyfOICSocToFyyBdLRHlgvpyYDb	0	f	0
3394	1070	2017-02-21 11:54:39.155505	2017-02-23 04:36:14.908777	\N		\N	ybkrHceIHLnbohxHzlPHdJryIIGcujcByNVHYkJFNjnywkiWWI	26	f	0
3354	1069	2017-02-11 11:34:17.617093	2017-02-21 17:48:04.793029	\N	Сыр	\N	XbCqObpezBhYFQtqVsrOnFNhDUrqQzgGFHXCxrCgcrhDRJTbjo	1	f	0
3388	1070	2017-02-17 19:45:31.448888	2017-02-23 04:36:14.925951	\N		Hellmann_s.png	EwuVtKdUzFxWngjkpynzrBsTVpMdQokjDyPlWgKekzvkfMTwgG	29	f	0
3385	1070	2017-02-17 19:33:37.434588	2017-02-23 04:36:14.939932	\N	Соус-крем	carandini.png	bDDHcXMkvszdAvFTkwpnqNTgclzHkGRGKCopKfRQRZIswYEANf	32	f	0
3387	1070	2017-02-17 19:38:20.952072	2017-02-23 04:36:14.930416	\N		De_Cecco.png	AhjzwYfqZglyZCLgpDAPUBvHPNAqsSkxqTVvaDLuWPQevCgaKs	30	f	0
3382	1070	2017-02-17 19:18:41.581824	2017-02-23 04:36:14.954926	\N	Соус Терияки	Spicy_World.png	mnyicswTsBaADAlFYnIwFJVsBeIqTDcGvPHHvDbXPHoLWziFgs	35	f	0
3383	1070	2017-02-17 19:21:17.471556	2017-02-23 04:36:14.950275	\N	Соус Тонкацу	такемура.jpg	nfzahyGuLZhXsFIQCdukVHGCYLnrHADFRcTPuPEtwLMzDHgUde	34	f	0
3381	1070	2017-02-17 19:16:07.52517	2017-02-23 04:36:14.959956	\N	Соус Табаско	tabascologo_00.jpg	bKbctIlLAISifoySIYmIidTzLqpOxrxFEbJkNYPoOfzEzOniQs	36	f	0
3379	1070	2017-02-17 18:49:17.853764	2017-02-23 04:36:14.970084	\N	Соус соевый	санта_мария.png	IpXqCvsvqmXLpRmguLsKqDXDBZnnWJzLiesyFwtzXsOeFVbXuk	38	f	0
3378	1070	2017-02-17 18:33:07.869618	2017-02-23 04:42:12.361785	\N	Паста	aroy-d.jpg	HCXAbRpXJqEVFHdNyYimYGDRlSGcYWilxQGLRYppjfuSYHWuXL	39	f	0
3357	1068	2017-02-11 12:39:47.553385	2017-02-23 04:14:46.607394	\N		La_Tourangelle.jpg	sKUrFdquyKxYDFcaymUdYlSFoFxlkcMdZVmoEmYVtsSVAeXkmA	0	f	0
3376	1063	2017-02-17 12:42:04.363211	2017-02-22 07:42:20.083644	Крылокур_1.jpg		\N	jmaNiamfRMknpbfVLQTYRkyNYumWMZWgpFngdCYQKeQFvYhnOF	2	f	0
3366	1070	2017-02-11 19:14:09.865956	2017-02-23 04:40:48.188674	\N	Уксус	\N	pSxZDZFmTHemBZwNPqopzwkISYZrvRiFLeainnIHBLQtIbCZAh	45	f	0
3384	1070	2017-02-17 19:24:15.26367	2017-02-23 04:36:14.944964	\N	Соус Чили	aroy-d.jpg	OtTNfeJgIcmfPTCJEAZCULYMpqmpLOOBwgICYfmfZKytOnALxx	33	f	0
3380	1070	2017-02-17 18:56:54.119044	2017-02-23 04:36:14.965202	\N	Соус соевый PEARL RIVER BRIDGE 	Pearl_River_Bridge.jpg	OwQrMoGhDZYfutHbVCEzCHKEifJHmnESHdMHJBVphawRQxIABx	37	f	0
3353	1069	2017-02-10 19:08:01.455914	2017-02-21 17:48:28.853728	\N	Сыр с плесенью	\N	wgyjcjmPDLAdRxOpRZHsAyuhjNVZktStMuYjgDRXpcAlefECwD	2	f	0
3386	1070	2017-02-17 19:36:01.006538	2017-02-23 04:36:14.935083	\N		De_Cecco.png	aDzjXsHoWKAxayFDuDQVcaKJUdMFpBfgfumfiXaVHJheZkIXXK	31	f	0
3355	1069	2017-02-11 11:36:41.884091	2017-02-21 17:47:40.082664	\N	Сыр безлактозный	д.png	nBpXHBqhMtlGQxmZTUTOsthDBndgIUQDRatZUnIKjiaTXHBsos	0	f	0
3390	1070	2017-02-17 20:02:15.737314	2017-02-23 04:36:14.921013	\N	Соус КимЧи	Pearl_River_Bridge.jpg	MFTHMnDzszzrMQDdAdGnklUnupRyjPuISUPMxDxeioQablomsk	28	f	0
3377	1070	2017-02-17 18:26:43.395819	2017-02-23 04:41:35.953421	\N		\N	GCkwMAvXFnKhxoTBCBfHPblAUAfkraCARWjDkAONKppVHALcSu	40	f	0
3365	1070	2017-02-11 14:14:56.417204	2017-02-23 04:40:16.390621	\N		\N	QoKIjRxoGjVqUaJoEeWrmanKqLkUfsLvnrMzYRQrOYBHVHgqQL	46	f	0
3364	1070	2017-02-11 14:13:56.290772	2017-02-23 04:40:05.648044	\N		\N	rHOBYYKpRVzxKWuUmvOCRCdGJRSwfbksoaUqdJQhxrdtjqsrEf	47	f	0
3319	1062	2017-02-08 14:36:38.072474	2017-02-21 14:13:29.326498	\N	Мини-колбаски замороженные	logo_copy_2.png	KNOeMTrwMkLhMarsWZFnMEVXFIfBLyrkBDNtqnhylbyQvJmJXv	27	f	0
3401	1070	2017-02-21 12:03:46.005703	2017-02-23 04:38:41.514132	\N		\N	tgItBOQDOXliveYJqmYMKuOiuZykClBvcHeFCmRinuPLEfXmxR	19	f	0
3403	1070	2017-02-21 12:05:34.429402	2017-02-23 04:38:16.362027	\N		De_Cecco.png	GirodqYJilXieFFydoeoAKymnyTapptRHlnAaPkTQcQZHUeLbQ	17	f	0
3368	1070	2017-02-11 19:29:43.093265	2017-02-23 04:41:05.612704	\N	Кетчуп	Kraft-Heinz-Company-Logo-Design-3.png	VhWEGNLXaDlgMpNaKvCSDrVQtpqHtuiKCEPxBRQfUTlfNdQMGl	43	f	0
3398	1070	2017-02-21 12:00:11.862324	2017-02-23 04:39:33.205743	\N		такемура.jpg	BmkOUVtfqfGkdxsxIIropmZgtqOnLvJjYVvlBcBDzHGFmLVejn	22	f	0
3396	1070	2017-02-21 11:58:06.513965	2017-02-23 04:36:14.888633	\N		\N	vaiVluRRMeaWqmDeFVQcJIiBRgEYDaXSdcpXlMkuEJZSjkfdPk	24	f	0
3370	1070	2017-02-11 19:36:06.881907	2017-02-23 04:41:54.218985	\N	Майонез	Mr._Ricco.jpg	MnAxzpTMPiuXdeqqJGPsPSJefVsGvauNNIIKeUxOdqOUSNhIax	41	f	0
3442	1073	2017-02-21 20:20:50.499445	2017-02-23 04:28:27.711168	\N		\N	NwHrnIVFJwnlBZwJWVSnzbdzFFNuNSnCJXdURFkMhUBFhahqrM	6	f	0
3413	1070	2017-02-21 16:11:05.235461	2017-02-23 04:36:28.727375	\N	Уксус винный 	\N	kzTUQlhzymBJqdmMCoiPnTmruSVCvhnJWxubAPEquYifPFDdlY	8	f	0
3404	1070	2017-02-21 12:06:31.051907	2017-02-23 04:38:04.669778	\N		\N	wDNlWqtPwcifaKFExDEoZppamnyNEODrAIEFHupHMRHjaKdSkF	16	f	0
3405	1070	2017-02-21 14:45:33.908417	2017-02-23 04:37:47.868365	\N	Майонез	Hellmann_s.png	xOiZMEaeCzOVNCdMJxWusyrYJXsLzKbSvXIjYHuPnSMcwaIMnS	15	f	0
3409	1070	2017-02-21 15:42:54.51508	2017-02-23 04:37:19.012364	\N		ямаса.jpg	XILoHaFnpzroKkPbFMlwvedIxkkEKDmsIIYPWdRfgBSeHBHKVE	12	f	0
3407	1070	2017-02-21 15:35:54.78229	2017-02-23 04:37:32.01251	\N		Kikkoman-logo.jpg	crdpNFQOcHfJMdqQmNCBfnmJeaplzTQrXiuHGMHVckXDaJTuRO	14	f	0
3417	1070	2017-02-21 16:25:38.561434	2017-02-23 04:36:14.780254	\N	Соус барбекю	санта_мария.png	XGdAELvAxgswoMsyBRyexjlpAHeMauissXxnPCuDtvaZfMXpWA	4	f	0
3410	1070	2017-02-21 15:51:46.400782	2017-02-23 04:36:56.324938	\N		ил.jpg	hEjfmbDBojfALuQkBiVAeJYckioFGrzxpLfhXqqXCNGYAApIhl	11	f	0
3411	1070	2017-02-21 15:57:39.256352	2017-02-23 04:36:42.063682	\N		\N	CvnbwxbqafomLIFQPuIOuJKvOaNzjwMdJWZpcZMAnwGhjcBQIG	10	f	0
3416	1070	2017-02-21 16:18:43.703222	2017-02-23 04:36:14.787498	\N		Beaufor.jpg	JEmMCpZkykEbXwPwRfqvxnxEvNhbimtIhBKWTYaKpSYWCNzWCi	5	f	0
3414	1070	2017-02-21 16:12:42.973536	2017-02-23 04:36:14.801537	\N	Уксус рисовый	такемура.jpg	PVnmlDDJjLLzNEzZbjUBsJrZnShEjEzsOENdeoImjLRWGqRIwm	7	f	0
3419	1069	2017-02-21 16:40:14.683484	2017-02-21 16:43:08.311927	\N		logo-parmalat.png	kzHWoNARfGVXqWlgWLnsXkHdpGhQocSqbrLvCpTKiZfXdgtosf	14	f	0
3352	1069	2017-02-10 18:59:51.536208	2017-06-07 09:11:43.720788	\N	Творожный сыр	arla-logo-300.jpg	skmjJmLvBAncQfPrmQRanZSCslsOAscOMDkilnvwVsABnAlMEm	3	f	0
3351	1069	2017-02-10 18:56:22.487243	2017-06-07 09:12:56.983415	\N	Сыр «Моцарелла»	logo.png	RdWLWsDEoDvDOACHKnyzGwNatqDAKivYCeTlBnPknrTOStKxOy	4	f	0
3349	1069	2017-02-10 18:48:37.174999	2017-06-07 09:15:38.496575	\N	Сыр «Маасдам»	\N	WuSqmgmGqsDhOYUXofEaPWRZuFHbYJUsYwAcliduHZvhNXLURl	6	f	0
3418	1069	2017-02-21 16:38:35.633158	2017-02-21 16:43:29.727072	\N		\N	YQWKwQqbWwImecufGwDXBTgQTgSLrPpCpydYXBHRWjpRuHxiWc	15	f	0
3435	1073	2017-02-21 18:59:43.011439	2017-02-23 04:29:41.686763	\N		КНОРР.jpg	raApKGPrSRdWGoVPxsDgvwUtXCHgzOTDuighlgqGEVOmkayqIx	13	f	0
3415	1070	2017-02-21 16:15:36.340515	2017-02-23 04:36:14.79493	\N		ил.jpg	iTwraQbGXHKcyyNRqBrJvowkQWVfRmhBlfcWRPfTTJpQYqXArs	6	f	0
3434	1073	2017-02-21 18:40:56.569625	2017-02-23 04:29:22.287692	\N		\N	wpBMctYgqOALxrYznXJwhHXmEBVpBSPgEscQageOnogYPEKMSQ	14	f	0
3425	1069	2017-02-21 17:09:27.953389	2017-02-21 17:45:49.885887	\N		logo_unagrande__2_.png	zcNpELQsrvwSSTDtXBalTYhJujMFdVNajrTKPsEvXTBLLjdSLG	0	f	0
3423	1069	2017-02-21 16:53:18.552621	2017-02-21 17:46:14.808349	\N		PURATOS.png	KUZfxikwZMRmqcszfdfhKStQahBSeGIiSFPKwvHVfgwZYqiRaw	0	f	0
3424	1069	2017-02-21 17:02:36.18321	2017-02-21 17:46:34.793058	\N		pretto.png	mKHOIebxuXgblOvuVUTRgpKbPrDzQKFiLclkBmSbUcIHjOxIjw	0	f	0
3422	1069	2017-02-21 16:47:18.683405	2017-02-21 17:46:58.619878	\N	Сливки	logo-parmalat.png	rpLlcsDEJGERfJKHJItwYjGninKqkZZOFdaYliUZtSmOYcPfDE	0	f	0
3421	1069	2017-02-21 16:45:38.656117	2017-02-21 17:47:17.294977	\N	Сливки	логотип-Чудское-озеро.jpg	RzhWoabNQdzzDFokdTMkTBoOkWXPpQpqNFNwxUfjjZPABqSzvz	0	f	0
3433	1069	2017-02-21 17:41:13.032327	2017-02-21 17:43:07.935563	\N		мкс.png	gkhfxiLwsaaPkcEmOnzhwzdxkGeppbKqEqdPQSMvHWvWcEqckB	0	f	0
3432	1069	2017-02-21 17:36:17.314736	2017-02-21 17:43:29.526843	\N		logo-hochland.png	mupdSgajEcaPCsJJCHIbtPEuZFSMnrFxyjlXXGiwYAVMUUzkVV	0	f	0
3430	1069	2017-02-21 17:26:42.429165	2017-02-21 17:43:49.929433	\N	Сыр	logo_unagrande__2_.png	TXkEhgQVuypiLbMoNKOYuBuWfaBLCrozYqZvxGsOplgKljetrd	0	f	0
3431	1069	2017-02-21 17:29:20.613287	2017-02-21 17:44:11.368208	\N	Сыр	a_a7d46763.jpg	CSfWQWqiDCOiqpxZHUTICbcogXDjGtHxexlwMdnnSwsgQTgFQI	0	f	0
3439	1073	2017-02-21 20:08:11.807025	2017-02-23 04:28:54.912698	\N	Грибы	Lutik.png	pFhizxQEyjmoBhNBgZQqcwPXCZmJGtStABcxyagznhEcYwedic	9	f	0
3444	1073	2017-02-21 20:26:23.968696	2017-02-23 04:28:12.979012	\N		maestrodeoliva-logo.png	oZrjoFSfvkhBivCBIEpymexVIoVrSoZmrgNIYLOxjubWcocwQR	4	f	0
3443	1073	2017-02-21 20:22:42.601801	2017-02-23 04:27:56.686539	\N		\N	IDOOhVRzPFCCvRbEcjkVSjEfoxwupgzszbWUYYMWVcgrYxnGEt	5	f	0
3440	1073	2017-02-21 20:10:41.918813	2017-02-23 04:28:42.72506	\N		\N	ccegdgtQWolqDDrkUETtbsPEEkLarjhNLyjVfeiIOfseXkZtfl	8	f	0
3436	1073	2017-02-21 19:08:08.95777	2017-02-23 04:29:07.22064	\N		КНОРР.jpg	PJghvevLJIAyAPussTSctDlzVhMaxBkLbQzNEoTiTsyojQudgF	12	f	0
3406	1068	2017-02-21 15:22:11.702265	2017-02-23 04:11:24.067888	\N		_Мутти.jpg	CNThYIaUMvrDbUrKdQwpDfmAcSlFvfOLRTVLYgkjhRqotUDNvv	0	f	0
3412	1070	2017-02-21 16:01:08.487972	2017-02-23 04:36:14.812625	\N		Spicy_World.png	IjnNqUaEzqtHxERhMSPXjSzQOHrYWZYlIoyGMRSRPdatlBQNOh	9	f	0
3408	1070	2017-02-21 15:37:50.142165	2017-02-23 04:37:07.503023	\N		free-vector-amoy_040180_amoy.png	jmILoaZHDsFODLKkNuVBiQoMkNlTjScxckZPkRBgfsbXqHQuWe	13	f	0
3402	1070	2017-02-21 12:04:44.252368	2017-02-23 04:38:29.462229	\N		\N	VQUPFKtxEjxQmEkqZfinfEdSvyGSuZlpQaytdcDnzzXXdqelFO	18	f	0
3400	1070	2017-02-21 12:02:39.251991	2017-02-23 04:38:54.472566	\N		\N	ezhwYDWluuPvmMBzOmzQjzCdVmVLBwfYljpNYvsFLeUMfNnWNN	20	f	0
3399	1070	2017-02-21 12:01:09.698041	2017-02-23 04:39:08.543221	\N		КНОРР.jpg	gQVaQenDToaEgWAcKuoJazyaThTZQlcxFoGBeBORlzuTcmVSxb	21	f	0
3397	1070	2017-02-21 11:59:18.815199	2017-02-23 04:39:19.550063	\N		санта_мария.png	iAMclxdmReGHgTroGoCcSHpHIWFqWdAfiCEJcCwERROXuzzaCF	23	f	0
3395	1070	2017-02-21 11:57:03.10648	2017-02-23 04:36:14.893293	\N		Lee_Kum_Kee.svg.png	NNcuMhqeixHgJCpJIypdPfpJmWiXQGktJENaqfAFrzCbFFYZtF	25	f	0
3449	1068	2017-02-22 06:43:48.529386	2017-02-23 04:10:23.394684	\N		cinquina.jpg	OMxZKZrBdVgxbdFEqYJZFMCoyVfmbZllxxFgyxaxZivOabqBOU	0	f	0
3448	1073	2017-02-21 20:31:52.300357	2017-02-23 04:26:56.385457	\N		И.П.О.С.Е.А.png	wPgexvKSpfmrkFTcXjepQSqEkFggVsMLUqALZYnzxVyRdfGEVQ	0	f	0
3447	1073	2017-02-21 20:31:17.950394	2017-02-23 04:27:09.721049	\N		maestrodeoliva-logo.png	nqZAqrNCWSDlOrvNJRvZtLjFOvRYhVqbkeBnYcevqXlHZxiTXo	1	f	0
3446	1073	2017-02-21 20:29:11.486131	2017-02-23 04:27:26.243858	\N		\N	aQRvorgAlUUyniYEmuEILGWjaUyOYXHxJNeTRscizvdDctWUKY	2	f	0
3445	1073	2017-02-21 20:27:46.049802	2017-02-23 04:27:41.073321	\N		И.П.О.С.Е.А.png	OSBEHDpAwYLblFdIpXqvyyeTsnSUilkRzWRLJVloUyNERvIrNY	3	f	0
3454	1068	2017-02-22 07:18:11.078707	2017-02-22 07:18:11.078707	\N		\N	KNZKkRVuTySORhVCLbYWoRxxxbtwKqlFkiBzVMlFIklTJAFMyo	0	f	0
3464	1070	2017-02-22 07:49:57.344142	2017-02-23 04:34:04.548544	\N		\N	RAaIfTfUlyRUCKHlefrxPOMwvqsQVCfLlECmzAPTxfmObeiPPp	0	f	0
3463	1070	2017-02-22 07:48:48.027858	2017-02-23 04:36:14.756734	\N		Pearl_River_Bridge.jpg	aBdzZDrlsWVggXnyoCwrHjKyfzMzvdnOrSIqUkQrdSEwcZTxNv	1	f	0
3460	1068	2017-02-22 07:32:17.575898	2017-02-22 07:59:33.453528	\N	Паста Папарделле	De_Cecco.png	WpBqalPMthtYjxvjbbQXNdClrzGvimZBSaguEXsHziSjfPUbtT	0	f	0
3459	1068	2017-02-22 07:30:11.614103	2017-02-22 07:59:52.224795	\N	Паста Фузилли	De_Cecco.png	QDHRYGQcxDfXTuDLeTlVZnOHQwNipwFlGbWqPQLaVAWsXvimbR	0	f	0
3456	1068	2017-02-22 07:25:43.602359	2017-02-22 08:00:10.729318	\N	Паста спагетини	De_Cecco.png	pLxNpnLCLFgmtaBLXMBawYkDTZMEIwRVzhTaItGmfWWHijWWQf	0	f	0
3458	1068	2017-02-22 07:28:31.66481	2017-02-22 08:00:28.242982	\N	Паста Пенне Ригате	De_Cecco.png	ErJAvmirzWlSDXJRWaIFYFlwobMhgzKfpCjQoFpZCIGHQvqMrC	0	f	0
3455	1068	2017-02-22 07:22:45.822015	2017-02-22 08:00:48.754613	\N	Паста Фетуччини	De_Cecco.png	MBykaGNxMtYhlhMRgSVrORbVstInUYdjrECFKecqRdBgwoCAJV	0	f	0
3453	1068	2017-02-22 07:06:34.730168	2017-02-22 08:01:05.82504	\N		распак.jpg	gdvWRDsUBgoHQxqkSOkGTHkqtSIKrxdLVFRWUdgTcjxFZxzfLn	0	f	0
3451	1068	2017-02-22 06:59:31.853064	2017-02-22 08:01:36.525355	\N	Рис	Ризо_Галло.jpg	brrwTFeLBbskXLMSqiNpQAKvVxsUgqyFzXzbGBkGgDfOITWsjA	0	f	0
3462	1070	2017-02-22 07:47:05.255313	2017-02-23 04:36:14.765446	\N		Lee_Kum_Kee.svg.png	KoGFbOFzjeTgoWbnuVrOvqcRVbJNlShCHOncaVpTSjhBklHKkm	2	f	0
3461	1070	2017-02-22 07:45:33.903313	2017-02-23 04:36:14.77302	\N		aroy-d.jpg	LquOsRvXynNDFPrDEgMdPTAIQKpYFeOeenpsQXGCuWHKoWYkPl	3	f	0
3450	1068	2017-02-22 06:45:01.811337	2017-02-23 04:10:03.007852	\N	Оливковое масло	Lulglio.png	nGvcdRSqbpFQFgAtUHrJVdtdgpoxrfwgZrrVjynLdCyVfTLKkM	0	f	0
3472	1067	2017-03-01 17:46:41.348587	2017-03-21 12:22:31.246289	\N	Мороженое «Movenpick» шоколадное	movenpick-logo.jpg		0	f	0
3471	1067	2017-02-28 18:46:40.545357	2017-03-02 13:40:01.159314	\N	Мороженое	\N		0	f	0
3473	1067	2017-03-01 17:49:55.569331	2017-03-02 13:42:30.707046	\N	Мороженое «Movenpick» ягодное	movenpick-logo.jpg		0	f	0
3470	1067	2017-02-28 18:42:08.959111	2017-03-02 13:43:08.685674	\N	Мороженое «Nestle DeLuxe»	Nestle_logo_new2017-1.png		0	f	0
3468	1065	2017-02-23 08:14:57.342092	2017-06-07 08:12:15.142788	\N	Профитроли	ac7f4eb5df90dbbf69c055fe714c92e6.jpg	PgCFfXrJwuPuPBhiMiJRVqvrScbKDPLkNupIlXVUDjKcTSpkWj	6	f	0
3429	1069	2017-02-21 17:23:42.676013	2017-06-07 08:55:22.804836	\N	Сыр «Салаксис»	image001.png	cozcYhpEZKApdmFFdztCnANtXQFNpPVxeQgHwCpytriijOnFwG	0	f	0
3428	1069	2017-02-21 17:15:58.828515	2017-06-07 09:03:05.648447	\N	Сыр «Фуэте»	a_a7d46763.jpg	HlCHmMigdzLklyCKzggnZbfAUexSaazUdQQZKnUqiWkinaTkjV	0	f	0
3441	1073	2017-02-21 20:17:27.685101	2017-06-08 13:12:55.560831	\N		logo-bonduelle.png	CZOGYOpQyxYDnrLibhujuuSnivLmRiPqhZbJhGrUWGZOzcglIy	7	f	0
3438	1073	2017-02-21 20:02:59.533126	2017-06-08 13:17:12.165071	\N		\N	ywwZhNtIrKsVjIaPKGBrVGKgFyriOaNtxTRkXVdYMczZKZTFCH	10	f	0
3437	1073	2017-02-21 19:11:45.288886	2017-06-08 13:18:37.115062	\N		_Мутти.jpg	iwhNTUZhKTMZSxRHBKQXxTMEBBMDEbtuVLptqVKdhUIjsWCUnv	11	f	0
3322	1062	2017-02-08 16:28:21.54638	2017-06-01 13:07:39.915247	foto-275.jpg	Мраморная говядина	logo_copy_3.png	fQzUshLVFjKPHZonRYILGOkLkmGIpNhxbFbLIMqWcdeVZEddzz	0	f	0
3469	1065	2017-02-23 08:21:26.774328	2017-06-07 08:09:13.058866	\N	Чизкейки	ac7f4eb5df90dbbf69c055fe714c92e6.jpg	JJoQHrfCtvEsqdsaDYwuMokOjOatnYCItlKMFORlrkIgkaTVMZ	3	f	0
3426	1069	2017-02-21 17:14:22.114902	2017-06-07 09:02:46.209199	\N	Сыр «Турне»	a_a7d46763.jpg	wfIErguiLyLPgDwNLmxWTVKalvtfzDznmrOTSdEDiNxFcPdxYB	0	f	0
3296	1064	2017-02-01 13:55:23.50851	2017-06-08 08:09:16.309261	foto-402.jpg	Картофель фри	McCain_logo.svg.png	EJjBzYYbxBzYUUTswYArZnhOhzOBERyQRuzMFFuYiKORHjZDhC	0	f	0
3477	1072	2017-07-05 12:12:12.43946	2017-07-05 14:06:29.561597	\N	Маринады	logo.png	BSexpKkjwvUuUTQVlTUtYmEomcYFGmlwZxPNXYxbcZrbFzspNu	6	f	0
3476	1072	2017-07-05 11:39:23.125438	2017-07-05 14:13:43.064069	\N	 Кондитерские специи Santa Maria	logo.png	aJygDDloBBrSoHicUaoYiECjucMUJRgZyOAnAaDnqQeCAlzZOC	5	f	0
3478	1076	2017-07-18 12:57:25.609276	2017-07-18 14:38:28.073188	\N	Шашлык из морепродуктов	\N	rGHFOTNbcqItntSvuGgLRaWtmgJUMXIQfwrUKDlIJyDTxWQBlW	17	f	0
3328	1076	2017-02-09 09:15:27.498736	2017-07-18 14:38:30.270087	foto-95.jpg	Крабовые палочки	logo_copy_4.png	zBWspXFQZJLWXCrLtdnAQVXnILnXbpXwOMECPTOyJtyjWobrUI	14	f	0
3479	1076	2017-07-18 13:04:38.729501	2017-07-18 14:38:32.855869	\N	Гребешок	\N	olSaWgVcWtnnwddgTRTnZqRPdRwSkWcGtmQkIhOriowhaZzlyZ	12	f	0
3480	1073	2017-11-01 14:21:34.932399	2017-11-01 14:21:34.932399	\N	Test souce	\N	EJMBpfmXmZloqAHBOdeaynJCQyRbssdFffWCzmnYebDxfHYDAN	0	f	1
3481	1066	2017-11-21 13:04:24.886135	2017-11-21 13:04:25.196428	bread.jpg	Образец карточки	vilcomlogo.png	nKuLcqJvnwNqKPOBPdlwHDLeMgBuzhZjJyoOOGaopLHXGEIwAV	0	f	0
\.


--
-- Name: cards_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.cards_id_seq', 3481, true);


--
-- Data for Name: categories; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.categories (id, name, slug, priority, parent_id, lft, rgt, depth, children_count) FROM stdin;
1062	Мясо	\N	\N	\N	1	2	0	0
1063	Птица	\N	\N	\N	3	4	0	0
1064	Овощи, ягоды, грибы	\N	\N	\N	5	6	0	0
1065	Кондитерские изделия	\N	\N	\N	7	8	0	0
1066	Выпечка	\N	\N	\N	9	10	0	0
1067	Мороженое	\N	\N	\N	11	12	0	0
1068	Бакалея	\N	\N	\N	13	14	0	0
1069	Молочные продукты	\N	\N	\N	15	16	0	0
1070	Соусы	\N	\N	\N	17	18	0	0
1072	Специи	\N	\N	\N	21	22	0	0
1073	Консервы	\N	\N	\N	23	24	0	0
1074	Японская кухня	\N	\N	\N	25	26	0	0
1075	Все продукты	\N	\N	\N	27	28	0	0
1071	Сиропы	\N	\N	\N	19	20	0	0
1076	Рыба и морепродукты	\N	\N	\N	29	30	0	0
\.


--
-- Name: categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.categories_id_seq', 1077, false);


--
-- Data for Name: clients; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.clients (id, name, tel, password, discount, address, delivery_city_id, created_at, updated_at, remember_token) FROM stdin;
1	Pavel	+79992185022	178840	50	\N	\N	2017-02-23 16:31:27.025893	2017-03-03 13:35:55.756606	8c62f48940ea76824601d0b44cbd12e13ba6e78e
2	Aleona	+79885682077	124229	10	\N	\N	2017-02-23 17:10:59.871865	2017-12-05 18:14:39.048832	e9a4781054e9f4a55e9bfbc034beb0a4ebc8c313
5	Alyona M	+79044491257	101683	20	\N	\N	2017-12-26 12:15:28.500172	2017-12-26 12:17:23.903658	e2afdcda5d7fc304bdd126efe8f4e34f7e53603d
4	Eugene	89034633731	179215	15	\N	\N	2017-11-10 11:58:33.790771	2018-02-05 10:06:54.017525	ce54986d6f8fd3437251a110c84da53320dc18c2
\.


--
-- Name: clients_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.clients_id_seq', 5, true);


--
-- Data for Name: delivering_cities; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.delivering_cities (id, name, estimated_delivery_time, created_at, updated_at) FROM stdin;
1	Сочи	1 день	2017-02-14 20:05:21.41612	2017-02-14 20:05:21.41612
2	Адлер	2 дня	2017-02-14 20:05:36.082993	2017-02-14 20:05:36.082993
3	Хоста	3 дня	2017-02-14 20:05:49.092264	2017-02-14 20:05:49.092264
4	Лоо	4 дня	2017-02-14 20:06:03.021308	2017-02-14 20:06:03.021308
5	Лазаревское	5 дней	2017-02-14 20:06:25.37979	2017-02-14 20:06:25.37979
6	Красная Поляна	6 дней	2017-02-14 20:06:45.977909	2017-03-01 23:25:07.651795
\.


--
-- Name: delivering_cities_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.delivering_cities_id_seq', 6, true);


--
-- Data for Name: images; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.images (id, file, temp_key, created_at, updated_at, image_type) FROM stdin;
59	Crab.jpg	fcLFNkISJijQdelgOVCZnhomvRwKUTDPuGbsxyqAzEtXWpaMrH	2017-02-09 09:12:15.069732	2017-02-09 09:12:15.069732	image
119	bread.jpg	xsgHbaocayzAnViNadWUDMXdudhoCyVaPQrFOwHLCaCSLcYxbs	2017-11-21 12:59:43.580943	2017-11-21 12:59:43.580943	image
70	tumblr_n268h0yUtP1sib0ygo1_500.jpg	MWyViknbLIzxBNZERdDoOAKcTvaqGQChrYeJlwSPumfpjUXFgH	2017-02-15 14:41:55.373305	2017-02-15 14:41:55.373305	image
23	IMG_2016-10-30_21_20_02.jpg	LjcQrClmzBNTCwOKsHjvIlUGLpqShwlpXzgIdWCdggiLCaibrU	2016-12-19 01:59:27.359832	2016-12-19 01:59:27.359832	image
33	15871639_1304252656280187_8977622654139991355_n.jpg	VCfwhRAJNTLMWkODtnlKBYbQrTZBNzfbdhkPQOiFbIxuOvVAGX	2017-01-12 10:12:11.80544	2017-01-12 10:12:11.80544	image
34	15871639_1304252656280187_8977622654139991355_n.jpg	VzzAELmrBNltkicMosRheJHNGVeujJHmODvlbjzgcoxLOSCqfA	2017-01-12 10:12:24.351265	2017-01-12 10:12:24.351265	image
\.


--
-- Name: images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.images_id_seq', 121, true);


--
-- Data for Name: order_items; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.order_items (id, order_id, card_item_id, count) FROM stdin;
3	10	11955	\N
4	10	11960	\N
5	11	12602	\N
6	11	12621	\N
7	12	12596	\N
8	12	12597	\N
9	13	12478	\N
10	13	12479	\N
11	14	12478	\N
12	14	12479	\N
13	15	12478	\N
14	15	12479	\N
15	16	12478	\N
16	16	12479	\N
17	17	12478	\N
18	17	12479	\N
19	18	12478	\N
20	18	12479	\N
21	19	12478	\N
22	19	12479	\N
23	20	12478	\N
24	20	12479	\N
25	21	12478	\N
26	21	12479	\N
27	22	12478	\N
28	22	12479	\N
29	23	12478	\N
30	23	12479	\N
31	24	12478	\N
32	24	12479	\N
33	25	12478	\N
34	25	12479	\N
35	26	12478	\N
36	26	12479	\N
37	27	12478	\N
38	27	12479	\N
39	28	12478	\N
40	28	12479	\N
41	29	12478	\N
42	29	12479	\N
43	29	12478	\N
44	30	12478	\N
45	30	12479	\N
46	31	12478	\N
47	31	12479	\N
48	32	12478	\N
49	32	12479	\N
50	33	12478	\N
51	33	12479	\N
52	33	\N	\N
53	33	\N	\N
54	33	12479	\N
55	33	12479	\N
56	34	12478	\N
57	34	12478	\N
58	34	12479	\N
59	35	12478	\N
60	35	12479	\N
61	36	12478	\N
62	36	12479	\N
63	37	12478	1
64	37	12479	2
65	38	12518	2
66	38	12521	3
67	40	12575	3
68	40	12578	1
69	40	12580	5
70	41	11970	1
71	41	11972	1
72	41	12482	2
73	42	11970	1
74	42	11972	1
75	42	12482	1
76	43	11890	2
77	43	11970	1
78	43	11972	1
79	43	12482	1
80	44	11890	1
81	44	11970	1
82	44	11972	1
83	44	12482	1
84	45	11883	1
85	45	11890	1
86	46	12556	1
87	46	12559	1
88	47	11878	3
89	47	11886	4
90	47	12478	1
91	48	12305	1
92	48	12306	1
93	48	12307	1
94	48	12440	1
95	48	12441	1
96	48	12442	1
97	48	12443	1
98	48	12444	1
99	48	12449	10
100	48	12450	1
101	48	12452	1
102	48	12454	1
103	49	11890	1
104	51	12025	1
105	51	12027	1
106	52	11877	1
107	52	11879	1
108	52	11881	1
109	52	11883	1
110	52	11887	1
111	52	11889	1
112	52	12416	1
113	52	12424	1
114	52	12430	1
115	52	12432	1
116	52	12433	1
117	52	12447	1
118	52	12456	1
119	52	12463	1
120	55	12022	1
121	55	12020	1
122	55	12015	1
123	55	12014	1
124	55	12011	1
125	56	12826	1
126	56	12655	1
127	57	12826	1
128	57	12655	1
129	58	12826	1
130	58	12655	1
131	59	12826	2
132	59	12655	1
133	60	12826	2
134	60	12655	1
135	61	13118	1
136	61	13027	15
137	61	12682	1
138	63	13223	1
139	63	13106	1
140	66	13105	1
141	66	13106	1
142	66	12548	1
143	70	12551	2
144	70	12550	1
145	70	12549	1
146	70	12542	1
147	70	12537	1
148	70	12538	1
149	70	12539	1
150	70	12540	1
151	70	12541	1
152	70	12534	1
153	70	12533	1
154	70	12532	1
155	70	12531	1
156	70	12536	1
157	70	12535	1
158	70	12530	1
159	70	12529	1
160	70	12526	1
161	70	12525	1
162	70	12522	1
163	70	12521	1
164	72	13146	1
165	72	13149	1
166	75	13069	1
167	76	13106	1
168	76	13105	1
169	76	13104	1
170	77	13036	1
171	77	12701	1
172	79	13149	1
173	79	13146	1
174	79	13140	1
175	79	13139	1
176	79	13137	1
177	79	13136	1
178	79	13138	1
179	79	13141	1
180	79	13142	1
181	79	13135	1
182	79	13145	1
183	79	13144	13
184	79	13143	1
185	79	13129	1
186	79	13128	1
187	79	13126	1
188	79	13127	1
189	79	13130	1
190	79	13131	1
191	79	13110	1
192	79	13002	1
193	79	12996	1
194	79	12997	1
195	81	13039	1
196	83	13036	1
197	83	13035	1
198	83	13034	1
199	83	12701	1
200	83	12697	1
201	83	12694	1
202	83	13117	1
203	83	13113	1
204	83	13114	1
205	83	13112	1
206	83	11995	1
207	83	11994	1
208	83	11993	1
209	83	12700	1
210	83	13106	1
211	83	13105	1
212	83	13104	1
213	83	13103	1
214	84	13111	1
215	84	12055	1
216	86	13040	1
217	86	12672	1
218	86	12668	1
219	86	13232	1
220	86	13231	1
221	86	12683	1
222	86	12682	1
223	86	13101	1
224	86	12805	1
225	86	13012	1
226	86	13037	1
227	86	12926	1
228	86	12055	1
229	86	13111	1
230	86	12936	1
231	86	12129	1
232	86	13188	1
233	86	13280	1
234	86	12375	1
235	86	13065	1
236	86	13062	1
237	86	12540	1
238	86	12541	1
239	86	13145	1
240	86	13189	1
241	86	13169	1
242	86	13175	1
243	86	12656	1
244	86	12655	1
245	87	13106	1
246	87	13096	1
247	87	13091	1
248	87	13101	1
249	87	13098	1
250	87	13097	1
251	87	13100	1
252	87	13088	1
253	87	12803	1
254	87	12805	1
255	87	12802	1
256	87	12801	1
257	87	12796	1
258	87	12795	1
259	87	12794	1
260	87	12793	1
261	87	12815	1
262	87	12700	1
263	87	12698	1
264	88	12410	1
265	88	12409	1
266	88	12408	1
267	88	12407	1
268	89	13146	1
269	89	13144	1
270	89	13143	1
271	90	12700	1
272	90	13037	1
273	90	13149	116
274	91	12143	1
275	91	13043	1
276	91	12142	1
277	91	12141	1
278	91	12140	1
279	91	12139	1
280	91	12144	1
281	91	12145	1
282	91	12146	1
283	91	13042	1
284	91	13041	1
285	91	12133	1
286	91	12134	1
287	91	12135	1
288	91	13045	1
289	91	13046	10
290	91	13044	1
291	91	12137	1
292	91	12136	1
293	92	13232	1
294	92	13012	1
295	92	13011	1
296	93	12891	1
297	93	12863	1
298	93	12307	1
299	93	12308	1
300	93	12309	1
301	93	12373	1
302	93	12369	1
303	93	12368	1
304	93	12401	1
305	93	12405	1
306	93	12406	1
307	93	12407	1
308	93	12408	1
309	93	12399	1
310	93	12398	1
311	93	12371	1
312	94	13012	1
313	94	13008	1
314	94	13007	1
315	94	13006	1
316	96	13144	1
317	96	13145	1
318	97	\N	0
319	97	\N	0
320	97	12803	1
321	97	12802	1
322	98	12866	1
323	98	12865	1
324	98	12867	1
325	99	12803	10
326	99	12801	1
327	100	13006	1
328	100	13007	1
329	101	12672	1
330	101	12668	1
331	101	13040	1
332	101	13232	1
333	101	13231	1
334	102	12893	1
335	102	12892	1
336	102	12862	1
337	102	12856	1
338	102	12410	1
339	102	12375	1
340	104	13040	1
341	104	12697	1
342	104	12952	1
343	104	12951	1
344	104	13350	1
345	104	13349	1
346	104	13101	1
347	104	13100	1
348	105	13394	170
349	105	13393	1
350	106	12917	1
351	106	12916	1
352	106	12918	1
353	106	12919	1
354	106	12920	1
355	106	12921	1
356	106	13011	1
357	106	13010	1
358	106	13009	1
359	106	13008	1
360	106	13007	1
361	106	12796	1
362	106	12801	1
363	106	12802	1
364	106	12803	1
365	106	12805	1
366	106	13101	1
367	106	13100	1
368	106	13099	1
369	106	13098	1
370	106	13097	1
371	106	13016	1
372	106	13015	1
373	106	13013	1
374	106	13014	1
375	106	13018	1
376	106	13017	1
377	106	13107	1
378	106	13085	1
379	106	13023	1
380	106	13005	1
381	106	13004	1
382	106	13003	1
383	106	12770	1
384	106	11934	1
385	106	11931	1
386	106	12922	1
387	106	12934	1000
388	106	12936	1
389	106	12935	1
390	111	13038	1
391	113	13350	1
392	113	13349	1
393	116	13040	11
394	116	12668	1
395	116	12672	1
396	117	12922	1
397	117	13146	1
398	120	\N	0
399	120	\N	0
400	120	13012	1
401	121	12862	1
402	121	12861	1
403	121	12701	1
404	121	13040	1
405	121	13231	4
406	123	12700	1
407	123	12699	4
408	123	13037	1
409	123	13038	1
410	123	12698	1
411	123	11981	1
412	123	11982	1
413	123	11984	1
414	123	11985	1
415	123	11986	1
416	125	12861	1
417	125	12860	1
418	125	12856	1
419	125	13101	1
420	125	12683	1
421	125	13004	1
422	126	13232	1
423	126	13231	1
424	127	13386	1
425	127	13385	1
426	127	13388	1
427	127	13387	1
428	127	13040	1
429	127	12672	1
430	129	12805	2
431	129	12803	4
432	130	12472	1
433	130	12469	1
434	130	12439	1
435	130	12453	1
436	130	12429	1
437	130	12443	1
438	130	12445	1
439	130	12447	1
440	130	12458	1
441	130	12459	1
442	130	12460	1
443	130	12461	1
444	130	12462	1
445	130	12463	1
446	130	12444	1
447	130	12449	1
448	130	13101	1
449	130	12805	2
450	131	12541	1
451	131	12536	1
452	131	12802	2
453	133	11981	1
454	134	12805	1
455	134	12803	1
456	134	13393	1
457	134	13392	1
458	134	13315	1
459	134	13314	1
460	134	13391	1
461	134	13305	1
462	134	13304	1
463	134	13306	1
464	134	13302	1
465	134	13301	1
466	134	13300	1
467	134	13299	1
468	134	13297	1
469	134	13296	1
470	134	13298	1
471	134	13284	1
472	134	13283	1
473	134	13289	1
474	134	13288	1
475	134	13287	1
476	135	13036	7
477	136	13071	1
478	136	13128	1
479	136	12622	1
480	136	12725	1
481	136	12514	1
482	136	12515	1
483	136	12460	1
484	136	12456	1
485	136	12452	1
486	140	12768	1
487	140	12642	1
488	140	13124	3
489	140	13123	1
490	141	13124	15
491	142	12767	1234
492	143	13071	1
493	143	13128	15
494	143	12622	10000
495	143	12725	1
496	143	12514	1
497	143	12515	1
498	143	12460	1
499	143	12456	1
500	143	12452	1
501	143	13124	23
502	145	13368	1346
503	146	13071	1
504	146	13128	1
505	146	12622	1
506	146	12725	13
507	146	12514	1
508	146	12515	1
509	146	12460	1
510	146	12456	1
511	146	12452	1
512	147	12407	1
513	147	12402	2
514	153	12532	1
515	153	12531	1
516	153	12528	1
517	153	12527	1
518	153	12893	1
\.


--
-- Name: order_items_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.order_items_id_seq', 518, true);


--
-- Data for Name: orders; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.orders (id, name, created_at, updated_at, address, contacts, phone, comments, delivering_city_id) FROM stdin;
\.


--
-- Name: orders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.orders_id_seq', 153, true);


--
-- Data for Name: post_category_relations; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.post_category_relations (id, post_id, category_id, created_at, updated_at) FROM stdin;
1	2	1062	2017-02-15 07:28:02.211606	2017-02-15 07:28:02.211606
7	6	1064	2017-07-11 11:37:21.053907	2017-07-11 11:37:21.053907
8	6	1072	2017-07-11 11:37:21.062232	2017-07-11 11:37:21.062232
9	6	1076	2017-07-11 11:37:21.06434	2017-07-11 11:37:21.06434
\.


--
-- Name: post_category_relations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.post_category_relations_id_seq', 9, true);


--
-- Data for Name: post_images; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.post_images (id, post_id, image, created_at, updated_at) FROM stdin;
2	2	IMG_9217.JPG	2017-02-15 07:28:02.219418	2017-02-15 07:28:02.219418
9	2	IMG_9216.JPG	2017-02-22 12:48:17.161913	2017-02-22 12:48:17.161913
10	2	IMG_9222.JPG	2017-02-22 12:48:17.167308	2017-02-22 12:48:17.167308
11	2	IMG_9223.JPG	2017-02-22 12:48:17.169253	2017-02-22 12:48:17.169253
12	2	IMG_9224.JPG	2017-02-22 12:48:17.171199	2017-02-22 12:48:17.171199
15	6	2017-05-26_03.21.15.jpg	2017-07-10 11:07:59.459566	2017-07-10 11:07:59.459566
16	6	2017-05-26_03.21.55.jpg	2017-07-10 11:07:59.481316	2017-07-10 11:07:59.481316
17	6	2017-05-26_03.22.52.jpg	2017-07-10 11:07:59.483455	2017-07-10 11:07:59.483455
18	6	2017-05-26_03.22.10.jpg	2017-07-10 11:07:59.485114	2017-07-10 11:07:59.485114
19	6	2017-05-26_03.22.06.jpg	2017-07-10 11:07:59.486776	2017-07-10 11:07:59.486776
20	6	2017-05-26_03.23.02.jpg	2017-07-10 11:07:59.488499	2017-07-10 11:07:59.488499
21	6	2017-05-26_03.23.12.jpg	2017-07-10 11:07:59.490129	2017-07-10 11:07:59.490129
22	6	2017-05-26_03.23.35.jpg	2017-07-10 11:07:59.491743	2017-07-10 11:07:59.491743
23	6	2017-05-26_03.22.23.jpg	2017-07-10 11:07:59.493353	2017-07-10 11:07:59.493353
24	6	2017-05-26_03.24.12.jpg	2017-07-10 11:07:59.494986	2017-07-10 11:07:59.494986
25	6	2017-05-26_03.24.39.jpg	2017-07-10 11:07:59.496589	2017-07-10 11:07:59.496589
26	6	2017-05-26_03.24.17.jpg	2017-07-10 11:07:59.498194	2017-07-10 11:07:59.498194
27	6	2017-05-26_03.25.24.jpg	2017-07-10 11:07:59.499822	2017-07-10 11:07:59.499822
30	7	intext_800_auto_100_800_600_100.jpg	2017-07-10 11:18:41.882631	2017-07-10 11:18:41.882631
37	7	img_4008.jpg	2017-11-21 14:49:12.482683	2017-11-21 14:49:12.482683
38	7	2.jpg	2017-11-21 14:49:12.493822	2017-11-21 14:49:12.493822
39	7	6.jpg	2017-11-21 14:49:12.497185	2017-11-21 14:49:12.497185
41	8	vegetables-vegetable-basket-harvest-garden.jpg	2017-11-21 15:05:14.635218	2017-11-21 15:05:14.635218
\.


--
-- Name: post_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.post_images_id_seq', 41, true);


--
-- Data for Name: posts; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.posts (id, title, first_text_section, second_text_section, created_at, updated_at, url) FROM stdin;
6	<a target="" rel="" title="" href="http://vilcom.bvz.name/news/gastreet2017">Мы все — участники Gastreetа</a> <br>	&nbsp; &nbsp; &nbsp;На ежегодном фестивале "Gastreet", проходившем в Сочи в мае, мы&nbsp;с партнёрами поставили свой фудтрак с меню из морепродуктов. Мы&nbsp;старались доказать идею о том, что&nbsp;премиальный продукт гармоничен, востребован и рентабелен в стритфуде: готовили тигровые креветки на завтрак, тунец на обед, гигантские креветки и гребешки на ужин. Хитом меню стал «Конверт с тунцом». Не забывая, что&nbsp;мы&nbsp;—&nbsp;поставщики,<b></b>&nbsp;показывали гостям новые идеи для fast-casual, streetfood, upper-casual форматов. Увидели, что&nbsp;людям это&nbsp;интересно!<br>&nbsp; &nbsp; &nbsp;Продали о-о-о-очень много, все довольны. Получили фантастический опыт, увидели кухню изнутри, причем с загрузкой по максимуму. Как&nbsp;сказал представитель McCain: «Теперь я ВСЁ понял!»<br>&nbsp; &nbsp; &nbsp;Спасибо за поддержку и участие нашим партнёрам: "Seafood Line", "McCain", "Santa Maria", "Heinz", чайно-кофейной компании "TeCo", производителю фуд-траков "Aformer". А&nbsp;также&nbsp;Ивану Касьянову —&nbsp;повару бургерной «Му»,&nbsp;Сергею Косолапову —&nbsp;бренд-шефу сети «Модус» в городе Сочи, директору компании «Фрукты моря» Алексею Лобанову, лично&nbsp;принимавшему заказы у наших гостей.<br><p>&nbsp; &nbsp; &nbsp;До новых встреч на&nbsp;<a target="" rel="" title="" href="https://gastreet.com">Gastreet</a>!</p>	\N	2017-05-26 12:33:11	2017-11-21 15:25:16.590129	gastreet2017
7	<a target="" rel="" title="Вилком представляет: «Праймбиф» и «Заречное»" href="http://vilcom.bvz.name/news/zarechnoe_official">Вилком представляет: «Праймбиф» и&nbsp;«Заречное»</a>	<div>Наша компания стала официальным представителем в Сочи группы компаний «Заречное». Это мраморная говядина брендов «Заречное» и&nbsp;«Праймбиф» высшего качества из бычков&nbsp;породы «чёрный ангус» зернового откорма. Корм для животных в «Заречном» выращивают сами,&nbsp;все компоненты кормов выращиваются на&nbsp;собственных землях. Также компания заботится об окружающей среде, а&nbsp;животные&nbsp;животные всегда содержатся в&nbsp;чистоте, не&nbsp;испытывают стрессовых воздействий и получают весь уход, необходимый для соблюдения требований к&nbsp;качеству продукции.</div>	\N	2017-04-03 21:00:00	2017-11-21 14:51:57.562342	zarechnoe_official
8	<a target="" rel="" title="" href="http://vilcom.bvz.name/news/delivery">Доставляем в любой день</a>	Мы запустили воскресную доставку в города Сочи и Адлер, а в скором времени планируем наладить доставку по&nbsp;воскресеньям и&nbsp;в&nbsp;Красную Поляну. Сейчас доставляем заказы по будням в Дагомыс, Сочи, Хосту, Адлер, Красную Поляну, Имеретинку и до границы с&nbsp;Абхазией.	\N	2017-07-01 21:00:00	2017-11-21 15:06:32.801506	delivery
2	<a target="" rel="" title="" href="http://vilcom.bvz.name/news/mramor">Провели мастер-класс по&nbsp;приготовлению мраморного мяса</a>	<p>&nbsp; &nbsp; В гриль-баре «Мангал» санатория «Русь» состоялся мастер-класс по правильному приготовлению основных и&nbsp;альтернативных отрубов американской мраморной говядины для профессионалов.</p>\r\n<p>&nbsp; &nbsp; Практической части предшествовало выступление главы представительства некоммерческой корпорации «Американская федерация по экспорту мяса» Галины Кочубеевой, рассказавшей собравшимся шеф-поварам и&nbsp;администраторам сочинских ресторанов о производстве американской мраморной говядины — начиная со&nbsp;стадии выращивания и заканчивая строжайшим контролем качества при экспорте.</p>\r\n<p>&nbsp; &nbsp; &nbsp;Проводивший мероприятие шеф-повар Андрей Сердин (ГК «Арпиком») сосредоточил внимание аудитории на разрубах говядины. Именно в тонкостях разделки скрывается главное: использование альтернативных разрубов позволяет получить блюда высочайшего вкусового качества при сравнительно низкой стоимости. Не&nbsp;выпуская нож из рук в течение почти шести часов, Андрей Сердин наглядно демонстрировал основные и&nbsp;альтернативные разрубы, объяснял способы сведения отходов при разделке к минимуму, показывал, как&nbsp;правильно подготавливать и зачищать мясо, а также как подбирать специи, готовить соусы и подавать блюда. При этом всем желающим господин Сердин давал поработать с мясом самостоятельно под своим руководством.</p>\r\n<div class="fotorama fotorama1487136074522">\r\n<div class="fotorama__wrap fotorama__wrap--css3 fotorama__wrap--slide fotorama__wrap--toggle-arrows fotorama__wrap--no-controls">\r\n<div class="fotorama__nav-wrap"><div class="fotorama__nav fotorama__nav--thumbs">\r\n<div class="fotorama__nav__shaft"><div class="fotorama__nav__frame fotorama__nav__frame--thumb">\r\n<div class="fotorama__thumb fotorama__loaded fotorama__loaded--img">\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Приготовленные в ходе мастер-класса четыре вида стейков, бургеры, ростбиф и салат наглядно показали, что мраморное мясо — это не всегда дорого, но всегда вкусно. Если знать, как его правильно разделывать и готовить.</p></div></div></div></div></div></div></div>	\N	2017-02-17 07:28:02	2017-11-21 16:36:11.197674	mramor
\.


--
-- Name: posts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.posts_id_seq', 9, true);


--
-- Data for Name: promo_cards; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.promo_cards (id, category_id, title, list, image, card_class, created_at, updated_at) FROM stdin;
2	1062	Мясо	Баранина,Ягнятина,Деликатесы,Кролик,Оленина,Мясные изделия,Мраморная говядина,Замороженная говядина,Стейки,Говядина	meat2.jpg	huge	2016-06-08 11:20:36.270728	2016-06-08 11:20:36.270728
3	1076	Морепродукты	Кальмар,Каракатица,Краб,Мясо камчатского краба,Крабовые палочки,Креветки,Лягушачьи лапки,Лобстер,Мидии,Осминог,Трубки кальмара,Филе гребешка	seafood4.jpg	big	2016-06-08 11:20:37.748906	2016-06-08 11:20:37.748906
4	1063	Птица	Цыплята,Утка,Перепёлка,Гусь,Куриное филе,Ребрышки	chiken2.jpg		2016-06-08 11:20:38.369853	2016-06-08 11:20:38.369853
5	1068	Бакалея	Молочные продукты,Масло,Мучные изделия,Соусы,Специи,Сиропы и топпинги,Консервация,КНОРР,Соль и сахар,Чай и кофе	\N		2016-06-08 11:20:38.505307	2016-06-08 11:20:38.505307
6	1068	Хлеб		bread2.jpg	small	2016-06-08 11:20:39.736265	2016-06-08 11:20:39.736265
7	1064	Овощи, грибы, ягоды		vegetables2.jpg		2016-06-08 11:20:40.4956	2016-06-08 11:20:40.4956
8	1065	Кондитерские изделия	Шоколад,Выпечка,Торты,Хлеб,Чизкейки,КД Селезнева,Мороженное,Ягодное пюре,Ягодный соус	\N	big	2016-06-08 11:20:40.64216	2016-06-08 11:20:40.64216
\.


--
-- Name: promo_cards_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.promo_cards_id_seq', 9, false);


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.schema_migrations (version) FROM stdin;
20160601135721
20160601140503
20160601143736
20160601143837
20160603103126
20160603114634
20160603114635
20160603114636
20160603114637
20160603114638
20160603123527
20160606132050
20160606132114
20160606134908
20160608111341
20160609142037
20160609150541
20160616133532
20160801124815
20160811023854
20160811180429
20160811193615
20160813122105
20161216225323
20170114193725
20170129101621
20170129101813
20170129101819
20170129103002
20170129105235
20170129105236
20170130180508
20170130180705
20170211102956
20170212150329
20170214143706
20170214152809
20170214185913
20170221164606
20170222085939
20170223142042
20170303143417
20170601125623
\.


--
-- Data for Name: taggings; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.taggings (id, tag_id, taggable_id, taggable_type, tagger_id, tagger_type, context, created_at) FROM stdin;
1	1	1731	Card	\N	\N	tags	2016-06-03 11:56:30.545124
2	2	1731	Card	\N	\N	tags	2016-06-03 11:56:30.56789
3	1	1732	Card	\N	\N	tags	2016-06-03 11:57:45.945071
4	2	1732	Card	\N	\N	tags	2016-06-03 11:57:45.96469
5	3	1732	Card	\N	\N	tags	2016-06-03 11:57:45.985992
6	1	1733	Card	\N	\N	tags	2016-06-03 11:57:47.095832
7	4	1733	Card	\N	\N	tags	2016-06-03 11:57:47.118106
8	5	1733	Card	\N	\N	tags	2016-06-03 11:57:47.139968
9	1	1734	Card	\N	\N	tags	2016-06-03 11:57:49.492819
10	4	1734	Card	\N	\N	tags	2016-06-03 11:57:49.51936
11	5	1734	Card	\N	\N	tags	2016-06-03 11:57:49.549024
12	1	1735	Card	\N	\N	tags	2016-06-03 11:57:52.386737
13	4	1735	Card	\N	\N	tags	2016-06-03 11:57:52.412866
14	6	1735	Card	\N	\N	tags	2016-06-03 11:57:52.440072
15	1	1736	Card	\N	\N	tags	2016-06-03 12:01:47.501741
16	2	1736	Card	\N	\N	tags	2016-06-03 12:01:47.523902
17	1	1737	Card	\N	\N	tags	2016-06-03 12:01:47.692136
18	2	1737	Card	\N	\N	tags	2016-06-03 12:01:47.711492
19	3	1737	Card	\N	\N	tags	2016-06-03 12:01:47.732296
20	1	1738	Card	\N	\N	tags	2016-06-03 12:01:47.833288
21	4	1738	Card	\N	\N	tags	2016-06-03 12:01:47.855059
22	5	1738	Card	\N	\N	tags	2016-06-03 12:01:47.875927
23	1	1739	Card	\N	\N	tags	2016-06-03 12:01:49.903859
24	4	1739	Card	\N	\N	tags	2016-06-03 12:01:49.929659
25	5	1739	Card	\N	\N	tags	2016-06-03 12:01:49.948083
26	1	1740	Card	\N	\N	tags	2016-06-03 12:01:52.849767
27	4	1740	Card	\N	\N	tags	2016-06-03 12:01:52.886719
28	6	1740	Card	\N	\N	tags	2016-06-03 12:01:52.91114
29	1	1741	Card	\N	\N	tags	2016-06-03 12:01:55.469461
30	4	1741	Card	\N	\N	tags	2016-06-03 12:01:55.497303
31	6	1741	Card	\N	\N	tags	2016-06-03 12:01:55.52549
32	1	1742	Card	\N	\N	tags	2016-06-03 12:01:57.74025
33	4	1742	Card	\N	\N	tags	2016-06-03 12:01:57.77359
34	6	1742	Card	\N	\N	tags	2016-06-03 12:01:57.805326
35	1	1743	Card	\N	\N	tags	2016-06-03 12:02:00.210785
36	4	1743	Card	\N	\N	tags	2016-06-03 12:02:00.240868
37	6	1743	Card	\N	\N	tags	2016-06-03 12:02:00.272619
38	1	1744	Card	\N	\N	tags	2016-06-03 12:02:02.269382
39	4	1744	Card	\N	\N	tags	2016-06-03 12:02:02.304834
40	6	1744	Card	\N	\N	tags	2016-06-03 12:02:02.335808
41	1	1745	Card	\N	\N	tags	2016-06-03 12:02:04.099022
42	4	1745	Card	\N	\N	tags	2016-06-03 12:02:04.130523
43	6	1745	Card	\N	\N	tags	2016-06-03 12:02:04.164597
44	1	1746	Card	\N	\N	tags	2016-06-03 12:02:06.085133
45	4	1746	Card	\N	\N	tags	2016-06-03 12:02:06.115848
46	6	1746	Card	\N	\N	tags	2016-06-03 12:02:06.140509
47	1	1747	Card	\N	\N	tags	2016-06-03 12:02:09.550331
48	4	1747	Card	\N	\N	tags	2016-06-03 12:02:09.58224
49	6	1747	Card	\N	\N	tags	2016-06-03 12:02:09.604587
50	1	1748	Card	\N	\N	tags	2016-06-03 12:02:11.625936
51	4	1748	Card	\N	\N	tags	2016-06-03 12:02:11.657697
52	7	1748	Card	\N	\N	tags	2016-06-03 12:02:11.687846
53	1	1749	Card	\N	\N	tags	2016-06-03 12:02:13.208597
54	4	1749	Card	\N	\N	tags	2016-06-03 12:02:13.240004
55	6	1749	Card	\N	\N	tags	2016-06-03 12:02:13.268905
56	1	1750	Card	\N	\N	tags	2016-06-03 12:02:15.648229
57	4	1750	Card	\N	\N	tags	2016-06-03 12:02:15.677932
58	6	1750	Card	\N	\N	tags	2016-06-03 12:02:15.709097
59	1	1751	Card	\N	\N	tags	2016-06-03 12:02:16.074235
60	4	1751	Card	\N	\N	tags	2016-06-03 12:02:16.096042
61	6	1751	Card	\N	\N	tags	2016-06-03 12:02:16.115546
62	1	1752	Card	\N	\N	tags	2016-06-03 12:02:16.376548
63	4	1752	Card	\N	\N	tags	2016-06-03 12:02:16.398911
64	6	1752	Card	\N	\N	tags	2016-06-03 12:02:16.420244
65	1	1753	Card	\N	\N	tags	2016-06-03 12:02:16.519015
66	4	1753	Card	\N	\N	tags	2016-06-03 12:02:16.540947
67	6	1753	Card	\N	\N	tags	2016-06-03 12:02:16.564469
68	1	1754	Card	\N	\N	tags	2016-06-03 12:02:16.719454
69	4	1754	Card	\N	\N	tags	2016-06-03 12:02:16.747443
70	8	1754	Card	\N	\N	tags	2016-06-03 12:02:16.772463
71	1	1755	Card	\N	\N	tags	2016-06-03 12:02:16.962652
72	4	1755	Card	\N	\N	tags	2016-06-03 12:02:16.981973
73	1	1756	Card	\N	\N	tags	2016-06-03 12:02:17.10948
74	9	1756	Card	\N	\N	tags	2016-06-03 12:02:17.126473
75	1	1757	Card	\N	\N	tags	2016-06-03 12:02:17.301265
76	10	1757	Card	\N	\N	tags	2016-06-03 12:02:17.32001
77	1	1758	Card	\N	\N	tags	2016-06-03 12:02:17.521463
78	11	1758	Card	\N	\N	tags	2016-06-03 12:02:17.548438
79	1	1759	Card	\N	\N	tags	2016-06-03 12:02:17.662858
80	11	1759	Card	\N	\N	tags	2016-06-03 12:02:17.688937
81	1	1760	Card	\N	\N	tags	2016-06-03 12:02:17.785708
82	11	1760	Card	\N	\N	tags	2016-06-03 12:02:17.804605
83	1	1761	Card	\N	\N	tags	2016-06-03 12:02:18.957538
84	11	1761	Card	\N	\N	tags	2016-06-03 12:02:18.992607
85	1	1762	Card	\N	\N	tags	2016-06-03 12:02:19.154302
86	12	1762	Card	\N	\N	tags	2016-06-03 12:02:19.174693
87	1	1763	Card	\N	\N	tags	2016-06-03 12:02:19.269612
88	11	1763	Card	\N	\N	tags	2016-06-03 12:02:19.287214
89	1	1764	Card	\N	\N	tags	2016-06-03 12:02:19.84145
90	11	1764	Card	\N	\N	tags	2016-06-03 12:02:19.88043
91	1	1765	Card	\N	\N	tags	2016-06-03 12:02:20.454921
92	11	1765	Card	\N	\N	tags	2016-06-03 12:02:20.486802
93	1	1766	Card	\N	\N	tags	2016-06-03 12:02:21.001836
94	11	1766	Card	\N	\N	tags	2016-06-03 12:02:21.041759
95	1	1767	Card	\N	\N	tags	2016-06-03 12:02:21.193347
96	11	1767	Card	\N	\N	tags	2016-06-03 12:02:21.214439
97	1	1768	Card	\N	\N	tags	2016-06-03 12:02:21.775625
98	13	1768	Card	\N	\N	tags	2016-06-03 12:02:21.808294
99	14	1768	Card	\N	\N	tags	2016-06-03 12:02:21.837272
100	1	1769	Card	\N	\N	tags	2016-06-03 12:02:21.984282
101	14	1769	Card	\N	\N	tags	2016-06-03 12:02:22.012419
102	1	1770	Card	\N	\N	tags	2016-06-03 12:02:22.126181
103	14	1770	Card	\N	\N	tags	2016-06-03 12:02:22.14758
104	11	1770	Card	\N	\N	tags	2016-06-03 12:02:22.175549
105	1	1771	Card	\N	\N	tags	2016-06-03 12:02:22.299652
106	14	1771	Card	\N	\N	tags	2016-06-03 12:02:22.320196
107	11	1771	Card	\N	\N	tags	2016-06-03 12:02:22.343053
108	15	1772	Card	\N	\N	tags	2016-06-03 12:02:22.797672
109	16	1772	Card	\N	\N	tags	2016-06-03 12:02:22.818827
110	15	1773	Card	\N	\N	tags	2016-06-03 12:02:24.05201
111	16	1773	Card	\N	\N	tags	2016-06-03 12:02:24.080386
112	15	1774	Card	\N	\N	tags	2016-06-03 12:02:25.269997
113	16	1774	Card	\N	\N	tags	2016-06-03 12:02:25.298002
114	15	1775	Card	\N	\N	tags	2016-06-03 12:02:25.474963
115	17	1775	Card	\N	\N	tags	2016-06-03 12:02:25.497039
116	15	1776	Card	\N	\N	tags	2016-06-03 12:02:25.595471
117	18	1776	Card	\N	\N	tags	2016-06-03 12:02:25.615241
118	19	1777	Card	\N	\N	tags	2016-06-03 12:02:25.963306
119	20	1777	Card	\N	\N	tags	2016-06-03 12:02:25.983804
120	21	1777	Card	\N	\N	tags	2016-06-03 12:02:26.004092
121	19	1778	Card	\N	\N	tags	2016-06-03 12:02:26.672848
122	20	1778	Card	\N	\N	tags	2016-06-03 12:02:26.719936
123	21	1778	Card	\N	\N	tags	2016-06-03 12:02:26.75386
124	19	1779	Card	\N	\N	tags	2016-06-03 12:02:28.564396
125	20	1779	Card	\N	\N	tags	2016-06-03 12:02:28.598733
126	21	1779	Card	\N	\N	tags	2016-06-03 12:02:28.633385
127	19	1780	Card	\N	\N	tags	2016-06-03 12:02:29.695661
128	20	1780	Card	\N	\N	tags	2016-06-03 12:02:29.729174
129	21	1780	Card	\N	\N	tags	2016-06-03 12:02:29.759178
130	19	1781	Card	\N	\N	tags	2016-06-03 12:02:30.444459
131	20	1781	Card	\N	\N	tags	2016-06-03 12:02:30.484468
132	21	1781	Card	\N	\N	tags	2016-06-03 12:02:30.520769
133	19	1782	Card	\N	\N	tags	2016-06-03 12:02:31.327775
134	20	1782	Card	\N	\N	tags	2016-06-03 12:02:31.355838
135	21	1782	Card	\N	\N	tags	2016-06-03 12:02:31.387592
136	19	1783	Card	\N	\N	tags	2016-06-03 12:02:32.222634
137	20	1783	Card	\N	\N	tags	2016-06-03 12:02:32.256235
138	21	1783	Card	\N	\N	tags	2016-06-03 12:02:32.28313
139	19	1784	Card	\N	\N	tags	2016-06-03 12:02:32.539309
140	22	1784	Card	\N	\N	tags	2016-06-03 12:02:32.563558
141	19	1785	Card	\N	\N	tags	2016-06-03 12:02:34.391088
142	22	1785	Card	\N	\N	tags	2016-06-03 12:02:34.412672
143	19	1786	Card	\N	\N	tags	2016-06-03 12:02:34.621261
144	23	1787	Card	\N	\N	tags	2016-06-03 12:02:34.815755
145	23	1788	Card	\N	\N	tags	2016-06-03 12:02:34.940269
146	24	1789	Card	\N	\N	tags	2016-06-03 12:02:35.04983
147	19	1789	Card	\N	\N	tags	2016-06-03 12:02:35.073082
148	25	1790	Card	\N	\N	tags	2016-06-03 12:02:35.355249
149	26	1790	Card	\N	\N	tags	2016-06-03 12:02:35.37661
150	25	1791	Card	\N	\N	tags	2016-06-03 12:02:36.746423
151	26	1791	Card	\N	\N	tags	2016-06-03 12:02:36.776893
152	25	1792	Card	\N	\N	tags	2016-06-03 12:02:37.866034
153	26	1792	Card	\N	\N	tags	2016-06-03 12:02:37.891293
154	25	1793	Card	\N	\N	tags	2016-06-03 12:02:39.126799
155	26	1793	Card	\N	\N	tags	2016-06-03 12:02:39.157718
156	25	1794	Card	\N	\N	tags	2016-06-03 12:02:39.268709
157	26	1794	Card	\N	\N	tags	2016-06-03 12:02:39.285849
158	25	1795	Card	\N	\N	tags	2016-06-03 12:02:40.052784
159	26	1795	Card	\N	\N	tags	2016-06-03 12:02:40.086124
160	25	1796	Card	\N	\N	tags	2016-06-03 12:02:41.821336
161	26	1796	Card	\N	\N	tags	2016-06-03 12:02:41.853139
162	25	1797	Card	\N	\N	tags	2016-06-03 12:02:42.88705
163	26	1797	Card	\N	\N	tags	2016-06-03 12:02:42.916903
164	25	1798	Card	\N	\N	tags	2016-06-03 12:02:43.697304
165	26	1798	Card	\N	\N	tags	2016-06-03 12:02:43.731847
166	25	1799	Card	\N	\N	tags	2016-06-03 12:02:44.004001
167	26	1799	Card	\N	\N	tags	2016-06-03 12:02:44.029381
168	25	1800	Card	\N	\N	tags	2016-06-03 12:02:45.966158
169	26	1800	Card	\N	\N	tags	2016-06-03 12:02:45.994308
170	25	1801	Card	\N	\N	tags	2016-06-03 12:02:46.982368
171	26	1801	Card	\N	\N	tags	2016-06-03 12:02:47.036068
172	27	1802	Card	\N	\N	tags	2016-06-03 12:02:48.977917
173	28	1802	Card	\N	\N	tags	2016-06-03 12:02:49.009559
174	27	1803	Card	\N	\N	tags	2016-06-03 12:02:49.305202
175	29	1803	Card	\N	\N	tags	2016-06-03 12:02:49.326725
176	30	1804	Card	\N	\N	tags	2016-06-03 12:02:49.592755
177	31	1804	Card	\N	\N	tags	2016-06-03 12:02:49.613479
178	32	1804	Card	\N	\N	tags	2016-06-03 12:02:49.633006
179	33	1805	Card	\N	\N	tags	2016-06-03 12:02:50.015297
180	34	1805	Card	\N	\N	tags	2016-06-03 12:02:50.034656
181	33	1806	Card	\N	\N	tags	2016-06-03 12:02:50.265076
182	35	1806	Card	\N	\N	tags	2016-06-03 12:02:50.286222
183	36	1806	Card	\N	\N	tags	2016-06-03 12:02:50.313499
184	37	1807	Card	\N	\N	tags	2016-06-03 12:02:50.490687
185	38	1808	Card	\N	\N	tags	2016-06-03 12:02:51.019169
186	39	1808	Card	\N	\N	tags	2016-06-03 12:02:51.039245
187	40	1808	Card	\N	\N	tags	2016-06-03 12:02:51.061608
188	38	1809	Card	\N	\N	tags	2016-06-03 12:02:51.232281
189	41	1809	Card	\N	\N	tags	2016-06-03 12:02:51.253622
190	40	1809	Card	\N	\N	tags	2016-06-03 12:02:51.272936
191	38	1810	Card	\N	\N	tags	2016-06-03 12:02:51.401578
192	42	1810	Card	\N	\N	tags	2016-06-03 12:02:51.422653
193	43	1811	Card	\N	\N	tags	2016-06-03 12:02:51.526436
194	38	1811	Card	\N	\N	tags	2016-06-03 12:02:51.552014
195	44	1812	Card	\N	\N	tags	2016-06-03 12:02:51.74181
196	38	1812	Card	\N	\N	tags	2016-06-03 12:02:51.759727
197	45	1812	Card	\N	\N	tags	2016-06-03 12:02:51.778833
198	44	1813	Card	\N	\N	tags	2016-06-03 12:02:51.98082
199	38	1813	Card	\N	\N	tags	2016-06-03 12:02:51.999808
200	45	1813	Card	\N	\N	tags	2016-06-03 12:02:52.020506
201	45	1814	Card	\N	\N	tags	2016-06-03 12:02:52.338118
202	23	1814	Card	\N	\N	tags	2016-06-03 12:02:52.356667
203	46	1814	Card	\N	\N	tags	2016-06-03 12:02:52.374446
204	47	1815	Card	\N	\N	tags	2016-06-03 12:02:52.524804
205	23	1815	Card	\N	\N	tags	2016-06-03 12:02:52.546922
206	45	1815	Card	\N	\N	tags	2016-06-03 12:02:52.571953
207	48	1816	Card	\N	\N	tags	2016-06-03 12:02:52.959755
208	49	1816	Card	\N	\N	tags	2016-06-03 12:02:52.980073
209	50	1817	Card	\N	\N	tags	2016-06-03 12:02:53.096142
210	49	1817	Card	\N	\N	tags	2016-06-03 12:02:53.114753
211	50	1818	Card	\N	\N	tags	2016-06-03 12:02:53.256715
212	49	1818	Card	\N	\N	tags	2016-06-03 12:02:53.276766
213	51	1819	Card	\N	\N	tags	2016-06-03 12:02:53.41477
214	49	1819	Card	\N	\N	tags	2016-06-03 12:02:53.437348
215	52	1820	Card	\N	\N	tags	2016-06-03 12:02:53.528907
216	49	1820	Card	\N	\N	tags	2016-06-03 12:02:53.548667
217	53	1821	Card	\N	\N	tags	2016-06-03 12:02:53.658992
218	54	1821	Card	\N	\N	tags	2016-06-03 12:02:53.676529
219	49	1821	Card	\N	\N	tags	2016-06-03 12:02:53.695788
220	53	1822	Card	\N	\N	tags	2016-06-03 12:02:53.787744
221	54	1822	Card	\N	\N	tags	2016-06-03 12:02:53.805833
222	49	1822	Card	\N	\N	tags	2016-06-03 12:02:53.829011
223	53	1823	Card	\N	\N	tags	2016-06-03 12:02:54.016906
224	55	1823	Card	\N	\N	tags	2016-06-03 12:02:54.038016
225	49	1823	Card	\N	\N	tags	2016-06-03 12:02:54.056388
226	53	1824	Card	\N	\N	tags	2016-06-03 12:02:54.174692
227	56	1824	Card	\N	\N	tags	2016-06-03 12:02:54.197183
228	49	1824	Card	\N	\N	tags	2016-06-03 12:02:54.21831
229	57	1825	Card	\N	\N	tags	2016-06-03 12:02:54.323459
230	49	1825	Card	\N	\N	tags	2016-06-03 12:02:54.342817
231	35	1826	Card	\N	\N	tags	2016-06-03 12:02:54.428858
232	49	1826	Card	\N	\N	tags	2016-06-03 12:02:54.447832
233	58	1827	Card	\N	\N	tags	2016-06-03 12:02:54.550712
234	33	1827	Card	\N	\N	tags	2016-06-03 12:02:54.575226
235	49	1827	Card	\N	\N	tags	2016-06-03 12:02:54.596941
236	59	1828	Card	\N	\N	tags	2016-06-03 12:02:54.759615
237	60	1828	Card	\N	\N	tags	2016-06-03 12:02:54.780256
238	49	1828	Card	\N	\N	tags	2016-06-03 12:02:54.804365
239	60	1829	Card	\N	\N	tags	2016-06-03 12:02:55.095782
240	59	1829	Card	\N	\N	tags	2016-06-03 12:02:55.112347
241	49	1829	Card	\N	\N	tags	2016-06-03 12:02:55.132755
242	61	1830	Card	\N	\N	tags	2016-06-03 12:02:55.285547
243	62	1830	Card	\N	\N	tags	2016-06-03 12:02:55.306368
244	49	1830	Card	\N	\N	tags	2016-06-03 12:02:55.324033
245	61	1831	Card	\N	\N	tags	2016-06-03 12:02:55.446749
246	62	1831	Card	\N	\N	tags	2016-06-03 12:02:55.467209
247	49	1831	Card	\N	\N	tags	2016-06-03 12:02:55.487886
248	53	1832	Card	\N	\N	tags	2016-06-03 12:02:55.930879
249	63	1832	Card	\N	\N	tags	2016-06-03 12:02:55.955827
250	49	1832	Card	\N	\N	tags	2016-06-03 12:02:55.976703
251	64	1833	Card	\N	\N	tags	2016-06-03 12:02:56.094173
252	63	1833	Card	\N	\N	tags	2016-06-03 12:02:56.112688
253	49	1833	Card	\N	\N	tags	2016-06-03 12:02:56.1317
254	64	1834	Card	\N	\N	tags	2016-06-03 12:02:56.265794
255	63	1834	Card	\N	\N	tags	2016-06-03 12:02:56.285764
256	49	1834	Card	\N	\N	tags	2016-06-03 12:02:56.305184
257	64	1835	Card	\N	\N	tags	2016-06-03 12:02:56.400417
258	63	1835	Card	\N	\N	tags	2016-06-03 12:02:56.419125
259	49	1835	Card	\N	\N	tags	2016-06-03 12:02:56.446862
260	64	1836	Card	\N	\N	tags	2016-06-03 12:02:56.58038
261	63	1836	Card	\N	\N	tags	2016-06-03 12:02:56.602334
262	65	1836	Card	\N	\N	tags	2016-06-03 12:02:56.627486
263	64	1837	Card	\N	\N	tags	2016-06-03 12:02:56.76059
264	63	1837	Card	\N	\N	tags	2016-06-03 12:02:56.778526
265	65	1837	Card	\N	\N	tags	2016-06-03 12:02:56.797467
266	64	1838	Card	\N	\N	tags	2016-06-03 12:02:56.912631
267	63	1838	Card	\N	\N	tags	2016-06-03 12:02:56.932646
268	65	1838	Card	\N	\N	tags	2016-06-03 12:02:56.959494
269	64	1839	Card	\N	\N	tags	2016-06-03 12:02:57.079052
270	63	1839	Card	\N	\N	tags	2016-06-03 12:02:57.097635
271	65	1839	Card	\N	\N	tags	2016-06-03 12:02:57.115164
272	64	1840	Card	\N	\N	tags	2016-06-03 12:02:57.35677
273	63	1840	Card	\N	\N	tags	2016-06-03 12:02:57.375335
274	65	1840	Card	\N	\N	tags	2016-06-03 12:02:57.394112
275	47	1841	Card	\N	\N	tags	2016-06-03 12:02:57.939244
276	66	1841	Card	\N	\N	tags	2016-06-03 12:02:57.963248
277	66	1842	Card	\N	\N	tags	2016-06-03 12:02:58.108406
278	67	1842	Card	\N	\N	tags	2016-06-03 12:02:58.128178
279	66	1843	Card	\N	\N	tags	2016-06-03 12:02:58.236521
280	68	1843	Card	\N	\N	tags	2016-06-03 12:02:58.256579
281	66	1844	Card	\N	\N	tags	2016-06-03 12:02:58.409611
282	69	1844	Card	\N	\N	tags	2016-06-03 12:02:58.429968
283	66	1845	Card	\N	\N	tags	2016-06-03 12:02:58.573405
284	70	1845	Card	\N	\N	tags	2016-06-03 12:02:58.595598
285	66	1846	Card	\N	\N	tags	2016-06-03 12:02:58.975654
286	47	1846	Card	\N	\N	tags	2016-06-03 12:02:58.995506
287	66	1847	Card	\N	\N	tags	2016-06-03 12:02:59.180265
288	71	1847	Card	\N	\N	tags	2016-06-03 12:02:59.208632
289	66	1848	Card	\N	\N	tags	2016-06-03 12:02:59.381389
290	72	1848	Card	\N	\N	tags	2016-06-03 12:02:59.40429
291	73	1848	Card	\N	\N	tags	2016-06-03 12:02:59.427462
292	66	1849	Card	\N	\N	tags	2016-06-03 12:02:59.578863
293	74	1849	Card	\N	\N	tags	2016-06-03 12:02:59.603262
294	72	1849	Card	\N	\N	tags	2016-06-03 12:02:59.623789
295	66	1850	Card	\N	\N	tags	2016-06-03 12:02:59.737968
296	75	1850	Card	\N	\N	tags	2016-06-03 12:02:59.759524
297	66	1851	Card	\N	\N	tags	2016-06-03 12:02:59.894266
298	74	1851	Card	\N	\N	tags	2016-06-03 12:02:59.915474
299	66	1852	Card	\N	\N	tags	2016-06-03 12:03:00.024075
300	68	1852	Card	\N	\N	tags	2016-06-03 12:03:00.047259
301	76	1853	Card	\N	\N	tags	2016-06-03 12:03:00.444851
302	77	1853	Card	\N	\N	tags	2016-06-03 12:03:00.463677
303	76	1854	Card	\N	\N	tags	2016-06-03 12:03:00.628282
304	78	1854	Card	\N	\N	tags	2016-06-03 12:03:00.649748
305	76	1855	Card	\N	\N	tags	2016-06-03 12:03:01.402146
306	77	1855	Card	\N	\N	tags	2016-06-03 12:03:01.42367
307	76	1856	Card	\N	\N	tags	2016-06-03 12:03:01.645165
308	77	1856	Card	\N	\N	tags	2016-06-03 12:03:01.66461
309	76	1857	Card	\N	\N	tags	2016-06-03 12:03:02.156917
310	77	1857	Card	\N	\N	tags	2016-06-03 12:03:02.175817
311	79	1858	Card	\N	\N	tags	2016-06-03 12:03:02.810532
312	80	1858	Card	\N	\N	tags	2016-06-03 12:03:02.833902
313	79	1859	Card	\N	\N	tags	2016-06-03 12:03:03.105222
314	81	1859	Card	\N	\N	tags	2016-06-03 12:03:03.122872
315	79	1860	Card	\N	\N	tags	2016-06-03 12:03:03.329424
316	66	1860	Card	\N	\N	tags	2016-06-03 12:03:03.348366
317	79	1861	Card	\N	\N	tags	2016-06-03 12:03:03.479591
318	66	1861	Card	\N	\N	tags	2016-06-03 12:03:03.503365
319	60	1861	Card	\N	\N	tags	2016-06-03 12:03:03.523147
320	79	1862	Card	\N	\N	tags	2016-06-03 12:03:03.644189
321	66	1862	Card	\N	\N	tags	2016-06-03 12:03:03.662895
322	79	1863	Card	\N	\N	tags	2016-06-03 12:03:03.785812
323	82	1863	Card	\N	\N	tags	2016-06-03 12:03:03.803897
324	32	1864	Card	\N	\N	tags	2016-06-03 12:03:04.161828
325	83	1864	Card	\N	\N	tags	2016-06-03 12:03:04.179951
326	84	1864	Card	\N	\N	tags	2016-06-03 12:03:04.19992
327	32	1865	Card	\N	\N	tags	2016-06-03 12:03:04.320812
328	85	1865	Card	\N	\N	tags	2016-06-03 12:03:04.341231
329	66	1865	Card	\N	\N	tags	2016-06-03 12:03:04.36189
330	32	1866	Card	\N	\N	tags	2016-06-03 12:03:04.487278
331	86	1866	Card	\N	\N	tags	2016-06-03 12:03:04.511669
332	32	1867	Card	\N	\N	tags	2016-06-03 12:03:04.639378
333	87	1867	Card	\N	\N	tags	2016-06-03 12:03:04.664329
334	32	1868	Card	\N	\N	tags	2016-06-03 12:03:04.843323
335	24	1868	Card	\N	\N	tags	2016-06-03 12:03:04.870171
336	32	1869	Card	\N	\N	tags	2016-06-03 12:03:04.991519
337	84	1869	Card	\N	\N	tags	2016-06-03 12:03:05.007279
338	88	1869	Card	\N	\N	tags	2016-06-03 12:03:05.026743
339	32	1870	Card	\N	\N	tags	2016-06-03 12:03:05.142692
340	89	1870	Card	\N	\N	tags	2016-06-03 12:03:05.163454
341	66	1870	Card	\N	\N	tags	2016-06-03 12:03:05.182702
342	32	1871	Card	\N	\N	tags	2016-06-03 12:03:05.287684
343	90	1871	Card	\N	\N	tags	2016-06-03 12:03:05.309041
344	32	1872	Card	\N	\N	tags	2016-06-03 12:03:05.393579
345	86	1872	Card	\N	\N	tags	2016-06-03 12:03:05.411292
346	32	1873	Card	\N	\N	tags	2016-06-03 12:03:05.516988
347	22	1873	Card	\N	\N	tags	2016-06-03 12:03:05.53495
348	32	1874	Card	\N	\N	tags	2016-06-03 12:03:05.644567
349	91	1874	Card	\N	\N	tags	2016-06-03 12:03:05.665777
350	32	1875	Card	\N	\N	tags	2016-06-03 12:03:05.823396
351	92	1875	Card	\N	\N	tags	2016-06-03 12:03:05.843649
352	32	1876	Card	\N	\N	tags	2016-06-03 12:03:05.996753
353	93	1876	Card	\N	\N	tags	2016-06-03 12:03:06.01792
354	32	1877	Card	\N	\N	tags	2016-06-03 12:03:06.115337
355	94	1877	Card	\N	\N	tags	2016-06-03 12:03:06.135138
356	32	1878	Card	\N	\N	tags	2016-06-03 12:03:06.306863
357	95	1878	Card	\N	\N	tags	2016-06-03 12:03:06.327502
358	32	1879	Card	\N	\N	tags	2016-06-03 12:03:06.428576
359	96	1879	Card	\N	\N	tags	2016-06-03 12:03:06.447437
360	32	1880	Card	\N	\N	tags	2016-06-03 12:03:06.573787
361	97	1880	Card	\N	\N	tags	2016-06-03 12:03:06.594221
362	98	1880	Card	\N	\N	tags	2016-06-03 12:03:06.618454
363	32	1881	Card	\N	\N	tags	2016-06-03 12:03:06.740996
364	99	1881	Card	\N	\N	tags	2016-06-03 12:03:06.764536
365	32	1882	Card	\N	\N	tags	2016-06-03 12:03:06.869974
366	81	1882	Card	\N	\N	tags	2016-06-03 12:03:06.890809
367	100	1883	Card	\N	\N	tags	2016-06-03 12:03:07.183537
368	101	1883	Card	\N	\N	tags	2016-06-03 12:03:07.203181
369	66	1883	Card	\N	\N	tags	2016-06-03 12:03:07.221208
370	100	1884	Card	\N	\N	tags	2016-06-03 12:03:07.327096
371	101	1884	Card	\N	\N	tags	2016-06-03 12:03:07.342334
372	66	1884	Card	\N	\N	tags	2016-06-03 12:03:07.362042
373	100	1885	Card	\N	\N	tags	2016-06-03 12:03:07.462606
374	24	1885	Card	\N	\N	tags	2016-06-03 12:03:07.483482
375	66	1885	Card	\N	\N	tags	2016-06-03 12:03:07.501732
376	100	1886	Card	\N	\N	tags	2016-06-03 12:03:07.650683
377	102	1886	Card	\N	\N	tags	2016-06-03 12:03:07.671657
378	100	1887	Card	\N	\N	tags	2016-06-03 12:03:07.804306
379	102	1887	Card	\N	\N	tags	2016-06-03 12:03:07.82315
380	66	1887	Card	\N	\N	tags	2016-06-03 12:03:07.840276
381	100	1888	Card	\N	\N	tags	2016-06-03 12:03:07.952221
382	103	1888	Card	\N	\N	tags	2016-06-03 12:03:07.976602
383	66	1888	Card	\N	\N	tags	2016-06-03 12:03:07.99773
384	100	1889	Card	\N	\N	tags	2016-06-03 12:03:08.109567
385	104	1889	Card	\N	\N	tags	2016-06-03 12:03:08.127721
386	66	1889	Card	\N	\N	tags	2016-06-03 12:03:08.146467
387	100	1890	Card	\N	\N	tags	2016-06-03 12:03:08.263235
388	105	1890	Card	\N	\N	tags	2016-06-03 12:03:08.28295
389	66	1890	Card	\N	\N	tags	2016-06-03 12:03:08.300775
390	100	1891	Card	\N	\N	tags	2016-06-03 12:03:08.449214
391	106	1891	Card	\N	\N	tags	2016-06-03 12:03:08.46853
392	100	1892	Card	\N	\N	tags	2016-06-03 12:03:08.670669
393	66	1892	Card	\N	\N	tags	2016-06-03 12:03:08.690756
394	60	1893	Card	\N	\N	tags	2016-06-03 12:03:10.847997
395	107	1893	Card	\N	\N	tags	2016-06-03 12:03:10.874361
396	47	1893	Card	\N	\N	tags	2016-06-03 12:03:10.902785
397	1	1894	Card	\N	\N	tags	2016-06-03 12:03:11.671459
398	13	1894	Card	\N	\N	tags	2016-06-03 12:03:11.703363
399	1	1895	Card	\N	\N	tags	2016-06-03 12:03:14.23895
400	13	1895	Card	\N	\N	tags	2016-06-03 12:03:14.259416
401	60	1896	Card	\N	\N	tags	2016-06-03 12:03:15.519849
402	107	1896	Card	\N	\N	tags	2016-06-03 12:03:15.549637
403	47	1896	Card	\N	\N	tags	2016-06-03 12:03:15.58149
404	1	1897	Card	\N	\N	tags	2016-06-03 12:03:16.539637
405	13	1897	Card	\N	\N	tags	2016-06-03 12:03:16.568407
406	60	1899	Card	\N	\N	tags	2016-06-03 12:03:17.372438
407	107	1899	Card	\N	\N	tags	2016-06-03 12:03:17.401398
408	47	1899	Card	\N	\N	tags	2016-06-03 12:03:17.428561
409	1	1900	Card	\N	\N	tags	2016-06-03 12:03:17.99706
410	13	1900	Card	\N	\N	tags	2016-06-03 12:03:18.026025
411	1	1901	Card	\N	\N	tags	2016-06-03 12:03:18.665361
412	13	1901	Card	\N	\N	tags	2016-06-03 12:03:18.693216
413	60	1902	Card	\N	\N	tags	2016-06-03 12:03:19.401102
414	107	1902	Card	\N	\N	tags	2016-06-03 12:03:19.428471
415	47	1902	Card	\N	\N	tags	2016-06-03 12:03:19.459356
416	1	1903	Card	\N	\N	tags	2016-06-03 12:03:20.175547
417	13	1903	Card	\N	\N	tags	2016-06-03 12:03:20.206562
418	1	1905	Card	\N	\N	tags	2016-06-03 12:04:46.269235
419	2	1905	Card	\N	\N	tags	2016-06-03 12:04:46.286556
420	1	1906	Card	\N	\N	tags	2016-06-03 12:04:46.424363
421	2	1906	Card	\N	\N	tags	2016-06-03 12:04:46.441828
422	3	1906	Card	\N	\N	tags	2016-06-03 12:04:46.456183
423	1	1907	Card	\N	\N	tags	2016-06-03 12:04:46.52919
424	4	1907	Card	\N	\N	tags	2016-06-03 12:04:46.543576
425	5	1907	Card	\N	\N	tags	2016-06-03 12:04:46.557681
426	1	1908	Card	\N	\N	tags	2016-06-03 12:04:48.414838
427	4	1908	Card	\N	\N	tags	2016-06-03 12:04:48.443929
428	5	1908	Card	\N	\N	tags	2016-06-03 12:04:48.467809
429	1	1909	Card	\N	\N	tags	2016-06-03 12:04:50.887639
430	4	1909	Card	\N	\N	tags	2016-06-03 12:04:50.922077
431	6	1909	Card	\N	\N	tags	2016-06-03 12:04:50.949499
432	1	1910	Card	\N	\N	tags	2016-06-03 12:04:53.194027
433	4	1910	Card	\N	\N	tags	2016-06-03 12:04:53.224063
434	6	1910	Card	\N	\N	tags	2016-06-03 12:04:53.246553
435	1	1911	Card	\N	\N	tags	2016-06-03 12:04:55.745586
436	4	1911	Card	\N	\N	tags	2016-06-03 12:04:55.777017
437	6	1911	Card	\N	\N	tags	2016-06-03 12:04:55.840694
438	1	1912	Card	\N	\N	tags	2016-06-03 12:04:58.123527
439	4	1912	Card	\N	\N	tags	2016-06-03 12:04:58.151677
440	6	1912	Card	\N	\N	tags	2016-06-03 12:04:58.171556
441	1	1913	Card	\N	\N	tags	2016-06-03 12:05:00.792063
442	4	1913	Card	\N	\N	tags	2016-06-03 12:05:00.824057
443	6	1913	Card	\N	\N	tags	2016-06-03 12:05:00.845515
444	1	1914	Card	\N	\N	tags	2016-06-03 12:05:03.150089
445	4	1914	Card	\N	\N	tags	2016-06-03 12:05:03.175856
446	6	1914	Card	\N	\N	tags	2016-06-03 12:05:03.200498
447	1	1915	Card	\N	\N	tags	2016-06-03 12:05:05.365724
448	4	1915	Card	\N	\N	tags	2016-06-03 12:05:05.395337
449	6	1915	Card	\N	\N	tags	2016-06-03 12:05:05.415547
450	1	1916	Card	\N	\N	tags	2016-06-03 12:05:09.271001
451	4	1916	Card	\N	\N	tags	2016-06-03 12:05:09.301358
452	6	1916	Card	\N	\N	tags	2016-06-03 12:05:09.33198
453	1	1917	Card	\N	\N	tags	2016-06-03 12:05:11.713453
454	4	1917	Card	\N	\N	tags	2016-06-03 12:05:11.744226
455	7	1917	Card	\N	\N	tags	2016-06-03 12:05:11.768782
456	1	1918	Card	\N	\N	tags	2016-06-03 12:05:13.406423
457	4	1918	Card	\N	\N	tags	2016-06-03 12:05:13.434077
458	6	1918	Card	\N	\N	tags	2016-06-03 12:05:13.45809
459	1	1919	Card	\N	\N	tags	2016-06-03 12:05:16.079916
460	4	1919	Card	\N	\N	tags	2016-06-03 12:05:16.106782
461	6	1919	Card	\N	\N	tags	2016-06-03 12:05:16.130619
462	1	1920	Card	\N	\N	tags	2016-06-03 12:05:16.403688
463	4	1920	Card	\N	\N	tags	2016-06-03 12:05:16.426421
464	6	1920	Card	\N	\N	tags	2016-06-03 12:05:16.450264
465	1	1921	Card	\N	\N	tags	2016-06-03 12:05:17.021488
466	4	1921	Card	\N	\N	tags	2016-06-03 12:05:17.042895
467	6	1921	Card	\N	\N	tags	2016-06-03 12:05:17.065865
468	1	1922	Card	\N	\N	tags	2016-06-03 12:05:17.16105
469	4	1922	Card	\N	\N	tags	2016-06-03 12:05:17.179248
470	6	1922	Card	\N	\N	tags	2016-06-03 12:05:17.199838
471	1	1923	Card	\N	\N	tags	2016-06-03 12:05:17.343235
472	4	1923	Card	\N	\N	tags	2016-06-03 12:05:17.365266
473	8	1923	Card	\N	\N	tags	2016-06-03 12:05:17.385038
474	1	1924	Card	\N	\N	tags	2016-06-03 12:05:17.534581
475	4	1924	Card	\N	\N	tags	2016-06-03 12:05:17.554362
476	1	1925	Card	\N	\N	tags	2016-06-03 12:05:17.692413
477	9	1925	Card	\N	\N	tags	2016-06-03 12:05:17.713849
478	1	1926	Card	\N	\N	tags	2016-06-03 12:05:17.891182
479	10	1926	Card	\N	\N	tags	2016-06-03 12:05:17.910066
480	1	1927	Card	\N	\N	tags	2016-06-03 12:05:18.095748
481	11	1927	Card	\N	\N	tags	2016-06-03 12:05:18.117019
482	1	1928	Card	\N	\N	tags	2016-06-03 12:05:18.221945
483	11	1928	Card	\N	\N	tags	2016-06-03 12:05:18.245464
484	1	1929	Card	\N	\N	tags	2016-06-03 12:05:18.350238
485	11	1929	Card	\N	\N	tags	2016-06-03 12:05:18.37061
486	1	1930	Card	\N	\N	tags	2016-06-03 12:05:19.37123
487	11	1930	Card	\N	\N	tags	2016-06-03 12:05:19.40415
488	1	1931	Card	\N	\N	tags	2016-06-03 12:05:19.532942
489	12	1931	Card	\N	\N	tags	2016-06-03 12:05:19.556728
490	1	1932	Card	\N	\N	tags	2016-06-03 12:05:19.658359
491	11	1932	Card	\N	\N	tags	2016-06-03 12:05:19.680292
492	1	1933	Card	\N	\N	tags	2016-06-03 12:05:20.185218
493	11	1933	Card	\N	\N	tags	2016-06-03 12:05:20.226866
494	1	1934	Card	\N	\N	tags	2016-06-03 12:05:20.778565
495	11	1934	Card	\N	\N	tags	2016-06-03 12:05:20.803678
496	1	1935	Card	\N	\N	tags	2016-06-03 12:05:21.301042
497	11	1935	Card	\N	\N	tags	2016-06-03 12:05:21.331883
498	1	1936	Card	\N	\N	tags	2016-06-03 12:05:21.443973
499	11	1936	Card	\N	\N	tags	2016-06-03 12:05:21.463969
500	1	1937	Card	\N	\N	tags	2016-06-03 12:05:21.942313
501	13	1937	Card	\N	\N	tags	2016-06-03 12:05:21.980469
502	14	1937	Card	\N	\N	tags	2016-06-03 12:05:22.00558
503	1	1938	Card	\N	\N	tags	2016-06-03 12:05:22.164584
504	14	1938	Card	\N	\N	tags	2016-06-03 12:05:22.181404
505	1	1939	Card	\N	\N	tags	2016-06-03 12:05:22.2774
506	14	1939	Card	\N	\N	tags	2016-06-03 12:05:22.299283
507	11	1939	Card	\N	\N	tags	2016-06-03 12:05:22.319806
508	1	1940	Card	\N	\N	tags	2016-06-03 12:05:22.420805
509	14	1940	Card	\N	\N	tags	2016-06-03 12:05:22.444224
510	11	1940	Card	\N	\N	tags	2016-06-03 12:05:22.462674
511	15	1941	Card	\N	\N	tags	2016-06-03 12:05:22.798064
512	16	1941	Card	\N	\N	tags	2016-06-03 12:05:22.820657
513	15	1942	Card	\N	\N	tags	2016-06-03 12:05:24.020152
514	16	1942	Card	\N	\N	tags	2016-06-03 12:05:24.050522
515	15	1943	Card	\N	\N	tags	2016-06-03 12:05:25.041996
516	16	1943	Card	\N	\N	tags	2016-06-03 12:05:25.078397
517	15	1944	Card	\N	\N	tags	2016-06-03 12:05:25.26206
518	17	1944	Card	\N	\N	tags	2016-06-03 12:05:25.280222
519	15	1945	Card	\N	\N	tags	2016-06-03 12:05:25.374842
520	18	1945	Card	\N	\N	tags	2016-06-03 12:05:25.396604
521	19	1946	Card	\N	\N	tags	2016-06-03 12:05:25.704847
522	20	1946	Card	\N	\N	tags	2016-06-03 12:05:25.729275
523	21	1946	Card	\N	\N	tags	2016-06-03 12:05:25.75139
524	19	1947	Card	\N	\N	tags	2016-06-03 12:05:26.475533
525	20	1947	Card	\N	\N	tags	2016-06-03 12:05:26.509106
526	21	1947	Card	\N	\N	tags	2016-06-03 12:05:26.540839
527	19	1948	Card	\N	\N	tags	2016-06-03 12:05:27.94328
528	20	1948	Card	\N	\N	tags	2016-06-03 12:05:27.968188
529	21	1948	Card	\N	\N	tags	2016-06-03 12:05:27.994391
530	19	1949	Card	\N	\N	tags	2016-06-03 12:05:28.937601
531	20	1949	Card	\N	\N	tags	2016-06-03 12:05:28.972433
532	21	1949	Card	\N	\N	tags	2016-06-03 12:05:29.002389
533	19	1950	Card	\N	\N	tags	2016-06-03 12:05:29.741982
534	20	1950	Card	\N	\N	tags	2016-06-03 12:05:29.768261
535	21	1950	Card	\N	\N	tags	2016-06-03 12:05:29.796236
536	19	1951	Card	\N	\N	tags	2016-06-03 12:05:30.424634
537	20	1951	Card	\N	\N	tags	2016-06-03 12:05:30.470513
538	21	1951	Card	\N	\N	tags	2016-06-03 12:05:30.502824
539	19	1952	Card	\N	\N	tags	2016-06-03 12:05:31.489904
540	20	1952	Card	\N	\N	tags	2016-06-03 12:05:31.533798
541	21	1952	Card	\N	\N	tags	2016-06-03 12:05:31.562169
542	19	1953	Card	\N	\N	tags	2016-06-03 12:05:32.041741
543	22	1953	Card	\N	\N	tags	2016-06-03 12:05:32.061966
544	19	1954	Card	\N	\N	tags	2016-06-03 12:05:33.530933
545	22	1954	Card	\N	\N	tags	2016-06-03 12:05:33.565298
546	19	1955	Card	\N	\N	tags	2016-06-03 12:05:33.792464
547	23	1956	Card	\N	\N	tags	2016-06-03 12:05:33.994024
548	23	1957	Card	\N	\N	tags	2016-06-03 12:05:34.108485
549	24	1958	Card	\N	\N	tags	2016-06-03 12:05:34.212243
550	19	1958	Card	\N	\N	tags	2016-06-03 12:05:34.23242
551	25	1959	Card	\N	\N	tags	2016-06-03 12:05:34.509782
552	26	1959	Card	\N	\N	tags	2016-06-03 12:05:34.528595
553	25	1960	Card	\N	\N	tags	2016-06-03 12:05:35.895895
554	26	1960	Card	\N	\N	tags	2016-06-03 12:05:35.928846
555	25	1961	Card	\N	\N	tags	2016-06-03 12:05:37.017054
556	26	1961	Card	\N	\N	tags	2016-06-03 12:05:37.048528
557	25	1962	Card	\N	\N	tags	2016-06-03 12:05:38.32206
558	26	1962	Card	\N	\N	tags	2016-06-03 12:05:38.351718
559	25	1963	Card	\N	\N	tags	2016-06-03 12:05:38.457409
560	26	1963	Card	\N	\N	tags	2016-06-03 12:05:38.474939
561	25	1964	Card	\N	\N	tags	2016-06-03 12:05:39.219525
562	26	1964	Card	\N	\N	tags	2016-06-03 12:05:39.258867
563	25	1965	Card	\N	\N	tags	2016-06-03 12:05:40.691947
564	26	1965	Card	\N	\N	tags	2016-06-03 12:05:40.715418
565	25	1966	Card	\N	\N	tags	2016-06-03 12:05:41.729137
566	26	1966	Card	\N	\N	tags	2016-06-03 12:05:41.774256
567	25	1967	Card	\N	\N	tags	2016-06-03 12:05:42.510424
568	26	1967	Card	\N	\N	tags	2016-06-03 12:05:42.54286
569	25	1968	Card	\N	\N	tags	2016-06-03 12:05:42.822539
570	26	1968	Card	\N	\N	tags	2016-06-03 12:05:42.842559
571	25	1969	Card	\N	\N	tags	2016-06-03 12:05:44.541287
572	26	1969	Card	\N	\N	tags	2016-06-03 12:05:44.571771
573	25	1970	Card	\N	\N	tags	2016-06-03 12:05:45.589082
574	26	1970	Card	\N	\N	tags	2016-06-03 12:05:45.628099
575	27	1971	Card	\N	\N	tags	2016-06-03 12:05:47.732366
576	28	1971	Card	\N	\N	tags	2016-06-03 12:05:47.765833
577	27	1972	Card	\N	\N	tags	2016-06-03 12:05:48.086824
578	29	1972	Card	\N	\N	tags	2016-06-03 12:05:48.111439
579	30	1973	Card	\N	\N	tags	2016-06-03 12:05:48.363433
580	31	1973	Card	\N	\N	tags	2016-06-03 12:05:48.380853
581	32	1973	Card	\N	\N	tags	2016-06-03 12:05:48.401877
582	33	1974	Card	\N	\N	tags	2016-06-03 12:05:48.853
583	34	1974	Card	\N	\N	tags	2016-06-03 12:05:48.869413
584	33	1975	Card	\N	\N	tags	2016-06-03 12:05:49.049586
585	35	1975	Card	\N	\N	tags	2016-06-03 12:05:49.069555
586	36	1975	Card	\N	\N	tags	2016-06-03 12:05:49.087433
587	37	1976	Card	\N	\N	tags	2016-06-03 12:05:49.228751
588	38	1977	Card	\N	\N	tags	2016-06-03 12:05:49.68469
589	39	1977	Card	\N	\N	tags	2016-06-03 12:05:49.707138
590	40	1977	Card	\N	\N	tags	2016-06-03 12:05:49.728985
591	38	1978	Card	\N	\N	tags	2016-06-03 12:05:49.907713
592	41	1978	Card	\N	\N	tags	2016-06-03 12:05:49.93393
593	40	1978	Card	\N	\N	tags	2016-06-03 12:05:49.959442
594	38	1979	Card	\N	\N	tags	2016-06-03 12:05:50.085746
595	42	1979	Card	\N	\N	tags	2016-06-03 12:05:50.104005
596	43	1980	Card	\N	\N	tags	2016-06-03 12:05:50.207051
597	38	1980	Card	\N	\N	tags	2016-06-03 12:05:50.22597
598	44	1981	Card	\N	\N	tags	2016-06-03 12:05:50.375253
599	38	1981	Card	\N	\N	tags	2016-06-03 12:05:50.391963
600	45	1981	Card	\N	\N	tags	2016-06-03 12:05:50.41376
601	44	1982	Card	\N	\N	tags	2016-06-03 12:05:50.604048
602	38	1982	Card	\N	\N	tags	2016-06-03 12:05:50.624706
603	45	1982	Card	\N	\N	tags	2016-06-03 12:05:50.644088
604	45	1983	Card	\N	\N	tags	2016-06-03 12:05:50.975513
605	23	1983	Card	\N	\N	tags	2016-06-03 12:05:50.995265
606	46	1983	Card	\N	\N	tags	2016-06-03 12:05:51.013445
607	47	1984	Card	\N	\N	tags	2016-06-03 12:05:51.162042
608	23	1984	Card	\N	\N	tags	2016-06-03 12:05:51.181753
609	45	1984	Card	\N	\N	tags	2016-06-03 12:05:51.200782
610	48	1985	Card	\N	\N	tags	2016-06-03 12:05:51.644541
611	49	1985	Card	\N	\N	tags	2016-06-03 12:05:51.666192
612	50	1986	Card	\N	\N	tags	2016-06-03 12:05:51.807802
613	49	1986	Card	\N	\N	tags	2016-06-03 12:05:51.827698
614	50	1987	Card	\N	\N	tags	2016-06-03 12:05:51.978329
615	49	1987	Card	\N	\N	tags	2016-06-03 12:05:51.999036
616	51	1988	Card	\N	\N	tags	2016-06-03 12:05:52.116435
617	49	1988	Card	\N	\N	tags	2016-06-03 12:05:52.135288
618	52	1989	Card	\N	\N	tags	2016-06-03 12:05:52.229627
619	49	1989	Card	\N	\N	tags	2016-06-03 12:05:52.248494
620	53	1990	Card	\N	\N	tags	2016-06-03 12:05:52.347713
621	54	1990	Card	\N	\N	tags	2016-06-03 12:05:52.369026
622	49	1990	Card	\N	\N	tags	2016-06-03 12:05:52.386407
623	53	1991	Card	\N	\N	tags	2016-06-03 12:05:52.479347
624	54	1991	Card	\N	\N	tags	2016-06-03 12:05:52.498147
625	49	1991	Card	\N	\N	tags	2016-06-03 12:05:52.517661
626	53	1992	Card	\N	\N	tags	2016-06-03 12:05:52.694478
627	55	1992	Card	\N	\N	tags	2016-06-03 12:05:52.712119
628	49	1992	Card	\N	\N	tags	2016-06-03 12:05:52.731607
629	53	1993	Card	\N	\N	tags	2016-06-03 12:05:52.831072
630	56	1993	Card	\N	\N	tags	2016-06-03 12:05:52.852182
631	49	1993	Card	\N	\N	tags	2016-06-03 12:05:52.87076
632	57	1994	Card	\N	\N	tags	2016-06-03 12:05:52.958478
633	49	1994	Card	\N	\N	tags	2016-06-03 12:05:52.979278
634	35	1995	Card	\N	\N	tags	2016-06-03 12:05:53.066182
635	49	1995	Card	\N	\N	tags	2016-06-03 12:05:53.084046
636	58	1996	Card	\N	\N	tags	2016-06-03 12:05:53.172266
637	33	1996	Card	\N	\N	tags	2016-06-03 12:05:53.189655
638	49	1996	Card	\N	\N	tags	2016-06-03 12:05:53.208643
639	59	1997	Card	\N	\N	tags	2016-06-03 12:05:53.320904
640	60	1997	Card	\N	\N	tags	2016-06-03 12:05:53.33928
641	49	1997	Card	\N	\N	tags	2016-06-03 12:05:53.357236
642	60	1998	Card	\N	\N	tags	2016-06-03 12:05:53.661273
643	59	1998	Card	\N	\N	tags	2016-06-03 12:05:53.685299
644	49	1998	Card	\N	\N	tags	2016-06-03 12:05:53.711893
645	61	1999	Card	\N	\N	tags	2016-06-03 12:05:53.870289
646	62	1999	Card	\N	\N	tags	2016-06-03 12:05:53.892355
647	49	1999	Card	\N	\N	tags	2016-06-03 12:05:53.913314
648	61	2000	Card	\N	\N	tags	2016-06-03 12:05:54.023917
649	62	2000	Card	\N	\N	tags	2016-06-03 12:05:54.046015
650	49	2000	Card	\N	\N	tags	2016-06-03 12:05:54.062611
651	53	2001	Card	\N	\N	tags	2016-06-03 12:05:54.373181
652	63	2001	Card	\N	\N	tags	2016-06-03 12:05:54.393403
653	49	2001	Card	\N	\N	tags	2016-06-03 12:05:54.411614
654	64	2002	Card	\N	\N	tags	2016-06-03 12:05:54.521227
655	63	2002	Card	\N	\N	tags	2016-06-03 12:05:54.542825
656	49	2002	Card	\N	\N	tags	2016-06-03 12:05:54.568632
657	64	2003	Card	\N	\N	tags	2016-06-03 12:05:54.708814
658	63	2003	Card	\N	\N	tags	2016-06-03 12:05:54.72697
659	49	2003	Card	\N	\N	tags	2016-06-03 12:05:54.746708
660	64	2004	Card	\N	\N	tags	2016-06-03 12:05:54.838075
661	63	2004	Card	\N	\N	tags	2016-06-03 12:05:54.858272
662	49	2004	Card	\N	\N	tags	2016-06-03 12:05:54.878894
663	64	2005	Card	\N	\N	tags	2016-06-03 12:05:54.991718
664	63	2005	Card	\N	\N	tags	2016-06-03 12:05:55.011852
665	65	2005	Card	\N	\N	tags	2016-06-03 12:05:55.032664
666	64	2006	Card	\N	\N	tags	2016-06-03 12:05:55.15187
667	63	2006	Card	\N	\N	tags	2016-06-03 12:05:55.172351
668	65	2006	Card	\N	\N	tags	2016-06-03 12:05:55.18921
669	64	2007	Card	\N	\N	tags	2016-06-03 12:05:55.293442
670	63	2007	Card	\N	\N	tags	2016-06-03 12:05:55.315599
671	65	2007	Card	\N	\N	tags	2016-06-03 12:05:55.333809
672	64	2008	Card	\N	\N	tags	2016-06-03 12:05:55.450708
673	63	2008	Card	\N	\N	tags	2016-06-03 12:05:55.46997
674	65	2008	Card	\N	\N	tags	2016-06-03 12:05:55.491213
675	64	2009	Card	\N	\N	tags	2016-06-03 12:05:55.751038
676	63	2009	Card	\N	\N	tags	2016-06-03 12:05:55.773612
677	65	2009	Card	\N	\N	tags	2016-06-03 12:05:55.794374
678	47	2010	Card	\N	\N	tags	2016-06-03 12:05:56.219331
679	66	2010	Card	\N	\N	tags	2016-06-03 12:05:56.236916
680	66	2011	Card	\N	\N	tags	2016-06-03 12:05:56.377997
681	67	2011	Card	\N	\N	tags	2016-06-03 12:05:56.396746
682	66	2012	Card	\N	\N	tags	2016-06-03 12:05:56.499795
683	68	2012	Card	\N	\N	tags	2016-06-03 12:05:56.516662
684	66	2013	Card	\N	\N	tags	2016-06-03 12:05:56.646769
685	69	2013	Card	\N	\N	tags	2016-06-03 12:05:56.664119
686	66	2014	Card	\N	\N	tags	2016-06-03 12:05:56.773338
687	70	2014	Card	\N	\N	tags	2016-06-03 12:05:56.791487
688	66	2015	Card	\N	\N	tags	2016-06-03 12:05:57.163961
689	47	2015	Card	\N	\N	tags	2016-06-03 12:05:57.181186
690	66	2016	Card	\N	\N	tags	2016-06-03 12:05:57.357664
691	71	2016	Card	\N	\N	tags	2016-06-03 12:05:57.37351
692	66	2017	Card	\N	\N	tags	2016-06-03 12:05:57.498171
693	72	2017	Card	\N	\N	tags	2016-06-03 12:05:57.520462
694	73	2017	Card	\N	\N	tags	2016-06-03 12:05:57.541699
695	66	2018	Card	\N	\N	tags	2016-06-03 12:05:57.675748
696	74	2018	Card	\N	\N	tags	2016-06-03 12:05:57.700246
697	72	2018	Card	\N	\N	tags	2016-06-03 12:05:57.724412
698	66	2019	Card	\N	\N	tags	2016-06-03 12:05:57.831583
699	75	2019	Card	\N	\N	tags	2016-06-03 12:05:57.851129
700	66	2020	Card	\N	\N	tags	2016-06-03 12:05:57.972836
701	74	2020	Card	\N	\N	tags	2016-06-03 12:05:57.992669
702	66	2021	Card	\N	\N	tags	2016-06-03 12:05:58.082466
703	68	2021	Card	\N	\N	tags	2016-06-03 12:05:58.103182
704	76	2022	Card	\N	\N	tags	2016-06-03 12:05:58.495801
705	77	2022	Card	\N	\N	tags	2016-06-03 12:05:58.517748
706	76	2023	Card	\N	\N	tags	2016-06-03 12:05:58.667882
707	78	2023	Card	\N	\N	tags	2016-06-03 12:05:58.685837
708	76	2024	Card	\N	\N	tags	2016-06-03 12:05:59.41281
709	77	2024	Card	\N	\N	tags	2016-06-03 12:05:59.431682
710	76	2025	Card	\N	\N	tags	2016-06-03 12:05:59.659432
711	77	2025	Card	\N	\N	tags	2016-06-03 12:05:59.681593
712	76	2026	Card	\N	\N	tags	2016-06-03 12:06:00.221989
713	77	2026	Card	\N	\N	tags	2016-06-03 12:06:00.244178
714	79	2027	Card	\N	\N	tags	2016-06-03 12:06:00.852759
715	80	2027	Card	\N	\N	tags	2016-06-03 12:06:00.873865
716	79	2028	Card	\N	\N	tags	2016-06-03 12:06:01.132243
717	81	2028	Card	\N	\N	tags	2016-06-03 12:06:01.149338
718	79	2029	Card	\N	\N	tags	2016-06-03 12:06:01.35925
719	66	2029	Card	\N	\N	tags	2016-06-03 12:06:01.377926
720	79	2030	Card	\N	\N	tags	2016-06-03 12:06:01.509883
721	66	2030	Card	\N	\N	tags	2016-06-03 12:06:01.529448
722	60	2030	Card	\N	\N	tags	2016-06-03 12:06:01.546639
723	79	2031	Card	\N	\N	tags	2016-06-03 12:06:01.671931
724	66	2031	Card	\N	\N	tags	2016-06-03 12:06:01.69356
725	79	2032	Card	\N	\N	tags	2016-06-03 12:06:01.821909
726	82	2032	Card	\N	\N	tags	2016-06-03 12:06:01.842247
727	32	2033	Card	\N	\N	tags	2016-06-03 12:06:02.153634
728	83	2033	Card	\N	\N	tags	2016-06-03 12:06:02.175156
729	84	2033	Card	\N	\N	tags	2016-06-03 12:06:02.195884
730	32	2034	Card	\N	\N	tags	2016-06-03 12:06:02.312768
731	85	2034	Card	\N	\N	tags	2016-06-03 12:06:02.33543
732	66	2034	Card	\N	\N	tags	2016-06-03 12:06:02.365084
733	32	2035	Card	\N	\N	tags	2016-06-03 12:06:02.485076
734	86	2035	Card	\N	\N	tags	2016-06-03 12:06:02.504631
735	32	2036	Card	\N	\N	tags	2016-06-03 12:06:02.609087
736	87	2036	Card	\N	\N	tags	2016-06-03 12:06:02.632116
737	32	2037	Card	\N	\N	tags	2016-06-03 12:06:02.800743
738	24	2037	Card	\N	\N	tags	2016-06-03 12:06:02.82142
739	32	2038	Card	\N	\N	tags	2016-06-03 12:06:02.934887
740	84	2038	Card	\N	\N	tags	2016-06-03 12:06:02.957572
741	88	2038	Card	\N	\N	tags	2016-06-03 12:06:02.983182
742	32	2039	Card	\N	\N	tags	2016-06-03 12:06:03.091584
743	89	2039	Card	\N	\N	tags	2016-06-03 12:06:03.112261
744	66	2039	Card	\N	\N	tags	2016-06-03 12:06:03.132152
745	32	2040	Card	\N	\N	tags	2016-06-03 12:06:03.216249
746	90	2040	Card	\N	\N	tags	2016-06-03 12:06:03.23645
747	32	2041	Card	\N	\N	tags	2016-06-03 12:06:03.335181
748	86	2041	Card	\N	\N	tags	2016-06-03 12:06:03.354867
749	32	2042	Card	\N	\N	tags	2016-06-03 12:06:03.455635
750	22	2042	Card	\N	\N	tags	2016-06-03 12:06:03.47566
751	32	2043	Card	\N	\N	tags	2016-06-03 12:06:03.569549
752	91	2043	Card	\N	\N	tags	2016-06-03 12:06:03.588354
753	32	2044	Card	\N	\N	tags	2016-06-03 12:06:03.74418
754	92	2044	Card	\N	\N	tags	2016-06-03 12:06:03.768825
755	32	2045	Card	\N	\N	tags	2016-06-03 12:06:03.936796
756	93	2045	Card	\N	\N	tags	2016-06-03 12:06:03.956226
757	32	2046	Card	\N	\N	tags	2016-06-03 12:06:04.048469
758	94	2046	Card	\N	\N	tags	2016-06-03 12:06:04.068193
759	32	2047	Card	\N	\N	tags	2016-06-03 12:06:04.21621
760	95	2047	Card	\N	\N	tags	2016-06-03 12:06:04.236206
761	32	2048	Card	\N	\N	tags	2016-06-03 12:06:04.329457
762	96	2048	Card	\N	\N	tags	2016-06-03 12:06:04.346466
763	32	2049	Card	\N	\N	tags	2016-06-03 12:06:04.445498
764	97	2049	Card	\N	\N	tags	2016-06-03 12:06:04.462871
765	98	2049	Card	\N	\N	tags	2016-06-03 12:06:04.486225
766	32	2050	Card	\N	\N	tags	2016-06-03 12:06:04.579341
767	99	2050	Card	\N	\N	tags	2016-06-03 12:06:04.598231
768	32	2051	Card	\N	\N	tags	2016-06-03 12:06:04.678797
769	81	2051	Card	\N	\N	tags	2016-06-03 12:06:04.698159
770	100	2052	Card	\N	\N	tags	2016-06-03 12:06:05.077117
771	101	2052	Card	\N	\N	tags	2016-06-03 12:06:05.096408
772	66	2052	Card	\N	\N	tags	2016-06-03 12:06:05.114248
773	100	2053	Card	\N	\N	tags	2016-06-03 12:06:05.227914
774	101	2053	Card	\N	\N	tags	2016-06-03 12:06:05.249169
775	66	2053	Card	\N	\N	tags	2016-06-03 12:06:05.269791
776	100	2054	Card	\N	\N	tags	2016-06-03 12:06:05.3787
777	24	2054	Card	\N	\N	tags	2016-06-03 12:06:05.398189
778	66	2054	Card	\N	\N	tags	2016-06-03 12:06:05.422077
779	100	2055	Card	\N	\N	tags	2016-06-03 12:06:05.552814
780	102	2055	Card	\N	\N	tags	2016-06-03 12:06:05.574282
781	100	2056	Card	\N	\N	tags	2016-06-03 12:06:05.728603
782	102	2056	Card	\N	\N	tags	2016-06-03 12:06:05.751789
783	66	2056	Card	\N	\N	tags	2016-06-03 12:06:05.77336
784	100	2057	Card	\N	\N	tags	2016-06-03 12:06:05.892097
785	103	2057	Card	\N	\N	tags	2016-06-03 12:06:05.911388
786	66	2057	Card	\N	\N	tags	2016-06-03 12:06:05.931758
787	100	2058	Card	\N	\N	tags	2016-06-03 12:06:06.03171
788	104	2058	Card	\N	\N	tags	2016-06-03 12:06:06.048575
789	66	2058	Card	\N	\N	tags	2016-06-03 12:06:06.066917
790	100	2059	Card	\N	\N	tags	2016-06-03 12:06:06.174995
791	105	2059	Card	\N	\N	tags	2016-06-03 12:06:06.195686
792	66	2059	Card	\N	\N	tags	2016-06-03 12:06:06.21559
793	100	2060	Card	\N	\N	tags	2016-06-03 12:06:06.364915
794	106	2060	Card	\N	\N	tags	2016-06-03 12:06:06.381328
795	100	2061	Card	\N	\N	tags	2016-06-03 12:06:06.587305
796	66	2061	Card	\N	\N	tags	2016-06-03 12:06:06.604649
797	60	2062	Card	\N	\N	tags	2016-06-03 12:06:08.379129
798	107	2062	Card	\N	\N	tags	2016-06-03 12:06:08.403722
799	47	2062	Card	\N	\N	tags	2016-06-03 12:06:08.432427
800	1	2063	Card	\N	\N	tags	2016-06-03 12:06:09.298477
801	13	2063	Card	\N	\N	tags	2016-06-03 12:06:09.332252
802	1	2064	Card	\N	\N	tags	2016-06-03 12:06:11.478127
803	13	2064	Card	\N	\N	tags	2016-06-03 12:06:11.497013
804	60	2065	Card	\N	\N	tags	2016-06-03 12:06:12.890057
805	107	2065	Card	\N	\N	tags	2016-06-03 12:06:12.914125
806	47	2065	Card	\N	\N	tags	2016-06-03 12:06:12.944221
807	1	2066	Card	\N	\N	tags	2016-06-03 12:06:14.329637
808	13	2066	Card	\N	\N	tags	2016-06-03 12:06:14.353756
809	60	2068	Card	\N	\N	tags	2016-06-03 12:06:15.25885
810	107	2068	Card	\N	\N	tags	2016-06-03 12:06:15.280465
811	47	2068	Card	\N	\N	tags	2016-06-03 12:06:15.299736
812	1	2069	Card	\N	\N	tags	2016-06-03 12:06:15.946915
813	13	2069	Card	\N	\N	tags	2016-06-03 12:06:15.982907
814	1	2070	Card	\N	\N	tags	2016-06-03 12:06:16.63506
815	13	2070	Card	\N	\N	tags	2016-06-03 12:06:16.663751
816	60	2071	Card	\N	\N	tags	2016-06-03 12:06:17.270877
817	107	2071	Card	\N	\N	tags	2016-06-03 12:06:17.30845
818	47	2071	Card	\N	\N	tags	2016-06-03 12:06:17.338808
819	1	2072	Card	\N	\N	tags	2016-06-03 12:06:18.017876
820	13	2072	Card	\N	\N	tags	2016-06-03 12:06:18.045506
821	1	2074	Card	\N	\N	tags	2016-06-03 12:07:36.41834
822	2	2074	Card	\N	\N	tags	2016-06-03 12:07:36.434194
823	1	2075	Card	\N	\N	tags	2016-06-03 12:07:36.565165
824	2	2075	Card	\N	\N	tags	2016-06-03 12:07:36.581572
825	3	2075	Card	\N	\N	tags	2016-06-03 12:07:36.59509
826	1	2076	Card	\N	\N	tags	2016-06-03 12:07:36.666585
827	4	2076	Card	\N	\N	tags	2016-06-03 12:07:36.679413
828	5	2076	Card	\N	\N	tags	2016-06-03 12:07:36.694443
829	1	2077	Card	\N	\N	tags	2016-06-03 12:07:39.111635
830	4	2077	Card	\N	\N	tags	2016-06-03 12:07:39.130586
831	5	2077	Card	\N	\N	tags	2016-06-03 12:07:39.149614
832	1	2078	Card	\N	\N	tags	2016-06-03 12:07:41.783396
833	4	2078	Card	\N	\N	tags	2016-06-03 12:07:41.802685
834	6	2078	Card	\N	\N	tags	2016-06-03 12:07:41.821703
835	1	2079	Card	\N	\N	tags	2016-06-03 12:07:44.307371
836	4	2079	Card	\N	\N	tags	2016-06-03 12:07:44.337483
837	6	2079	Card	\N	\N	tags	2016-06-03 12:07:44.369121
838	1	2080	Card	\N	\N	tags	2016-06-03 12:07:47.771183
839	4	2080	Card	\N	\N	tags	2016-06-03 12:07:47.79021
840	6	2080	Card	\N	\N	tags	2016-06-03 12:07:47.806886
841	1	2081	Card	\N	\N	tags	2016-06-03 12:07:50.881199
842	4	2081	Card	\N	\N	tags	2016-06-03 12:07:50.895171
843	6	2081	Card	\N	\N	tags	2016-06-03 12:07:50.909651
844	1	2082	Card	\N	\N	tags	2016-06-03 12:07:53.248744
845	4	2082	Card	\N	\N	tags	2016-06-03 12:07:53.265875
846	6	2082	Card	\N	\N	tags	2016-06-03 12:07:53.278759
847	1	2083	Card	\N	\N	tags	2016-06-03 12:07:55.88713
848	4	2083	Card	\N	\N	tags	2016-06-03 12:07:55.904091
849	6	2083	Card	\N	\N	tags	2016-06-03 12:07:55.920916
850	1	2085	Card	\N	\N	tags	2016-06-03 12:08:17.318444
851	2	2085	Card	\N	\N	tags	2016-06-03 12:08:17.350651
852	1	2086	Card	\N	\N	tags	2016-06-03 12:08:17.519212
853	2	2086	Card	\N	\N	tags	2016-06-03 12:08:17.53883
854	3	2086	Card	\N	\N	tags	2016-06-03 12:08:17.558511
855	1	2087	Card	\N	\N	tags	2016-06-03 12:08:17.662666
856	4	2087	Card	\N	\N	tags	2016-06-03 12:08:17.68523
857	5	2087	Card	\N	\N	tags	2016-06-03 12:08:17.703898
858	1	2088	Card	\N	\N	tags	2016-06-03 12:08:19.934307
859	4	2088	Card	\N	\N	tags	2016-06-03 12:08:19.966467
860	5	2088	Card	\N	\N	tags	2016-06-03 12:08:19.99896
861	1	2089	Card	\N	\N	tags	2016-06-03 12:08:22.706469
862	4	2089	Card	\N	\N	tags	2016-06-03 12:08:22.734869
863	6	2089	Card	\N	\N	tags	2016-06-03 12:08:22.751098
864	1	2090	Card	\N	\N	tags	2016-06-03 12:08:25.367697
865	4	2090	Card	\N	\N	tags	2016-06-03 12:08:25.396508
866	6	2090	Card	\N	\N	tags	2016-06-03 12:08:25.427015
867	1	2091	Card	\N	\N	tags	2016-06-03 12:08:27.723053
868	4	2091	Card	\N	\N	tags	2016-06-03 12:08:27.742077
869	6	2091	Card	\N	\N	tags	2016-06-03 12:08:27.762659
870	1	2092	Card	\N	\N	tags	2016-06-03 12:08:31.234967
871	4	2092	Card	\N	\N	tags	2016-06-03 12:08:31.262284
872	6	2092	Card	\N	\N	tags	2016-06-03 12:08:31.284847
873	1	2093	Card	\N	\N	tags	2016-06-03 12:08:34.384851
874	4	2093	Card	\N	\N	tags	2016-06-03 12:08:34.417656
875	6	2093	Card	\N	\N	tags	2016-06-03 12:08:34.447709
876	1	2094	Card	\N	\N	tags	2016-06-03 12:08:37.821093
877	4	2094	Card	\N	\N	tags	2016-06-03 12:08:37.848041
878	6	2094	Card	\N	\N	tags	2016-06-03 12:08:37.868165
879	1	2095	Card	\N	\N	tags	2016-06-03 12:08:40.896414
880	4	2095	Card	\N	\N	tags	2016-06-03 12:08:40.916341
881	6	2095	Card	\N	\N	tags	2016-06-03 12:08:40.933313
882	1	2096	Card	\N	\N	tags	2016-06-03 12:08:44.338381
883	4	2096	Card	\N	\N	tags	2016-06-03 12:08:44.365396
884	6	2096	Card	\N	\N	tags	2016-06-03 12:08:44.394323
885	1	2097	Card	\N	\N	tags	2016-06-03 12:08:49.845112
886	4	2097	Card	\N	\N	tags	2016-06-03 12:08:49.904228
887	7	2097	Card	\N	\N	tags	2016-06-03 12:08:50.001789
888	1	2098	Card	\N	\N	tags	2016-06-03 12:08:52.27961
889	4	2098	Card	\N	\N	tags	2016-06-03 12:08:52.313744
890	6	2098	Card	\N	\N	tags	2016-06-03 12:08:52.34489
891	1	2099	Card	\N	\N	tags	2016-06-03 12:08:55.378071
892	4	2099	Card	\N	\N	tags	2016-06-03 12:08:55.406179
893	6	2099	Card	\N	\N	tags	2016-06-03 12:08:55.426678
894	1	2100	Card	\N	\N	tags	2016-06-03 12:08:55.909905
895	4	2100	Card	\N	\N	tags	2016-06-03 12:08:55.930586
896	6	2100	Card	\N	\N	tags	2016-06-03 12:08:55.952812
897	1	2101	Card	\N	\N	tags	2016-06-03 12:08:56.253657
898	4	2101	Card	\N	\N	tags	2016-06-03 12:08:56.27254
899	6	2101	Card	\N	\N	tags	2016-06-03 12:08:56.290797
900	1	2102	Card	\N	\N	tags	2016-06-03 12:08:56.380122
901	4	2102	Card	\N	\N	tags	2016-06-03 12:08:56.400644
902	6	2102	Card	\N	\N	tags	2016-06-03 12:08:56.417047
903	1	2103	Card	\N	\N	tags	2016-06-03 12:08:56.542874
904	4	2103	Card	\N	\N	tags	2016-06-03 12:08:56.562064
905	8	2103	Card	\N	\N	tags	2016-06-03 12:08:56.581555
906	1	2104	Card	\N	\N	tags	2016-06-03 12:08:56.778189
907	4	2104	Card	\N	\N	tags	2016-06-03 12:08:56.806591
908	1	2105	Card	\N	\N	tags	2016-06-03 12:08:56.978239
909	9	2105	Card	\N	\N	tags	2016-06-03 12:08:57.010924
910	1	2106	Card	\N	\N	tags	2016-06-03 12:08:57.24117
911	10	2106	Card	\N	\N	tags	2016-06-03 12:08:57.266568
912	1	2107	Card	\N	\N	tags	2016-06-03 12:08:57.460318
913	11	2107	Card	\N	\N	tags	2016-06-03 12:08:57.479742
914	1	2108	Card	\N	\N	tags	2016-06-03 12:08:57.580184
915	11	2108	Card	\N	\N	tags	2016-06-03 12:08:57.600286
916	1	2109	Card	\N	\N	tags	2016-06-03 12:08:57.703556
917	11	2109	Card	\N	\N	tags	2016-06-03 12:08:57.7214
918	1	2110	Card	\N	\N	tags	2016-06-03 12:08:59.269414
919	11	2110	Card	\N	\N	tags	2016-06-03 12:08:59.31047
920	1	2111	Card	\N	\N	tags	2016-06-03 12:08:59.441344
921	12	2111	Card	\N	\N	tags	2016-06-03 12:08:59.461004
922	1	2112	Card	\N	\N	tags	2016-06-03 12:08:59.576159
923	11	2112	Card	\N	\N	tags	2016-06-03 12:08:59.60018
924	1	2113	Card	\N	\N	tags	2016-06-03 12:09:00.229836
925	11	2113	Card	\N	\N	tags	2016-06-03 12:09:00.279868
926	1	2114	Card	\N	\N	tags	2016-06-03 12:09:01.015356
927	11	2114	Card	\N	\N	tags	2016-06-03 12:09:01.102495
928	1	2115	Card	\N	\N	tags	2016-06-03 12:09:01.852428
929	11	2115	Card	\N	\N	tags	2016-06-03 12:09:01.918782
930	1	2116	Card	\N	\N	tags	2016-06-03 12:09:02.211911
931	11	2116	Card	\N	\N	tags	2016-06-03 12:09:02.246412
932	1	2117	Card	\N	\N	tags	2016-06-03 12:09:02.975665
933	13	2117	Card	\N	\N	tags	2016-06-03 12:09:03.023355
934	14	2117	Card	\N	\N	tags	2016-06-03 12:09:03.054542
935	1	2118	Card	\N	\N	tags	2016-06-03 12:09:03.286346
936	14	2118	Card	\N	\N	tags	2016-06-03 12:09:03.315206
937	1	2119	Card	\N	\N	tags	2016-06-03 12:09:03.44599
938	14	2119	Card	\N	\N	tags	2016-06-03 12:09:03.475076
939	11	2119	Card	\N	\N	tags	2016-06-03 12:09:03.500726
940	1	2120	Card	\N	\N	tags	2016-06-03 12:09:03.620537
941	14	2120	Card	\N	\N	tags	2016-06-03 12:09:03.641816
942	11	2120	Card	\N	\N	tags	2016-06-03 12:09:03.664176
943	15	2121	Card	\N	\N	tags	2016-06-03 12:09:04.169331
944	16	2121	Card	\N	\N	tags	2016-06-03 12:09:04.196165
945	15	2122	Card	\N	\N	tags	2016-06-03 12:09:06.1531
946	16	2122	Card	\N	\N	tags	2016-06-03 12:09:06.181097
947	15	2123	Card	\N	\N	tags	2016-06-03 12:09:07.27669
948	16	2123	Card	\N	\N	tags	2016-06-03 12:09:07.311329
949	15	2124	Card	\N	\N	tags	2016-06-03 12:09:07.471528
950	17	2124	Card	\N	\N	tags	2016-06-03 12:09:07.490116
951	15	2125	Card	\N	\N	tags	2016-06-03 12:09:07.584613
952	18	2125	Card	\N	\N	tags	2016-06-03 12:09:07.602689
953	19	2126	Card	\N	\N	tags	2016-06-03 12:09:07.923231
954	20	2126	Card	\N	\N	tags	2016-06-03 12:09:07.941762
955	21	2126	Card	\N	\N	tags	2016-06-03 12:09:07.960548
956	19	2127	Card	\N	\N	tags	2016-06-03 12:09:08.849818
957	20	2127	Card	\N	\N	tags	2016-06-03 12:09:08.875217
958	21	2127	Card	\N	\N	tags	2016-06-03 12:09:08.922012
959	19	2128	Card	\N	\N	tags	2016-06-03 12:09:10.625779
960	20	2128	Card	\N	\N	tags	2016-06-03 12:09:10.657645
961	21	2128	Card	\N	\N	tags	2016-06-03 12:09:10.677935
962	19	2129	Card	\N	\N	tags	2016-06-03 12:09:11.800026
963	20	2129	Card	\N	\N	tags	2016-06-03 12:09:11.837837
964	21	2129	Card	\N	\N	tags	2016-06-03 12:09:11.882716
965	19	2130	Card	\N	\N	tags	2016-06-03 12:09:12.831988
966	20	2130	Card	\N	\N	tags	2016-06-03 12:09:12.877341
967	21	2130	Card	\N	\N	tags	2016-06-03 12:09:12.912229
968	19	2131	Card	\N	\N	tags	2016-06-03 12:09:14.39738
969	20	2131	Card	\N	\N	tags	2016-06-03 12:09:14.446099
970	21	2131	Card	\N	\N	tags	2016-06-03 12:09:14.484443
971	19	2132	Card	\N	\N	tags	2016-06-03 12:09:15.454562
972	20	2132	Card	\N	\N	tags	2016-06-03 12:09:15.500806
973	21	2132	Card	\N	\N	tags	2016-06-03 12:09:15.536113
974	19	2133	Card	\N	\N	tags	2016-06-03 12:09:15.897382
975	22	2133	Card	\N	\N	tags	2016-06-03 12:09:15.913161
976	19	2134	Card	\N	\N	tags	2016-06-03 12:09:18.95127
977	22	2134	Card	\N	\N	tags	2016-06-03 12:09:18.978179
978	19	2135	Card	\N	\N	tags	2016-06-03 12:09:19.128988
979	23	2136	Card	\N	\N	tags	2016-06-03 12:09:19.328982
980	23	2137	Card	\N	\N	tags	2016-06-03 12:09:19.45054
981	24	2138	Card	\N	\N	tags	2016-06-03 12:09:19.582975
982	19	2138	Card	\N	\N	tags	2016-06-03 12:09:19.609934
983	25	2139	Card	\N	\N	tags	2016-06-03 12:09:20.035821
984	26	2139	Card	\N	\N	tags	2016-06-03 12:09:20.049607
985	25	2140	Card	\N	\N	tags	2016-06-03 12:09:22.251735
986	26	2140	Card	\N	\N	tags	2016-06-03 12:09:22.289561
987	25	2141	Card	\N	\N	tags	2016-06-03 12:09:23.722673
988	26	2141	Card	\N	\N	tags	2016-06-03 12:09:23.797905
989	25	2142	Card	\N	\N	tags	2016-06-03 12:09:25.308652
990	26	2142	Card	\N	\N	tags	2016-06-03 12:09:25.335486
991	25	2143	Card	\N	\N	tags	2016-06-03 12:09:25.460629
992	26	2143	Card	\N	\N	tags	2016-06-03 12:09:25.484111
993	25	2144	Card	\N	\N	tags	2016-06-03 12:09:26.541412
994	26	2144	Card	\N	\N	tags	2016-06-03 12:09:26.571398
995	25	2145	Card	\N	\N	tags	2016-06-03 12:09:29.176408
996	26	2145	Card	\N	\N	tags	2016-06-03 12:09:29.207145
997	25	2146	Card	\N	\N	tags	2016-06-03 12:09:30.639341
998	26	2146	Card	\N	\N	tags	2016-06-03 12:09:30.67726
999	25	2147	Card	\N	\N	tags	2016-06-03 12:09:31.579839
1000	26	2147	Card	\N	\N	tags	2016-06-03 12:09:31.692632
1001	25	2148	Card	\N	\N	tags	2016-06-03 12:09:31.967809
1002	26	2148	Card	\N	\N	tags	2016-06-03 12:09:31.99212
1003	25	2149	Card	\N	\N	tags	2016-06-03 12:09:34.924989
1004	26	2149	Card	\N	\N	tags	2016-06-03 12:09:34.951865
1005	25	2150	Card	\N	\N	tags	2016-06-03 12:09:36.170149
1006	26	2150	Card	\N	\N	tags	2016-06-03 12:09:36.199375
1007	27	2151	Card	\N	\N	tags	2016-06-03 12:09:38.732779
1008	28	2151	Card	\N	\N	tags	2016-06-03 12:09:38.765501
1009	27	2152	Card	\N	\N	tags	2016-06-03 12:09:39.003356
1010	29	2152	Card	\N	\N	tags	2016-06-03 12:09:39.01829
1011	30	2153	Card	\N	\N	tags	2016-06-03 12:09:39.202156
1012	31	2153	Card	\N	\N	tags	2016-06-03 12:09:39.217612
1013	32	2153	Card	\N	\N	tags	2016-06-03 12:09:39.230696
1014	33	2154	Card	\N	\N	tags	2016-06-03 12:09:39.658595
1015	34	2154	Card	\N	\N	tags	2016-06-03 12:09:39.671874
1016	33	2155	Card	\N	\N	tags	2016-06-03 12:09:39.814924
1017	35	2155	Card	\N	\N	tags	2016-06-03 12:09:39.82835
1018	36	2155	Card	\N	\N	tags	2016-06-03 12:09:39.849728
1019	37	2156	Card	\N	\N	tags	2016-06-03 12:09:39.972729
1020	38	2157	Card	\N	\N	tags	2016-06-03 12:09:40.501892
1021	39	2157	Card	\N	\N	tags	2016-06-03 12:09:40.518098
1022	40	2157	Card	\N	\N	tags	2016-06-03 12:09:40.53263
1023	38	2158	Card	\N	\N	tags	2016-06-03 12:09:40.777768
1024	41	2158	Card	\N	\N	tags	2016-06-03 12:09:40.801653
1025	40	2158	Card	\N	\N	tags	2016-06-03 12:09:40.822828
1026	38	2159	Card	\N	\N	tags	2016-06-03 12:09:40.955484
1027	42	2159	Card	\N	\N	tags	2016-06-03 12:09:40.975223
1028	43	2160	Card	\N	\N	tags	2016-06-03 12:09:41.09173
1029	38	2160	Card	\N	\N	tags	2016-06-03 12:09:41.124258
1030	44	2161	Card	\N	\N	tags	2016-06-03 12:09:41.279546
1031	38	2161	Card	\N	\N	tags	2016-06-03 12:09:41.29795
1032	45	2161	Card	\N	\N	tags	2016-06-03 12:09:41.315477
1033	44	2162	Card	\N	\N	tags	2016-06-03 12:09:41.475367
1034	38	2162	Card	\N	\N	tags	2016-06-03 12:09:41.489019
1035	45	2162	Card	\N	\N	tags	2016-06-03 12:09:41.501252
1036	45	2163	Card	\N	\N	tags	2016-06-03 12:09:41.84263
1037	23	2163	Card	\N	\N	tags	2016-06-03 12:09:41.878335
1038	46	2163	Card	\N	\N	tags	2016-06-03 12:09:41.911052
1039	47	2164	Card	\N	\N	tags	2016-06-03 12:09:42.106663
1040	23	2164	Card	\N	\N	tags	2016-06-03 12:09:42.148356
1041	45	2164	Card	\N	\N	tags	2016-06-03 12:09:42.225753
1042	48	2165	Card	\N	\N	tags	2016-06-03 12:09:42.733409
1043	49	2165	Card	\N	\N	tags	2016-06-03 12:09:42.755973
1044	50	2166	Card	\N	\N	tags	2016-06-03 12:09:43.004326
1045	49	2166	Card	\N	\N	tags	2016-06-03 12:09:43.039063
1046	50	2167	Card	\N	\N	tags	2016-06-03 12:09:43.225496
1047	49	2167	Card	\N	\N	tags	2016-06-03 12:09:43.257939
1048	51	2168	Card	\N	\N	tags	2016-06-03 12:09:43.394877
1049	49	2168	Card	\N	\N	tags	2016-06-03 12:09:43.417363
1050	52	2169	Card	\N	\N	tags	2016-06-03 12:09:43.537478
1051	49	2169	Card	\N	\N	tags	2016-06-03 12:09:43.559443
1052	53	2170	Card	\N	\N	tags	2016-06-03 12:09:43.74111
1053	54	2170	Card	\N	\N	tags	2016-06-03 12:09:43.762826
1054	49	2170	Card	\N	\N	tags	2016-06-03 12:09:43.786448
1055	53	2171	Card	\N	\N	tags	2016-06-03 12:09:43.914974
1056	54	2171	Card	\N	\N	tags	2016-06-03 12:09:43.940231
1057	49	2171	Card	\N	\N	tags	2016-06-03 12:09:43.965336
1058	53	2172	Card	\N	\N	tags	2016-06-03 12:09:44.340217
1059	55	2172	Card	\N	\N	tags	2016-06-03 12:09:44.380503
1060	49	2172	Card	\N	\N	tags	2016-06-03 12:09:44.418186
1061	53	2173	Card	\N	\N	tags	2016-06-03 12:09:44.619079
1062	56	2173	Card	\N	\N	tags	2016-06-03 12:09:44.660482
1063	49	2173	Card	\N	\N	tags	2016-06-03 12:09:44.7017
1064	57	2174	Card	\N	\N	tags	2016-06-03 12:09:45.214147
1065	49	2174	Card	\N	\N	tags	2016-06-03 12:09:45.256571
1066	35	2175	Card	\N	\N	tags	2016-06-03 12:09:45.429098
1067	49	2175	Card	\N	\N	tags	2016-06-03 12:09:45.452693
1068	58	2176	Card	\N	\N	tags	2016-06-03 12:09:45.603311
1069	33	2176	Card	\N	\N	tags	2016-06-03 12:09:45.633973
1070	49	2176	Card	\N	\N	tags	2016-06-03 12:09:45.654899
1071	59	2177	Card	\N	\N	tags	2016-06-03 12:09:45.792877
1072	60	2177	Card	\N	\N	tags	2016-06-03 12:09:45.816242
1073	49	2177	Card	\N	\N	tags	2016-06-03 12:09:45.845703
1074	60	2178	Card	\N	\N	tags	2016-06-03 12:09:46.174159
1075	59	2178	Card	\N	\N	tags	2016-06-03 12:09:46.200597
1076	49	2178	Card	\N	\N	tags	2016-06-03 12:09:46.221101
1077	61	2179	Card	\N	\N	tags	2016-06-03 12:09:46.41624
1078	62	2179	Card	\N	\N	tags	2016-06-03 12:09:46.439197
1079	49	2179	Card	\N	\N	tags	2016-06-03 12:09:46.459464
1080	61	2180	Card	\N	\N	tags	2016-06-03 12:09:46.578048
1081	62	2180	Card	\N	\N	tags	2016-06-03 12:09:46.603592
1082	49	2180	Card	\N	\N	tags	2016-06-03 12:09:46.628278
1083	53	2181	Card	\N	\N	tags	2016-06-03 12:09:47.320787
1084	63	2181	Card	\N	\N	tags	2016-06-03 12:09:47.363802
1085	49	2181	Card	\N	\N	tags	2016-06-03 12:09:47.420588
1086	64	2182	Card	\N	\N	tags	2016-06-03 12:09:47.651677
1087	63	2182	Card	\N	\N	tags	2016-06-03 12:09:47.690431
1088	49	2182	Card	\N	\N	tags	2016-06-03 12:09:47.722346
1089	64	2183	Card	\N	\N	tags	2016-06-03 12:09:47.944657
1090	63	2183	Card	\N	\N	tags	2016-06-03 12:09:48.000014
1091	49	2183	Card	\N	\N	tags	2016-06-03 12:09:48.055921
1092	64	2184	Card	\N	\N	tags	2016-06-03 12:09:48.201466
1093	63	2184	Card	\N	\N	tags	2016-06-03 12:09:48.225409
1094	49	2184	Card	\N	\N	tags	2016-06-03 12:09:48.248515
1095	64	2185	Card	\N	\N	tags	2016-06-03 12:09:48.402305
1096	63	2185	Card	\N	\N	tags	2016-06-03 12:09:48.436942
1097	65	2185	Card	\N	\N	tags	2016-06-03 12:09:48.485344
1098	64	2186	Card	\N	\N	tags	2016-06-03 12:09:48.642438
1099	63	2186	Card	\N	\N	tags	2016-06-03 12:09:48.682356
1100	65	2186	Card	\N	\N	tags	2016-06-03 12:09:48.715177
1101	64	2187	Card	\N	\N	tags	2016-06-03 12:09:48.925601
1102	63	2187	Card	\N	\N	tags	2016-06-03 12:09:48.964213
1103	65	2187	Card	\N	\N	tags	2016-06-03 12:09:49.000673
1104	64	2188	Card	\N	\N	tags	2016-06-03 12:09:49.211897
1105	63	2188	Card	\N	\N	tags	2016-06-03 12:09:49.242307
1106	65	2188	Card	\N	\N	tags	2016-06-03 12:09:49.275359
1107	64	2189	Card	\N	\N	tags	2016-06-03 12:09:49.561557
1108	63	2189	Card	\N	\N	tags	2016-06-03 12:09:49.586655
1109	65	2189	Card	\N	\N	tags	2016-06-03 12:09:49.612885
1110	47	2190	Card	\N	\N	tags	2016-06-03 12:09:50.297113
1111	66	2190	Card	\N	\N	tags	2016-06-03 12:09:50.33204
1112	66	2191	Card	\N	\N	tags	2016-06-03 12:09:50.547151
1113	67	2191	Card	\N	\N	tags	2016-06-03 12:09:50.577667
1114	66	2192	Card	\N	\N	tags	2016-06-03 12:09:50.775373
1115	68	2192	Card	\N	\N	tags	2016-06-03 12:09:50.800535
1116	66	2193	Card	\N	\N	tags	2016-06-03 12:09:51.020177
1117	69	2193	Card	\N	\N	tags	2016-06-03 12:09:51.043893
1118	66	2194	Card	\N	\N	tags	2016-06-03 12:09:51.187469
1119	70	2194	Card	\N	\N	tags	2016-06-03 12:09:51.207991
1120	66	2195	Card	\N	\N	tags	2016-06-03 12:09:51.587536
1121	47	2195	Card	\N	\N	tags	2016-06-03 12:09:51.632784
1122	66	2196	Card	\N	\N	tags	2016-06-03 12:09:51.827687
1123	71	2196	Card	\N	\N	tags	2016-06-03 12:09:51.847836
1124	66	2197	Card	\N	\N	tags	2016-06-03 12:09:52.006911
1125	72	2197	Card	\N	\N	tags	2016-06-03 12:09:52.038791
1126	73	2197	Card	\N	\N	tags	2016-06-03 12:09:52.062906
1127	66	2198	Card	\N	\N	tags	2016-06-03 12:09:52.185277
1128	74	2198	Card	\N	\N	tags	2016-06-03 12:09:52.205194
1129	72	2198	Card	\N	\N	tags	2016-06-03 12:09:52.227808
1130	66	2199	Card	\N	\N	tags	2016-06-03 12:09:52.363087
1131	75	2199	Card	\N	\N	tags	2016-06-03 12:09:52.39224
1132	66	2200	Card	\N	\N	tags	2016-06-03 12:09:52.565342
1133	74	2200	Card	\N	\N	tags	2016-06-03 12:09:52.585502
1134	66	2201	Card	\N	\N	tags	2016-06-03 12:09:52.710651
1135	68	2201	Card	\N	\N	tags	2016-06-03 12:09:52.733428
1136	76	2202	Card	\N	\N	tags	2016-06-03 12:09:53.685008
1137	77	2202	Card	\N	\N	tags	2016-06-03 12:09:53.713231
1138	76	2203	Card	\N	\N	tags	2016-06-03 12:09:53.919613
1139	78	2203	Card	\N	\N	tags	2016-06-03 12:09:53.936132
1140	76	2204	Card	\N	\N	tags	2016-06-03 12:09:54.806947
1141	77	2204	Card	\N	\N	tags	2016-06-03 12:09:54.834193
1142	76	2205	Card	\N	\N	tags	2016-06-03 12:09:55.107139
1143	77	2205	Card	\N	\N	tags	2016-06-03 12:09:55.124955
1144	76	2206	Card	\N	\N	tags	2016-06-03 12:09:55.691726
1145	77	2206	Card	\N	\N	tags	2016-06-03 12:09:55.711386
1146	79	2207	Card	\N	\N	tags	2016-06-03 12:09:56.654742
1147	80	2207	Card	\N	\N	tags	2016-06-03 12:09:56.687947
1148	79	2208	Card	\N	\N	tags	2016-06-03 12:09:57.107071
1149	81	2208	Card	\N	\N	tags	2016-06-03 12:09:57.139711
1150	79	2209	Card	\N	\N	tags	2016-06-03 12:09:57.442672
1151	66	2209	Card	\N	\N	tags	2016-06-03 12:09:57.473548
1152	79	2210	Card	\N	\N	tags	2016-06-03 12:09:57.670333
1153	66	2210	Card	\N	\N	tags	2016-06-03 12:09:57.694101
1154	60	2210	Card	\N	\N	tags	2016-06-03 12:09:57.71575
1155	79	2211	Card	\N	\N	tags	2016-06-03 12:09:57.872031
1156	66	2211	Card	\N	\N	tags	2016-06-03 12:09:57.893635
1157	79	2212	Card	\N	\N	tags	2016-06-03 12:09:58.024651
1158	82	2212	Card	\N	\N	tags	2016-06-03 12:09:58.044896
1159	32	2213	Card	\N	\N	tags	2016-06-03 12:09:58.380556
1160	83	2213	Card	\N	\N	tags	2016-06-03 12:09:58.396535
1161	84	2213	Card	\N	\N	tags	2016-06-03 12:09:58.411463
1162	32	2214	Card	\N	\N	tags	2016-06-03 12:09:58.509904
1163	85	2214	Card	\N	\N	tags	2016-06-03 12:09:58.526909
1164	66	2214	Card	\N	\N	tags	2016-06-03 12:09:58.543921
1165	32	2215	Card	\N	\N	tags	2016-06-03 12:09:58.642897
1166	86	2215	Card	\N	\N	tags	2016-06-03 12:09:58.6648
1167	32	2216	Card	\N	\N	tags	2016-06-03 12:09:58.765502
1168	87	2216	Card	\N	\N	tags	2016-06-03 12:09:58.787798
1169	32	2217	Card	\N	\N	tags	2016-06-03 12:09:58.972457
1170	24	2217	Card	\N	\N	tags	2016-06-03 12:09:58.99385
1171	32	2218	Card	\N	\N	tags	2016-06-03 12:09:59.138992
1172	84	2218	Card	\N	\N	tags	2016-06-03 12:09:59.162763
1173	88	2218	Card	\N	\N	tags	2016-06-03 12:09:59.1855
1174	32	2219	Card	\N	\N	tags	2016-06-03 12:09:59.319548
1175	89	2219	Card	\N	\N	tags	2016-06-03 12:09:59.34348
1176	66	2219	Card	\N	\N	tags	2016-06-03 12:09:59.371423
1177	32	2220	Card	\N	\N	tags	2016-06-03 12:09:59.497429
1178	90	2220	Card	\N	\N	tags	2016-06-03 12:09:59.5206
1179	32	2221	Card	\N	\N	tags	2016-06-03 12:09:59.653003
1180	86	2221	Card	\N	\N	tags	2016-06-03 12:09:59.676884
1181	32	2222	Card	\N	\N	tags	2016-06-03 12:09:59.802294
1182	22	2222	Card	\N	\N	tags	2016-06-03 12:09:59.825267
1183	32	2223	Card	\N	\N	tags	2016-06-03 12:09:59.937163
1184	91	2223	Card	\N	\N	tags	2016-06-03 12:09:59.960337
1185	32	2224	Card	\N	\N	tags	2016-06-03 12:10:00.163311
1186	92	2224	Card	\N	\N	tags	2016-06-03 12:10:00.187354
1187	32	2225	Card	\N	\N	tags	2016-06-03 12:10:00.347508
1188	93	2225	Card	\N	\N	tags	2016-06-03 12:10:00.377736
1189	32	2226	Card	\N	\N	tags	2016-06-03 12:10:00.480246
1190	94	2226	Card	\N	\N	tags	2016-06-03 12:10:00.497145
1191	32	2227	Card	\N	\N	tags	2016-06-03 12:10:00.676928
1192	95	2227	Card	\N	\N	tags	2016-06-03 12:10:00.702948
1193	32	2228	Card	\N	\N	tags	2016-06-03 12:10:00.821782
1194	96	2228	Card	\N	\N	tags	2016-06-03 12:10:00.848716
1195	32	2229	Card	\N	\N	tags	2016-06-03 12:10:01.00136
1196	97	2229	Card	\N	\N	tags	2016-06-03 12:10:01.024159
1197	98	2229	Card	\N	\N	tags	2016-06-03 12:10:01.05044
1198	32	2230	Card	\N	\N	tags	2016-06-03 12:10:01.170095
1199	99	2230	Card	\N	\N	tags	2016-06-03 12:10:01.191998
1200	32	2231	Card	\N	\N	tags	2016-06-03 12:10:01.301088
1201	81	2231	Card	\N	\N	tags	2016-06-03 12:10:01.320921
1202	100	2232	Card	\N	\N	tags	2016-06-03 12:10:01.686182
1203	101	2232	Card	\N	\N	tags	2016-06-03 12:10:01.705608
1204	66	2232	Card	\N	\N	tags	2016-06-03 12:10:01.728384
1205	100	2233	Card	\N	\N	tags	2016-06-03 12:10:01.848033
1206	101	2233	Card	\N	\N	tags	2016-06-03 12:10:01.871646
1207	66	2233	Card	\N	\N	tags	2016-06-03 12:10:01.895718
1208	100	2234	Card	\N	\N	tags	2016-06-03 12:10:02.036788
1209	24	2234	Card	\N	\N	tags	2016-06-03 12:10:02.053645
1210	66	2234	Card	\N	\N	tags	2016-06-03 12:10:02.070329
1211	100	2235	Card	\N	\N	tags	2016-06-03 12:10:02.202628
1212	102	2235	Card	\N	\N	tags	2016-06-03 12:10:02.221836
1213	100	2236	Card	\N	\N	tags	2016-06-03 12:10:02.348228
1214	102	2236	Card	\N	\N	tags	2016-06-03 12:10:02.369395
1215	66	2236	Card	\N	\N	tags	2016-06-03 12:10:02.385617
1216	100	2237	Card	\N	\N	tags	2016-06-03 12:10:02.486801
1217	103	2237	Card	\N	\N	tags	2016-06-03 12:10:02.503632
1218	66	2237	Card	\N	\N	tags	2016-06-03 12:10:02.520849
1219	100	2238	Card	\N	\N	tags	2016-06-03 12:10:02.635194
1220	104	2238	Card	\N	\N	tags	2016-06-03 12:10:02.657195
1221	66	2238	Card	\N	\N	tags	2016-06-03 12:10:02.675979
1222	100	2239	Card	\N	\N	tags	2016-06-03 12:10:02.792606
1223	105	2239	Card	\N	\N	tags	2016-06-03 12:10:02.81718
1224	66	2239	Card	\N	\N	tags	2016-06-03 12:10:02.832429
1225	100	2240	Card	\N	\N	tags	2016-06-03 12:10:02.959895
1226	106	2240	Card	\N	\N	tags	2016-06-03 12:10:02.975965
1227	100	2241	Card	\N	\N	tags	2016-06-03 12:10:03.192326
1228	66	2241	Card	\N	\N	tags	2016-06-03 12:10:03.209844
1229	60	2242	Card	\N	\N	tags	2016-06-03 12:10:05.797967
1230	107	2242	Card	\N	\N	tags	2016-06-03 12:10:05.830301
1231	47	2242	Card	\N	\N	tags	2016-06-03 12:10:05.860422
1232	1	2243	Card	\N	\N	tags	2016-06-03 12:10:06.852023
1233	13	2243	Card	\N	\N	tags	2016-06-03 12:10:06.896153
1234	1	2244	Card	\N	\N	tags	2016-06-03 12:10:09.539034
1235	13	2244	Card	\N	\N	tags	2016-06-03 12:10:09.575299
1236	60	2245	Card	\N	\N	tags	2016-06-03 12:10:13.525011
1237	107	2245	Card	\N	\N	tags	2016-06-03 12:10:13.552957
1238	47	2245	Card	\N	\N	tags	2016-06-03 12:10:13.57244
1239	1	2246	Card	\N	\N	tags	2016-06-03 12:10:15.180447
1240	13	2246	Card	\N	\N	tags	2016-06-03 12:10:15.199684
1241	60	2248	Card	\N	\N	tags	2016-06-03 12:10:16.203009
1242	107	2248	Card	\N	\N	tags	2016-06-03 12:10:16.229857
1243	47	2248	Card	\N	\N	tags	2016-06-03 12:10:16.251507
1244	1	2249	Card	\N	\N	tags	2016-06-03 12:10:18.185377
1245	13	2249	Card	\N	\N	tags	2016-06-03 12:10:18.214259
1246	1	2250	Card	\N	\N	tags	2016-06-03 12:10:19.134069
1247	13	2250	Card	\N	\N	tags	2016-06-03 12:10:19.17016
1248	60	2251	Card	\N	\N	tags	2016-06-03 12:10:20.173967
1249	107	2251	Card	\N	\N	tags	2016-06-03 12:10:20.204174
1250	47	2251	Card	\N	\N	tags	2016-06-03 12:10:20.250629
1251	1	2252	Card	\N	\N	tags	2016-06-03 12:10:21.206889
1252	13	2252	Card	\N	\N	tags	2016-06-03 12:10:21.237085
1253	1	2254	Card	\N	\N	tags	2016-06-03 12:21:31.94336
1254	2	2254	Card	\N	\N	tags	2016-06-03 12:21:31.963787
1255	1	2255	Card	\N	\N	tags	2016-06-03 12:21:32.113282
1256	2	2255	Card	\N	\N	tags	2016-06-03 12:21:32.130484
1257	3	2255	Card	\N	\N	tags	2016-06-03 12:21:32.14837
1258	1	2256	Card	\N	\N	tags	2016-06-03 12:21:32.238018
1259	4	2256	Card	\N	\N	tags	2016-06-03 12:21:32.252983
1260	5	2256	Card	\N	\N	tags	2016-06-03 12:21:32.270919
1261	1	2257	Card	\N	\N	tags	2016-06-03 12:21:34.452035
1262	4	2257	Card	\N	\N	tags	2016-06-03 12:21:34.475743
1263	5	2257	Card	\N	\N	tags	2016-06-03 12:21:34.503386
1264	1	2258	Card	\N	\N	tags	2016-06-03 12:21:37.628658
1265	4	2258	Card	\N	\N	tags	2016-06-03 12:21:37.656021
1266	6	2258	Card	\N	\N	tags	2016-06-03 12:21:37.67542
1267	1	2259	Card	\N	\N	tags	2016-06-03 12:21:40.982633
1268	4	2259	Card	\N	\N	tags	2016-06-03 12:21:41.017086
1269	6	2259	Card	\N	\N	tags	2016-06-03 12:21:41.066454
1270	1	2260	Card	\N	\N	tags	2016-06-03 12:21:44.62939
1271	4	2260	Card	\N	\N	tags	2016-06-03 12:21:44.647928
1272	6	2260	Card	\N	\N	tags	2016-06-03 12:21:44.66379
1273	1	2261	Card	\N	\N	tags	2016-06-03 12:21:49.154707
1274	4	2261	Card	\N	\N	tags	2016-06-03 12:21:49.176332
1275	6	2261	Card	\N	\N	tags	2016-06-03 12:21:49.201534
1276	1	2262	Card	\N	\N	tags	2016-06-03 12:21:52.433004
1277	4	2262	Card	\N	\N	tags	2016-06-03 12:21:52.457033
1278	6	2262	Card	\N	\N	tags	2016-06-03 12:21:52.476915
1279	1	2263	Card	\N	\N	tags	2016-06-03 12:21:55.456265
1280	4	2263	Card	\N	\N	tags	2016-06-03 12:21:55.483606
1281	6	2263	Card	\N	\N	tags	2016-06-03 12:21:55.502855
1282	1	2264	Card	\N	\N	tags	2016-06-03 12:21:57.937943
1283	4	2264	Card	\N	\N	tags	2016-06-03 12:21:57.952698
1284	6	2264	Card	\N	\N	tags	2016-06-03 12:21:57.972838
1285	1	2265	Card	\N	\N	tags	2016-06-03 12:22:00.431231
1286	4	2265	Card	\N	\N	tags	2016-06-03 12:22:00.450589
1287	6	2265	Card	\N	\N	tags	2016-06-03 12:22:00.468243
1288	1	2266	Card	\N	\N	tags	2016-06-03 12:22:03.140399
1289	4	2266	Card	\N	\N	tags	2016-06-03 12:22:03.15996
1290	7	2266	Card	\N	\N	tags	2016-06-03 12:22:03.175811
1291	1	2267	Card	\N	\N	tags	2016-06-03 12:22:05.317107
1292	4	2267	Card	\N	\N	tags	2016-06-03 12:22:05.343755
1293	6	2267	Card	\N	\N	tags	2016-06-03 12:22:05.363473
1294	1	2268	Card	\N	\N	tags	2016-06-03 12:22:10.28328
1295	4	2268	Card	\N	\N	tags	2016-06-03 12:22:10.318616
1296	6	2268	Card	\N	\N	tags	2016-06-03 12:22:10.343532
1297	1	2269	Card	\N	\N	tags	2016-06-03 12:22:10.687847
1298	4	2269	Card	\N	\N	tags	2016-06-03 12:22:10.709853
1299	6	2269	Card	\N	\N	tags	2016-06-03 12:22:10.734048
1300	1	2270	Card	\N	\N	tags	2016-06-03 12:22:11.123674
1301	4	2270	Card	\N	\N	tags	2016-06-03 12:22:11.14082
1302	6	2270	Card	\N	\N	tags	2016-06-03 12:22:11.158419
1303	1	2271	Card	\N	\N	tags	2016-06-03 12:22:11.24381
1304	4	2271	Card	\N	\N	tags	2016-06-03 12:22:11.261128
1305	6	2271	Card	\N	\N	tags	2016-06-03 12:22:11.278769
1306	1	2272	Card	\N	\N	tags	2016-06-03 12:22:11.394271
1307	4	2272	Card	\N	\N	tags	2016-06-03 12:22:11.412388
1308	8	2272	Card	\N	\N	tags	2016-06-03 12:22:11.431342
1309	1	2273	Card	\N	\N	tags	2016-06-03 12:22:11.581173
1310	4	2273	Card	\N	\N	tags	2016-06-03 12:22:11.601777
1311	1	2274	Card	\N	\N	tags	2016-06-03 12:22:11.714677
1312	9	2274	Card	\N	\N	tags	2016-06-03 12:22:11.736363
1313	1	2275	Card	\N	\N	tags	2016-06-03 12:22:11.907973
1314	10	2275	Card	\N	\N	tags	2016-06-03 12:22:11.934393
1315	1	2276	Card	\N	\N	tags	2016-06-03 12:22:12.154913
1316	11	2276	Card	\N	\N	tags	2016-06-03 12:22:12.179149
1317	1	2277	Card	\N	\N	tags	2016-06-03 12:22:12.278561
1318	11	2277	Card	\N	\N	tags	2016-06-03 12:22:12.305423
1319	1	2278	Card	\N	\N	tags	2016-06-03 12:22:12.438719
1320	11	2278	Card	\N	\N	tags	2016-06-03 12:22:12.461899
1321	1	2279	Card	\N	\N	tags	2016-06-03 12:22:13.763784
1322	11	2279	Card	\N	\N	tags	2016-06-03 12:22:13.792612
1323	1	2280	Card	\N	\N	tags	2016-06-03 12:22:13.952974
1324	12	2280	Card	\N	\N	tags	2016-06-03 12:22:13.983337
1325	1	2281	Card	\N	\N	tags	2016-06-03 12:22:14.096755
1326	11	2281	Card	\N	\N	tags	2016-06-03 12:22:14.116476
1327	1	2282	Card	\N	\N	tags	2016-06-03 12:22:14.682344
1328	11	2282	Card	\N	\N	tags	2016-06-03 12:22:14.70592
1329	1	2283	Card	\N	\N	tags	2016-06-03 12:22:15.289342
1330	11	2283	Card	\N	\N	tags	2016-06-03 12:22:15.307081
1331	1	2284	Card	\N	\N	tags	2016-06-03 12:22:15.939948
1332	11	2284	Card	\N	\N	tags	2016-06-03 12:22:15.967063
1333	1	2285	Card	\N	\N	tags	2016-06-03 12:22:16.081681
1334	11	2285	Card	\N	\N	tags	2016-06-03 12:22:16.098358
1335	1	2286	Card	\N	\N	tags	2016-06-03 12:22:16.893032
1336	13	2286	Card	\N	\N	tags	2016-06-03 12:22:16.921197
1337	14	2286	Card	\N	\N	tags	2016-06-03 12:22:16.951468
1338	1	2287	Card	\N	\N	tags	2016-06-03 12:22:17.099007
1339	14	2287	Card	\N	\N	tags	2016-06-03 12:22:17.127481
1340	1	2288	Card	\N	\N	tags	2016-06-03 12:22:17.245605
1341	14	2288	Card	\N	\N	tags	2016-06-03 12:22:17.350336
1342	11	2288	Card	\N	\N	tags	2016-06-03 12:22:17.375805
1343	1	2289	Card	\N	\N	tags	2016-06-03 12:22:17.507869
1344	14	2289	Card	\N	\N	tags	2016-06-03 12:22:17.530918
1345	11	2289	Card	\N	\N	tags	2016-06-03 12:22:17.578558
1346	15	2290	Card	\N	\N	tags	2016-06-03 12:22:18.005209
1347	16	2290	Card	\N	\N	tags	2016-06-03 12:22:18.039865
1348	15	2291	Card	\N	\N	tags	2016-06-03 12:22:19.874506
1349	16	2291	Card	\N	\N	tags	2016-06-03 12:22:19.907723
1350	15	2292	Card	\N	\N	tags	2016-06-03 12:22:20.942687
1351	16	2292	Card	\N	\N	tags	2016-06-03 12:22:20.964501
1352	15	2293	Card	\N	\N	tags	2016-06-03 12:22:21.121877
1353	17	2293	Card	\N	\N	tags	2016-06-03 12:22:21.142632
1354	15	2294	Card	\N	\N	tags	2016-06-03 12:22:21.254508
1355	18	2294	Card	\N	\N	tags	2016-06-03 12:22:21.289688
1356	19	2295	Card	\N	\N	tags	2016-06-03 12:22:21.784921
1357	20	2295	Card	\N	\N	tags	2016-06-03 12:22:21.807117
1358	21	2295	Card	\N	\N	tags	2016-06-03 12:22:21.834209
1359	19	2296	Card	\N	\N	tags	2016-06-03 12:22:22.547128
1360	20	2296	Card	\N	\N	tags	2016-06-03 12:22:22.567202
1361	21	2296	Card	\N	\N	tags	2016-06-03 12:22:22.584788
1362	19	2297	Card	\N	\N	tags	2016-06-03 12:22:24.215881
1363	20	2297	Card	\N	\N	tags	2016-06-03 12:22:24.239532
1364	21	2297	Card	\N	\N	tags	2016-06-03 12:22:24.259191
1365	19	2298	Card	\N	\N	tags	2016-06-03 12:22:25.430577
1366	20	2298	Card	\N	\N	tags	2016-06-03 12:22:25.465372
1367	21	2298	Card	\N	\N	tags	2016-06-03 12:22:25.489292
1368	19	2299	Card	\N	\N	tags	2016-06-03 12:22:26.281663
1369	20	2299	Card	\N	\N	tags	2016-06-03 12:22:26.306182
1370	21	2299	Card	\N	\N	tags	2016-06-03 12:22:26.327879
1371	19	2300	Card	\N	\N	tags	2016-06-03 12:22:27.177419
1372	20	2300	Card	\N	\N	tags	2016-06-03 12:22:27.200747
1373	21	2300	Card	\N	\N	tags	2016-06-03 12:22:27.238368
1374	19	2301	Card	\N	\N	tags	2016-06-03 12:22:28.149424
1375	20	2301	Card	\N	\N	tags	2016-06-03 12:22:28.168272
1376	21	2301	Card	\N	\N	tags	2016-06-03 12:22:28.190729
1377	19	2302	Card	\N	\N	tags	2016-06-03 12:22:28.471213
1378	22	2302	Card	\N	\N	tags	2016-06-03 12:22:28.493571
1379	19	2303	Card	\N	\N	tags	2016-06-03 12:22:30.232782
1380	22	2303	Card	\N	\N	tags	2016-06-03 12:22:30.254549
1381	19	2304	Card	\N	\N	tags	2016-06-03 12:22:30.416304
1382	23	2305	Card	\N	\N	tags	2016-06-03 12:22:30.592129
1383	23	2306	Card	\N	\N	tags	2016-06-03 12:22:30.681606
1384	24	2307	Card	\N	\N	tags	2016-06-03 12:22:30.762319
1385	19	2307	Card	\N	\N	tags	2016-06-03 12:22:30.776735
1386	25	2308	Card	\N	\N	tags	2016-06-03 12:22:31.034436
1387	26	2308	Card	\N	\N	tags	2016-06-03 12:22:31.05022
1388	25	2309	Card	\N	\N	tags	2016-06-03 12:22:32.542893
1389	26	2309	Card	\N	\N	tags	2016-06-03 12:22:32.561375
1390	25	2310	Card	\N	\N	tags	2016-06-03 12:22:33.585137
1391	26	2310	Card	\N	\N	tags	2016-06-03 12:22:33.605904
1392	25	2311	Card	\N	\N	tags	2016-06-03 12:22:35.012492
1393	26	2311	Card	\N	\N	tags	2016-06-03 12:22:35.044429
1394	25	2312	Card	\N	\N	tags	2016-06-03 12:22:35.235449
1395	26	2312	Card	\N	\N	tags	2016-06-03 12:22:35.275679
1396	25	2313	Card	\N	\N	tags	2016-06-03 12:22:36.141478
1397	26	2313	Card	\N	\N	tags	2016-06-03 12:22:36.173889
1398	25	2314	Card	\N	\N	tags	2016-06-03 12:22:38.108663
1399	26	2314	Card	\N	\N	tags	2016-06-03 12:22:38.126558
1400	25	2315	Card	\N	\N	tags	2016-06-03 12:22:39.428996
1401	26	2315	Card	\N	\N	tags	2016-06-03 12:22:39.458731
1402	25	2316	Card	\N	\N	tags	2016-06-03 12:22:40.303254
1403	26	2316	Card	\N	\N	tags	2016-06-03 12:22:40.331774
1404	25	2317	Card	\N	\N	tags	2016-06-03 12:22:42.942899
1405	26	2317	Card	\N	\N	tags	2016-06-03 12:22:42.970548
1406	25	2318	Card	\N	\N	tags	2016-06-03 12:22:44.865373
1407	26	2318	Card	\N	\N	tags	2016-06-03 12:22:44.897152
1408	25	2319	Card	\N	\N	tags	2016-06-03 12:22:45.872207
1409	26	2319	Card	\N	\N	tags	2016-06-03 12:22:45.896132
1410	27	2320	Card	\N	\N	tags	2016-06-03 12:22:49.801827
1411	28	2320	Card	\N	\N	tags	2016-06-03 12:22:49.901601
1412	27	2321	Card	\N	\N	tags	2016-06-03 12:22:50.242011
1413	29	2321	Card	\N	\N	tags	2016-06-03 12:22:50.272737
1414	30	2322	Card	\N	\N	tags	2016-06-03 12:22:50.510857
1415	31	2322	Card	\N	\N	tags	2016-06-03 12:22:50.542717
1416	32	2322	Card	\N	\N	tags	2016-06-03 12:22:50.56332
1417	33	2323	Card	\N	\N	tags	2016-06-03 12:22:51.079715
1418	34	2323	Card	\N	\N	tags	2016-06-03 12:22:51.123033
1419	33	2324	Card	\N	\N	tags	2016-06-03 12:22:51.338758
1420	35	2324	Card	\N	\N	tags	2016-06-03 12:22:51.360648
1421	36	2324	Card	\N	\N	tags	2016-06-03 12:22:51.379906
1422	37	2325	Card	\N	\N	tags	2016-06-03 12:22:51.561678
1423	38	2326	Card	\N	\N	tags	2016-06-03 12:22:52.150598
1424	39	2326	Card	\N	\N	tags	2016-06-03 12:22:52.172577
1425	40	2326	Card	\N	\N	tags	2016-06-03 12:22:52.19406
1426	38	2327	Card	\N	\N	tags	2016-06-03 12:22:52.375469
1427	41	2327	Card	\N	\N	tags	2016-06-03 12:22:52.397831
1428	40	2327	Card	\N	\N	tags	2016-06-03 12:22:52.417122
1429	38	2328	Card	\N	\N	tags	2016-06-03 12:22:52.545869
1430	42	2328	Card	\N	\N	tags	2016-06-03 12:22:52.565416
1431	43	2329	Card	\N	\N	tags	2016-06-03 12:22:52.665973
1432	38	2329	Card	\N	\N	tags	2016-06-03 12:22:52.684269
1433	44	2330	Card	\N	\N	tags	2016-06-03 12:22:52.837959
1434	38	2330	Card	\N	\N	tags	2016-06-03 12:22:52.859529
1435	45	2330	Card	\N	\N	tags	2016-06-03 12:22:52.87917
1436	44	2331	Card	\N	\N	tags	2016-06-03 12:22:53.082422
1437	38	2331	Card	\N	\N	tags	2016-06-03 12:22:53.110607
1438	45	2331	Card	\N	\N	tags	2016-06-03 12:22:53.131358
1439	45	2332	Card	\N	\N	tags	2016-06-03 12:22:53.468277
1440	23	2332	Card	\N	\N	tags	2016-06-03 12:22:53.501733
1441	46	2332	Card	\N	\N	tags	2016-06-03 12:22:53.553415
1442	47	2333	Card	\N	\N	tags	2016-06-03 12:22:53.78772
1443	23	2333	Card	\N	\N	tags	2016-06-03 12:22:53.817472
1444	45	2333	Card	\N	\N	tags	2016-06-03 12:22:53.853579
1445	48	2334	Card	\N	\N	tags	2016-06-03 12:22:54.396475
1446	49	2334	Card	\N	\N	tags	2016-06-03 12:22:54.420341
1447	50	2335	Card	\N	\N	tags	2016-06-03 12:22:54.573417
1448	49	2335	Card	\N	\N	tags	2016-06-03 12:22:54.599223
1449	50	2336	Card	\N	\N	tags	2016-06-03 12:22:55.042913
1450	49	2336	Card	\N	\N	tags	2016-06-03 12:22:55.090579
1451	51	2337	Card	\N	\N	tags	2016-06-03 12:22:55.251303
1452	49	2337	Card	\N	\N	tags	2016-06-03 12:22:55.287084
1453	52	2338	Card	\N	\N	tags	2016-06-03 12:22:55.438229
1454	49	2338	Card	\N	\N	tags	2016-06-03 12:22:55.470151
1455	53	2339	Card	\N	\N	tags	2016-06-03 12:22:55.59396
1456	54	2339	Card	\N	\N	tags	2016-06-03 12:22:55.619135
1457	49	2339	Card	\N	\N	tags	2016-06-03 12:22:55.641974
1458	53	2340	Card	\N	\N	tags	2016-06-03 12:22:55.802173
1459	54	2340	Card	\N	\N	tags	2016-06-03 12:22:55.824966
1460	49	2340	Card	\N	\N	tags	2016-06-03 12:22:55.850348
1461	53	2341	Card	\N	\N	tags	2016-06-03 12:22:56.09362
1462	55	2341	Card	\N	\N	tags	2016-06-03 12:22:56.115699
1463	49	2341	Card	\N	\N	tags	2016-06-03 12:22:56.138031
1464	53	2342	Card	\N	\N	tags	2016-06-03 12:22:56.292875
1465	56	2342	Card	\N	\N	tags	2016-06-03 12:22:56.320002
1466	49	2342	Card	\N	\N	tags	2016-06-03 12:22:56.342809
1467	57	2343	Card	\N	\N	tags	2016-06-03 12:22:56.467275
1468	49	2343	Card	\N	\N	tags	2016-06-03 12:22:56.489911
1469	35	2344	Card	\N	\N	tags	2016-06-03 12:22:56.616825
1470	49	2344	Card	\N	\N	tags	2016-06-03 12:22:56.640382
1471	58	2345	Card	\N	\N	tags	2016-06-03 12:22:56.754916
1472	33	2345	Card	\N	\N	tags	2016-06-03 12:22:56.777097
1473	49	2345	Card	\N	\N	tags	2016-06-03 12:22:56.825299
1474	59	2346	Card	\N	\N	tags	2016-06-03 12:22:56.984791
1475	60	2346	Card	\N	\N	tags	2016-06-03 12:22:57.006093
1476	49	2346	Card	\N	\N	tags	2016-06-03 12:22:57.027956
1477	60	2347	Card	\N	\N	tags	2016-06-03 12:22:57.444125
1478	59	2347	Card	\N	\N	tags	2016-06-03 12:22:57.468987
1479	49	2347	Card	\N	\N	tags	2016-06-03 12:22:57.491023
1480	61	2348	Card	\N	\N	tags	2016-06-03 12:22:57.656655
1481	62	2348	Card	\N	\N	tags	2016-06-03 12:22:57.696719
1482	49	2348	Card	\N	\N	tags	2016-06-03 12:22:57.722913
1483	61	2349	Card	\N	\N	tags	2016-06-03 12:22:57.878992
1484	62	2349	Card	\N	\N	tags	2016-06-03 12:22:57.904117
1485	49	2349	Card	\N	\N	tags	2016-06-03 12:22:57.929757
1486	53	2350	Card	\N	\N	tags	2016-06-03 12:22:58.36144
1487	63	2350	Card	\N	\N	tags	2016-06-03 12:22:58.389358
1488	49	2350	Card	\N	\N	tags	2016-06-03 12:22:58.411497
1489	64	2351	Card	\N	\N	tags	2016-06-03 12:22:58.547455
1490	63	2351	Card	\N	\N	tags	2016-06-03 12:22:58.575333
1491	49	2351	Card	\N	\N	tags	2016-06-03 12:22:58.598712
1492	64	2352	Card	\N	\N	tags	2016-06-03 12:22:58.771764
1493	63	2352	Card	\N	\N	tags	2016-06-03 12:22:58.793775
1494	49	2352	Card	\N	\N	tags	2016-06-03 12:22:58.82005
1495	64	2353	Card	\N	\N	tags	2016-06-03 12:22:58.944335
1496	63	2353	Card	\N	\N	tags	2016-06-03 12:22:58.97406
1497	49	2353	Card	\N	\N	tags	2016-06-03 12:22:59.000575
1498	64	2354	Card	\N	\N	tags	2016-06-03 12:22:59.180974
1499	63	2354	Card	\N	\N	tags	2016-06-03 12:22:59.212229
1500	65	2354	Card	\N	\N	tags	2016-06-03 12:22:59.241534
1501	64	2355	Card	\N	\N	tags	2016-06-03 12:22:59.39073
1502	63	2355	Card	\N	\N	tags	2016-06-03 12:22:59.413003
1503	65	2355	Card	\N	\N	tags	2016-06-03 12:22:59.433215
1504	64	2356	Card	\N	\N	tags	2016-06-03 12:22:59.581796
1505	63	2356	Card	\N	\N	tags	2016-06-03 12:22:59.607993
1506	65	2356	Card	\N	\N	tags	2016-06-03 12:22:59.631724
1507	64	2357	Card	\N	\N	tags	2016-06-03 12:22:59.818632
1508	63	2357	Card	\N	\N	tags	2016-06-03 12:22:59.84203
1509	65	2357	Card	\N	\N	tags	2016-06-03 12:22:59.869044
1510	64	2358	Card	\N	\N	tags	2016-06-03 12:23:00.192226
1511	63	2358	Card	\N	\N	tags	2016-06-03 12:23:00.216823
1512	65	2358	Card	\N	\N	tags	2016-06-03 12:23:00.244063
1513	47	2359	Card	\N	\N	tags	2016-06-03 12:23:00.865893
1514	66	2359	Card	\N	\N	tags	2016-06-03 12:23:00.913997
1515	66	2360	Card	\N	\N	tags	2016-06-03 12:23:01.194598
1516	67	2360	Card	\N	\N	tags	2016-06-03 12:23:01.231095
1517	66	2361	Card	\N	\N	tags	2016-06-03 12:23:01.428679
1518	68	2361	Card	\N	\N	tags	2016-06-03 12:23:01.456733
1519	66	2362	Card	\N	\N	tags	2016-06-03 12:23:01.786535
1520	69	2362	Card	\N	\N	tags	2016-06-03 12:23:01.813461
1521	66	2363	Card	\N	\N	tags	2016-06-03 12:23:01.974587
1522	70	2363	Card	\N	\N	tags	2016-06-03 12:23:02.025949
1523	66	2364	Card	\N	\N	tags	2016-06-03 12:23:02.53483
1524	47	2364	Card	\N	\N	tags	2016-06-03 12:23:02.559852
1525	66	2365	Card	\N	\N	tags	2016-06-03 12:23:02.776925
1526	71	2365	Card	\N	\N	tags	2016-06-03 12:23:02.820529
1527	66	2366	Card	\N	\N	tags	2016-06-03 12:23:03.020429
1528	72	2366	Card	\N	\N	tags	2016-06-03 12:23:03.04312
1529	73	2366	Card	\N	\N	tags	2016-06-03 12:23:03.068995
1530	66	2367	Card	\N	\N	tags	2016-06-03 12:23:03.236308
1531	74	2367	Card	\N	\N	tags	2016-06-03 12:23:03.26985
1532	72	2367	Card	\N	\N	tags	2016-06-03 12:23:03.30227
1533	66	2368	Card	\N	\N	tags	2016-06-03 12:23:03.42774
1534	75	2368	Card	\N	\N	tags	2016-06-03 12:23:03.450281
1535	66	2369	Card	\N	\N	tags	2016-06-03 12:23:03.604621
1536	74	2369	Card	\N	\N	tags	2016-06-03 12:23:03.62739
1537	66	2370	Card	\N	\N	tags	2016-06-03 12:23:03.770639
1538	68	2370	Card	\N	\N	tags	2016-06-03 12:23:03.789254
1539	76	2371	Card	\N	\N	tags	2016-06-03 12:23:04.457877
1540	77	2371	Card	\N	\N	tags	2016-06-03 12:23:04.501806
1541	76	2372	Card	\N	\N	tags	2016-06-03 12:23:04.752713
1542	78	2372	Card	\N	\N	tags	2016-06-03 12:23:04.774332
1543	76	2373	Card	\N	\N	tags	2016-06-03 12:23:05.947665
1544	77	2373	Card	\N	\N	tags	2016-06-03 12:23:05.969275
1545	76	2374	Card	\N	\N	tags	2016-06-03 12:23:06.264872
1546	77	2374	Card	\N	\N	tags	2016-06-03 12:23:06.297401
1547	76	2375	Card	\N	\N	tags	2016-06-03 12:23:07.1994
1548	77	2375	Card	\N	\N	tags	2016-06-03 12:23:07.241572
1549	79	2376	Card	\N	\N	tags	2016-06-03 12:23:08.323412
1550	80	2376	Card	\N	\N	tags	2016-06-03 12:23:08.353384
1551	79	2377	Card	\N	\N	tags	2016-06-03 12:23:08.7669
1552	81	2377	Card	\N	\N	tags	2016-06-03 12:23:08.788679
1553	79	2378	Card	\N	\N	tags	2016-06-03 12:23:09.124908
1554	66	2378	Card	\N	\N	tags	2016-06-03 12:23:09.164588
1555	79	2379	Card	\N	\N	tags	2016-06-03 12:23:09.359837
1556	66	2379	Card	\N	\N	tags	2016-06-03 12:23:09.392301
1557	60	2379	Card	\N	\N	tags	2016-06-03 12:23:09.423068
1558	79	2380	Card	\N	\N	tags	2016-06-03 12:23:09.625064
1559	66	2380	Card	\N	\N	tags	2016-06-03 12:23:09.660028
1560	79	2381	Card	\N	\N	tags	2016-06-03 12:23:09.873591
1561	82	2381	Card	\N	\N	tags	2016-06-03 12:23:09.910821
1562	32	2382	Card	\N	\N	tags	2016-06-03 12:23:10.294643
1563	83	2382	Card	\N	\N	tags	2016-06-03 12:23:10.324303
1564	84	2382	Card	\N	\N	tags	2016-06-03 12:23:10.358716
1565	32	2383	Card	\N	\N	tags	2016-06-03 12:23:10.544526
1566	85	2383	Card	\N	\N	tags	2016-06-03 12:23:10.574184
1567	66	2383	Card	\N	\N	tags	2016-06-03 12:23:10.60075
1568	32	2384	Card	\N	\N	tags	2016-06-03 12:23:10.745588
1569	86	2384	Card	\N	\N	tags	2016-06-03 12:23:10.784821
1570	32	2385	Card	\N	\N	tags	2016-06-03 12:23:10.960756
1571	87	2385	Card	\N	\N	tags	2016-06-03 12:23:11.004344
1572	32	2386	Card	\N	\N	tags	2016-06-03 12:23:11.240239
1573	24	2386	Card	\N	\N	tags	2016-06-03 12:23:11.278885
1574	32	2387	Card	\N	\N	tags	2016-06-03 12:23:11.436355
1575	84	2387	Card	\N	\N	tags	2016-06-03 12:23:11.472202
1576	88	2387	Card	\N	\N	tags	2016-06-03 12:23:11.508393
1577	32	2388	Card	\N	\N	tags	2016-06-03 12:23:11.693567
1578	89	2388	Card	\N	\N	tags	2016-06-03 12:23:11.726148
1579	66	2388	Card	\N	\N	tags	2016-06-03 12:23:11.7588
1580	32	2389	Card	\N	\N	tags	2016-06-03 12:23:11.913459
1581	90	2389	Card	\N	\N	tags	2016-06-03 12:23:11.94606
1582	32	2390	Card	\N	\N	tags	2016-06-03 12:23:12.120019
1583	86	2390	Card	\N	\N	tags	2016-06-03 12:23:12.15988
1584	32	2391	Card	\N	\N	tags	2016-06-03 12:23:12.35774
1585	22	2391	Card	\N	\N	tags	2016-06-03 12:23:12.384827
1586	32	2392	Card	\N	\N	tags	2016-06-03 12:23:12.557484
1587	91	2392	Card	\N	\N	tags	2016-06-03 12:23:12.587307
1588	32	2393	Card	\N	\N	tags	2016-06-03 12:23:12.817935
1589	92	2393	Card	\N	\N	tags	2016-06-03 12:23:12.846072
1590	32	2394	Card	\N	\N	tags	2016-06-03 12:23:13.073468
1591	93	2394	Card	\N	\N	tags	2016-06-03 12:23:13.102503
1592	32	2395	Card	\N	\N	tags	2016-06-03 12:23:13.295024
1593	94	2395	Card	\N	\N	tags	2016-06-03 12:23:13.334072
1594	32	2396	Card	\N	\N	tags	2016-06-03 12:23:13.595892
1595	95	2396	Card	\N	\N	tags	2016-06-03 12:23:13.634103
1596	32	2397	Card	\N	\N	tags	2016-06-03 12:23:13.821365
1597	96	2397	Card	\N	\N	tags	2016-06-03 12:23:13.854192
1598	32	2398	Card	\N	\N	tags	2016-06-03 12:23:14.05801
1599	97	2398	Card	\N	\N	tags	2016-06-03 12:23:14.096381
1600	98	2398	Card	\N	\N	tags	2016-06-03 12:23:14.129805
1601	32	2399	Card	\N	\N	tags	2016-06-03 12:23:14.332127
1602	99	2399	Card	\N	\N	tags	2016-06-03 12:23:14.361198
1603	32	2400	Card	\N	\N	tags	2016-06-03 12:23:14.526291
1604	81	2400	Card	\N	\N	tags	2016-06-03 12:23:14.555684
1605	100	2401	Card	\N	\N	tags	2016-06-03 12:23:14.964414
1606	101	2401	Card	\N	\N	tags	2016-06-03 12:23:14.998385
1607	66	2401	Card	\N	\N	tags	2016-06-03 12:23:15.029174
1608	100	2402	Card	\N	\N	tags	2016-06-03 12:23:15.213598
1609	101	2402	Card	\N	\N	tags	2016-06-03 12:23:15.253842
1610	66	2402	Card	\N	\N	tags	2016-06-03 12:23:15.289979
1611	100	2403	Card	\N	\N	tags	2016-06-03 12:23:15.471496
1612	24	2403	Card	\N	\N	tags	2016-06-03 12:23:15.504702
1613	66	2403	Card	\N	\N	tags	2016-06-03 12:23:15.541188
1614	100	2404	Card	\N	\N	tags	2016-06-03 12:23:15.775277
1615	102	2404	Card	\N	\N	tags	2016-06-03 12:23:15.813208
1616	100	2405	Card	\N	\N	tags	2016-06-03 12:23:15.959128
1617	102	2405	Card	\N	\N	tags	2016-06-03 12:23:15.985246
1618	66	2405	Card	\N	\N	tags	2016-06-03 12:23:16.00346
1619	100	2406	Card	\N	\N	tags	2016-06-03 12:23:16.136713
1620	103	2406	Card	\N	\N	tags	2016-06-03 12:23:16.158315
1621	66	2406	Card	\N	\N	tags	2016-06-03 12:23:16.177209
1622	100	2407	Card	\N	\N	tags	2016-06-03 12:23:16.321298
1623	104	2407	Card	\N	\N	tags	2016-06-03 12:23:16.350184
1624	66	2407	Card	\N	\N	tags	2016-06-03 12:23:16.370938
1625	100	2408	Card	\N	\N	tags	2016-06-03 12:23:16.507516
1626	105	2408	Card	\N	\N	tags	2016-06-03 12:23:16.531003
1627	66	2408	Card	\N	\N	tags	2016-06-03 12:23:16.551783
1628	100	2409	Card	\N	\N	tags	2016-06-03 12:23:16.736676
1629	106	2409	Card	\N	\N	tags	2016-06-03 12:23:16.761089
1630	100	2410	Card	\N	\N	tags	2016-06-03 12:23:16.973131
1631	66	2410	Card	\N	\N	tags	2016-06-03 12:23:16.99264
1632	60	2411	Card	\N	\N	tags	2016-06-03 12:23:19.382442
1633	107	2411	Card	\N	\N	tags	2016-06-03 12:23:19.443815
1634	47	2411	Card	\N	\N	tags	2016-06-03 12:23:19.494736
1635	1	2412	Card	\N	\N	tags	2016-06-03 12:23:20.305156
1636	13	2412	Card	\N	\N	tags	2016-06-03 12:23:20.353658
1637	1	2413	Card	\N	\N	tags	2016-06-03 12:23:23.438701
1638	13	2413	Card	\N	\N	tags	2016-06-03 12:23:23.488356
1639	60	2414	Card	\N	\N	tags	2016-06-03 12:23:24.964253
1640	107	2414	Card	\N	\N	tags	2016-06-03 12:23:24.995785
1641	47	2414	Card	\N	\N	tags	2016-06-03 12:23:25.027653
1642	1	2415	Card	\N	\N	tags	2016-06-03 12:23:26.161267
1643	13	2415	Card	\N	\N	tags	2016-06-03 12:23:26.184242
1644	60	2417	Card	\N	\N	tags	2016-06-03 12:23:27.184841
1645	107	2417	Card	\N	\N	tags	2016-06-03 12:23:27.216231
1646	47	2417	Card	\N	\N	tags	2016-06-03 12:23:27.277713
1647	1	2418	Card	\N	\N	tags	2016-06-03 12:23:27.888501
1648	13	2418	Card	\N	\N	tags	2016-06-03 12:23:27.942956
1649	1	2419	Card	\N	\N	tags	2016-06-03 12:23:28.684425
1650	13	2419	Card	\N	\N	tags	2016-06-03 12:23:28.705629
1651	60	2420	Card	\N	\N	tags	2016-06-03 12:23:29.332087
1652	107	2420	Card	\N	\N	tags	2016-06-03 12:23:29.358592
1653	47	2420	Card	\N	\N	tags	2016-06-03 12:23:29.386914
1654	1	2421	Card	\N	\N	tags	2016-06-03 12:23:30.016367
1655	13	2421	Card	\N	\N	tags	2016-06-03 12:23:30.04201
1656	1	2425	Card	\N	\N	tags	2016-06-03 12:47:59.345675
1657	2	2425	Card	\N	\N	tags	2016-06-03 12:47:59.376643
1658	1	2426	Card	\N	\N	tags	2016-06-03 12:49:48.471697
1659	2	2426	Card	\N	\N	tags	2016-06-03 12:49:48.503457
1660	1	2427	Card	\N	\N	tags	2016-06-03 12:49:48.693278
1661	2	2427	Card	\N	\N	tags	2016-06-03 12:49:48.721743
1662	3	2427	Card	\N	\N	tags	2016-06-03 12:49:48.745243
1663	1	2428	Card	\N	\N	tags	2016-06-03 12:49:48.865188
1664	4	2428	Card	\N	\N	tags	2016-06-03 12:49:48.88645
1665	5	2428	Card	\N	\N	tags	2016-06-03 12:49:48.911815
1666	1	2429	Card	\N	\N	tags	2016-06-03 12:51:05.026431
1667	2	2429	Card	\N	\N	tags	2016-06-03 12:51:05.049475
1668	1	2430	Card	\N	\N	tags	2016-06-03 12:51:05.244974
1669	2	2430	Card	\N	\N	tags	2016-06-03 12:51:05.266558
1670	3	2430	Card	\N	\N	tags	2016-06-03 12:51:05.287525
1671	1	2431	Card	\N	\N	tags	2016-06-03 12:51:05.400649
1672	4	2431	Card	\N	\N	tags	2016-06-03 12:51:05.422613
1673	5	2431	Card	\N	\N	tags	2016-06-03 12:51:05.450094
1674	1	2432	Card	\N	\N	tags	2016-06-03 12:51:30.987981
1675	2	2432	Card	\N	\N	tags	2016-06-03 12:51:31.025645
1676	1	2433	Card	\N	\N	tags	2016-06-03 12:51:31.247365
1677	2	2433	Card	\N	\N	tags	2016-06-03 12:51:31.269686
1678	3	2433	Card	\N	\N	tags	2016-06-03 12:51:31.292268
1679	1	2434	Card	\N	\N	tags	2016-06-03 12:51:31.41363
1680	4	2434	Card	\N	\N	tags	2016-06-03 12:51:31.438525
1681	5	2434	Card	\N	\N	tags	2016-06-03 12:51:31.462325
1682	1	2435	Card	\N	\N	tags	2016-06-03 12:51:32.938042
1683	4	2435	Card	\N	\N	tags	2016-06-03 12:51:32.982003
1684	5	2435	Card	\N	\N	tags	2016-06-03 12:51:33.015152
1685	1	2436	Card	\N	\N	tags	2016-06-03 12:51:35.066817
1686	4	2436	Card	\N	\N	tags	2016-06-03 12:51:35.097471
1687	6	2436	Card	\N	\N	tags	2016-06-03 12:51:35.171892
1688	1	2437	Card	\N	\N	tags	2016-06-03 12:51:37.319281
1689	4	2437	Card	\N	\N	tags	2016-06-03 12:51:37.354627
1690	6	2437	Card	\N	\N	tags	2016-06-03 12:51:37.393991
1691	1	2438	Card	\N	\N	tags	2016-06-03 12:51:39.697304
1692	4	2438	Card	\N	\N	tags	2016-06-03 12:51:39.751946
1693	6	2438	Card	\N	\N	tags	2016-06-03 12:51:39.828325
1694	1	2439	Card	\N	\N	tags	2016-06-03 12:51:42.382859
1695	4	2439	Card	\N	\N	tags	2016-06-03 12:51:42.424773
1696	6	2439	Card	\N	\N	tags	2016-06-03 12:51:42.481316
1697	1	2440	Card	\N	\N	tags	2016-06-03 12:51:44.462897
1698	4	2440	Card	\N	\N	tags	2016-06-03 12:51:44.503098
1699	6	2440	Card	\N	\N	tags	2016-06-03 12:51:44.542339
1700	1	2441	Card	\N	\N	tags	2016-06-03 12:51:46.256188
1701	4	2441	Card	\N	\N	tags	2016-06-03 12:51:46.306649
1702	6	2441	Card	\N	\N	tags	2016-06-03 12:51:46.351331
1703	1	2442	Card	\N	\N	tags	2016-06-03 12:51:48.385323
1704	4	2442	Card	\N	\N	tags	2016-06-03 12:51:48.420899
1705	6	2442	Card	\N	\N	tags	2016-06-03 12:51:48.480354
1706	1	2443	Card	\N	\N	tags	2016-06-03 12:51:50.440469
1707	4	2443	Card	\N	\N	tags	2016-06-03 12:51:50.490969
1708	6	2443	Card	\N	\N	tags	2016-06-03 12:51:50.533666
1709	1	2444	Card	\N	\N	tags	2016-06-03 12:51:53.43346
1710	4	2444	Card	\N	\N	tags	2016-06-03 12:51:53.475297
1711	7	2444	Card	\N	\N	tags	2016-06-03 12:51:53.506244
1712	1	2445	Card	\N	\N	tags	2016-06-03 12:51:55.384788
1713	4	2445	Card	\N	\N	tags	2016-06-03 12:51:55.415932
1714	6	2445	Card	\N	\N	tags	2016-06-03 12:51:55.446032
1715	1	2446	Card	\N	\N	tags	2016-06-03 12:51:57.817067
1716	4	2446	Card	\N	\N	tags	2016-06-03 12:51:57.855156
1717	6	2446	Card	\N	\N	tags	2016-06-03 12:51:57.905251
1718	1	2447	Card	\N	\N	tags	2016-06-03 12:51:58.225566
1719	4	2447	Card	\N	\N	tags	2016-06-03 12:51:58.24813
1720	6	2447	Card	\N	\N	tags	2016-06-03 12:51:58.269136
1721	1	2448	Card	\N	\N	tags	2016-06-03 12:51:58.538527
1722	4	2448	Card	\N	\N	tags	2016-06-03 12:51:58.559028
1723	6	2448	Card	\N	\N	tags	2016-06-03 12:51:58.581598
1724	1	2449	Card	\N	\N	tags	2016-06-03 12:51:58.693311
1725	4	2449	Card	\N	\N	tags	2016-06-03 12:51:58.715876
1726	6	2449	Card	\N	\N	tags	2016-06-03 12:51:58.738059
1727	1	2450	Card	\N	\N	tags	2016-06-03 12:51:58.873557
1728	4	2450	Card	\N	\N	tags	2016-06-03 12:51:58.899681
1729	8	2450	Card	\N	\N	tags	2016-06-03 12:51:58.917009
1730	1	2451	Card	\N	\N	tags	2016-06-03 12:51:59.083267
1731	4	2451	Card	\N	\N	tags	2016-06-03 12:51:59.10677
1732	1	2452	Card	\N	\N	tags	2016-06-03 12:51:59.250753
1733	9	2452	Card	\N	\N	tags	2016-06-03 12:51:59.27172
1734	1	2453	Card	\N	\N	tags	2016-06-03 12:51:59.460894
1735	10	2453	Card	\N	\N	tags	2016-06-03 12:51:59.48279
1736	1	2454	Card	\N	\N	tags	2016-06-03 12:51:59.707893
1737	11	2454	Card	\N	\N	tags	2016-06-03 12:51:59.731673
1738	1	2455	Card	\N	\N	tags	2016-06-03 12:51:59.841801
1739	11	2455	Card	\N	\N	tags	2016-06-03 12:51:59.863972
1740	1	2456	Card	\N	\N	tags	2016-06-03 12:51:59.984297
1741	11	2456	Card	\N	\N	tags	2016-06-03 12:52:00.008594
1742	1	2457	Card	\N	\N	tags	2016-06-03 12:52:01.002545
1743	11	2457	Card	\N	\N	tags	2016-06-03 12:52:01.038104
1744	1	2458	Card	\N	\N	tags	2016-06-03 12:52:01.304255
1745	12	2458	Card	\N	\N	tags	2016-06-03 12:52:01.343123
1746	1	2459	Card	\N	\N	tags	2016-06-03 12:52:01.540404
1747	11	2459	Card	\N	\N	tags	2016-06-03 12:52:01.579902
1748	1	2460	Card	\N	\N	tags	2016-06-03 12:52:02.237637
1749	11	2460	Card	\N	\N	tags	2016-06-03 12:52:02.303042
1750	1	2461	Card	\N	\N	tags	2016-06-03 12:52:03.071598
1751	11	2461	Card	\N	\N	tags	2016-06-03 12:52:03.121126
1752	1	2462	Card	\N	\N	tags	2016-06-03 12:52:03.750583
1753	11	2462	Card	\N	\N	tags	2016-06-03 12:52:03.804663
1754	1	2463	Card	\N	\N	tags	2016-06-03 12:52:03.995941
1755	11	2463	Card	\N	\N	tags	2016-06-03 12:52:04.032817
1756	1	2464	Card	\N	\N	tags	2016-06-03 12:52:04.65875
1757	13	2464	Card	\N	\N	tags	2016-06-03 12:52:04.700141
1758	14	2464	Card	\N	\N	tags	2016-06-03 12:52:04.761044
1759	1	2465	Card	\N	\N	tags	2016-06-03 12:52:05.006229
1760	14	2465	Card	\N	\N	tags	2016-06-03 12:52:05.044381
1761	1	2466	Card	\N	\N	tags	2016-06-03 12:52:05.228297
1762	14	2466	Card	\N	\N	tags	2016-06-03 12:52:05.26535
1763	11	2466	Card	\N	\N	tags	2016-06-03 12:52:05.296571
1764	1	2467	Card	\N	\N	tags	2016-06-03 12:52:05.483719
1765	14	2467	Card	\N	\N	tags	2016-06-03 12:52:05.507799
1766	11	2467	Card	\N	\N	tags	2016-06-03 12:52:05.5331
1767	15	2468	Card	\N	\N	tags	2016-06-03 12:52:05.878425
1768	16	2468	Card	\N	\N	tags	2016-06-03 12:52:05.906759
1769	15	2469	Card	\N	\N	tags	2016-06-03 12:52:06.912245
1770	16	2469	Card	\N	\N	tags	2016-06-03 12:52:06.959799
1771	15	2470	Card	\N	\N	tags	2016-06-03 12:52:07.785186
1772	16	2470	Card	\N	\N	tags	2016-06-03 12:52:07.819056
1773	15	2471	Card	\N	\N	tags	2016-06-03 12:52:08.143433
1774	17	2471	Card	\N	\N	tags	2016-06-03 12:52:08.178936
1775	15	2472	Card	\N	\N	tags	2016-06-03 12:52:08.358429
1776	18	2472	Card	\N	\N	tags	2016-06-03 12:52:08.40129
1777	19	2473	Card	\N	\N	tags	2016-06-03 12:52:25.656365
1778	20	2473	Card	\N	\N	tags	2016-06-03 12:52:25.724243
1779	21	2473	Card	\N	\N	tags	2016-06-03 12:52:25.760841
1780	19	2474	Card	\N	\N	tags	2016-06-03 12:52:28.470904
1781	20	2474	Card	\N	\N	tags	2016-06-03 12:52:28.509346
1782	21	2474	Card	\N	\N	tags	2016-06-03 12:52:28.549276
1783	19	2475	Card	\N	\N	tags	2016-06-03 12:52:31.138329
1784	20	2475	Card	\N	\N	tags	2016-06-03 12:52:31.168579
1785	21	2475	Card	\N	\N	tags	2016-06-03 12:52:31.201117
1786	19	2476	Card	\N	\N	tags	2016-06-03 12:52:33.614683
1787	20	2476	Card	\N	\N	tags	2016-06-03 12:52:33.660675
1788	21	2476	Card	\N	\N	tags	2016-06-03 12:52:33.780555
1789	19	2477	Card	\N	\N	tags	2016-06-03 12:52:35.45883
1790	20	2477	Card	\N	\N	tags	2016-06-03 12:52:35.50762
1791	21	2477	Card	\N	\N	tags	2016-06-03 12:52:35.540129
1792	19	2478	Card	\N	\N	tags	2016-06-03 12:52:38.094727
1793	20	2478	Card	\N	\N	tags	2016-06-03 12:52:38.129661
1794	21	2478	Card	\N	\N	tags	2016-06-03 12:52:38.191165
1795	19	2479	Card	\N	\N	tags	2016-06-03 12:52:40.8643
1796	20	2479	Card	\N	\N	tags	2016-06-03 12:52:40.900098
1797	21	2479	Card	\N	\N	tags	2016-06-03 12:52:40.945738
1798	19	2480	Card	\N	\N	tags	2016-06-03 12:52:41.381815
1799	22	2480	Card	\N	\N	tags	2016-06-03 12:52:41.413778
1800	19	2481	Card	\N	\N	tags	2016-06-03 12:52:42.817664
1801	22	2481	Card	\N	\N	tags	2016-06-03 12:52:42.869286
1802	19	2482	Card	\N	\N	tags	2016-06-03 12:52:43.258571
1803	23	2483	Card	\N	\N	tags	2016-06-03 12:52:43.559778
1804	23	2484	Card	\N	\N	tags	2016-06-03 12:52:43.702197
1805	24	2485	Card	\N	\N	tags	2016-06-03 12:52:43.839753
1806	19	2485	Card	\N	\N	tags	2016-06-03 12:52:43.866648
1807	25	2486	Card	\N	\N	tags	2016-06-03 12:52:44.785105
1808	26	2486	Card	\N	\N	tags	2016-06-03 12:52:44.803457
1809	25	2487	Card	\N	\N	tags	2016-06-03 12:52:46.692051
1810	26	2487	Card	\N	\N	tags	2016-06-03 12:52:46.764872
1811	25	2488	Card	\N	\N	tags	2016-06-03 12:52:48.574675
1812	26	2488	Card	\N	\N	tags	2016-06-03 12:52:48.614868
1813	25	2489	Card	\N	\N	tags	2016-06-03 12:52:50.402699
1814	26	2489	Card	\N	\N	tags	2016-06-03 12:52:50.442173
1815	25	2490	Card	\N	\N	tags	2016-06-03 12:52:51.418478
1816	26	2490	Card	\N	\N	tags	2016-06-03 12:52:51.439933
1817	25	2491	Card	\N	\N	tags	2016-06-03 12:52:53.197402
1818	26	2491	Card	\N	\N	tags	2016-06-03 12:52:53.241971
1819	25	2492	Card	\N	\N	tags	2016-06-03 12:52:55.769506
1820	26	2492	Card	\N	\N	tags	2016-06-03 12:52:55.847454
1821	25	2493	Card	\N	\N	tags	2016-06-03 12:52:57.711295
1822	26	2493	Card	\N	\N	tags	2016-06-03 12:52:57.809967
1823	25	2494	Card	\N	\N	tags	2016-06-03 12:52:59.871796
1824	26	2494	Card	\N	\N	tags	2016-06-03 12:52:59.921603
1825	25	2495	Card	\N	\N	tags	2016-06-03 12:53:00.802963
1826	26	2495	Card	\N	\N	tags	2016-06-03 12:53:00.828431
1827	25	2496	Card	\N	\N	tags	2016-06-03 12:53:04.077581
1828	26	2496	Card	\N	\N	tags	2016-06-03 12:53:04.111976
1829	25	2497	Card	\N	\N	tags	2016-06-03 12:53:05.696912
1830	26	2497	Card	\N	\N	tags	2016-06-03 12:53:05.755223
1831	27	2498	Card	\N	\N	tags	2016-06-03 12:53:08.061093
1832	28	2498	Card	\N	\N	tags	2016-06-03 12:53:08.175594
1833	27	2499	Card	\N	\N	tags	2016-06-03 12:53:08.57615
1834	29	2499	Card	\N	\N	tags	2016-06-03 12:53:08.600799
1835	30	2500	Card	\N	\N	tags	2016-06-03 12:53:08.914573
1836	31	2500	Card	\N	\N	tags	2016-06-03 12:53:08.934959
1837	32	2500	Card	\N	\N	tags	2016-06-03 12:53:08.963262
1838	33	2501	Card	\N	\N	tags	2016-06-03 12:53:10.082706
1839	34	2501	Card	\N	\N	tags	2016-06-03 12:53:10.102278
1840	33	2502	Card	\N	\N	tags	2016-06-03 12:53:10.344154
1841	35	2502	Card	\N	\N	tags	2016-06-03 12:53:10.368926
1842	36	2502	Card	\N	\N	tags	2016-06-03 12:53:10.390567
1843	37	2503	Card	\N	\N	tags	2016-06-03 12:53:11.396882
1844	38	2504	Card	\N	\N	tags	2016-06-03 12:53:14.511875
1845	39	2504	Card	\N	\N	tags	2016-06-03 12:53:14.537214
1846	40	2504	Card	\N	\N	tags	2016-06-03 12:53:14.560754
1847	38	2505	Card	\N	\N	tags	2016-06-03 12:53:15.658534
1848	41	2505	Card	\N	\N	tags	2016-06-03 12:53:15.687071
1849	40	2505	Card	\N	\N	tags	2016-06-03 12:53:15.714969
1850	38	2506	Card	\N	\N	tags	2016-06-03 12:53:16.686577
1851	42	2506	Card	\N	\N	tags	2016-06-03 12:53:16.712692
1852	43	2507	Card	\N	\N	tags	2016-06-03 12:53:16.840958
1853	38	2507	Card	\N	\N	tags	2016-06-03 12:53:16.862797
1854	44	2508	Card	\N	\N	tags	2016-06-03 12:53:17.87574
1855	38	2508	Card	\N	\N	tags	2016-06-03 12:53:17.898561
1856	45	2508	Card	\N	\N	tags	2016-06-03 12:53:17.926911
1857	44	2509	Card	\N	\N	tags	2016-06-03 12:53:18.156192
1858	38	2509	Card	\N	\N	tags	2016-06-03 12:53:18.181447
1859	45	2509	Card	\N	\N	tags	2016-06-03 12:53:18.205388
1860	45	2510	Card	\N	\N	tags	2016-06-03 12:53:18.590815
1861	23	2510	Card	\N	\N	tags	2016-06-03 12:53:18.614451
1862	46	2510	Card	\N	\N	tags	2016-06-03 12:53:18.638504
1863	47	2511	Card	\N	\N	tags	2016-06-03 12:53:18.81152
1864	23	2511	Card	\N	\N	tags	2016-06-03 12:53:18.833937
1865	45	2511	Card	\N	\N	tags	2016-06-03 12:53:18.857016
1866	48	2512	Card	\N	\N	tags	2016-06-03 12:53:19.282771
1867	49	2512	Card	\N	\N	tags	2016-06-03 12:53:19.306801
1868	50	2513	Card	\N	\N	tags	2016-06-03 12:53:19.461486
1869	49	2513	Card	\N	\N	tags	2016-06-03 12:53:19.48622
1870	50	2514	Card	\N	\N	tags	2016-06-03 12:53:19.659859
1871	49	2514	Card	\N	\N	tags	2016-06-03 12:53:19.680496
1872	51	2515	Card	\N	\N	tags	2016-06-03 12:53:19.84074
1873	49	2515	Card	\N	\N	tags	2016-06-03 12:53:19.864854
1874	52	2516	Card	\N	\N	tags	2016-06-03 12:53:19.995982
1875	49	2516	Card	\N	\N	tags	2016-06-03 12:53:20.017414
1876	53	2517	Card	\N	\N	tags	2016-06-03 12:53:20.137917
1877	54	2517	Card	\N	\N	tags	2016-06-03 12:53:20.159745
1878	49	2517	Card	\N	\N	tags	2016-06-03 12:53:20.18126
1879	53	2518	Card	\N	\N	tags	2016-06-03 12:53:20.307535
1880	54	2518	Card	\N	\N	tags	2016-06-03 12:53:20.331151
1881	49	2518	Card	\N	\N	tags	2016-06-03 12:53:20.353679
1882	53	2519	Card	\N	\N	tags	2016-06-03 12:53:20.582086
1883	55	2519	Card	\N	\N	tags	2016-06-03 12:53:20.605672
1884	49	2519	Card	\N	\N	tags	2016-06-03 12:53:20.629371
1885	53	2520	Card	\N	\N	tags	2016-06-03 12:53:20.797285
1886	56	2520	Card	\N	\N	tags	2016-06-03 12:53:20.82118
1887	49	2520	Card	\N	\N	tags	2016-06-03 12:53:20.847316
1888	57	2521	Card	\N	\N	tags	2016-06-03 12:53:20.971491
1889	49	2521	Card	\N	\N	tags	2016-06-03 12:53:20.99553
1890	35	2522	Card	\N	\N	tags	2016-06-03 12:53:21.12378
1891	49	2522	Card	\N	\N	tags	2016-06-03 12:53:21.148102
1892	58	2523	Card	\N	\N	tags	2016-06-03 12:53:21.272613
1893	33	2523	Card	\N	\N	tags	2016-06-03 12:53:21.298515
1894	49	2523	Card	\N	\N	tags	2016-06-03 12:53:21.321679
1895	59	2524	Card	\N	\N	tags	2016-06-03 12:53:21.475812
1896	60	2524	Card	\N	\N	tags	2016-06-03 12:53:21.49644
1897	49	2524	Card	\N	\N	tags	2016-06-03 12:53:21.518569
1898	60	2525	Card	\N	\N	tags	2016-06-03 12:53:21.87436
1899	59	2525	Card	\N	\N	tags	2016-06-03 12:53:21.901213
1900	49	2525	Card	\N	\N	tags	2016-06-03 12:53:21.921023
1901	61	2526	Card	\N	\N	tags	2016-06-03 12:53:22.104664
1902	62	2526	Card	\N	\N	tags	2016-06-03 12:53:22.130025
1903	49	2526	Card	\N	\N	tags	2016-06-03 12:53:22.148697
1904	61	2527	Card	\N	\N	tags	2016-06-03 12:53:22.285295
1905	62	2527	Card	\N	\N	tags	2016-06-03 12:53:22.309625
1906	49	2527	Card	\N	\N	tags	2016-06-03 12:53:22.335405
1907	53	2528	Card	\N	\N	tags	2016-06-03 12:53:22.660861
1908	63	2528	Card	\N	\N	tags	2016-06-03 12:53:22.684851
1909	49	2528	Card	\N	\N	tags	2016-06-03 12:53:22.704554
1910	64	2529	Card	\N	\N	tags	2016-06-03 12:53:22.841051
1911	63	2529	Card	\N	\N	tags	2016-06-03 12:53:22.866683
1912	49	2529	Card	\N	\N	tags	2016-06-03 12:53:22.888915
1913	64	2530	Card	\N	\N	tags	2016-06-03 12:53:23.077913
1914	63	2530	Card	\N	\N	tags	2016-06-03 12:53:23.102903
1915	49	2530	Card	\N	\N	tags	2016-06-03 12:53:23.128276
1916	64	2531	Card	\N	\N	tags	2016-06-03 12:53:23.256656
1917	63	2531	Card	\N	\N	tags	2016-06-03 12:53:23.277759
1918	49	2531	Card	\N	\N	tags	2016-06-03 12:53:23.302811
1919	64	2532	Card	\N	\N	tags	2016-06-03 12:53:23.448935
1920	63	2532	Card	\N	\N	tags	2016-06-03 12:53:23.480584
1921	65	2532	Card	\N	\N	tags	2016-06-03 12:53:23.504109
1922	64	2533	Card	\N	\N	tags	2016-06-03 12:53:23.650184
1923	63	2533	Card	\N	\N	tags	2016-06-03 12:53:23.6683
1924	65	2533	Card	\N	\N	tags	2016-06-03 12:53:23.695592
1925	64	2534	Card	\N	\N	tags	2016-06-03 12:53:23.844967
1926	63	2534	Card	\N	\N	tags	2016-06-03 12:53:23.866133
1927	65	2534	Card	\N	\N	tags	2016-06-03 12:53:23.884014
1928	64	2535	Card	\N	\N	tags	2016-06-03 12:53:24.048947
1929	63	2535	Card	\N	\N	tags	2016-06-03 12:53:24.073806
1930	65	2535	Card	\N	\N	tags	2016-06-03 12:53:24.096462
1931	64	2536	Card	\N	\N	tags	2016-06-03 12:53:24.379328
1932	63	2536	Card	\N	\N	tags	2016-06-03 12:53:24.400285
1933	65	2536	Card	\N	\N	tags	2016-06-03 12:53:24.421993
1934	47	2537	Card	\N	\N	tags	2016-06-03 12:53:24.880546
1935	66	2537	Card	\N	\N	tags	2016-06-03 12:53:24.900209
1936	66	2538	Card	\N	\N	tags	2016-06-03 12:53:25.101477
1937	67	2538	Card	\N	\N	tags	2016-06-03 12:53:25.124102
1938	66	2539	Card	\N	\N	tags	2016-06-03 12:53:25.249792
1939	68	2539	Card	\N	\N	tags	2016-06-03 12:53:25.271118
1940	66	2540	Card	\N	\N	tags	2016-06-03 12:53:25.456473
1941	69	2540	Card	\N	\N	tags	2016-06-03 12:53:25.477423
1942	66	2541	Card	\N	\N	tags	2016-06-03 12:53:25.618687
1943	70	2541	Card	\N	\N	tags	2016-06-03 12:53:25.639242
1944	66	2542	Card	\N	\N	tags	2016-06-03 12:53:26.05839
1945	47	2542	Card	\N	\N	tags	2016-06-03 12:53:26.081454
1946	66	2543	Card	\N	\N	tags	2016-06-03 12:53:26.279698
1947	71	2543	Card	\N	\N	tags	2016-06-03 12:53:26.310517
1948	66	2544	Card	\N	\N	tags	2016-06-03 12:53:26.492132
1949	72	2544	Card	\N	\N	tags	2016-06-03 12:53:26.512046
1950	73	2544	Card	\N	\N	tags	2016-06-03 12:53:26.530878
1951	66	2545	Card	\N	\N	tags	2016-06-03 12:53:26.686166
1952	74	2545	Card	\N	\N	tags	2016-06-03 12:53:26.717748
1953	72	2545	Card	\N	\N	tags	2016-06-03 12:53:26.748814
1954	66	2546	Card	\N	\N	tags	2016-06-03 12:53:26.918259
1955	75	2546	Card	\N	\N	tags	2016-06-03 12:53:26.945257
1956	66	2547	Card	\N	\N	tags	2016-06-03 12:53:27.093642
1957	74	2547	Card	\N	\N	tags	2016-06-03 12:53:27.115818
1958	66	2548	Card	\N	\N	tags	2016-06-03 12:53:27.24519
1959	68	2548	Card	\N	\N	tags	2016-06-03 12:53:27.264
1960	76	2549	Card	\N	\N	tags	2016-06-03 12:53:27.760282
1961	77	2549	Card	\N	\N	tags	2016-06-03 12:53:27.788091
1962	76	2550	Card	\N	\N	tags	2016-06-03 12:53:27.970604
1963	78	2550	Card	\N	\N	tags	2016-06-03 12:53:27.991441
1964	76	2551	Card	\N	\N	tags	2016-06-03 12:53:28.909347
1965	77	2551	Card	\N	\N	tags	2016-06-03 12:53:28.935336
1966	76	2552	Card	\N	\N	tags	2016-06-03 12:53:29.242445
1967	77	2552	Card	\N	\N	tags	2016-06-03 12:53:29.268756
1968	76	2553	Card	\N	\N	tags	2016-06-03 12:53:29.92417
1969	77	2553	Card	\N	\N	tags	2016-06-03 12:53:29.9685
1970	79	2554	Card	\N	\N	tags	2016-06-03 12:53:30.763476
1971	80	2554	Card	\N	\N	tags	2016-06-03 12:53:30.798679
1972	79	2555	Card	\N	\N	tags	2016-06-03 12:53:31.357986
1973	81	2555	Card	\N	\N	tags	2016-06-03 12:53:31.38228
1974	79	2556	Card	\N	\N	tags	2016-06-03 12:53:31.692777
1975	66	2556	Card	\N	\N	tags	2016-06-03 12:53:31.714097
1976	79	2557	Card	\N	\N	tags	2016-06-03 12:53:31.92394
1977	66	2557	Card	\N	\N	tags	2016-06-03 12:53:31.945257
1978	60	2557	Card	\N	\N	tags	2016-06-03 12:53:31.971734
1979	79	2558	Card	\N	\N	tags	2016-06-03 12:53:32.128193
1980	66	2558	Card	\N	\N	tags	2016-06-03 12:53:32.152422
1981	79	2559	Card	\N	\N	tags	2016-06-03 12:53:32.332775
1982	82	2559	Card	\N	\N	tags	2016-06-03 12:53:32.364272
1983	32	2560	Card	\N	\N	tags	2016-06-03 12:53:32.748947
1984	83	2560	Card	\N	\N	tags	2016-06-03 12:53:32.7767
1985	84	2560	Card	\N	\N	tags	2016-06-03 12:53:32.799589
1986	32	2561	Card	\N	\N	tags	2016-06-03 12:53:32.945765
1987	85	2561	Card	\N	\N	tags	2016-06-03 12:53:32.970479
1988	66	2561	Card	\N	\N	tags	2016-06-03 12:53:33.000237
1989	32	2562	Card	\N	\N	tags	2016-06-03 12:53:33.135442
1990	86	2562	Card	\N	\N	tags	2016-06-03 12:53:33.154353
1991	32	2563	Card	\N	\N	tags	2016-06-03 12:53:33.322139
1992	87	2563	Card	\N	\N	tags	2016-06-03 12:53:33.350545
1993	32	2564	Card	\N	\N	tags	2016-06-03 12:53:33.53757
1994	24	2564	Card	\N	\N	tags	2016-06-03 12:53:33.56343
1995	32	2565	Card	\N	\N	tags	2016-06-03 12:53:33.68822
1996	84	2565	Card	\N	\N	tags	2016-06-03 12:53:33.71451
1997	88	2565	Card	\N	\N	tags	2016-06-03 12:53:33.738826
1998	32	2566	Card	\N	\N	tags	2016-06-03 12:53:33.881644
1999	89	2566	Card	\N	\N	tags	2016-06-03 12:53:33.910396
2000	66	2566	Card	\N	\N	tags	2016-06-03 12:53:33.934718
2001	32	2567	Card	\N	\N	tags	2016-06-03 12:53:34.061896
2002	90	2567	Card	\N	\N	tags	2016-06-03 12:53:34.085165
2003	32	2568	Card	\N	\N	tags	2016-06-03 12:53:34.21098
2004	86	2568	Card	\N	\N	tags	2016-06-03 12:53:34.239474
2005	32	2569	Card	\N	\N	tags	2016-06-03 12:53:34.386512
2006	22	2569	Card	\N	\N	tags	2016-06-03 12:53:34.415056
2007	32	2570	Card	\N	\N	tags	2016-06-03 12:53:34.539726
2008	91	2570	Card	\N	\N	tags	2016-06-03 12:53:34.564928
2009	32	2571	Card	\N	\N	tags	2016-06-03 12:53:34.746915
2010	92	2571	Card	\N	\N	tags	2016-06-03 12:53:34.780984
2011	32	2572	Card	\N	\N	tags	2016-06-03 12:53:34.955866
2012	93	2572	Card	\N	\N	tags	2016-06-03 12:53:34.984583
2013	32	2573	Card	\N	\N	tags	2016-06-03 12:53:35.117169
2014	94	2573	Card	\N	\N	tags	2016-06-03 12:53:35.140031
2015	32	2574	Card	\N	\N	tags	2016-06-03 12:53:35.352875
2016	95	2574	Card	\N	\N	tags	2016-06-03 12:53:35.374116
2017	32	2575	Card	\N	\N	tags	2016-06-03 12:53:35.507305
2018	96	2575	Card	\N	\N	tags	2016-06-03 12:53:35.532049
2019	32	2576	Card	\N	\N	tags	2016-06-03 12:53:35.673739
2020	97	2576	Card	\N	\N	tags	2016-06-03 12:53:35.696977
2021	98	2576	Card	\N	\N	tags	2016-06-03 12:53:35.723444
2022	32	2577	Card	\N	\N	tags	2016-06-03 12:53:35.8535
2023	99	2577	Card	\N	\N	tags	2016-06-03 12:53:35.875416
2024	32	2578	Card	\N	\N	tags	2016-06-03 12:53:36.006614
2025	81	2578	Card	\N	\N	tags	2016-06-03 12:53:36.030996
2026	100	2579	Card	\N	\N	tags	2016-06-03 12:53:36.342499
2027	101	2579	Card	\N	\N	tags	2016-06-03 12:53:36.361487
2028	66	2579	Card	\N	\N	tags	2016-06-03 12:53:36.386424
2029	100	2580	Card	\N	\N	tags	2016-06-03 12:53:36.526296
2030	101	2580	Card	\N	\N	tags	2016-06-03 12:53:36.555568
2031	66	2580	Card	\N	\N	tags	2016-06-03 12:53:36.57438
2032	100	2581	Card	\N	\N	tags	2016-06-03 12:53:36.725129
2033	24	2581	Card	\N	\N	tags	2016-06-03 12:53:36.751058
2034	66	2581	Card	\N	\N	tags	2016-06-03 12:53:36.77259
2035	100	2582	Card	\N	\N	tags	2016-06-03 12:53:36.937949
2036	102	2582	Card	\N	\N	tags	2016-06-03 12:53:36.959616
2037	100	2583	Card	\N	\N	tags	2016-06-03 12:53:37.128832
2038	102	2583	Card	\N	\N	tags	2016-06-03 12:53:37.154291
2039	66	2583	Card	\N	\N	tags	2016-06-03 12:53:37.174785
2040	100	2584	Card	\N	\N	tags	2016-06-03 12:53:37.314889
2041	103	2584	Card	\N	\N	tags	2016-06-03 12:53:37.344077
2042	66	2584	Card	\N	\N	tags	2016-06-03 12:53:37.368677
2043	100	2585	Card	\N	\N	tags	2016-06-03 12:53:37.508075
2044	104	2585	Card	\N	\N	tags	2016-06-03 12:53:37.541365
2045	66	2585	Card	\N	\N	tags	2016-06-03 12:53:37.569418
2046	100	2586	Card	\N	\N	tags	2016-06-03 12:53:37.704097
2047	105	2586	Card	\N	\N	tags	2016-06-03 12:53:37.724483
2048	66	2586	Card	\N	\N	tags	2016-06-03 12:53:37.748445
2049	100	2587	Card	\N	\N	tags	2016-06-03 12:53:37.931694
2050	106	2587	Card	\N	\N	tags	2016-06-03 12:53:37.95488
2051	100	2588	Card	\N	\N	tags	2016-06-03 12:53:38.197559
2052	66	2588	Card	\N	\N	tags	2016-06-03 12:53:38.226513
2053	60	2589	Card	\N	\N	tags	2016-06-03 12:53:42.238013
2054	107	2589	Card	\N	\N	tags	2016-06-03 12:53:42.309498
2055	47	2589	Card	\N	\N	tags	2016-06-03 12:53:42.353107
2056	1	2590	Card	\N	\N	tags	2016-06-03 12:53:43.241203
2057	13	2590	Card	\N	\N	tags	2016-06-03 12:53:43.301858
2058	1	2591	Card	\N	\N	tags	2016-06-03 12:53:46.441201
2059	13	2591	Card	\N	\N	tags	2016-06-03 12:53:46.509985
2060	60	2592	Card	\N	\N	tags	2016-06-03 12:53:48.681515
2061	107	2592	Card	\N	\N	tags	2016-06-03 12:53:48.73584
2062	47	2592	Card	\N	\N	tags	2016-06-03 12:53:48.840821
2063	1	2593	Card	\N	\N	tags	2016-06-03 12:53:49.871833
2064	13	2593	Card	\N	\N	tags	2016-06-03 12:53:49.909739
2065	60	2595	Card	\N	\N	tags	2016-06-03 12:53:52.000856
2066	107	2595	Card	\N	\N	tags	2016-06-03 12:53:52.055643
2067	47	2595	Card	\N	\N	tags	2016-06-03 12:53:52.092169
2068	1	2596	Card	\N	\N	tags	2016-06-03 12:53:52.829741
2069	13	2596	Card	\N	\N	tags	2016-06-03 12:53:52.880017
2070	1	2597	Card	\N	\N	tags	2016-06-03 12:53:54.76495
2071	13	2597	Card	\N	\N	tags	2016-06-03 12:53:54.808864
2072	60	2598	Card	\N	\N	tags	2016-06-03 12:53:57.438475
2073	107	2598	Card	\N	\N	tags	2016-06-03 12:53:57.475848
2074	47	2598	Card	\N	\N	tags	2016-06-03 12:53:57.52055
2075	1	2599	Card	\N	\N	tags	2016-06-03 12:53:58.393605
2076	13	2599	Card	\N	\N	tags	2016-06-03 12:53:58.430439
2077	1	2601	Card	\N	\N	tags	2016-06-03 12:55:17.664916
2078	2	2601	Card	\N	\N	tags	2016-06-03 12:55:17.688198
2079	1	2602	Card	\N	\N	tags	2016-06-03 12:55:17.867904
2080	2	2602	Card	\N	\N	tags	2016-06-03 12:55:17.889517
2081	3	2602	Card	\N	\N	tags	2016-06-03 12:55:17.912484
2082	1	2603	Card	\N	\N	tags	2016-06-03 12:55:18.023659
2083	4	2603	Card	\N	\N	tags	2016-06-03 12:55:18.048766
2084	5	2603	Card	\N	\N	tags	2016-06-03 12:55:18.073273
2085	1	2604	Card	\N	\N	tags	2016-06-03 12:55:20.000042
2086	4	2604	Card	\N	\N	tags	2016-06-03 12:55:20.033754
2087	5	2604	Card	\N	\N	tags	2016-06-03 12:55:20.06324
2088	1	2605	Card	\N	\N	tags	2016-06-03 12:55:22.526005
2089	4	2605	Card	\N	\N	tags	2016-06-03 12:55:22.571572
2090	6	2605	Card	\N	\N	tags	2016-06-03 12:55:22.607891
2091	1	2606	Card	\N	\N	tags	2016-06-03 12:55:25.152829
2092	4	2606	Card	\N	\N	tags	2016-06-03 12:55:25.193814
2093	6	2606	Card	\N	\N	tags	2016-06-03 12:55:25.266621
2094	1	2607	Card	\N	\N	tags	2016-06-03 12:55:27.636774
2095	4	2607	Card	\N	\N	tags	2016-06-03 12:55:27.675509
2096	6	2607	Card	\N	\N	tags	2016-06-03 12:55:27.706215
2097	1	2608	Card	\N	\N	tags	2016-06-03 12:55:31.063183
2098	4	2608	Card	\N	\N	tags	2016-06-03 12:55:31.095199
2099	6	2608	Card	\N	\N	tags	2016-06-03 12:55:31.142523
2100	1	2609	Card	\N	\N	tags	2016-06-03 12:55:33.185926
2101	4	2609	Card	\N	\N	tags	2016-06-03 12:55:33.243928
2102	6	2609	Card	\N	\N	tags	2016-06-03 12:55:33.280011
2103	1	2610	Card	\N	\N	tags	2016-06-03 12:55:35.27731
2104	4	2610	Card	\N	\N	tags	2016-06-03 12:55:35.316248
2105	6	2610	Card	\N	\N	tags	2016-06-03 12:55:35.343464
2106	1	2611	Card	\N	\N	tags	2016-06-03 12:55:39.409334
2107	4	2611	Card	\N	\N	tags	2016-06-03 12:55:39.449321
2108	6	2611	Card	\N	\N	tags	2016-06-03 12:55:39.47899
2109	1	2612	Card	\N	\N	tags	2016-06-03 12:55:42.83719
2110	4	2612	Card	\N	\N	tags	2016-06-03 12:55:42.876396
2111	6	2612	Card	\N	\N	tags	2016-06-03 12:55:42.909644
2112	1	2613	Card	\N	\N	tags	2016-06-03 12:55:45.998457
2113	4	2613	Card	\N	\N	tags	2016-06-03 12:55:46.038912
2114	7	2613	Card	\N	\N	tags	2016-06-03 12:55:46.085731
2115	1	2614	Card	\N	\N	tags	2016-06-03 12:55:48.182733
2116	4	2614	Card	\N	\N	tags	2016-06-03 12:55:48.211187
2117	6	2614	Card	\N	\N	tags	2016-06-03 12:55:48.247318
2118	1	2615	Card	\N	\N	tags	2016-06-03 12:55:50.56154
2119	4	2615	Card	\N	\N	tags	2016-06-03 12:55:50.591502
2120	6	2615	Card	\N	\N	tags	2016-06-03 12:55:50.631464
2121	1	2616	Card	\N	\N	tags	2016-06-03 12:55:51.04892
2122	4	2616	Card	\N	\N	tags	2016-06-03 12:55:51.068473
2123	6	2616	Card	\N	\N	tags	2016-06-03 12:55:51.090507
2124	1	2617	Card	\N	\N	tags	2016-06-03 12:55:51.346844
2125	4	2617	Card	\N	\N	tags	2016-06-03 12:55:51.366017
2126	6	2617	Card	\N	\N	tags	2016-06-03 12:55:51.384578
2127	1	2618	Card	\N	\N	tags	2016-06-03 12:55:51.584038
2128	4	2618	Card	\N	\N	tags	2016-06-03 12:55:51.602566
2129	6	2618	Card	\N	\N	tags	2016-06-03 12:55:51.626333
2130	1	2619	Card	\N	\N	tags	2016-06-03 12:55:51.762853
2131	4	2619	Card	\N	\N	tags	2016-06-03 12:55:51.78381
2132	8	2619	Card	\N	\N	tags	2016-06-03 12:55:51.803527
2133	1	2620	Card	\N	\N	tags	2016-06-03 12:55:51.96624
2134	4	2620	Card	\N	\N	tags	2016-06-03 12:55:51.98715
2135	1	2621	Card	\N	\N	tags	2016-06-03 12:55:52.110157
2136	9	2621	Card	\N	\N	tags	2016-06-03 12:55:52.127622
2137	1	2622	Card	\N	\N	tags	2016-06-03 12:55:52.313978
2138	10	2622	Card	\N	\N	tags	2016-06-03 12:55:52.329382
2139	1	2623	Card	\N	\N	tags	2016-06-03 12:55:52.497149
2140	11	2623	Card	\N	\N	tags	2016-06-03 12:55:52.512336
2141	1	2624	Card	\N	\N	tags	2016-06-03 12:55:52.597029
2142	11	2624	Card	\N	\N	tags	2016-06-03 12:55:52.611555
2143	1	2625	Card	\N	\N	tags	2016-06-03 12:55:52.69445
2144	11	2625	Card	\N	\N	tags	2016-06-03 12:55:52.710028
2145	1	2626	Card	\N	\N	tags	2016-06-03 12:55:53.834304
2146	11	2626	Card	\N	\N	tags	2016-06-03 12:55:53.873099
2147	1	2627	Card	\N	\N	tags	2016-06-03 12:55:54.135691
2148	12	2627	Card	\N	\N	tags	2016-06-03 12:55:54.187154
2149	1	2628	Card	\N	\N	tags	2016-06-03 12:55:54.363318
2150	11	2628	Card	\N	\N	tags	2016-06-03 12:55:54.382942
2151	1	2629	Card	\N	\N	tags	2016-06-03 12:55:54.918427
2152	11	2629	Card	\N	\N	tags	2016-06-03 12:55:54.959683
2153	1	2630	Card	\N	\N	tags	2016-06-03 12:55:55.569917
2154	11	2630	Card	\N	\N	tags	2016-06-03 12:55:55.633608
2155	1	2631	Card	\N	\N	tags	2016-06-03 12:55:56.293456
2156	11	2631	Card	\N	\N	tags	2016-06-03 12:55:56.337961
2157	1	2632	Card	\N	\N	tags	2016-06-03 12:55:56.588104
2158	11	2632	Card	\N	\N	tags	2016-06-03 12:55:56.625957
2159	1	2633	Card	\N	\N	tags	2016-06-03 12:55:57.242892
2160	13	2633	Card	\N	\N	tags	2016-06-03 12:55:57.28088
2161	14	2633	Card	\N	\N	tags	2016-06-03 12:55:57.308342
2162	1	2634	Card	\N	\N	tags	2016-06-03 12:55:57.634993
2163	14	2634	Card	\N	\N	tags	2016-06-03 12:55:57.668427
2164	1	2635	Card	\N	\N	tags	2016-06-03 12:55:57.800487
2165	14	2635	Card	\N	\N	tags	2016-06-03 12:55:57.822757
2166	11	2635	Card	\N	\N	tags	2016-06-03 12:55:57.83924
2167	1	2636	Card	\N	\N	tags	2016-06-03 12:55:57.92639
2168	14	2636	Card	\N	\N	tags	2016-06-03 12:55:57.941802
2169	11	2636	Card	\N	\N	tags	2016-06-03 12:55:57.95823
2170	15	2637	Card	\N	\N	tags	2016-06-03 12:55:58.374884
2171	16	2637	Card	\N	\N	tags	2016-06-03 12:55:58.391247
2172	15	2638	Card	\N	\N	tags	2016-06-03 12:55:59.878106
2173	16	2638	Card	\N	\N	tags	2016-06-03 12:55:59.914669
2174	15	2639	Card	\N	\N	tags	2016-06-03 12:56:00.876267
2175	16	2639	Card	\N	\N	tags	2016-06-03 12:56:00.915788
2176	15	2640	Card	\N	\N	tags	2016-06-03 12:56:01.215984
2177	17	2640	Card	\N	\N	tags	2016-06-03 12:56:01.287185
2178	15	2641	Card	\N	\N	tags	2016-06-03 12:56:01.476475
2179	18	2641	Card	\N	\N	tags	2016-06-03 12:56:01.494264
2180	19	2642	Card	\N	\N	tags	2016-06-03 12:56:02.57938
2181	20	2642	Card	\N	\N	tags	2016-06-03 12:56:02.594576
2182	21	2642	Card	\N	\N	tags	2016-06-03 12:56:02.610022
2183	19	2643	Card	\N	\N	tags	2016-06-03 12:56:04.190513
2184	20	2643	Card	\N	\N	tags	2016-06-03 12:56:04.217821
2185	21	2643	Card	\N	\N	tags	2016-06-03 12:56:04.273826
2186	19	2644	Card	\N	\N	tags	2016-06-03 12:56:06.801629
2187	20	2644	Card	\N	\N	tags	2016-06-03 12:56:06.847142
2188	21	2644	Card	\N	\N	tags	2016-06-03 12:56:06.890701
2189	19	2645	Card	\N	\N	tags	2016-06-03 12:56:09.067934
2190	20	2645	Card	\N	\N	tags	2016-06-03 12:56:09.130347
2191	21	2645	Card	\N	\N	tags	2016-06-03 12:56:09.186561
2192	19	2646	Card	\N	\N	tags	2016-06-03 12:56:10.831275
2193	20	2646	Card	\N	\N	tags	2016-06-03 12:56:10.90724
2194	21	2646	Card	\N	\N	tags	2016-06-03 12:56:10.966457
2195	19	2647	Card	\N	\N	tags	2016-06-03 12:56:12.784295
2196	20	2647	Card	\N	\N	tags	2016-06-03 12:56:12.838367
2197	21	2647	Card	\N	\N	tags	2016-06-03 12:56:12.879637
2198	19	2648	Card	\N	\N	tags	2016-06-03 12:56:14.689844
2199	20	2648	Card	\N	\N	tags	2016-06-03 12:56:14.80535
2200	21	2648	Card	\N	\N	tags	2016-06-03 12:56:14.85466
2201	19	2649	Card	\N	\N	tags	2016-06-03 12:56:15.271306
2202	22	2649	Card	\N	\N	tags	2016-06-03 12:56:15.29496
2203	19	2650	Card	\N	\N	tags	2016-06-03 12:56:16.998687
2204	22	2650	Card	\N	\N	tags	2016-06-03 12:56:17.046647
2205	19	2651	Card	\N	\N	tags	2016-06-03 12:56:17.461006
2206	23	2652	Card	\N	\N	tags	2016-06-03 12:56:17.767375
2207	23	2653	Card	\N	\N	tags	2016-06-03 12:56:17.922479
2208	24	2654	Card	\N	\N	tags	2016-06-03 12:56:18.078888
2209	19	2654	Card	\N	\N	tags	2016-06-03 12:56:18.104391
2210	25	2655	Card	\N	\N	tags	2016-06-03 12:56:19.202289
2211	26	2655	Card	\N	\N	tags	2016-06-03 12:56:19.228167
2212	25	2656	Card	\N	\N	tags	2016-06-03 12:56:21.310654
2213	26	2656	Card	\N	\N	tags	2016-06-03 12:56:21.349916
2214	25	2657	Card	\N	\N	tags	2016-06-03 12:56:23.02244
2215	26	2657	Card	\N	\N	tags	2016-06-03 12:56:23.057401
2216	25	2658	Card	\N	\N	tags	2016-06-03 12:56:25.593302
2217	26	2658	Card	\N	\N	tags	2016-06-03 12:56:25.634856
2218	25	2659	Card	\N	\N	tags	2016-06-03 12:56:26.344456
2219	26	2659	Card	\N	\N	tags	2016-06-03 12:56:26.364262
2220	25	2660	Card	\N	\N	tags	2016-06-03 12:56:28.645269
2221	26	2660	Card	\N	\N	tags	2016-06-03 12:56:28.677469
2222	25	2661	Card	\N	\N	tags	2016-06-03 12:56:31.064928
2223	26	2661	Card	\N	\N	tags	2016-06-03 12:56:31.128933
2224	25	2662	Card	\N	\N	tags	2016-06-03 12:56:32.890579
2225	26	2662	Card	\N	\N	tags	2016-06-03 12:56:32.936133
2226	25	2663	Card	\N	\N	tags	2016-06-03 12:56:34.708679
2227	26	2663	Card	\N	\N	tags	2016-06-03 12:56:34.745383
2228	25	2664	Card	\N	\N	tags	2016-06-03 12:56:35.526128
2229	26	2664	Card	\N	\N	tags	2016-06-03 12:56:35.544111
2230	25	2665	Card	\N	\N	tags	2016-06-03 12:56:38.247065
2231	26	2665	Card	\N	\N	tags	2016-06-03 12:56:38.301814
2232	25	2666	Card	\N	\N	tags	2016-06-03 12:56:39.922373
2233	26	2666	Card	\N	\N	tags	2016-06-03 12:56:39.948382
2234	27	2667	Card	\N	\N	tags	2016-06-03 12:56:42.33976
2235	28	2667	Card	\N	\N	tags	2016-06-03 12:56:42.412871
2236	27	2668	Card	\N	\N	tags	2016-06-03 12:56:42.855545
2237	29	2668	Card	\N	\N	tags	2016-06-03 12:56:42.87067
2238	30	2669	Card	\N	\N	tags	2016-06-03 12:56:43.085406
2239	31	2669	Card	\N	\N	tags	2016-06-03 12:56:43.100145
2240	32	2669	Card	\N	\N	tags	2016-06-03 12:56:43.11603
2241	33	2670	Card	\N	\N	tags	2016-06-03 12:56:44.163319
2242	34	2670	Card	\N	\N	tags	2016-06-03 12:56:44.184967
2243	33	2671	Card	\N	\N	tags	2016-06-03 12:56:44.395335
2244	35	2671	Card	\N	\N	tags	2016-06-03 12:56:44.414757
2245	36	2671	Card	\N	\N	tags	2016-06-03 12:56:44.433621
2246	37	2672	Card	\N	\N	tags	2016-06-03 12:56:45.369523
2247	38	2673	Card	\N	\N	tags	2016-06-03 12:56:49.184399
2248	39	2673	Card	\N	\N	tags	2016-06-03 12:56:49.201148
2249	40	2673	Card	\N	\N	tags	2016-06-03 12:56:49.224118
2250	38	2674	Card	\N	\N	tags	2016-06-03 12:56:50.054522
2251	41	2674	Card	\N	\N	tags	2016-06-03 12:56:50.079232
2252	40	2674	Card	\N	\N	tags	2016-06-03 12:56:50.106503
2253	38	2675	Card	\N	\N	tags	2016-06-03 12:56:50.92897
2254	42	2675	Card	\N	\N	tags	2016-06-03 12:56:50.946818
2255	43	2676	Card	\N	\N	tags	2016-06-03 12:56:51.038157
2256	38	2676	Card	\N	\N	tags	2016-06-03 12:56:51.056692
2257	44	2677	Card	\N	\N	tags	2016-06-03 12:56:51.769585
2258	38	2677	Card	\N	\N	tags	2016-06-03 12:56:51.803601
2259	45	2677	Card	\N	\N	tags	2016-06-03 12:56:51.819712
2260	44	2678	Card	\N	\N	tags	2016-06-03 12:56:51.977436
2261	38	2678	Card	\N	\N	tags	2016-06-03 12:56:51.992839
2262	45	2678	Card	\N	\N	tags	2016-06-03 12:56:52.015419
2263	45	2679	Card	\N	\N	tags	2016-06-03 12:56:52.345436
2264	23	2679	Card	\N	\N	tags	2016-06-03 12:56:52.363891
2265	46	2679	Card	\N	\N	tags	2016-06-03 12:56:52.382874
2266	47	2680	Card	\N	\N	tags	2016-06-03 12:56:52.532492
2267	23	2680	Card	\N	\N	tags	2016-06-03 12:56:52.552884
2268	45	2680	Card	\N	\N	tags	2016-06-03 12:56:52.570755
2269	48	2681	Card	\N	\N	tags	2016-06-03 12:56:52.945178
2270	49	2681	Card	\N	\N	tags	2016-06-03 12:56:52.963478
2271	50	2682	Card	\N	\N	tags	2016-06-03 12:56:53.081007
2272	49	2682	Card	\N	\N	tags	2016-06-03 12:56:53.099318
2273	50	2683	Card	\N	\N	tags	2016-06-03 12:56:53.249175
2274	49	2683	Card	\N	\N	tags	2016-06-03 12:56:53.26518
2275	51	2684	Card	\N	\N	tags	2016-06-03 12:56:53.373905
2276	49	2684	Card	\N	\N	tags	2016-06-03 12:56:53.390721
2277	52	2685	Card	\N	\N	tags	2016-06-03 12:56:53.48101
2278	49	2685	Card	\N	\N	tags	2016-06-03 12:56:53.502539
2279	53	2686	Card	\N	\N	tags	2016-06-03 12:56:53.591547
2280	54	2686	Card	\N	\N	tags	2016-06-03 12:56:53.607246
2281	49	2686	Card	\N	\N	tags	2016-06-03 12:56:53.624081
2282	53	2687	Card	\N	\N	tags	2016-06-03 12:56:53.716353
2283	54	2687	Card	\N	\N	tags	2016-06-03 12:56:53.735406
2284	49	2687	Card	\N	\N	tags	2016-06-03 12:56:53.752245
2285	53	2688	Card	\N	\N	tags	2016-06-03 12:56:53.922206
2286	55	2688	Card	\N	\N	tags	2016-06-03 12:56:53.940221
2287	49	2688	Card	\N	\N	tags	2016-06-03 12:56:53.957883
2288	53	2689	Card	\N	\N	tags	2016-06-03 12:56:54.07652
2289	56	2689	Card	\N	\N	tags	2016-06-03 12:56:54.103187
2290	49	2689	Card	\N	\N	tags	2016-06-03 12:56:54.124002
2291	57	2690	Card	\N	\N	tags	2016-06-03 12:56:54.222216
2292	49	2690	Card	\N	\N	tags	2016-06-03 12:56:54.241083
2293	35	2691	Card	\N	\N	tags	2016-06-03 12:56:54.337016
2294	49	2691	Card	\N	\N	tags	2016-06-03 12:56:54.35477
2295	58	2692	Card	\N	\N	tags	2016-06-03 12:56:54.447643
2296	33	2692	Card	\N	\N	tags	2016-06-03 12:56:54.473734
2297	49	2692	Card	\N	\N	tags	2016-06-03 12:56:54.490573
2298	59	2693	Card	\N	\N	tags	2016-06-03 12:56:54.595287
2299	60	2693	Card	\N	\N	tags	2016-06-03 12:56:54.610144
2300	49	2693	Card	\N	\N	tags	2016-06-03 12:56:54.628679
2301	60	2694	Card	\N	\N	tags	2016-06-03 12:56:54.875306
2302	59	2694	Card	\N	\N	tags	2016-06-03 12:56:54.891036
2303	49	2694	Card	\N	\N	tags	2016-06-03 12:56:54.907355
2304	61	2695	Card	\N	\N	tags	2016-06-03 12:56:55.042468
2305	62	2695	Card	\N	\N	tags	2016-06-03 12:56:55.058118
2306	49	2695	Card	\N	\N	tags	2016-06-03 12:56:55.075605
2307	61	2696	Card	\N	\N	tags	2016-06-03 12:56:55.172731
2308	62	2696	Card	\N	\N	tags	2016-06-03 12:56:55.188397
2309	49	2696	Card	\N	\N	tags	2016-06-03 12:56:55.20381
2310	53	2697	Card	\N	\N	tags	2016-06-03 12:56:55.510115
2311	63	2697	Card	\N	\N	tags	2016-06-03 12:56:55.526598
2312	49	2697	Card	\N	\N	tags	2016-06-03 12:56:55.543831
2313	64	2698	Card	\N	\N	tags	2016-06-03 12:56:55.650885
2314	63	2698	Card	\N	\N	tags	2016-06-03 12:56:55.669424
2315	49	2698	Card	\N	\N	tags	2016-06-03 12:56:55.686877
2316	64	2699	Card	\N	\N	tags	2016-06-03 12:56:55.82532
2317	63	2699	Card	\N	\N	tags	2016-06-03 12:56:55.841753
2318	49	2699	Card	\N	\N	tags	2016-06-03 12:56:55.857147
2319	64	2700	Card	\N	\N	tags	2016-06-03 12:56:55.941025
2320	63	2700	Card	\N	\N	tags	2016-06-03 12:56:55.957224
2321	49	2700	Card	\N	\N	tags	2016-06-03 12:56:55.97427
2322	64	2701	Card	\N	\N	tags	2016-06-03 12:56:56.094943
2323	63	2701	Card	\N	\N	tags	2016-06-03 12:56:56.111116
2324	65	2701	Card	\N	\N	tags	2016-06-03 12:56:56.126851
2325	64	2702	Card	\N	\N	tags	2016-06-03 12:56:56.240139
2326	63	2702	Card	\N	\N	tags	2016-06-03 12:56:56.258179
2327	65	2702	Card	\N	\N	tags	2016-06-03 12:56:56.277031
2328	64	2703	Card	\N	\N	tags	2016-06-03 12:56:56.381439
2329	63	2703	Card	\N	\N	tags	2016-06-03 12:56:56.397725
2330	65	2703	Card	\N	\N	tags	2016-06-03 12:56:56.413304
2331	64	2704	Card	\N	\N	tags	2016-06-03 12:56:56.528412
2332	63	2704	Card	\N	\N	tags	2016-06-03 12:56:56.546207
2333	65	2704	Card	\N	\N	tags	2016-06-03 12:56:56.562279
2334	64	2705	Card	\N	\N	tags	2016-06-03 12:56:56.766829
2335	63	2705	Card	\N	\N	tags	2016-06-03 12:56:56.78266
2336	65	2705	Card	\N	\N	tags	2016-06-03 12:56:56.799332
2337	47	2706	Card	\N	\N	tags	2016-06-03 12:56:57.208864
2338	66	2706	Card	\N	\N	tags	2016-06-03 12:56:57.226538
2339	66	2707	Card	\N	\N	tags	2016-06-03 12:56:57.358307
2340	67	2707	Card	\N	\N	tags	2016-06-03 12:56:57.376853
2341	66	2708	Card	\N	\N	tags	2016-06-03 12:56:57.483884
2342	68	2708	Card	\N	\N	tags	2016-06-03 12:56:57.507283
2343	66	2709	Card	\N	\N	tags	2016-06-03 12:56:57.647275
2344	69	2709	Card	\N	\N	tags	2016-06-03 12:56:57.662233
2345	66	2710	Card	\N	\N	tags	2016-06-03 12:56:57.768686
2346	70	2710	Card	\N	\N	tags	2016-06-03 12:56:57.783821
2347	66	2711	Card	\N	\N	tags	2016-06-03 12:56:58.114032
2348	47	2711	Card	\N	\N	tags	2016-06-03 12:56:58.132575
2349	66	2712	Card	\N	\N	tags	2016-06-03 12:56:58.27874
2350	71	2712	Card	\N	\N	tags	2016-06-03 12:56:58.295509
2351	66	2713	Card	\N	\N	tags	2016-06-03 12:56:58.410514
2352	72	2713	Card	\N	\N	tags	2016-06-03 12:56:58.425007
2353	73	2713	Card	\N	\N	tags	2016-06-03 12:56:58.439703
2354	66	2714	Card	\N	\N	tags	2016-06-03 12:56:58.544277
2355	74	2714	Card	\N	\N	tags	2016-06-03 12:56:58.560401
2356	72	2714	Card	\N	\N	tags	2016-06-03 12:56:58.574827
2357	66	2715	Card	\N	\N	tags	2016-06-03 12:56:58.658707
2358	75	2715	Card	\N	\N	tags	2016-06-03 12:56:58.674814
2359	66	2716	Card	\N	\N	tags	2016-06-03 12:56:58.797667
2360	74	2716	Card	\N	\N	tags	2016-06-03 12:56:58.816519
2361	66	2717	Card	\N	\N	tags	2016-06-03 12:56:58.900864
2362	68	2717	Card	\N	\N	tags	2016-06-03 12:56:58.916762
2363	76	2718	Card	\N	\N	tags	2016-06-03 12:56:59.317149
2364	77	2718	Card	\N	\N	tags	2016-06-03 12:56:59.332771
2365	76	2719	Card	\N	\N	tags	2016-06-03 12:56:59.473597
2366	78	2719	Card	\N	\N	tags	2016-06-03 12:56:59.488702
2367	76	2720	Card	\N	\N	tags	2016-06-03 12:57:00.168676
2368	77	2720	Card	\N	\N	tags	2016-06-03 12:57:00.185583
2369	76	2721	Card	\N	\N	tags	2016-06-03 12:57:00.383685
2370	77	2721	Card	\N	\N	tags	2016-06-03 12:57:00.400573
2371	76	2722	Card	\N	\N	tags	2016-06-03 12:57:00.841285
2372	77	2722	Card	\N	\N	tags	2016-06-03 12:57:00.858563
2373	79	2723	Card	\N	\N	tags	2016-06-03 12:57:01.486702
2374	80	2723	Card	\N	\N	tags	2016-06-03 12:57:01.504156
2375	79	2724	Card	\N	\N	tags	2016-06-03 12:57:01.739315
2376	81	2724	Card	\N	\N	tags	2016-06-03 12:57:01.754501
2377	79	2725	Card	\N	\N	tags	2016-06-03 12:57:01.925877
2378	66	2725	Card	\N	\N	tags	2016-06-03 12:57:01.943195
2379	79	2726	Card	\N	\N	tags	2016-06-03 12:57:02.078231
2380	66	2726	Card	\N	\N	tags	2016-06-03 12:57:02.101401
2381	60	2726	Card	\N	\N	tags	2016-06-03 12:57:02.121295
2382	79	2727	Card	\N	\N	tags	2016-06-03 12:57:02.241123
2383	66	2727	Card	\N	\N	tags	2016-06-03 12:57:02.257355
2384	79	2728	Card	\N	\N	tags	2016-06-03 12:57:02.362307
2385	82	2728	Card	\N	\N	tags	2016-06-03 12:57:02.379396
2386	32	2729	Card	\N	\N	tags	2016-06-03 12:57:02.756516
2387	83	2729	Card	\N	\N	tags	2016-06-03 12:57:02.770771
2388	84	2729	Card	\N	\N	tags	2016-06-03 12:57:02.785643
2389	32	2730	Card	\N	\N	tags	2016-06-03 12:57:02.894492
2390	85	2730	Card	\N	\N	tags	2016-06-03 12:57:02.909063
2391	66	2730	Card	\N	\N	tags	2016-06-03 12:57:02.925779
2392	32	2731	Card	\N	\N	tags	2016-06-03 12:57:03.02678
2393	86	2731	Card	\N	\N	tags	2016-06-03 12:57:03.045349
2394	32	2732	Card	\N	\N	tags	2016-06-03 12:57:03.130884
2395	87	2732	Card	\N	\N	tags	2016-06-03 12:57:03.1472
2396	32	2733	Card	\N	\N	tags	2016-06-03 12:57:03.281232
2397	24	2733	Card	\N	\N	tags	2016-06-03 12:57:03.301809
2398	32	2734	Card	\N	\N	tags	2016-06-03 12:57:03.392806
2399	84	2734	Card	\N	\N	tags	2016-06-03 12:57:03.409378
2400	88	2734	Card	\N	\N	tags	2016-06-03 12:57:03.425022
2401	32	2735	Card	\N	\N	tags	2016-06-03 12:57:03.52419
2402	89	2735	Card	\N	\N	tags	2016-06-03 12:57:03.54147
2403	66	2735	Card	\N	\N	tags	2016-06-03 12:57:03.557495
2404	32	2736	Card	\N	\N	tags	2016-06-03 12:57:03.644705
2405	90	2736	Card	\N	\N	tags	2016-06-03 12:57:03.661777
2406	32	2737	Card	\N	\N	tags	2016-06-03 12:57:03.748603
2407	86	2737	Card	\N	\N	tags	2016-06-03 12:57:03.765053
2408	32	2738	Card	\N	\N	tags	2016-06-03 12:57:03.861198
2409	22	2738	Card	\N	\N	tags	2016-06-03 12:57:03.877424
2410	32	2739	Card	\N	\N	tags	2016-06-03 12:57:03.966319
2411	91	2739	Card	\N	\N	tags	2016-06-03 12:57:03.984169
2412	32	2740	Card	\N	\N	tags	2016-06-03 12:57:04.135435
2413	92	2740	Card	\N	\N	tags	2016-06-03 12:57:04.152748
2414	32	2741	Card	\N	\N	tags	2016-06-03 12:57:04.294319
2415	93	2741	Card	\N	\N	tags	2016-06-03 12:57:04.313542
2416	32	2742	Card	\N	\N	tags	2016-06-03 12:57:04.400842
2417	94	2742	Card	\N	\N	tags	2016-06-03 12:57:04.418716
2418	32	2743	Card	\N	\N	tags	2016-06-03 12:57:04.563796
2419	95	2743	Card	\N	\N	tags	2016-06-03 12:57:04.579901
2420	32	2744	Card	\N	\N	tags	2016-06-03 12:57:04.666631
2421	96	2744	Card	\N	\N	tags	2016-06-03 12:57:04.682689
2422	32	2745	Card	\N	\N	tags	2016-06-03 12:57:04.778012
2423	97	2745	Card	\N	\N	tags	2016-06-03 12:57:04.793097
2424	98	2745	Card	\N	\N	tags	2016-06-03 12:57:04.807882
2425	32	2746	Card	\N	\N	tags	2016-06-03 12:57:04.896296
2426	99	2746	Card	\N	\N	tags	2016-06-03 12:57:04.912191
2427	32	2747	Card	\N	\N	tags	2016-06-03 12:57:04.995044
2428	81	2747	Card	\N	\N	tags	2016-06-03 12:57:05.009719
2429	100	2748	Card	\N	\N	tags	2016-06-03 12:57:05.281293
2430	101	2748	Card	\N	\N	tags	2016-06-03 12:57:05.29896
2431	66	2748	Card	\N	\N	tags	2016-06-03 12:57:05.314212
2432	100	2749	Card	\N	\N	tags	2016-06-03 12:57:05.41136
2433	101	2749	Card	\N	\N	tags	2016-06-03 12:57:05.428099
2434	66	2749	Card	\N	\N	tags	2016-06-03 12:57:05.442753
2435	100	2750	Card	\N	\N	tags	2016-06-03 12:57:05.548135
2436	24	2750	Card	\N	\N	tags	2016-06-03 12:57:05.564444
2437	66	2750	Card	\N	\N	tags	2016-06-03 12:57:05.580584
2438	100	2751	Card	\N	\N	tags	2016-06-03 12:57:05.689631
2439	102	2751	Card	\N	\N	tags	2016-06-03 12:57:05.706221
2440	100	2752	Card	\N	\N	tags	2016-06-03 12:57:05.814492
2441	102	2752	Card	\N	\N	tags	2016-06-03 12:57:05.830047
2442	66	2752	Card	\N	\N	tags	2016-06-03 12:57:05.84513
2443	100	2753	Card	\N	\N	tags	2016-06-03 12:57:05.936487
2444	103	2753	Card	\N	\N	tags	2016-06-03 12:57:05.970209
2445	66	2753	Card	\N	\N	tags	2016-06-03 12:57:05.986479
2446	100	2754	Card	\N	\N	tags	2016-06-03 12:57:06.10741
2447	104	2754	Card	\N	\N	tags	2016-06-03 12:57:06.129814
2448	66	2754	Card	\N	\N	tags	2016-06-03 12:57:06.149129
2449	100	2755	Card	\N	\N	tags	2016-06-03 12:57:06.262127
2450	105	2755	Card	\N	\N	tags	2016-06-03 12:57:06.283206
2451	66	2755	Card	\N	\N	tags	2016-06-03 12:57:06.301947
2452	100	2756	Card	\N	\N	tags	2016-06-03 12:57:06.440689
2453	106	2756	Card	\N	\N	tags	2016-06-03 12:57:06.457249
2454	100	2757	Card	\N	\N	tags	2016-06-03 12:57:06.641884
2455	66	2757	Card	\N	\N	tags	2016-06-03 12:57:06.659008
2456	60	2758	Card	\N	\N	tags	2016-06-03 12:57:09.293297
2457	107	2758	Card	\N	\N	tags	2016-06-03 12:57:09.38084
2458	47	2758	Card	\N	\N	tags	2016-06-03 12:57:09.446927
2459	1	2759	Card	\N	\N	tags	2016-06-03 12:57:10.294848
2460	13	2759	Card	\N	\N	tags	2016-06-03 12:57:10.327937
2461	1	2760	Card	\N	\N	tags	2016-06-03 12:57:12.919023
2462	13	2760	Card	\N	\N	tags	2016-06-03 12:57:12.952862
2463	60	2761	Card	\N	\N	tags	2016-06-03 12:57:14.851344
2464	107	2761	Card	\N	\N	tags	2016-06-03 12:57:14.893669
2465	47	2761	Card	\N	\N	tags	2016-06-03 12:57:14.99224
2466	1	2762	Card	\N	\N	tags	2016-06-03 12:57:16.741627
2467	13	2762	Card	\N	\N	tags	2016-06-03 12:57:16.777187
2468	60	2764	Card	\N	\N	tags	2016-06-03 12:57:18.195487
2469	107	2764	Card	\N	\N	tags	2016-06-03 12:57:18.237436
2470	47	2764	Card	\N	\N	tags	2016-06-03 12:57:18.285933
2471	1	2765	Card	\N	\N	tags	2016-06-03 12:57:18.944677
2472	13	2765	Card	\N	\N	tags	2016-06-03 12:57:18.987658
2473	1	2766	Card	\N	\N	tags	2016-06-03 12:57:20.363923
2474	13	2766	Card	\N	\N	tags	2016-06-03 12:57:20.404672
2475	60	2767	Card	\N	\N	tags	2016-06-03 12:57:21.584626
2476	107	2767	Card	\N	\N	tags	2016-06-03 12:57:21.61066
2477	47	2767	Card	\N	\N	tags	2016-06-03 12:57:21.646241
2478	1	2768	Card	\N	\N	tags	2016-06-03 12:57:22.424038
2479	13	2768	Card	\N	\N	tags	2016-06-03 12:57:22.473801
2480	1	2771	Card	\N	\N	tags	2016-06-06 15:05:30.687844
2481	2	2771	Card	\N	\N	tags	2016-06-06 15:05:30.729574
2482	1	2772	Card	\N	\N	tags	2016-06-06 15:05:30.992864
2483	2	2772	Card	\N	\N	tags	2016-06-06 15:05:31.024443
2484	3	2772	Card	\N	\N	tags	2016-06-06 15:05:31.046571
2485	1	2773	Card	\N	\N	tags	2016-06-06 15:05:31.217012
2486	4	2773	Card	\N	\N	tags	2016-06-06 15:05:31.246974
2487	5	2773	Card	\N	\N	tags	2016-06-06 15:05:31.274974
2488	1	2774	Card	\N	\N	tags	2016-06-06 15:05:31.598786
2489	4	2774	Card	\N	\N	tags	2016-06-06 15:05:31.634002
2490	5	2774	Card	\N	\N	tags	2016-06-06 15:05:31.661706
2491	1	2775	Card	\N	\N	tags	2016-06-06 15:05:31.950991
2492	4	2775	Card	\N	\N	tags	2016-06-06 15:05:31.984204
2493	6	2775	Card	\N	\N	tags	2016-06-06 15:05:32.014798
2494	1	2776	Card	\N	\N	tags	2016-06-06 15:05:32.340461
2495	4	2776	Card	\N	\N	tags	2016-06-06 15:05:32.372194
2496	6	2776	Card	\N	\N	tags	2016-06-06 15:05:32.403722
2497	1	2777	Card	\N	\N	tags	2016-06-06 15:05:32.736833
2498	4	2777	Card	\N	\N	tags	2016-06-06 15:05:32.76111
2499	6	2777	Card	\N	\N	tags	2016-06-06 15:05:32.793121
2500	1	2778	Card	\N	\N	tags	2016-06-06 15:05:33.091188
2501	4	2778	Card	\N	\N	tags	2016-06-06 15:05:33.12014
2502	6	2778	Card	\N	\N	tags	2016-06-06 15:05:33.14477
2503	1	2779	Card	\N	\N	tags	2016-06-06 15:05:33.50507
2504	4	2779	Card	\N	\N	tags	2016-06-06 15:05:33.537303
2505	6	2779	Card	\N	\N	tags	2016-06-06 15:05:33.561882
2506	1	2780	Card	\N	\N	tags	2016-06-06 15:05:33.862282
2507	4	2780	Card	\N	\N	tags	2016-06-06 15:05:33.888776
2508	6	2780	Card	\N	\N	tags	2016-06-06 15:05:33.917889
2509	1	2781	Card	\N	\N	tags	2016-06-06 15:05:34.203252
2510	4	2781	Card	\N	\N	tags	2016-06-06 15:05:34.231918
2511	6	2781	Card	\N	\N	tags	2016-06-06 15:05:34.262681
2512	1	2782	Card	\N	\N	tags	2016-06-06 15:05:34.672074
2513	4	2782	Card	\N	\N	tags	2016-06-06 15:05:34.703243
2514	6	2782	Card	\N	\N	tags	2016-06-06 15:05:34.729994
2515	1	2783	Card	\N	\N	tags	2016-06-06 15:05:35.017038
2516	4	2783	Card	\N	\N	tags	2016-06-06 15:05:35.047679
2517	7	2783	Card	\N	\N	tags	2016-06-06 15:05:35.081151
2518	1	2784	Card	\N	\N	tags	2016-06-06 15:05:35.394682
2519	4	2784	Card	\N	\N	tags	2016-06-06 15:05:35.418687
2520	6	2784	Card	\N	\N	tags	2016-06-06 15:05:35.456684
2521	1	2785	Card	\N	\N	tags	2016-06-06 15:05:35.758138
2522	4	2785	Card	\N	\N	tags	2016-06-06 15:05:35.790557
2523	6	2785	Card	\N	\N	tags	2016-06-06 15:05:35.825627
2524	1	2786	Card	\N	\N	tags	2016-06-06 15:05:36.119816
2525	4	2786	Card	\N	\N	tags	2016-06-06 15:05:36.148528
2526	6	2786	Card	\N	\N	tags	2016-06-06 15:05:36.194955
2527	1	2787	Card	\N	\N	tags	2016-06-06 15:05:36.506203
2528	4	2787	Card	\N	\N	tags	2016-06-06 15:05:36.546398
2529	6	2787	Card	\N	\N	tags	2016-06-06 15:05:36.590689
2530	1	2788	Card	\N	\N	tags	2016-06-06 15:05:36.790684
2531	4	2788	Card	\N	\N	tags	2016-06-06 15:05:36.824329
2532	6	2788	Card	\N	\N	tags	2016-06-06 15:05:36.858741
2533	1	2789	Card	\N	\N	tags	2016-06-06 15:05:37.072674
2534	4	2789	Card	\N	\N	tags	2016-06-06 15:05:37.100545
2535	8	2789	Card	\N	\N	tags	2016-06-06 15:05:37.129484
2536	1	2790	Card	\N	\N	tags	2016-06-06 15:05:37.368779
2537	4	2790	Card	\N	\N	tags	2016-06-06 15:05:37.405745
2538	1	2791	Card	\N	\N	tags	2016-06-06 15:05:37.627088
2539	9	2791	Card	\N	\N	tags	2016-06-06 15:05:37.657631
2540	1	2792	Card	\N	\N	tags	2016-06-06 15:05:37.94259
2541	10	2792	Card	\N	\N	tags	2016-06-06 15:05:37.967175
2542	1	2793	Card	\N	\N	tags	2016-06-06 15:05:38.351998
2543	11	2793	Card	\N	\N	tags	2016-06-06 15:05:38.384842
2544	1	2794	Card	\N	\N	tags	2016-06-06 15:05:38.557928
2545	11	2794	Card	\N	\N	tags	2016-06-06 15:05:38.594052
2546	1	2795	Card	\N	\N	tags	2016-06-06 15:05:38.795998
2547	11	2795	Card	\N	\N	tags	2016-06-06 15:05:38.831823
2548	1	2796	Card	\N	\N	tags	2016-06-06 15:05:39.244098
2549	11	2796	Card	\N	\N	tags	2016-06-06 15:05:39.272621
2550	1	2797	Card	\N	\N	tags	2016-06-06 15:05:39.474938
2551	12	2797	Card	\N	\N	tags	2016-06-06 15:05:39.515673
2552	1	2798	Card	\N	\N	tags	2016-06-06 15:05:39.697217
2553	11	2798	Card	\N	\N	tags	2016-06-06 15:05:39.730152
2554	1	2799	Card	\N	\N	tags	2016-06-06 15:05:40.034313
2555	11	2799	Card	\N	\N	tags	2016-06-06 15:05:40.059709
2556	1	2800	Card	\N	\N	tags	2016-06-06 15:05:40.349975
2557	11	2800	Card	\N	\N	tags	2016-06-06 15:05:40.382058
2558	1	2801	Card	\N	\N	tags	2016-06-06 15:05:40.699981
2559	11	2801	Card	\N	\N	tags	2016-06-06 15:05:40.736032
2560	1	2802	Card	\N	\N	tags	2016-06-06 15:05:40.910616
2561	11	2802	Card	\N	\N	tags	2016-06-06 15:05:40.950262
2562	1	2803	Card	\N	\N	tags	2016-06-06 15:05:41.327634
2563	13	2803	Card	\N	\N	tags	2016-06-06 15:05:41.355079
2564	14	2803	Card	\N	\N	tags	2016-06-06 15:05:41.390585
2565	1	2804	Card	\N	\N	tags	2016-06-06 15:05:41.605258
2566	14	2804	Card	\N	\N	tags	2016-06-06 15:05:41.640743
2567	1	2805	Card	\N	\N	tags	2016-06-06 15:05:41.82132
2568	14	2805	Card	\N	\N	tags	2016-06-06 15:05:41.844983
2569	11	2805	Card	\N	\N	tags	2016-06-06 15:05:41.873995
2570	1	2806	Card	\N	\N	tags	2016-06-06 15:05:42.051407
2571	14	2806	Card	\N	\N	tags	2016-06-06 15:05:42.083526
2572	11	2806	Card	\N	\N	tags	2016-06-06 15:05:42.114899
2573	15	2807	Card	\N	\N	tags	2016-06-06 15:05:42.55762
2574	16	2807	Card	\N	\N	tags	2016-06-06 15:05:42.597995
2575	15	2808	Card	\N	\N	tags	2016-06-06 15:05:42.95322
2576	16	2808	Card	\N	\N	tags	2016-06-06 15:05:42.988477
2577	15	2809	Card	\N	\N	tags	2016-06-06 15:05:43.332026
2578	16	2809	Card	\N	\N	tags	2016-06-06 15:05:43.361384
2579	15	2810	Card	\N	\N	tags	2016-06-06 15:05:43.642973
2580	17	2810	Card	\N	\N	tags	2016-06-06 15:05:43.679124
2581	15	2811	Card	\N	\N	tags	2016-06-06 15:05:43.880448
2582	18	2811	Card	\N	\N	tags	2016-06-06 15:05:43.919718
2583	19	2812	Card	\N	\N	tags	2016-06-06 15:05:44.4929
2584	20	2812	Card	\N	\N	tags	2016-06-06 15:05:44.526232
2585	21	2812	Card	\N	\N	tags	2016-06-06 15:05:44.55462
2586	19	2813	Card	\N	\N	tags	2016-06-06 15:05:45.016763
2587	20	2813	Card	\N	\N	tags	2016-06-06 15:05:45.042246
2588	21	2813	Card	\N	\N	tags	2016-06-06 15:05:45.078244
2589	19	2814	Card	\N	\N	tags	2016-06-06 15:05:45.601131
2590	20	2814	Card	\N	\N	tags	2016-06-06 15:05:45.633996
2591	21	2814	Card	\N	\N	tags	2016-06-06 15:05:45.669811
2592	19	2815	Card	\N	\N	tags	2016-06-06 15:05:46.125525
2593	20	2815	Card	\N	\N	tags	2016-06-06 15:05:46.153222
2594	21	2815	Card	\N	\N	tags	2016-06-06 15:05:46.176203
2595	19	2816	Card	\N	\N	tags	2016-06-06 15:05:46.660034
2596	20	2816	Card	\N	\N	tags	2016-06-06 15:05:46.692465
2597	21	2816	Card	\N	\N	tags	2016-06-06 15:05:46.721503
2598	19	2817	Card	\N	\N	tags	2016-06-06 15:05:47.182606
2599	20	2817	Card	\N	\N	tags	2016-06-06 15:05:47.218407
2600	21	2817	Card	\N	\N	tags	2016-06-06 15:05:47.253266
2601	19	2818	Card	\N	\N	tags	2016-06-06 15:05:47.724726
2602	20	2818	Card	\N	\N	tags	2016-06-06 15:05:47.751811
2603	21	2818	Card	\N	\N	tags	2016-06-06 15:05:47.778641
2604	19	2819	Card	\N	\N	tags	2016-06-06 15:05:48.070361
2605	22	2819	Card	\N	\N	tags	2016-06-06 15:05:48.101734
2606	19	2820	Card	\N	\N	tags	2016-06-06 15:05:48.434496
2607	22	2820	Card	\N	\N	tags	2016-06-06 15:05:48.464135
2608	19	2821	Card	\N	\N	tags	2016-06-06 15:05:48.747047
2609	23	2822	Card	\N	\N	tags	2016-06-06 15:05:49.071858
2610	23	2823	Card	\N	\N	tags	2016-06-06 15:05:49.28964
2611	24	2824	Card	\N	\N	tags	2016-06-06 15:05:49.519927
2612	19	2824	Card	\N	\N	tags	2016-06-06 15:05:49.548209
2613	25	2825	Card	\N	\N	tags	2016-06-06 15:05:50.008991
2614	26	2825	Card	\N	\N	tags	2016-06-06 15:05:50.043451
2615	25	2826	Card	\N	\N	tags	2016-06-06 15:05:50.500698
2616	26	2826	Card	\N	\N	tags	2016-06-06 15:05:50.524378
2617	25	2827	Card	\N	\N	tags	2016-06-06 15:05:50.948182
2618	26	2827	Card	\N	\N	tags	2016-06-06 15:05:50.980666
2619	25	2828	Card	\N	\N	tags	2016-06-06 15:05:51.414171
2620	26	2828	Card	\N	\N	tags	2016-06-06 15:05:51.441828
2621	25	2829	Card	\N	\N	tags	2016-06-06 15:05:51.730641
2622	26	2829	Card	\N	\N	tags	2016-06-06 15:05:51.756086
2623	25	2830	Card	\N	\N	tags	2016-06-06 15:05:52.211268
2624	26	2830	Card	\N	\N	tags	2016-06-06 15:05:52.243293
2625	25	2831	Card	\N	\N	tags	2016-06-06 15:05:52.679124
2626	26	2831	Card	\N	\N	tags	2016-06-06 15:05:52.714487
2627	25	2832	Card	\N	\N	tags	2016-06-06 15:05:53.159112
2628	26	2832	Card	\N	\N	tags	2016-06-06 15:05:53.196634
2629	25	2833	Card	\N	\N	tags	2016-06-06 15:05:53.694984
2630	26	2833	Card	\N	\N	tags	2016-06-06 15:05:53.730312
2631	25	2834	Card	\N	\N	tags	2016-06-06 15:05:54.209226
2632	26	2834	Card	\N	\N	tags	2016-06-06 15:05:54.241934
2633	25	2835	Card	\N	\N	tags	2016-06-06 15:05:54.724081
2634	26	2835	Card	\N	\N	tags	2016-06-06 15:05:54.754279
2635	25	2836	Card	\N	\N	tags	2016-06-06 15:05:55.203907
2636	26	2836	Card	\N	\N	tags	2016-06-06 15:05:55.233165
2637	27	2837	Card	\N	\N	tags	2016-06-06 15:05:55.687327
2638	28	2837	Card	\N	\N	tags	2016-06-06 15:05:55.731225
2639	27	2838	Card	\N	\N	tags	2016-06-06 15:05:56.186104
2640	29	2838	Card	\N	\N	tags	2016-06-06 15:05:56.220118
2641	30	2839	Card	\N	\N	tags	2016-06-06 15:05:56.605338
2642	31	2839	Card	\N	\N	tags	2016-06-06 15:05:56.640749
2643	32	2839	Card	\N	\N	tags	2016-06-06 15:05:56.667988
2644	33	2840	Card	\N	\N	tags	2016-06-06 15:05:57.299539
2645	34	2840	Card	\N	\N	tags	2016-06-06 15:05:57.327833
2646	33	2841	Card	\N	\N	tags	2016-06-06 15:05:57.685216
2647	35	2841	Card	\N	\N	tags	2016-06-06 15:05:57.713107
2648	36	2841	Card	\N	\N	tags	2016-06-06 15:05:57.753416
2649	37	2842	Card	\N	\N	tags	2016-06-06 15:05:58.139837
2650	38	2843	Card	\N	\N	tags	2016-06-06 15:05:58.964085
2651	39	2843	Card	\N	\N	tags	2016-06-06 15:05:59.00169
2652	40	2843	Card	\N	\N	tags	2016-06-06 15:05:59.02854
2653	38	2844	Card	\N	\N	tags	2016-06-06 15:05:59.849437
2654	41	2844	Card	\N	\N	tags	2016-06-06 15:05:59.892551
2655	40	2844	Card	\N	\N	tags	2016-06-06 15:05:59.927179
2656	38	2845	Card	\N	\N	tags	2016-06-06 15:06:00.25247
2657	42	2845	Card	\N	\N	tags	2016-06-06 15:06:00.314841
2658	43	2846	Card	\N	\N	tags	2016-06-06 15:06:00.464259
2659	38	2846	Card	\N	\N	tags	2016-06-06 15:06:00.504125
2660	44	2847	Card	\N	\N	tags	2016-06-06 15:06:01.057328
2661	38	2847	Card	\N	\N	tags	2016-06-06 15:06:01.09312
2662	45	2847	Card	\N	\N	tags	2016-06-06 15:06:01.119614
2663	44	2848	Card	\N	\N	tags	2016-06-06 15:06:01.494518
2664	38	2848	Card	\N	\N	tags	2016-06-06 15:06:01.531621
2665	45	2848	Card	\N	\N	tags	2016-06-06 15:06:01.56529
2666	45	2849	Card	\N	\N	tags	2016-06-06 15:06:02.083789
2667	23	2849	Card	\N	\N	tags	2016-06-06 15:06:02.110616
2668	46	2849	Card	\N	\N	tags	2016-06-06 15:06:02.141992
2669	47	2850	Card	\N	\N	tags	2016-06-06 15:06:02.399126
2670	23	2850	Card	\N	\N	tags	2016-06-06 15:06:02.442299
2671	45	2850	Card	\N	\N	tags	2016-06-06 15:06:02.47973
2672	48	2851	Card	\N	\N	tags	2016-06-06 15:06:02.931931
2673	49	2851	Card	\N	\N	tags	2016-06-06 15:06:02.963046
2674	50	2852	Card	\N	\N	tags	2016-06-06 15:06:03.162745
2675	49	2852	Card	\N	\N	tags	2016-06-06 15:06:03.196043
2676	50	2853	Card	\N	\N	tags	2016-06-06 15:06:03.457239
2677	49	2853	Card	\N	\N	tags	2016-06-06 15:06:03.48462
2678	51	2854	Card	\N	\N	tags	2016-06-06 15:06:03.741776
2679	49	2854	Card	\N	\N	tags	2016-06-06 15:06:03.796935
2680	52	2855	Card	\N	\N	tags	2016-06-06 15:06:04.011742
2681	49	2855	Card	\N	\N	tags	2016-06-06 15:06:04.051879
2682	53	2856	Card	\N	\N	tags	2016-06-06 15:06:04.23773
2683	54	2856	Card	\N	\N	tags	2016-06-06 15:06:04.276791
2684	49	2856	Card	\N	\N	tags	2016-06-06 15:06:04.310936
2685	53	2857	Card	\N	\N	tags	2016-06-06 15:06:04.473078
2686	54	2857	Card	\N	\N	tags	2016-06-06 15:06:04.505744
2687	49	2857	Card	\N	\N	tags	2016-06-06 15:06:04.539622
2688	53	2858	Card	\N	\N	tags	2016-06-06 15:06:04.907792
2689	55	2858	Card	\N	\N	tags	2016-06-06 15:06:04.940072
2690	49	2858	Card	\N	\N	tags	2016-06-06 15:06:04.972706
2691	53	2859	Card	\N	\N	tags	2016-06-06 15:06:05.178866
2692	56	2859	Card	\N	\N	tags	2016-06-06 15:06:05.21013
2693	49	2859	Card	\N	\N	tags	2016-06-06 15:06:05.236515
2694	57	2860	Card	\N	\N	tags	2016-06-06 15:06:05.402927
2695	49	2860	Card	\N	\N	tags	2016-06-06 15:06:05.439271
2696	35	2861	Card	\N	\N	tags	2016-06-06 15:06:05.654827
2697	49	2861	Card	\N	\N	tags	2016-06-06 15:06:05.69538
2698	58	2862	Card	\N	\N	tags	2016-06-06 15:06:05.863482
2699	33	2862	Card	\N	\N	tags	2016-06-06 15:06:05.891013
2700	49	2862	Card	\N	\N	tags	2016-06-06 15:06:05.92236
2701	59	2863	Card	\N	\N	tags	2016-06-06 15:06:06.147044
2702	60	2863	Card	\N	\N	tags	2016-06-06 15:06:06.182075
2703	49	2863	Card	\N	\N	tags	2016-06-06 15:06:06.219299
2704	60	2864	Card	\N	\N	tags	2016-06-06 15:06:06.779768
2705	59	2864	Card	\N	\N	tags	2016-06-06 15:06:06.813617
2706	49	2864	Card	\N	\N	tags	2016-06-06 15:06:06.86832
2707	61	2865	Card	\N	\N	tags	2016-06-06 15:06:07.162382
2708	62	2865	Card	\N	\N	tags	2016-06-06 15:06:07.196055
2709	49	2865	Card	\N	\N	tags	2016-06-06 15:06:07.233757
2710	61	2866	Card	\N	\N	tags	2016-06-06 15:06:07.440612
2711	62	2866	Card	\N	\N	tags	2016-06-06 15:06:07.482653
2712	49	2866	Card	\N	\N	tags	2016-06-06 15:06:07.516407
2713	53	2867	Card	\N	\N	tags	2016-06-06 15:06:07.953289
2714	63	2867	Card	\N	\N	tags	2016-06-06 15:06:07.983657
2715	49	2867	Card	\N	\N	tags	2016-06-06 15:06:08.015067
2716	64	2868	Card	\N	\N	tags	2016-06-06 15:06:08.2307
2717	63	2868	Card	\N	\N	tags	2016-06-06 15:06:08.268
2718	49	2868	Card	\N	\N	tags	2016-06-06 15:06:08.300076
2719	64	2869	Card	\N	\N	tags	2016-06-06 15:06:08.587221
2720	63	2869	Card	\N	\N	tags	2016-06-06 15:06:08.620408
2721	49	2869	Card	\N	\N	tags	2016-06-06 15:06:08.656356
2722	64	2870	Card	\N	\N	tags	2016-06-06 15:06:08.82909
2723	63	2870	Card	\N	\N	tags	2016-06-06 15:06:08.863586
2724	49	2870	Card	\N	\N	tags	2016-06-06 15:06:08.886924
2725	64	2871	Card	\N	\N	tags	2016-06-06 15:06:09.095663
2726	63	2871	Card	\N	\N	tags	2016-06-06 15:06:09.126209
2727	65	2871	Card	\N	\N	tags	2016-06-06 15:06:09.166025
2728	64	2872	Card	\N	\N	tags	2016-06-06 15:06:09.380154
2729	63	2872	Card	\N	\N	tags	2016-06-06 15:06:09.449365
2730	65	2872	Card	\N	\N	tags	2016-06-06 15:06:09.484408
2731	64	2873	Card	\N	\N	tags	2016-06-06 15:06:09.682978
2732	63	2873	Card	\N	\N	tags	2016-06-06 15:06:09.708428
2733	65	2873	Card	\N	\N	tags	2016-06-06 15:06:09.738176
2734	64	2874	Card	\N	\N	tags	2016-06-06 15:06:09.935039
2735	63	2874	Card	\N	\N	tags	2016-06-06 15:06:09.963792
2736	65	2874	Card	\N	\N	tags	2016-06-06 15:06:10.003801
2737	64	2875	Card	\N	\N	tags	2016-06-06 15:06:10.442325
2738	63	2875	Card	\N	\N	tags	2016-06-06 15:06:10.475391
2739	65	2875	Card	\N	\N	tags	2016-06-06 15:06:10.51474
2740	47	2876	Card	\N	\N	tags	2016-06-06 15:06:11.137449
2741	66	2876	Card	\N	\N	tags	2016-06-06 15:06:11.168286
2742	66	2877	Card	\N	\N	tags	2016-06-06 15:06:11.4106
2743	67	2877	Card	\N	\N	tags	2016-06-06 15:06:11.441893
2744	66	2878	Card	\N	\N	tags	2016-06-06 15:06:11.733226
2745	68	2878	Card	\N	\N	tags	2016-06-06 15:06:11.792048
2746	66	2879	Card	\N	\N	tags	2016-06-06 15:06:12.088151
2747	69	2879	Card	\N	\N	tags	2016-06-06 15:06:12.128215
2748	66	2880	Card	\N	\N	tags	2016-06-06 15:06:12.323512
2749	70	2880	Card	\N	\N	tags	2016-06-06 15:06:12.354024
2750	66	2881	Card	\N	\N	tags	2016-06-06 15:06:12.981227
2751	47	2881	Card	\N	\N	tags	2016-06-06 15:06:13.011018
2752	66	2882	Card	\N	\N	tags	2016-06-06 15:06:13.276906
2753	71	2882	Card	\N	\N	tags	2016-06-06 15:06:13.305103
2754	66	2883	Card	\N	\N	tags	2016-06-06 15:06:13.537276
2755	72	2883	Card	\N	\N	tags	2016-06-06 15:06:13.567549
2756	73	2883	Card	\N	\N	tags	2016-06-06 15:06:13.597641
2757	66	2884	Card	\N	\N	tags	2016-06-06 15:06:13.788432
2758	74	2884	Card	\N	\N	tags	2016-06-06 15:06:13.818245
2759	72	2884	Card	\N	\N	tags	2016-06-06 15:06:13.848402
2760	66	2885	Card	\N	\N	tags	2016-06-06 15:06:13.998132
2761	75	2885	Card	\N	\N	tags	2016-06-06 15:06:14.027293
2762	66	2886	Card	\N	\N	tags	2016-06-06 15:06:14.246362
2763	74	2886	Card	\N	\N	tags	2016-06-06 15:06:14.283066
2764	66	2887	Card	\N	\N	tags	2016-06-06 15:06:14.450811
2765	68	2887	Card	\N	\N	tags	2016-06-06 15:06:14.491417
2766	76	2888	Card	\N	\N	tags	2016-06-06 15:06:15.026918
2767	77	2888	Card	\N	\N	tags	2016-06-06 15:06:15.05346
2768	76	2889	Card	\N	\N	tags	2016-06-06 15:06:15.330016
2769	78	2889	Card	\N	\N	tags	2016-06-06 15:06:15.362943
2770	76	2890	Card	\N	\N	tags	2016-06-06 15:06:16.79948
2771	77	2890	Card	\N	\N	tags	2016-06-06 15:06:16.825048
2772	76	2891	Card	\N	\N	tags	2016-06-06 15:06:17.296047
2773	77	2891	Card	\N	\N	tags	2016-06-06 15:06:17.356974
2774	76	2892	Card	\N	\N	tags	2016-06-06 15:06:18.316478
2775	77	2892	Card	\N	\N	tags	2016-06-06 15:06:18.34963
2776	79	2893	Card	\N	\N	tags	2016-06-06 15:06:19.076336
2777	80	2893	Card	\N	\N	tags	2016-06-06 15:06:19.112471
2778	79	2894	Card	\N	\N	tags	2016-06-06 15:06:19.423215
2779	81	2894	Card	\N	\N	tags	2016-06-06 15:06:19.452827
2780	79	2895	Card	\N	\N	tags	2016-06-06 15:06:19.736562
2781	66	2895	Card	\N	\N	tags	2016-06-06 15:06:19.754316
2782	79	2896	Card	\N	\N	tags	2016-06-06 15:06:19.892877
2783	66	2896	Card	\N	\N	tags	2016-06-06 15:06:19.909682
2784	60	2896	Card	\N	\N	tags	2016-06-06 15:06:19.926074
2785	79	2897	Card	\N	\N	tags	2016-06-06 15:06:20.043479
2786	66	2897	Card	\N	\N	tags	2016-06-06 15:06:20.060461
2787	79	2898	Card	\N	\N	tags	2016-06-06 15:06:20.181513
2788	82	2898	Card	\N	\N	tags	2016-06-06 15:06:20.198804
2789	32	2899	Card	\N	\N	tags	2016-06-06 15:06:20.81896
2790	83	2899	Card	\N	\N	tags	2016-06-06 15:06:20.838314
2791	84	2899	Card	\N	\N	tags	2016-06-06 15:06:20.857576
2792	32	2900	Card	\N	\N	tags	2016-06-06 15:06:20.981281
2793	85	2900	Card	\N	\N	tags	2016-06-06 15:06:21.006105
2794	66	2900	Card	\N	\N	tags	2016-06-06 15:06:21.029611
2795	32	2901	Card	\N	\N	tags	2016-06-06 15:06:21.157251
2796	86	2901	Card	\N	\N	tags	2016-06-06 15:06:21.173683
2797	32	2902	Card	\N	\N	tags	2016-06-06 15:06:21.263636
2798	87	2902	Card	\N	\N	tags	2016-06-06 15:06:21.279661
2799	32	2903	Card	\N	\N	tags	2016-06-06 15:06:21.413029
2800	24	2903	Card	\N	\N	tags	2016-06-06 15:06:21.428387
2801	32	2904	Card	\N	\N	tags	2016-06-06 15:06:21.514906
2802	84	2904	Card	\N	\N	tags	2016-06-06 15:06:21.531592
2803	88	2904	Card	\N	\N	tags	2016-06-06 15:06:21.55515
2804	32	2905	Card	\N	\N	tags	2016-06-06 15:06:21.656357
2805	89	2905	Card	\N	\N	tags	2016-06-06 15:06:21.673854
2806	66	2905	Card	\N	\N	tags	2016-06-06 15:06:21.690087
2807	32	2906	Card	\N	\N	tags	2016-06-06 15:06:21.80087
2808	90	2906	Card	\N	\N	tags	2016-06-06 15:06:21.816637
2809	32	2907	Card	\N	\N	tags	2016-06-06 15:06:21.900236
2810	86	2907	Card	\N	\N	tags	2016-06-06 15:06:21.919455
2811	32	2908	Card	\N	\N	tags	2016-06-06 15:06:22.015246
2812	22	2908	Card	\N	\N	tags	2016-06-06 15:06:22.033373
2813	32	2909	Card	\N	\N	tags	2016-06-06 15:06:22.125537
2814	91	2909	Card	\N	\N	tags	2016-06-06 15:06:22.140809
2815	32	2910	Card	\N	\N	tags	2016-06-06 15:06:22.292804
2816	92	2910	Card	\N	\N	tags	2016-06-06 15:06:22.308143
2817	32	2911	Card	\N	\N	tags	2016-06-06 15:06:22.445396
2818	93	2911	Card	\N	\N	tags	2016-06-06 15:06:22.462278
2819	32	2912	Card	\N	\N	tags	2016-06-06 15:06:22.550455
2820	94	2912	Card	\N	\N	tags	2016-06-06 15:06:22.566351
2821	32	2913	Card	\N	\N	tags	2016-06-06 15:06:22.714343
2822	95	2913	Card	\N	\N	tags	2016-06-06 15:06:22.734124
2823	32	2914	Card	\N	\N	tags	2016-06-06 15:06:22.835977
2824	96	2914	Card	\N	\N	tags	2016-06-06 15:06:22.856607
2825	32	2915	Card	\N	\N	tags	2016-06-06 15:06:22.979537
2826	97	2915	Card	\N	\N	tags	2016-06-06 15:06:23.002262
2827	98	2915	Card	\N	\N	tags	2016-06-06 15:06:23.020166
2828	32	2916	Card	\N	\N	tags	2016-06-06 15:06:23.114179
2829	99	2916	Card	\N	\N	tags	2016-06-06 15:06:23.131644
2830	32	2917	Card	\N	\N	tags	2016-06-06 15:06:23.225462
2831	81	2917	Card	\N	\N	tags	2016-06-06 15:06:23.24375
2832	100	2918	Card	\N	\N	tags	2016-06-06 15:06:23.558038
2833	101	2918	Card	\N	\N	tags	2016-06-06 15:06:23.575682
2834	66	2918	Card	\N	\N	tags	2016-06-06 15:06:23.593081
2835	100	2919	Card	\N	\N	tags	2016-06-06 15:06:23.706739
2836	101	2919	Card	\N	\N	tags	2016-06-06 15:06:23.726881
2837	66	2919	Card	\N	\N	tags	2016-06-06 15:06:23.754564
2838	100	2920	Card	\N	\N	tags	2016-06-06 15:06:23.860676
2839	24	2920	Card	\N	\N	tags	2016-06-06 15:06:23.875732
2840	66	2920	Card	\N	\N	tags	2016-06-06 15:06:23.905615
2841	100	2921	Card	\N	\N	tags	2016-06-06 15:06:24.115975
2842	102	2921	Card	\N	\N	tags	2016-06-06 15:06:24.130209
2843	100	2922	Card	\N	\N	tags	2016-06-06 15:06:24.287613
2844	102	2922	Card	\N	\N	tags	2016-06-06 15:06:24.339593
2845	66	2922	Card	\N	\N	tags	2016-06-06 15:06:24.355
2846	100	2923	Card	\N	\N	tags	2016-06-06 15:06:24.472985
2847	103	2923	Card	\N	\N	tags	2016-06-06 15:06:24.497434
2848	66	2923	Card	\N	\N	tags	2016-06-06 15:06:24.518772
2849	100	2924	Card	\N	\N	tags	2016-06-06 15:06:24.641688
2850	104	2924	Card	\N	\N	tags	2016-06-06 15:06:24.664448
2851	66	2924	Card	\N	\N	tags	2016-06-06 15:06:24.698723
2852	100	2925	Card	\N	\N	tags	2016-06-06 15:06:24.841637
2853	105	2925	Card	\N	\N	tags	2016-06-06 15:06:24.862624
2854	66	2925	Card	\N	\N	tags	2016-06-06 15:06:24.884677
2855	100	2926	Card	\N	\N	tags	2016-06-06 15:06:25.075416
2856	106	2926	Card	\N	\N	tags	2016-06-06 15:06:25.099361
2857	100	2927	Card	\N	\N	tags	2016-06-06 15:06:25.294963
2858	66	2927	Card	\N	\N	tags	2016-06-06 15:06:25.311993
2859	60	2928	Card	\N	\N	tags	2016-06-06 15:06:27.044744
2860	107	2928	Card	\N	\N	tags	2016-06-06 15:06:27.076879
2861	47	2928	Card	\N	\N	tags	2016-06-06 15:06:27.113714
2862	1	2929	Card	\N	\N	tags	2016-06-06 15:06:27.490293
2863	13	2929	Card	\N	\N	tags	2016-06-06 15:06:27.520893
2864	1	2930	Card	\N	\N	tags	2016-06-06 15:06:28.010498
2865	13	2930	Card	\N	\N	tags	2016-06-06 15:06:28.041345
2866	60	2931	Card	\N	\N	tags	2016-06-06 15:06:28.611787
2867	107	2931	Card	\N	\N	tags	2016-06-06 15:06:28.639189
2868	47	2931	Card	\N	\N	tags	2016-06-06 15:06:28.666763
2869	1	2932	Card	\N	\N	tags	2016-06-06 15:06:28.998798
2870	13	2932	Card	\N	\N	tags	2016-06-06 15:06:29.027767
2871	60	2934	Card	\N	\N	tags	2016-06-06 15:06:29.82484
2872	107	2934	Card	\N	\N	tags	2016-06-06 15:06:29.849672
2873	47	2934	Card	\N	\N	tags	2016-06-06 15:06:29.876893
2874	1	2935	Card	\N	\N	tags	2016-06-06 15:06:30.205133
2875	13	2935	Card	\N	\N	tags	2016-06-06 15:06:30.237594
2876	1	2936	Card	\N	\N	tags	2016-06-06 15:06:31.256402
2877	13	2936	Card	\N	\N	tags	2016-06-06 15:06:31.283256
2878	60	2937	Card	\N	\N	tags	2016-06-06 15:06:32.293608
2879	107	2937	Card	\N	\N	tags	2016-06-06 15:06:32.326876
2880	47	2937	Card	\N	\N	tags	2016-06-06 15:06:32.358459
2881	1	2938	Card	\N	\N	tags	2016-06-06 15:06:32.949044
2882	13	2938	Card	\N	\N	tags	2016-06-06 15:06:32.979899
2883	1	2940	Card	\N	\N	tags	2016-06-06 15:07:54.302883
2884	2	2940	Card	\N	\N	tags	2016-06-06 15:07:54.335475
2885	1	2941	Card	\N	\N	tags	2016-06-06 15:07:58.942678
2886	2	2941	Card	\N	\N	tags	2016-06-06 15:07:58.974729
2887	3	2941	Card	\N	\N	tags	2016-06-06 15:07:59.00162
2888	1	2942	Card	\N	\N	tags	2016-06-06 15:08:00.405521
2889	4	2942	Card	\N	\N	tags	2016-06-06 15:08:00.435795
2890	5	2942	Card	\N	\N	tags	2016-06-06 15:08:00.47064
2891	1	2944	Card	\N	\N	tags	2016-06-06 15:16:35.097535
2892	4	2944	Card	\N	\N	tags	2016-06-06 15:16:35.126705
2893	5	2944	Card	\N	\N	tags	2016-06-06 15:16:35.155446
2894	1	2946	Card	\N	\N	tags	2016-06-06 15:21:07.762703
2895	4	2946	Card	\N	\N	tags	2016-06-06 15:21:07.789077
2896	6	2946	Card	\N	\N	tags	2016-06-06 15:21:07.822402
2927	1	2979	Card	\N	\N	tags	2016-06-07 08:15:45.501546
2928	2	2979	Card	\N	\N	tags	2016-06-07 08:15:45.531853
2929	1	2980	Card	\N	\N	tags	2016-06-07 08:15:45.689793
2930	2	2980	Card	\N	\N	tags	2016-06-07 08:15:45.709266
2931	3	2980	Card	\N	\N	tags	2016-06-07 08:15:45.728447
2932	1	2981	Card	\N	\N	tags	2016-06-07 08:15:45.829384
2933	4	2981	Card	\N	\N	tags	2016-06-07 08:15:45.850823
2934	5	2981	Card	\N	\N	tags	2016-06-07 08:15:45.880621
2935	1	2982	Card	\N	\N	tags	2016-06-07 08:16:04.415361
2936	4	2982	Card	\N	\N	tags	2016-06-07 08:16:04.43773
2937	5	2982	Card	\N	\N	tags	2016-06-07 08:16:04.464193
2938	1	2983	Card	\N	\N	tags	2016-06-07 08:16:51.213297
2939	2	2983	Card	\N	\N	tags	2016-06-07 08:16:51.240729
2940	1	2984	Card	\N	\N	tags	2016-06-07 08:16:51.410279
2941	2	2984	Card	\N	\N	tags	2016-06-07 08:16:51.429997
2942	3	2984	Card	\N	\N	tags	2016-06-07 08:16:51.449011
2943	1	2985	Card	\N	\N	tags	2016-06-07 08:16:51.550107
2944	4	2985	Card	\N	\N	tags	2016-06-07 08:16:51.569905
2945	5	2985	Card	\N	\N	tags	2016-06-07 08:16:51.589169
2946	1	2986	Card	\N	\N	tags	2016-06-07 08:17:08.349962
2947	4	2986	Card	\N	\N	tags	2016-06-07 08:17:08.368833
2948	5	2986	Card	\N	\N	tags	2016-06-07 08:17:08.387933
2949	1	2987	Card	\N	\N	tags	2016-06-07 08:17:09.434366
2950	4	2987	Card	\N	\N	tags	2016-06-07 08:17:09.454196
2951	6	2987	Card	\N	\N	tags	2016-06-07 08:17:09.473699
2952	1	2988	Card	\N	\N	tags	2016-06-07 08:17:10.203051
2953	4	2988	Card	\N	\N	tags	2016-06-07 08:17:10.223691
2954	6	2988	Card	\N	\N	tags	2016-06-07 08:17:10.242321
2955	1	2989	Card	\N	\N	tags	2016-06-07 08:17:58.962706
2956	2	2989	Card	\N	\N	tags	2016-06-07 08:17:58.992565
2957	1	2990	Card	\N	\N	tags	2016-06-07 08:17:59.193847
2958	2	2990	Card	\N	\N	tags	2016-06-07 08:17:59.213507
2959	3	2990	Card	\N	\N	tags	2016-06-07 08:17:59.232174
2960	1	2991	Card	\N	\N	tags	2016-06-07 08:17:59.347549
2961	4	2991	Card	\N	\N	tags	2016-06-07 08:17:59.367765
2962	5	2991	Card	\N	\N	tags	2016-06-07 08:17:59.386779
2963	1	2992	Card	\N	\N	tags	2016-06-07 08:23:03.325855
2964	2	2992	Card	\N	\N	tags	2016-06-07 08:23:03.358166
2965	1	2993	Card	\N	\N	tags	2016-06-07 08:23:03.545623
2966	2	2993	Card	\N	\N	tags	2016-06-07 08:23:03.568128
2967	3	2993	Card	\N	\N	tags	2016-06-07 08:23:03.584199
2968	1	2994	Card	\N	\N	tags	2016-06-07 08:23:03.693562
2969	4	2994	Card	\N	\N	tags	2016-06-07 08:23:03.718507
2970	5	2994	Card	\N	\N	tags	2016-06-07 08:23:03.738575
2971	1	2995	Card	\N	\N	tags	2016-06-07 08:27:38.879185
2972	2	2995	Card	\N	\N	tags	2016-06-07 08:27:38.905392
2973	1	2996	Card	\N	\N	tags	2016-06-07 08:27:39.036952
2974	2	2996	Card	\N	\N	tags	2016-06-07 08:27:39.05282
2975	3	2996	Card	\N	\N	tags	2016-06-07 08:27:39.067229
2976	1	2997	Card	\N	\N	tags	2016-06-07 08:27:39.146102
2977	4	2997	Card	\N	\N	tags	2016-06-07 08:27:39.161683
2978	5	2997	Card	\N	\N	tags	2016-06-07 08:27:39.17512
2979	1	2998	Card	\N	\N	tags	2016-06-07 08:28:12.118659
2980	2	2998	Card	\N	\N	tags	2016-06-07 08:28:12.143205
2981	1	2999	Card	\N	\N	tags	2016-06-07 08:28:12.361348
2982	2	2999	Card	\N	\N	tags	2016-06-07 08:28:12.388539
2983	3	2999	Card	\N	\N	tags	2016-06-07 08:28:12.41188
2984	1	3000	Card	\N	\N	tags	2016-06-07 08:28:12.531114
2985	4	3000	Card	\N	\N	tags	2016-06-07 08:28:12.556654
2986	5	3000	Card	\N	\N	tags	2016-06-07 08:28:12.577682
2987	1	3001	Card	\N	\N	tags	2016-06-07 08:28:59.519779
2988	2	3001	Card	\N	\N	tags	2016-06-07 08:28:59.555027
2989	1	3002	Card	\N	\N	tags	2016-06-07 08:28:59.750454
2990	2	3002	Card	\N	\N	tags	2016-06-07 08:28:59.769987
2991	3	3002	Card	\N	\N	tags	2016-06-07 08:28:59.792473
2992	1	3003	Card	\N	\N	tags	2016-06-07 08:28:59.903861
2993	4	3003	Card	\N	\N	tags	2016-06-07 08:28:59.924472
2994	5	3003	Card	\N	\N	tags	2016-06-07 08:28:59.942424
2995	1	3005	Card	\N	\N	tags	2016-06-07 08:31:52.895683
2996	2	3005	Card	\N	\N	tags	2016-06-07 08:31:52.931902
2997	1	3006	Card	\N	\N	tags	2016-06-07 08:31:55.106708
2998	2	3006	Card	\N	\N	tags	2016-06-07 08:31:55.1357
2999	3	3006	Card	\N	\N	tags	2016-06-07 08:31:55.156233
3000	1	3007	Card	\N	\N	tags	2016-06-07 08:31:56.776976
3001	4	3007	Card	\N	\N	tags	2016-06-07 08:31:56.798512
3002	5	3007	Card	\N	\N	tags	2016-06-07 08:31:56.81919
3003	1	3010	Card	\N	\N	tags	2016-06-07 08:38:39.495582
3004	2	3010	Card	\N	\N	tags	2016-06-07 08:38:39.525411
3005	1	3011	Card	\N	\N	tags	2016-06-07 08:38:41.458955
3006	2	3011	Card	\N	\N	tags	2016-06-07 08:38:41.47761
3007	3	3011	Card	\N	\N	tags	2016-06-07 08:38:41.499881
3008	1	3012	Card	\N	\N	tags	2016-06-07 08:38:42.822483
3009	4	3012	Card	\N	\N	tags	2016-06-07 08:38:42.84172
3010	5	3012	Card	\N	\N	tags	2016-06-07 08:38:42.859198
3011	1	3014	Card	\N	\N	tags	2016-06-07 08:39:11.333811
3012	2	3014	Card	\N	\N	tags	2016-06-07 08:39:11.359808
3013	1	3016	Card	\N	\N	tags	2016-06-07 08:39:13.097547
3014	2	3016	Card	\N	\N	tags	2016-06-07 08:39:13.121887
3015	3	3016	Card	\N	\N	tags	2016-06-07 08:39:13.146308
3016	1	3017	Card	\N	\N	tags	2016-06-07 08:39:15.591409
3017	4	3017	Card	\N	\N	tags	2016-06-07 08:39:15.615175
3018	5	3017	Card	\N	\N	tags	2016-06-07 08:39:15.637436
3019	1	3018	Card	\N	\N	tags	2016-06-07 08:39:52.825027
3020	2	3018	Card	\N	\N	tags	2016-06-07 08:39:52.846367
3021	1	3019	Card	\N	\N	tags	2016-06-07 08:39:54.641645
3022	2	3019	Card	\N	\N	tags	2016-06-07 08:39:54.659853
3023	3	3019	Card	\N	\N	tags	2016-06-07 08:39:54.680911
3024	1	3020	Card	\N	\N	tags	2016-06-07 08:39:56.094544
3025	4	3020	Card	\N	\N	tags	2016-06-07 08:39:56.114389
3026	5	3020	Card	\N	\N	tags	2016-06-07 08:39:56.132764
3027	1	3021	Card	\N	\N	tags	2016-06-07 08:40:21.778287
3028	2	3021	Card	\N	\N	tags	2016-06-07 08:40:21.800328
3029	1	3022	Card	\N	\N	tags	2016-06-07 08:40:23.352513
3030	2	3022	Card	\N	\N	tags	2016-06-07 08:40:23.374608
3031	3	3022	Card	\N	\N	tags	2016-06-07 08:40:23.393222
3032	1	3023	Card	\N	\N	tags	2016-06-07 08:40:25.195741
3033	4	3023	Card	\N	\N	tags	2016-06-07 08:40:25.216509
3034	5	3023	Card	\N	\N	tags	2016-06-07 08:40:25.238819
3035	1	3024	Card	\N	\N	tags	2016-06-07 08:40:59.730208
3036	2	3024	Card	\N	\N	tags	2016-06-07 08:40:59.752914
3037	1	3025	Card	\N	\N	tags	2016-06-07 08:41:01.857734
3038	2	3025	Card	\N	\N	tags	2016-06-07 08:41:01.878088
3039	3	3025	Card	\N	\N	tags	2016-06-07 08:41:01.897922
3040	1	3026	Card	\N	\N	tags	2016-06-07 08:41:03.386068
3041	4	3026	Card	\N	\N	tags	2016-06-07 08:41:03.405048
3042	5	3026	Card	\N	\N	tags	2016-06-07 08:41:03.425
3043	1	3027	Card	\N	\N	tags	2016-06-07 08:41:19.754599
3044	2	3027	Card	\N	\N	tags	2016-06-07 08:41:19.774761
3045	1	3028	Card	\N	\N	tags	2016-06-07 08:41:20.643391
3046	2	3028	Card	\N	\N	tags	2016-06-07 08:41:20.662933
3047	3	3028	Card	\N	\N	tags	2016-06-07 08:41:20.687346
3048	1	3029	Card	\N	\N	tags	2016-06-07 08:41:21.479801
3049	4	3029	Card	\N	\N	tags	2016-06-07 08:41:21.499918
3050	5	3029	Card	\N	\N	tags	2016-06-07 08:41:21.518567
3051	1	3030	Card	\N	\N	tags	2016-06-07 08:42:14.149425
3052	2	3030	Card	\N	\N	tags	2016-06-07 08:42:14.174277
3053	1	3031	Card	\N	\N	tags	2016-06-07 08:42:15.158897
3054	2	3031	Card	\N	\N	tags	2016-06-07 08:42:15.179949
3055	3	3031	Card	\N	\N	tags	2016-06-07 08:42:15.200541
3056	1	3032	Card	\N	\N	tags	2016-06-07 08:42:39.817605
3057	2	3032	Card	\N	\N	tags	2016-06-07 08:42:39.848068
3058	1	3033	Card	\N	\N	tags	2016-06-07 08:42:41.295097
3059	2	3033	Card	\N	\N	tags	2016-06-07 08:42:41.316187
3060	3	3033	Card	\N	\N	tags	2016-06-07 08:42:41.333233
3061	1	3034	Card	\N	\N	tags	2016-06-07 08:42:42.133518
3062	4	3034	Card	\N	\N	tags	2016-06-07 08:42:42.156812
3063	5	3034	Card	\N	\N	tags	2016-06-07 08:42:42.181654
3064	1	3035	Card	\N	\N	tags	2016-06-07 08:43:48.401771
3065	2	3035	Card	\N	\N	tags	2016-06-07 08:43:48.43534
3066	1	3036	Card	\N	\N	tags	2016-06-07 08:43:50.416743
3067	2	3036	Card	\N	\N	tags	2016-06-07 08:43:50.436479
3068	3	3036	Card	\N	\N	tags	2016-06-07 08:43:50.454069
3069	1	3037	Card	\N	\N	tags	2016-06-07 08:43:52.245354
3070	4	3037	Card	\N	\N	tags	2016-06-07 08:43:52.266674
3071	5	3037	Card	\N	\N	tags	2016-06-07 08:43:52.287637
3072	1	3039	Card	\N	\N	tags	2016-06-07 08:46:06.526718
3073	2	3039	Card	\N	\N	tags	2016-06-07 08:46:06.556134
3074	1	3040	Card	\N	\N	tags	2016-06-07 08:46:06.735688
3075	2	3040	Card	\N	\N	tags	2016-06-07 08:46:06.757412
3076	3	3040	Card	\N	\N	tags	2016-06-07 08:46:06.777125
3077	1	3041	Card	\N	\N	tags	2016-06-07 08:46:06.882425
3078	4	3041	Card	\N	\N	tags	2016-06-07 08:46:06.901426
3079	5	3041	Card	\N	\N	tags	2016-06-07 08:46:06.919814
3080	1	3043	Card	\N	\N	tags	2016-06-07 08:47:07.814822
3081	2	3043	Card	\N	\N	tags	2016-06-07 08:47:07.830947
3082	1	3044	Card	\N	\N	tags	2016-06-07 08:47:07.966775
3083	2	3044	Card	\N	\N	tags	2016-06-07 08:47:07.980991
3084	3	3044	Card	\N	\N	tags	2016-06-07 08:47:07.99767
3133	1	3061	Card	\N	\N	tags	2016-06-07 08:47:27.035804
3134	4	3061	Card	\N	\N	tags	2016-06-07 08:47:27.059309
3135	8	3061	Card	\N	\N	tags	2016-06-07 08:47:27.079291
3136	1	3062	Card	\N	\N	tags	2016-06-07 08:47:27.241554
3137	4	3062	Card	\N	\N	tags	2016-06-07 08:47:27.26033
3138	1	3063	Card	\N	\N	tags	2016-06-07 08:47:27.394792
3139	9	3063	Card	\N	\N	tags	2016-06-07 08:47:27.413022
3140	1	3064	Card	\N	\N	tags	2016-06-07 08:47:27.593154
3141	10	3064	Card	\N	\N	tags	2016-06-07 08:47:27.613129
3142	1	3065	Card	\N	\N	tags	2016-06-07 08:47:27.828301
3143	11	3065	Card	\N	\N	tags	2016-06-07 08:47:27.899576
3177	15	3081	Card	\N	\N	tags	2016-06-07 08:47:34.13404
3178	16	3081	Card	\N	\N	tags	2016-06-07 08:47:34.155418
3179	15	3082	Card	\N	\N	tags	2016-06-07 08:47:34.324281
3180	17	3082	Card	\N	\N	tags	2016-06-07 08:47:34.3413
3181	15	3083	Card	\N	\N	tags	2016-06-07 08:47:34.446019
3182	18	3083	Card	\N	\N	tags	2016-06-07 08:47:34.515911
3189	19	3086	Card	\N	\N	tags	2016-06-07 08:47:38.389403
3190	20	3086	Card	\N	\N	tags	2016-06-07 08:47:38.408688
3191	21	3086	Card	\N	\N	tags	2016-06-07 08:47:38.427481
3208	19	3093	Card	\N	\N	tags	2016-06-07 08:47:45.72669
3209	23	3094	Card	\N	\N	tags	2016-06-07 08:47:45.969423
3211	24	3096	Card	\N	\N	tags	2016-06-07 08:47:46.319635
3212	19	3096	Card	\N	\N	tags	2016-06-07 08:47:46.332878
3237	27	3109	Card	\N	\N	tags	2016-06-07 08:48:01.332934
3244	33	3112	Card	\N	\N	tags	2016-06-07 08:48:03.049159
3245	34	3112	Card	\N	\N	tags	2016-06-07 08:48:03.068507
3246	33	3113	Card	\N	\N	tags	2016-06-07 08:48:03.288381
3247	35	3113	Card	\N	\N	tags	2016-06-07 08:48:03.309858
3248	36	3113	Card	\N	\N	tags	2016-06-07 08:48:03.33359
3249	37	3114	Card	\N	\N	tags	2016-06-07 08:48:03.987814
3258	43	3118	Card	\N	\N	tags	2016-06-07 08:48:08.385087
3259	38	3118	Card	\N	\N	tags	2016-06-07 08:48:08.405953
3260	44	3119	Card	\N	\N	tags	2016-06-07 08:48:09.309006
3261	38	3119	Card	\N	\N	tags	2016-06-07 08:48:09.329695
3262	45	3119	Card	\N	\N	tags	2016-06-07 08:48:09.346411
3263	44	3120	Card	\N	\N	tags	2016-06-07 08:48:09.552123
3264	38	3120	Card	\N	\N	tags	2016-06-07 08:48:09.573845
3265	45	3120	Card	\N	\N	tags	2016-06-07 08:48:09.596733
3266	45	3121	Card	\N	\N	tags	2016-06-07 08:48:09.9257
3267	23	3121	Card	\N	\N	tags	2016-06-07 08:48:09.945455
3268	46	3121	Card	\N	\N	tags	2016-06-07 08:48:09.969179
3269	47	3122	Card	\N	\N	tags	2016-06-07 08:48:10.128301
3270	23	3122	Card	\N	\N	tags	2016-06-07 08:48:10.148424
3271	45	3122	Card	\N	\N	tags	2016-06-07 08:48:10.169035
3272	48	3123	Card	\N	\N	tags	2016-06-07 08:48:10.527187
3273	49	3123	Card	\N	\N	tags	2016-06-07 08:48:10.547879
3274	50	3124	Card	\N	\N	tags	2016-06-07 08:48:10.681738
3275	49	3124	Card	\N	\N	tags	2016-06-07 08:48:10.702548
3276	50	3125	Card	\N	\N	tags	2016-06-07 08:48:10.857
3277	49	3125	Card	\N	\N	tags	2016-06-07 08:48:10.877324
3278	51	3126	Card	\N	\N	tags	2016-06-07 08:48:11.005946
3279	49	3126	Card	\N	\N	tags	2016-06-07 08:48:11.025873
3280	52	3127	Card	\N	\N	tags	2016-06-07 08:48:11.128638
3281	49	3127	Card	\N	\N	tags	2016-06-07 08:48:11.148707
3282	53	3128	Card	\N	\N	tags	2016-06-07 08:48:11.256062
3283	54	3128	Card	\N	\N	tags	2016-06-07 08:48:11.274527
3284	49	3128	Card	\N	\N	tags	2016-06-07 08:48:11.294406
3285	53	3129	Card	\N	\N	tags	2016-06-07 08:48:11.406243
3286	54	3129	Card	\N	\N	tags	2016-06-07 08:48:11.428865
3287	49	3129	Card	\N	\N	tags	2016-06-07 08:48:11.449254
3288	53	3130	Card	\N	\N	tags	2016-06-07 08:48:11.644259
3289	55	3130	Card	\N	\N	tags	2016-06-07 08:48:11.66482
3290	49	3130	Card	\N	\N	tags	2016-06-07 08:48:11.685626
3291	53	3131	Card	\N	\N	tags	2016-06-07 08:48:11.804965
3292	56	3131	Card	\N	\N	tags	2016-06-07 08:48:11.827006
3293	49	3131	Card	\N	\N	tags	2016-06-07 08:48:11.848555
3304	60	3136	Card	\N	\N	tags	2016-06-07 08:48:12.750653
3305	59	3136	Card	\N	\N	tags	2016-06-07 08:48:12.772704
3306	49	3136	Card	\N	\N	tags	2016-06-07 08:48:12.792153
3307	61	3137	Card	\N	\N	tags	2016-06-07 08:48:12.959118
3308	62	3137	Card	\N	\N	tags	2016-06-07 08:48:12.978881
3309	49	3137	Card	\N	\N	tags	2016-06-07 08:48:12.998906
3313	53	3139	Card	\N	\N	tags	2016-06-07 08:48:13.484498
3314	63	3139	Card	\N	\N	tags	2016-06-07 08:48:13.503651
3316	64	3140	Card	\N	\N	tags	2016-06-07 08:48:13.642398
3317	63	3140	Card	\N	\N	tags	2016-06-07 08:48:13.660648
3320	63	3141	Card	\N	\N	tags	2016-06-07 08:48:13.856457
3323	63	3142	Card	\N	\N	tags	2016-06-07 08:48:14.013066
3366	76	3160	Card	\N	\N	tags	2016-06-07 08:48:17.895003
3367	77	3160	Card	\N	\N	tags	2016-06-07 08:48:17.913784
3368	76	3161	Card	\N	\N	tags	2016-06-07 08:48:18.083474
3369	78	3161	Card	\N	\N	tags	2016-06-07 08:48:18.10426
3370	76	3162	Card	\N	\N	tags	2016-06-07 08:48:18.896798
3371	77	3162	Card	\N	\N	tags	2016-06-07 08:48:18.916179
3372	76	3163	Card	\N	\N	tags	2016-06-07 08:48:19.157355
3373	77	3163	Card	\N	\N	tags	2016-06-07 08:48:19.177366
3374	76	3164	Card	\N	\N	tags	2016-06-07 08:48:19.725324
3375	77	3164	Card	\N	\N	tags	2016-06-07 08:48:19.747884
3376	79	3165	Card	\N	\N	tags	2016-06-07 08:48:20.358035
3377	80	3165	Card	\N	\N	tags	2016-06-07 08:48:20.378339
3378	79	3166	Card	\N	\N	tags	2016-06-07 08:48:20.666922
3379	81	3166	Card	\N	\N	tags	2016-06-07 08:48:20.686939
3380	79	3167	Card	\N	\N	tags	2016-06-07 08:48:20.920143
3381	66	3167	Card	\N	\N	tags	2016-06-07 08:48:20.943092
3382	79	3168	Card	\N	\N	tags	2016-06-07 08:48:21.083757
3383	66	3168	Card	\N	\N	tags	2016-06-07 08:48:21.107087
3384	60	3168	Card	\N	\N	tags	2016-06-07 08:48:21.132097
3385	79	3169	Card	\N	\N	tags	2016-06-07 08:48:21.273104
3386	66	3169	Card	\N	\N	tags	2016-06-07 08:48:21.294956
3387	79	3170	Card	\N	\N	tags	2016-06-07 08:48:21.423936
3388	82	3170	Card	\N	\N	tags	2016-06-07 08:48:21.44638
3389	32	3171	Card	\N	\N	tags	2016-06-07 08:48:21.743478
3390	83	3171	Card	\N	\N	tags	2016-06-07 08:48:21.763659
3391	84	3171	Card	\N	\N	tags	2016-06-07 08:48:21.781579
3392	32	3172	Card	\N	\N	tags	2016-06-07 08:48:21.903224
3393	85	3172	Card	\N	\N	tags	2016-06-07 08:48:21.924609
3394	66	3172	Card	\N	\N	tags	2016-06-07 08:48:21.947018
3395	32	3173	Card	\N	\N	tags	2016-06-07 08:48:22.069833
3396	86	3173	Card	\N	\N	tags	2016-06-07 08:48:22.090347
3397	32	3174	Card	\N	\N	tags	2016-06-07 08:48:22.196899
3398	87	3174	Card	\N	\N	tags	2016-06-07 08:48:22.217468
3399	32	3175	Card	\N	\N	tags	2016-06-07 08:48:22.377751
3400	24	3175	Card	\N	\N	tags	2016-06-07 08:48:22.398239
3401	32	3176	Card	\N	\N	tags	2016-06-07 08:48:22.50801
3402	84	3176	Card	\N	\N	tags	2016-06-07 08:48:22.526966
3403	88	3176	Card	\N	\N	tags	2016-06-07 08:48:22.549304
3404	32	3177	Card	\N	\N	tags	2016-06-07 08:48:22.672749
3405	89	3177	Card	\N	\N	tags	2016-06-07 08:48:22.694672
3406	66	3177	Card	\N	\N	tags	2016-06-07 08:48:22.714402
3407	32	3178	Card	\N	\N	tags	2016-06-07 08:48:22.822333
3408	90	3178	Card	\N	\N	tags	2016-06-07 08:48:22.846382
3411	32	3180	Card	\N	\N	tags	2016-06-07 08:48:23.086575
3412	22	3180	Card	\N	\N	tags	2016-06-07 08:48:23.106332
3415	32	3182	Card	\N	\N	tags	2016-06-07 08:48:23.391004
3416	92	3182	Card	\N	\N	tags	2016-06-07 08:48:23.41079
3417	32	3183	Card	\N	\N	tags	2016-06-07 08:48:23.558839
3418	93	3183	Card	\N	\N	tags	2016-06-07 08:48:23.578094
3419	32	3184	Card	\N	\N	tags	2016-06-07 08:48:23.685854
3420	94	3184	Card	\N	\N	tags	2016-06-07 08:48:23.70504
3421	32	3185	Card	\N	\N	tags	2016-06-07 08:48:23.88273
3422	95	3185	Card	\N	\N	tags	2016-06-07 08:48:23.902536
3423	32	3186	Card	\N	\N	tags	2016-06-07 08:48:24.009405
3424	96	3186	Card	\N	\N	tags	2016-06-07 08:48:24.030524
3425	32	3187	Card	\N	\N	tags	2016-06-07 08:48:24.154745
3426	97	3187	Card	\N	\N	tags	2016-06-07 08:48:24.175204
3427	98	3187	Card	\N	\N	tags	2016-06-07 08:48:24.197529
3428	32	3188	Card	\N	\N	tags	2016-06-07 08:48:24.307374
3429	99	3188	Card	\N	\N	tags	2016-06-07 08:48:24.327211
3430	32	3189	Card	\N	\N	tags	2016-06-07 08:48:24.437275
3431	81	3189	Card	\N	\N	tags	2016-06-07 08:48:24.459624
3432	100	3190	Card	\N	\N	tags	2016-06-07 08:48:24.730106
3433	101	3190	Card	\N	\N	tags	2016-06-07 08:48:24.752857
3434	66	3190	Card	\N	\N	tags	2016-06-07 08:48:24.77453
3435	100	3191	Card	\N	\N	tags	2016-06-07 08:48:24.900068
3436	101	3191	Card	\N	\N	tags	2016-06-07 08:48:24.921741
3437	66	3191	Card	\N	\N	tags	2016-06-07 08:48:24.941486
3438	100	3192	Card	\N	\N	tags	2016-06-07 08:48:25.069149
3439	24	3192	Card	\N	\N	tags	2016-06-07 08:48:25.089809
3440	66	3192	Card	\N	\N	tags	2016-06-07 08:48:25.110407
3441	100	3193	Card	\N	\N	tags	2016-06-07 08:48:25.25896
3442	102	3193	Card	\N	\N	tags	2016-06-07 08:48:25.277014
3443	100	3194	Card	\N	\N	tags	2016-06-07 08:48:25.423349
3444	102	3194	Card	\N	\N	tags	2016-06-07 08:48:25.441679
3445	66	3194	Card	\N	\N	tags	2016-06-07 08:48:25.461222
3446	100	3195	Card	\N	\N	tags	2016-06-07 08:48:25.584298
3447	103	3195	Card	\N	\N	tags	2016-06-07 08:48:25.607018
3448	66	3195	Card	\N	\N	tags	2016-06-07 08:48:25.624409
3449	100	3196	Card	\N	\N	tags	2016-06-07 08:48:25.742074
3450	104	3196	Card	\N	\N	tags	2016-06-07 08:48:25.760986
3451	66	3196	Card	\N	\N	tags	2016-06-07 08:48:25.778329
3452	100	3197	Card	\N	\N	tags	2016-06-07 08:48:25.903986
3453	105	3197	Card	\N	\N	tags	2016-06-07 08:48:25.926554
3454	66	3197	Card	\N	\N	tags	2016-06-07 08:48:25.94725
3455	100	3198	Card	\N	\N	tags	2016-06-07 08:48:26.103018
3456	106	3198	Card	\N	\N	tags	2016-06-07 08:48:26.120721
3457	100	3199	Card	\N	\N	tags	2016-06-07 08:48:26.354579
3458	66	3199	Card	\N	\N	tags	2016-06-07 08:48:26.387143
3459	60	3200	Card	\N	\N	tags	2016-06-07 08:48:27.869779
3460	107	3200	Card	\N	\N	tags	2016-06-07 08:48:27.887252
3461	47	3200	Card	\N	\N	tags	2016-06-07 08:48:27.907162
3462	1	3201	Card	\N	\N	tags	2016-06-07 08:48:28.58026
3463	13	3201	Card	\N	\N	tags	2016-06-07 08:48:28.599876
3464	1	3202	Card	\N	\N	tags	2016-06-07 08:48:29.827116
3465	13	3202	Card	\N	\N	tags	2016-06-07 08:48:29.851071
3466	60	3203	Card	\N	\N	tags	2016-06-07 08:48:30.97746
3467	107	3203	Card	\N	\N	tags	2016-06-07 08:48:30.995369
3468	47	3203	Card	\N	\N	tags	2016-06-07 08:48:31.01601
3469	1	3204	Card	\N	\N	tags	2016-06-07 08:48:31.704504
3470	13	3204	Card	\N	\N	tags	2016-06-07 08:48:31.725533
3471	60	3206	Card	\N	\N	tags	2016-06-07 08:48:32.920827
3472	107	3206	Card	\N	\N	tags	2016-06-07 08:48:32.945484
3473	47	3206	Card	\N	\N	tags	2016-06-07 08:48:32.973881
3474	1	3207	Card	\N	\N	tags	2016-06-07 08:48:33.616623
3475	13	3207	Card	\N	\N	tags	2016-06-07 08:48:33.640657
3476	1	3208	Card	\N	\N	tags	2016-06-07 08:48:34.804437
3477	13	3208	Card	\N	\N	tags	2016-06-07 08:48:34.821239
3478	60	3209	Card	\N	\N	tags	2016-06-07 08:48:35.913408
3479	107	3209	Card	\N	\N	tags	2016-06-07 08:48:35.934522
3480	47	3209	Card	\N	\N	tags	2016-06-07 08:48:35.95487
3481	1	3210	Card	\N	\N	tags	2016-06-07 08:48:36.579218
3482	13	3210	Card	\N	\N	tags	2016-06-07 08:48:36.599823
3485	108	3213	Card	\N	\N	tags	2016-06-08 10:53:30.693384
3486	110	3213	Card	\N	\N	tags	2016-06-08 10:53:30.723379
3487	108	3214	Card	\N	\N	tags	2016-06-08 10:53:30.934664
3488	111	3214	Card	\N	\N	tags	2016-06-08 10:53:30.962871
3489	108	3215	Card	\N	\N	tags	2016-06-08 10:53:32.497446
3490	112	3215	Card	\N	\N	tags	2016-06-08 10:53:32.534602
3493	108	3217	Card	\N	\N	tags	2016-06-08 10:53:33.207002
3494	112	3217	Card	\N	\N	tags	2016-06-08 10:53:33.239922
3499	108	3220	Card	\N	\N	tags	2016-06-08 10:53:35.620304
3500	113	3220	Card	\N	\N	tags	2016-06-08 10:53:35.657952
3501	108	3221	Card	\N	\N	tags	2016-06-08 10:53:35.863198
3502	11	3221	Card	\N	\N	tags	2016-06-08 10:53:35.906555
3503	102	3221	Card	\N	\N	tags	2016-06-08 10:53:35.974996
3504	108	3222	Card	\N	\N	tags	2016-06-08 10:53:37.04946
3505	98	3222	Card	\N	\N	tags	2016-06-08 10:53:37.136667
3506	108	3223	Card	\N	\N	tags	2016-06-08 10:53:37.529586
3507	98	3223	Card	\N	\N	tags	2016-06-08 10:53:37.57724
3508	114	3223	Card	\N	\N	tags	2016-06-08 10:53:37.615577
3541	15	3254	Card	\N	\N	tags	2017-01-13 08:55:47.077686
3542	117	3254	Card	\N	\N	tags	2017-01-13 08:55:47.08512
3543	16	3254	Card	\N	\N	tags	2017-01-13 08:55:47.09258
3549	15	3294	Card	\N	\N	tags	2017-02-01 12:55:37.812726
3550	114	3294	Card	\N	\N	tags	2017-02-01 12:55:37.82289
3551	1	3287	Card	\N	\N	tags	2017-02-01 13:36:15.533377
3552	4	3287	Card	\N	\N	tags	2017-02-01 13:36:15.542
3554	1	3288	Card	\N	\N	tags	2017-02-01 13:37:01.116869
3555	4	3288	Card	\N	\N	tags	2017-02-01 13:37:01.12299
3557	1	3286	Card	\N	\N	tags	2017-02-01 13:37:24.770397
3558	4	3286	Card	\N	\N	tags	2017-02-01 13:37:24.775656
3560	120	3296	Card	\N	\N	tags	2017-02-01 13:55:23.596969
3561	20	3296	Card	\N	\N	tags	2017-02-01 13:55:23.601647
3562	21	3296	Card	\N	\N	tags	2017-02-01 13:55:23.606214
3563	120	3297	Card	\N	\N	tags	2017-02-01 14:04:18.373778
3564	20	3297	Card	\N	\N	tags	2017-02-01 14:04:18.379318
3565	21	3297	Card	\N	\N	tags	2017-02-01 14:04:18.383801
3566	17	3298	Card	\N	\N	tags	2017-02-02 08:28:26.306664
3567	15	3298	Card	\N	\N	tags	2017-02-02 08:28:26.311734
3569	122	3300	Card	\N	\N	tags	2017-02-05 13:25:57.493717
3570	49	3300	Card	\N	\N	tags	2017-02-05 13:25:57.503362
3571	50	3299	Card	\N	\N	tags	2017-02-05 13:26:32.276964
3572	49	3299	Card	\N	\N	tags	2017-02-05 13:26:32.283264
3573	123	3265	Card	\N	\N	tags	2017-02-08 13:31:44.856577
3574	1	3265	Card	\N	\N	tags	2017-02-08 13:31:44.867815
3575	124	3265	Card	\N	\N	tags	2017-02-08 13:31:44.876409
3576	1	3313	Card	\N	\N	tags	2017-02-08 13:35:16.195971
3577	125	3313	Card	\N	\N	tags	2017-02-08 13:35:16.200517
3578	14	3264	Card	\N	\N	tags	2017-02-08 13:44:16.320718
3579	1	3314	Card	\N	\N	tags	2017-02-08 13:50:06.112487
3580	126	3314	Card	\N	\N	tags	2017-02-08 13:50:06.116512
3581	14	3314	Card	\N	\N	tags	2017-02-08 13:50:06.120501
3582	126	3315	Card	\N	\N	tags	2017-02-08 14:02:41.141023
3583	14	3315	Card	\N	\N	tags	2017-02-08 14:02:41.147263
3584	126	3316	Card	\N	\N	tags	2017-02-08 14:05:51.83305
3585	14	3316	Card	\N	\N	tags	2017-02-08 14:05:51.840546
3586	1	3293	Card	\N	\N	tags	2017-02-08 14:15:33.201421
3587	1	3292	Card	\N	\N	tags	2017-02-08 14:16:33.840179
3588	1	3285	Card	\N	\N	tags	2017-02-08 14:18:22.604271
3589	5	3285	Card	\N	\N	tags	2017-02-08 14:18:22.611095
3590	127	3285	Card	\N	\N	tags	2017-02-08 14:18:22.618731
3591	1	3291	Card	\N	\N	tags	2017-02-08 14:20:17.901863
3592	1	3290	Card	\N	\N	tags	2017-02-08 14:21:50.516931
3593	4	3290	Card	\N	\N	tags	2017-02-08 14:21:50.521959
3595	1	3289	Card	\N	\N	tags	2017-02-08 14:22:29.895613
3596	4	3289	Card	\N	\N	tags	2017-02-08 14:22:29.901265
3598	128	3317	Card	\N	\N	tags	2017-02-08 14:30:34.50001
3599	14	3317	Card	\N	\N	tags	2017-02-08 14:30:34.505665
3600	128	3318	Card	\N	\N	tags	2017-02-08 14:30:36.808975
3601	14	3318	Card	\N	\N	tags	2017-02-08 14:30:36.814471
3602	14	3319	Card	\N	\N	tags	2017-02-08 14:36:38.157878
3603	129	3319	Card	\N	\N	tags	2017-02-08 14:36:38.165993
3604	130	3319	Card	\N	\N	tags	2017-02-08 14:36:38.170271
3605	14	3320	Card	\N	\N	tags	2017-02-08 14:40:16.989716
3606	129	3320	Card	\N	\N	tags	2017-02-08 14:40:16.998604
3607	130	3320	Card	\N	\N	tags	2017-02-08 14:40:17.004834
3608	1	3321	Card	\N	\N	tags	2017-02-08 14:45:20.84564
3609	14	3321	Card	\N	\N	tags	2017-02-08 14:45:20.850267
3610	130	3321	Card	\N	\N	tags	2017-02-08 14:45:20.854448
3611	14	3263	Card	\N	\N	tags	2017-02-08 14:48:58.860335
3612	129	3263	Card	\N	\N	tags	2017-02-08 14:48:58.864904
3613	130	3263	Card	\N	\N	tags	2017-02-08 14:48:58.86931
3614	1	3322	Card	\N	\N	tags	2017-02-08 16:28:21.58997
3615	127	3322	Card	\N	\N	tags	2017-02-08 16:28:21.59536
3620	1	3262	Card	\N	\N	tags	2017-02-08 16:32:58.147657
3621	127	3262	Card	\N	\N	tags	2017-02-08 16:32:58.154747
3622	1	3324	Card	\N	\N	tags	2017-02-08 16:35:48.07286
3623	8	3324	Card	\N	\N	tags	2017-02-08 16:35:48.077851
3633	111	3328	Card	\N	\N	tags	2017-02-09 09:15:27.686426
3634	108	3328	Card	\N	\N	tags	2017-02-09 09:15:27.692294
3635	23	3332	Card	\N	\N	tags	2017-02-10 12:22:33.100095
3636	20	3333	Card	\N	\N	tags	2017-02-10 12:32:00.008084
3637	19	3333	Card	\N	\N	tags	2017-02-10 12:32:00.022652
3638	21	3333	Card	\N	\N	tags	2017-02-10 12:32:00.030835
3639	22	3334	Card	\N	\N	tags	2017-02-10 12:35:32.421895
3640	19	3334	Card	\N	\N	tags	2017-02-10 12:35:32.427091
3641	35	3331	Card	\N	\N	tags	2017-02-10 13:28:13.587306
3642	33	3331	Card	\N	\N	tags	2017-02-10 13:28:13.596797
3643	33	3342	Card	\N	\N	tags	2017-02-15 11:40:33.984625
3644	133	3342	Card	\N	\N	tags	2017-02-15 11:40:33.99922
3645	37	3341	Card	\N	\N	tags	2017-02-15 11:41:12.057619
3646	100	3341	Card	\N	\N	tags	2017-02-15 11:41:12.065488
3647	33	3339	Card	\N	\N	tags	2017-02-15 11:43:42.050826
3648	35	3339	Card	\N	\N	tags	2017-02-15 11:43:42.057046
3649	35	3330	Card	\N	\N	tags	2017-02-15 11:45:06.285602
3650	33	3330	Card	\N	\N	tags	2017-02-15 11:45:06.292305
3651	134	3330	Card	\N	\N	tags	2017-02-15 11:45:06.296645
3652	135	3329	Card	\N	\N	tags	2017-02-15 11:48:03.224861
3653	33	3329	Card	\N	\N	tags	2017-02-15 11:48:03.230938
3654	36	3339	Card	\N	\N	tags	2017-02-15 11:51:57.545311
3655	114	3374	Card	\N	\N	tags	2017-02-17 12:32:11.820217
3656	15	3374	Card	\N	\N	tags	2017-02-17 12:32:11.830065
3657	136	3295	Card	\N	\N	tags	2017-02-17 12:34:15.959181
3658	15	3295	Card	\N	\N	tags	2017-02-17 12:34:15.964655
3659	15	3375	Card	\N	\N	tags	2017-02-17 12:37:39.522288
3660	16	3375	Card	\N	\N	tags	2017-02-17 12:37:39.52831
3661	16	3376	Card	\N	\N	tags	2017-02-17 12:42:04.372941
3662	15	3376	Card	\N	\N	tags	2017-02-17 12:42:04.380662
3663	64	3419	Card	\N	\N	tags	2017-02-21 16:42:24.904026
3664	63	3419	Card	\N	\N	tags	2017-02-21 16:42:24.916732
3666	53	3418	Card	\N	\N	tags	2017-02-21 16:43:29.739109
3667	63	3418	Card	\N	\N	tags	2017-02-21 16:43:29.746459
3669	65	3433	Card	\N	\N	tags	2017-02-21 17:43:07.967646
3670	63	3433	Card	\N	\N	tags	2017-02-21 17:43:07.979026
3672	65	3432	Card	\N	\N	tags	2017-02-21 17:43:29.539961
3673	63	3432	Card	\N	\N	tags	2017-02-21 17:43:29.54821
3675	65	3430	Card	\N	\N	tags	2017-02-21 17:43:49.941819
3676	63	3430	Card	\N	\N	tags	2017-02-21 17:43:49.94864
3678	65	3431	Card	\N	\N	tags	2017-02-21 17:44:11.37652
3679	63	3431	Card	\N	\N	tags	2017-02-21 17:44:11.381359
3681	65	3429	Card	\N	\N	tags	2017-02-21 17:44:41.582366
3682	63	3429	Card	\N	\N	tags	2017-02-21 17:44:41.587639
3684	65	3428	Card	\N	\N	tags	2017-02-21 17:45:06.00237
3685	63	3428	Card	\N	\N	tags	2017-02-21 17:45:06.009577
3687	65	3426	Card	\N	\N	tags	2017-02-21 17:45:28.604202
3688	63	3426	Card	\N	\N	tags	2017-02-21 17:45:28.60821
3690	65	3425	Card	\N	\N	tags	2017-02-21 17:45:49.896954
3691	63	3425	Card	\N	\N	tags	2017-02-21 17:45:49.903575
3693	137	3423	Card	\N	\N	tags	2017-02-21 17:46:14.826207
3694	63	3423	Card	\N	\N	tags	2017-02-21 17:46:14.834399
3696	65	3424	Card	\N	\N	tags	2017-02-21 17:46:34.806944
3697	63	3424	Card	\N	\N	tags	2017-02-21 17:46:34.815815
3699	137	3422	Card	\N	\N	tags	2017-02-21 17:46:58.671751
3700	63	3422	Card	\N	\N	tags	2017-02-21 17:46:58.68367
3702	137	3421	Card	\N	\N	tags	2017-02-21 17:47:17.305719
3703	63	3421	Card	\N	\N	tags	2017-02-21 17:47:17.314356
3705	65	3355	Card	\N	\N	tags	2017-02-21 17:47:40.095088
3706	63	3355	Card	\N	\N	tags	2017-02-21 17:47:40.101901
3708	65	3354	Card	\N	\N	tags	2017-02-21 17:48:04.804376
3709	63	3354	Card	\N	\N	tags	2017-02-21 17:48:04.810699
3711	65	3353	Card	\N	\N	tags	2017-02-21 17:48:28.867583
3712	63	3353	Card	\N	\N	tags	2017-02-21 17:48:28.873193
3714	65	3352	Card	\N	\N	tags	2017-02-21 17:48:48.205631
3715	63	3352	Card	\N	\N	tags	2017-02-21 17:48:48.212005
3717	65	3351	Card	\N	\N	tags	2017-02-21 17:49:16.446831
3718	63	3351	Card	\N	\N	tags	2017-02-21 17:49:16.453469
3720	137	3141	Card	\N	\N	tags	2017-02-21 17:49:41.148018
3721	138	3142	Card	\N	\N	tags	2017-02-21 17:49:57.775717
3722	139	3312	Card	\N	\N	tags	2017-02-21 17:50:34.3207
3723	63	3312	Card	\N	\N	tags	2017-02-21 17:50:34.32683
3725	140	3311	Card	\N	\N	tags	2017-02-21 17:50:58.104057
3726	63	3311	Card	\N	\N	tags	2017-02-21 17:50:58.109296
3728	141	3301	Card	\N	\N	tags	2017-02-22 07:58:13.453797
3729	49	3301	Card	\N	\N	tags	2017-02-22 07:58:13.463827
3730	142	3303	Card	\N	\N	tags	2017-02-22 07:58:32.391837
3731	49	3303	Card	\N	\N	tags	2017-02-22 07:58:32.398719
3732	143	3304	Card	\N	\N	tags	2017-02-22 07:58:53.655742
3733	49	3304	Card	\N	\N	tags	2017-02-22 07:58:53.662458
3734	60	3460	Card	\N	\N	tags	2017-02-22 07:59:33.465757
3735	49	3460	Card	\N	\N	tags	2017-02-22 07:59:33.473955
3736	60	3459	Card	\N	\N	tags	2017-02-22 07:59:52.235137
3737	49	3459	Card	\N	\N	tags	2017-02-22 07:59:52.24174
3738	60	3456	Card	\N	\N	tags	2017-02-22 08:00:10.740034
3739	49	3456	Card	\N	\N	tags	2017-02-22 08:00:10.746556
3740	60	3458	Card	\N	\N	tags	2017-02-22 08:00:28.252601
3741	49	3458	Card	\N	\N	tags	2017-02-22 08:00:28.258507
3742	60	3455	Card	\N	\N	tags	2017-02-22 08:00:48.765083
3743	49	3455	Card	\N	\N	tags	2017-02-22 08:00:48.772628
3744	48	3453	Card	\N	\N	tags	2017-02-22 08:01:05.832476
3745	49	3453	Card	\N	\N	tags	2017-02-22 08:01:05.836894
3746	61	3451	Card	\N	\N	tags	2017-02-22 08:01:36.536096
3747	62	3451	Card	\N	\N	tags	2017-02-22 08:01:36.542652
3748	49	3451	Card	\N	\N	tags	2017-02-22 08:01:36.548829
3749	55	3450	Card	\N	\N	tags	2017-02-23 04:10:03.087996
3750	53	3450	Card	\N	\N	tags	2017-02-23 04:10:03.101497
3751	49	3450	Card	\N	\N	tags	2017-02-23 04:10:03.108102
3752	55	3449	Card	\N	\N	tags	2017-02-23 04:10:23.407671
3753	53	3449	Card	\N	\N	tags	2017-02-23 04:10:23.414143
3754	49	3449	Card	\N	\N	tags	2017-02-23 04:10:23.420414
3755	144	3406	Card	\N	\N	tags	2017-02-23 04:11:24.090361
3756	47	3406	Card	\N	\N	tags	2017-02-23 04:11:24.09783
3757	49	3406	Card	\N	\N	tags	2017-02-23 04:11:24.104387
3758	145	3363	Card	\N	\N	tags	2017-02-23 04:11:57.805505
3759	62	3363	Card	\N	\N	tags	2017-02-23 04:11:57.810437
3760	49	3363	Card	\N	\N	tags	2017-02-23 04:11:57.815036
3761	62	3362	Card	\N	\N	tags	2017-02-23 04:12:39.163246
3762	146	3362	Card	\N	\N	tags	2017-02-23 04:12:39.168372
3763	49	3362	Card	\N	\N	tags	2017-02-23 04:12:39.17399
3764	147	3361	Card	\N	\N	tags	2017-02-23 04:13:09.005422
3765	62	3361	Card	\N	\N	tags	2017-02-23 04:13:09.014808
3766	49	3361	Card	\N	\N	tags	2017-02-23 04:13:09.022243
3767	148	3359	Card	\N	\N	tags	2017-02-23 04:13:31.441657
3768	62	3359	Card	\N	\N	tags	2017-02-23 04:13:31.448128
3769	49	3359	Card	\N	\N	tags	2017-02-23 04:13:31.454241
3770	149	3360	Card	\N	\N	tags	2017-02-23 04:14:00.89359
3771	62	3360	Card	\N	\N	tags	2017-02-23 04:14:00.899126
3772	49	3360	Card	\N	\N	tags	2017-02-23 04:14:00.904225
3773	61	3358	Card	\N	\N	tags	2017-02-23 04:14:21.290961
3774	62	3358	Card	\N	\N	tags	2017-02-23 04:14:21.298588
3775	49	3358	Card	\N	\N	tags	2017-02-23 04:14:21.306159
3776	150	3357	Card	\N	\N	tags	2017-02-23 04:14:46.624086
3777	53	3357	Card	\N	\N	tags	2017-02-23 04:14:46.630141
3778	49	3357	Card	\N	\N	tags	2017-02-23 04:14:46.636176
3779	151	3356	Card	\N	\N	tags	2017-02-23 04:15:19.796999
3780	53	3356	Card	\N	\N	tags	2017-02-23 04:15:19.803522
3781	49	3356	Card	\N	\N	tags	2017-02-23 04:15:19.809865
3782	65	3350	Card	\N	\N	tags	2017-02-23 04:17:16.263256
3783	63	3350	Card	\N	\N	tags	2017-02-23 04:17:16.269792
3785	65	3349	Card	\N	\N	tags	2017-02-23 04:17:32.431204
3786	63	3349	Card	\N	\N	tags	2017-02-23 04:17:32.437751
3787	65	3348	Card	\N	\N	tags	2017-02-23 04:17:49.124421
3788	63	3348	Card	\N	\N	tags	2017-02-23 04:17:49.130634
3789	93	3448	Card	\N	\N	tags	2017-02-23 04:26:56.398602
3790	32	3448	Card	\N	\N	tags	2017-02-23 04:26:56.407285
3791	93	3447	Card	\N	\N	tags	2017-02-23 04:27:09.7352
3792	32	3447	Card	\N	\N	tags	2017-02-23 04:27:09.74203
3793	92	3446	Card	\N	\N	tags	2017-02-23 04:27:26.256687
3794	32	3446	Card	\N	\N	tags	2017-02-23 04:27:26.263121
3795	92	3445	Card	\N	\N	tags	2017-02-23 04:27:41.088297
3796	32	3445	Card	\N	\N	tags	2017-02-23 04:27:41.096329
3797	22	3443	Card	\N	\N	tags	2017-02-23 04:27:56.702938
3798	32	3443	Card	\N	\N	tags	2017-02-23 04:27:56.710652
3799	92	3444	Card	\N	\N	tags	2017-02-23 04:28:12.98843
3800	32	3444	Card	\N	\N	tags	2017-02-23 04:28:12.993511
3801	22	3442	Card	\N	\N	tags	2017-02-23 04:28:27.72379
3802	32	3442	Card	\N	\N	tags	2017-02-23 04:28:27.730186
3803	24	3440	Card	\N	\N	tags	2017-02-23 04:28:42.736546
3804	32	3440	Card	\N	\N	tags	2017-02-23 04:28:42.742256
3805	24	3439	Card	\N	\N	tags	2017-02-23 04:28:54.926501
3806	32	3439	Card	\N	\N	tags	2017-02-23 04:28:54.931778
3807	95	3436	Card	\N	\N	tags	2017-02-23 04:29:07.229919
3808	32	3436	Card	\N	\N	tags	2017-02-23 04:29:07.234756
3809	152	3434	Card	\N	\N	tags	2017-02-23 04:29:22.302524
3810	32	3434	Card	\N	\N	tags	2017-02-23 04:29:22.308922
3811	95	3435	Card	\N	\N	tags	2017-02-23 04:29:41.69871
3812	32	3435	Card	\N	\N	tags	2017-02-23 04:29:41.704832
3813	153	3310	Card	\N	\N	tags	2017-02-23 04:29:57.093299
3814	32	3310	Card	\N	\N	tags	2017-02-23 04:29:57.100224
3815	154	3307	Card	\N	\N	tags	2017-02-23 04:30:13.756461
3816	32	3307	Card	\N	\N	tags	2017-02-23 04:30:13.762188
3817	155	3306	Card	\N	\N	tags	2017-02-23 04:30:31.849656
3818	32	3306	Card	\N	\N	tags	2017-02-23 04:30:31.855408
3819	95	3305	Card	\N	\N	tags	2017-02-23 04:30:46.021515
3820	32	3305	Card	\N	\N	tags	2017-02-23 04:30:46.027486
3821	156	3464	Card	\N	\N	tags	2017-02-23 04:34:04.562597
3822	49	3464	Card	\N	\N	tags	2017-02-23 04:34:04.572081
3823	156	3463	Card	\N	\N	tags	2017-02-23 04:34:23.192607
3824	49	3463	Card	\N	\N	tags	2017-02-23 04:34:23.198652
3825	156	3461	Card	\N	\N	tags	2017-02-23 04:34:40.064743
3826	49	3461	Card	\N	\N	tags	2017-02-23 04:34:40.071376
3827	74	3416	Card	\N	\N	tags	2017-02-23 04:34:52.645682
3828	49	3416	Card	\N	\N	tags	2017-02-23 04:34:52.653229
3829	156	3462	Card	\N	\N	tags	2017-02-23 04:35:09.273205
3830	49	3462	Card	\N	\N	tags	2017-02-23 04:35:09.279235
3831	156	3417	Card	\N	\N	tags	2017-02-23 04:35:24.580973
3832	49	3417	Card	\N	\N	tags	2017-02-23 04:35:24.589856
3833	74	3415	Card	\N	\N	tags	2017-02-23 04:35:38.902212
3834	49	3415	Card	\N	\N	tags	2017-02-23 04:35:38.909042
3835	74	3414	Card	\N	\N	tags	2017-02-23 04:36:00.059632
3836	49	3414	Card	\N	\N	tags	2017-02-23 04:36:00.064721
3837	156	3412	Card	\N	\N	tags	2017-02-23 04:36:13.650694
3838	49	3412	Card	\N	\N	tags	2017-02-23 04:36:13.660336
3839	74	3413	Card	\N	\N	tags	2017-02-23 04:36:28.742635
3840	49	3413	Card	\N	\N	tags	2017-02-23 04:36:28.749246
3841	156	3411	Card	\N	\N	tags	2017-02-23 04:36:42.075544
3842	49	3411	Card	\N	\N	tags	2017-02-23 04:36:42.082124
3843	156	3410	Card	\N	\N	tags	2017-02-23 04:36:56.345205
3844	49	3410	Card	\N	\N	tags	2017-02-23 04:36:56.355004
3845	156	3408	Card	\N	\N	tags	2017-02-23 04:37:07.517593
3846	49	3408	Card	\N	\N	tags	2017-02-23 04:37:07.524969
3847	156	3409	Card	\N	\N	tags	2017-02-23 04:37:19.022644
3848	49	3409	Card	\N	\N	tags	2017-02-23 04:37:19.028048
3849	156	3407	Card	\N	\N	tags	2017-02-23 04:37:32.02533
3850	49	3407	Card	\N	\N	tags	2017-02-23 04:37:32.031744
3851	69	3405	Card	\N	\N	tags	2017-02-23 04:37:47.882754
3852	49	3405	Card	\N	\N	tags	2017-02-23 04:37:47.888802
3853	156	3404	Card	\N	\N	tags	2017-02-23 04:38:04.681674
3854	49	3404	Card	\N	\N	tags	2017-02-23 04:38:04.689736
3855	156	3403	Card	\N	\N	tags	2017-02-23 04:38:16.374221
3856	49	3403	Card	\N	\N	tags	2017-02-23 04:38:16.380291
3857	156	3402	Card	\N	\N	tags	2017-02-23 04:38:29.475164
3858	49	3402	Card	\N	\N	tags	2017-02-23 04:38:29.48203
3859	156	3401	Card	\N	\N	tags	2017-02-23 04:38:41.528292
3860	49	3401	Card	\N	\N	tags	2017-02-23 04:38:41.535992
3861	156	3400	Card	\N	\N	tags	2017-02-23 04:38:54.482019
3862	49	3400	Card	\N	\N	tags	2017-02-23 04:38:54.486803
3863	156	3399	Card	\N	\N	tags	2017-02-23 04:39:08.555404
3864	49	3399	Card	\N	\N	tags	2017-02-23 04:39:08.561667
3865	156	3397	Card	\N	\N	tags	2017-02-23 04:39:19.562596
3866	49	3397	Card	\N	\N	tags	2017-02-23 04:39:19.56944
3867	156	3398	Card	\N	\N	tags	2017-02-23 04:39:33.231083
3868	49	3398	Card	\N	\N	tags	2017-02-23 04:39:33.237605
3869	110	3364	Card	\N	\N	tags	2017-02-23 04:40:05.662473
3870	156	3364	Card	\N	\N	tags	2017-02-23 04:40:05.669003
3871	49	3364	Card	\N	\N	tags	2017-02-23 04:40:05.674978
3872	75	3365	Card	\N	\N	tags	2017-02-23 04:40:16.402488
3873	49	3365	Card	\N	\N	tags	2017-02-23 04:40:16.408925
3874	67	3367	Card	\N	\N	tags	2017-02-23 04:40:34.559856
3875	156	3367	Card	\N	\N	tags	2017-02-23 04:40:34.568524
3876	49	3367	Card	\N	\N	tags	2017-02-23 04:40:34.574953
3877	74	3366	Card	\N	\N	tags	2017-02-23 04:40:48.201234
3878	49	3366	Card	\N	\N	tags	2017-02-23 04:40:48.20673
3879	68	3368	Card	\N	\N	tags	2017-02-23 04:41:05.627713
3880	49	3368	Card	\N	\N	tags	2017-02-23 04:41:05.634214
3881	157	3369	Card	\N	\N	tags	2017-02-23 04:41:23.615997
3882	156	3369	Card	\N	\N	tags	2017-02-23 04:41:23.625428
3883	49	3369	Card	\N	\N	tags	2017-02-23 04:41:23.63396
3884	156	3377	Card	\N	\N	tags	2017-02-23 04:41:35.969664
3885	49	3377	Card	\N	\N	tags	2017-02-23 04:41:35.976663
3886	69	3370	Card	\N	\N	tags	2017-02-23 04:41:54.233492
3887	49	3370	Card	\N	\N	tags	2017-02-23 04:41:54.23975
3888	60	3378	Card	\N	\N	tags	2017-02-23 04:42:12.387071
3889	49	3378	Card	\N	\N	tags	2017-02-23 04:42:12.393654
3890	38	3473	Card	\N	\N	tags	2017-03-02 13:39:23.953993
3891	23	3473	Card	\N	\N	tags	2017-03-02 13:39:23.966905
3892	38	3472	Card	\N	\N	tags	2017-03-02 13:39:44.8682
3893	31	3472	Card	\N	\N	tags	2017-03-02 13:39:44.876336
3894	38	3471	Card	\N	\N	tags	2017-03-02 13:40:01.170304
3895	38	3277	Card	\N	\N	tags	2017-03-02 13:42:52.602081
3896	38	3470	Card	\N	\N	tags	2017-03-02 13:43:08.695843
3897	127	3286	Card	\N	\N	tags	2017-03-03 09:05:26.323595
3898	127	3287	Card	\N	\N	tags	2017-03-03 09:06:00.685509
3899	14	3065	Card	\N	\N	tags	2017-03-03 09:06:44.714367
3900	13	3376	Card	\N	\N	tags	2017-03-03 09:08:17.844993
3901	76	3474	Card	\N	\N	tags	2017-06-05 11:08:16.773665
3902	23	3474	Card	\N	\N	tags	2017-06-05 11:08:16.793474
3903	78	3474	Card	\N	\N	tags	2017-06-05 11:08:16.798444
3904	76	3475	Card	\N	\N	tags	2017-06-05 11:08:44.738944
3908	98	3347	Card	\N	\N	tags	2017-06-05 12:27:47.975559
3909	32	3347	Card	\N	\N	tags	2017-06-05 12:27:47.983068
3910	136	3347	Card	\N	\N	tags	2017-06-05 12:27:47.988544
3911	108	3346	Card	\N	\N	tags	2017-06-05 12:29:10.990557
3912	112	3346	Card	\N	\N	tags	2017-06-05 12:29:10.996299
3913	112	3345	Card	\N	\N	tags	2017-06-05 12:31:53.503894
3914	108	3345	Card	\N	\N	tags	2017-06-05 12:31:53.51085
3915	11	3284	Card	\N	\N	tags	2017-06-06 10:42:58.707081
3916	111	3283	Card	\N	\N	tags	2017-06-06 10:44:09.409132
3917	108	3283	Card	\N	\N	tags	2017-06-06 10:44:09.415947
3918	11	3283	Card	\N	\N	tags	2017-06-06 10:44:09.421564
3919	108	3282	Card	\N	\N	tags	2017-06-06 11:57:30.871537
3920	11	3282	Card	\N	\N	tags	2017-06-06 11:57:30.881036
3921	159	3280	Card	\N	\N	tags	2017-06-06 11:58:30.783361
3922	11	3280	Card	\N	\N	tags	2017-06-06 11:58:30.787146
3923	160	3282	Card	\N	\N	tags	2017-06-06 11:58:41.911196
3924	108	3260	Card	\N	\N	tags	2017-06-06 12:00:11.930929
3925	161	3260	Card	\N	\N	tags	2017-06-06 12:00:11.936509
3926	109	3258	Card	\N	\N	tags	2017-06-06 12:02:30.896819
3927	108	3258	Card	\N	\N	tags	2017-06-06 12:02:30.904512
3928	98	3335	Card	\N	\N	tags	2017-06-06 12:07:10.766262
3929	112	3344	Card	\N	\N	tags	2017-06-07 07:20:44.546334
3930	108	3344	Card	\N	\N	tags	2017-06-07 07:20:44.556648
3931	112	3343	Card	\N	\N	tags	2017-06-07 07:23:29.685552
3932	108	3343	Card	\N	\N	tags	2017-06-07 07:23:29.693823
3933	98	3338	Card	\N	\N	tags	2017-06-07 07:24:57.616903
3934	108	3338	Card	\N	\N	tags	2017-06-07 07:24:57.623861
3935	97	3337	Card	\N	\N	tags	2017-06-07 07:25:28.402458
3936	98	3337	Card	\N	\N	tags	2017-06-07 07:25:28.408127
3937	108	3337	Card	\N	\N	tags	2017-06-07 07:25:28.413465
3938	98	3336	Card	\N	\N	tags	2017-06-07 07:26:58.319142
3939	108	3336	Card	\N	\N	tags	2017-06-07 07:26:58.323959
3940	162	3465	Card	\N	\N	tags	2017-06-07 08:05:15.435527
3941	45	3465	Card	\N	\N	tags	2017-06-07 08:05:15.441306
3942	33	3465	Card	\N	\N	tags	2017-06-07 08:05:15.447083
3943	27	3466	Card	\N	\N	tags	2017-06-07 08:06:40.156345
3944	33	3466	Card	\N	\N	tags	2017-06-07 08:06:40.161911
3945	45	3466	Card	\N	\N	tags	2017-06-07 08:06:40.166027
3946	33	3109	Card	\N	\N	tags	2017-06-07 08:07:54.421586
3947	45	3109	Card	\N	\N	tags	2017-06-07 08:07:54.429256
3948	27	3469	Card	\N	\N	tags	2017-06-07 08:09:13.084636
3949	163	3469	Card	\N	\N	tags	2017-06-07 08:09:13.09179
3950	33	3469	Card	\N	\N	tags	2017-06-07 08:09:13.097729
3951	45	3469	Card	\N	\N	tags	2017-06-07 08:09:13.103764
3952	163	3272	Card	\N	\N	tags	2017-06-07 08:10:39.074233
3953	33	3272	Card	\N	\N	tags	2017-06-07 08:10:39.079249
3954	45	3272	Card	\N	\N	tags	2017-06-07 08:10:39.083805
3955	27	3272	Card	\N	\N	tags	2017-06-07 08:10:39.088076
3956	27	3273	Card	\N	\N	tags	2017-06-07 08:11:33.63123
3957	33	3273	Card	\N	\N	tags	2017-06-07 08:11:33.636612
3958	45	3273	Card	\N	\N	tags	2017-06-07 08:11:33.641963
3959	45	3468	Card	\N	\N	tags	2017-06-07 08:12:15.157526
3960	33	3468	Card	\N	\N	tags	2017-06-07 08:12:15.167663
3961	27	3467	Card	\N	\N	tags	2017-06-07 08:13:12.937431
3962	45	3467	Card	\N	\N	tags	2017-06-07 08:13:12.945138
3963	33	3467	Card	\N	\N	tags	2017-06-07 08:13:12.951047
3964	32	3441	Card	\N	\N	tags	2017-06-08 13:12:55.575805
3965	19	3441	Card	\N	\N	tags	2017-06-08 13:12:55.58592
3966	164	3438	Card	\N	\N	tags	2017-06-08 13:17:12.182337
3967	32	3438	Card	\N	\N	tags	2017-06-08 13:17:12.188766
3968	95	3437	Card	\N	\N	tags	2017-06-08 13:18:37.12508
3969	32	3437	Card	\N	\N	tags	2017-06-08 13:18:37.131303
3970	165	3309	Card	\N	\N	tags	2017-06-09 12:07:19.17692
3971	32	3309	Card	\N	\N	tags	2017-06-09 12:07:19.182515
3972	79	3476	Card	\N	\N	tags	2017-07-05 12:06:24.359729
3973	166	3476	Card	\N	\N	tags	2017-07-05 12:06:24.367431
3974	66	3477	Card	\N	\N	tags	2017-07-05 12:12:12.524495
3975	79	3477	Card	\N	\N	tags	2017-07-05 12:12:12.529767
3976	80	3477	Card	\N	\N	tags	2017-07-05 12:12:12.533449
3977	108	3478	Card	\N	\N	tags	2017-07-18 12:57:25.638322
3978	167	3479	Card	\N	\N	tags	2017-07-18 13:04:38.749563
3979	108	3479	Card	\N	\N	tags	2017-07-18 13:04:38.755208
3980	11	3479	Card	\N	\N	tags	2017-07-18 13:04:38.759925
3981	168	3481	Card	\N	\N	tags	2017-11-21 13:04:25.085769
3982	35	3481	Card	\N	\N	tags	2017-11-21 13:04:25.097775
3983	36	3481	Card	\N	\N	tags	2017-11-21 13:04:25.103337
\.


--
-- Name: taggings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.taggings_id_seq', 3983, true);


--
-- Data for Name: tags; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.tags (id, name, taggings_count) FROM stdin;
95	томат	12
19	овощи	83
164	артишоки	1
35	хлеб	19
165	абрикос	1
66	приправы	189
79	специи	50
108	морепродукты	22
11	деликатесы	84
36	булочки	10
34	Бельгия	8
43	посыпка	8
44	сухие смеси	16
46	пюре	8
51	соль	8
52	крахмал	8
54	фритюр	16
56	подсолнечное	8
81	перец	16
82	специи в масле	8
83	ананас	8
84	фрукты	16
85	анчоус	8
87	горошек	8
88	груши	8
89	каперсы	8
90	корнишон	8
94	кокос	8
96	персик	8
99	фасоль	8
101	суши	16
102	икра	17
103	имбирь	8
104	кунжут	8
105	паста соевая	8
106	лапша	8
107	мучные изделия	32
75	хрен	8
67	горчица	8
114	филе	3
20	картофель	53
122	чай	1
92	маслины	11
15	птица	45
117	куры	1
74	уксус	19
47	соусы	55
68	кетчуп	15
116	мороженое сиропы	0
5	вырезка	42
113	осьминог	1
50	кофе	17
55	оливковое	10
86	артишок	15
48	сахар	9
123	крольчатина	1
1	мясо	407
119	странное	0
59	макароны	15
120	фри	2
30	кондитерские изделия	7
29	чизкейки	7
28	селезнев	7
12	полуфабрикаты	7
18	перепёлка	8
60	паста	61
58	савоярди	7
37	тесто	9
157	личи	1
158	кули	0
7	Мираторг	7
128	сосиски	2
57	мука	7
10	свинина	8
16	цыплята	25
3	ягнятина	31
14	мясные изделия	39
69	майонез	9
125	колбасные изделия	1
91	лук	7
93	оливки	10
121	халяль	0
124	диетическое	1
4	говядина	170
126	бекон	3
42	пломбир	7
129	колбаски	3
65	сыр	52
26	торт	84
130	гриль	4
2	баранина	63
31	шоколад	8
110	каракатица	2
21	McCain	53
40	весовое	14
71	соевый соус	7
17	утка	9
76	сироп	42
133	вафли	1
70	барбекю	7
100	японская кухня	81
135	круассаны	1
134	для бургеров	1
13	заморозка	56
9	телятина	8
72	бальзамический	14
8	стейк	9
131	заречное	0
61	рис	17
127	мрамор	5
64	молоко	58
137	сливки	4
140	маргарин	1
138	сметана	1
139	творог	1
53	масло	45
22	кукуруза	25
62	крупы	22
63	молочные продукты	91
24	грибы	26
109	кальмар	1
78	наполнитель	9
98	рыба	15
136	субпродукты	2
77	топинг	32
23	ягоды	34
111	краб	3
112	креветка	6
159	улитки	1
160	мидии	1
97	тунец	9
161	морской коктейль	1
27	торты	20
45	десерты	40
33	выпечка	36
141	чайный напиток	1
142	мёд	1
143	орехи	1
144	томатный	1
49	бакалея	207
145	чечевица	1
32	консервы	178
146	пшеница	1
147	киноа	1
166	кондитерские	1
148	горох	1
80	сухие специи	9
149	гречневая	1
167	гребешок	1
150	фундук	1
168	#тесто	1
156	соус	23
152	черри	1
153	манго	1
154	помидор	1
38	мороженое	50
155	тамаринд	1
151	виноград	1
162	макаруны	1
163	чизкейк	2
\.


--
-- Name: tags_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.tags_id_seq', 168, true);


--
-- Name: active_admin_comments_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.active_admin_comments
    ADD CONSTRAINT active_admin_comments_pkey PRIMARY KEY (id);


--
-- Name: admin_users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.admin_users
    ADD CONSTRAINT admin_users_pkey PRIMARY KEY (id);


--
-- Name: assets_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.assets
    ADD CONSTRAINT assets_pkey PRIMARY KEY (id);


--
-- Name: card_items_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.card_items
    ADD CONSTRAINT card_items_pkey PRIMARY KEY (id);


--
-- Name: cards_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.cards
    ADD CONSTRAINT cards_pkey PRIMARY KEY (id);


--
-- Name: categories_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: clients_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.clients
    ADD CONSTRAINT clients_pkey PRIMARY KEY (id);


--
-- Name: delivering_cities_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.delivering_cities
    ADD CONSTRAINT delivering_cities_pkey PRIMARY KEY (id);


--
-- Name: images_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.images
    ADD CONSTRAINT images_pkey PRIMARY KEY (id);


--
-- Name: order_items_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.order_items
    ADD CONSTRAINT order_items_pkey PRIMARY KEY (id);


--
-- Name: orders_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (id);


--
-- Name: post_category_relations_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.post_category_relations
    ADD CONSTRAINT post_category_relations_pkey PRIMARY KEY (id);


--
-- Name: post_images_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.post_images
    ADD CONSTRAINT post_images_pkey PRIMARY KEY (id);


--
-- Name: posts_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.posts
    ADD CONSTRAINT posts_pkey PRIMARY KEY (id);


--
-- Name: promo_cards_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.promo_cards
    ADD CONSTRAINT promo_cards_pkey PRIMARY KEY (id);


--
-- Name: taggings_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.taggings
    ADD CONSTRAINT taggings_pkey PRIMARY KEY (id);


--
-- Name: tags_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (id);


--
-- Name: index_active_admin_comments_on_author_type_and_author_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_active_admin_comments_on_author_type_and_author_id ON public.active_admin_comments USING btree (author_type, author_id);


--
-- Name: index_active_admin_comments_on_namespace; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_active_admin_comments_on_namespace ON public.active_admin_comments USING btree (namespace);


--
-- Name: index_active_admin_comments_on_resource_type_and_resource_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_active_admin_comments_on_resource_type_and_resource_id ON public.active_admin_comments USING btree (resource_type, resource_id);


--
-- Name: index_admin_users_on_email; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_admin_users_on_email ON public.admin_users USING btree (email);


--
-- Name: index_admin_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_admin_users_on_reset_password_token ON public.admin_users USING btree (reset_password_token);


--
-- Name: index_cards_on_category_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_cards_on_category_id ON public.cards USING btree (category_id);


--
-- Name: index_categories_on_lft; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_categories_on_lft ON public.categories USING btree (lft);


--
-- Name: index_categories_on_parent_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_categories_on_parent_id ON public.categories USING btree (parent_id);


--
-- Name: index_categories_on_rgt; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_categories_on_rgt ON public.categories USING btree (rgt);


--
-- Name: index_categories_on_slug; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_categories_on_slug ON public.categories USING btree (slug);


--
-- Name: index_post_category_relations_on_category_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_post_category_relations_on_category_id ON public.post_category_relations USING btree (category_id);


--
-- Name: index_post_category_relations_on_post_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_post_category_relations_on_post_id ON public.post_category_relations USING btree (post_id);


--
-- Name: index_post_category_relations_on_post_id_and_category_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_post_category_relations_on_post_id_and_category_id ON public.post_category_relations USING btree (post_id, category_id);


--
-- Name: index_taggings_on_taggable_id_and_taggable_type_and_context; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_taggings_on_taggable_id_and_taggable_type_and_context ON public.taggings USING btree (taggable_id, taggable_type, context);


--
-- Name: index_tags_on_name; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_tags_on_name ON public.tags USING btree (name);


--
-- Name: taggings_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX taggings_idx ON public.taggings USING btree (tag_id, taggable_id, taggable_type, context, tagger_id, tagger_type);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON public.schema_migrations USING btree (version);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

