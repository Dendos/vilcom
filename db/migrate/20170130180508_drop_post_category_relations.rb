class DropPostCategoryRelations < ActiveRecord::Migration
  def change
    drop_table :post_category_relations if (table_exists? :post_category_relations)
    drop_table :post_categories if (table_exists? :post_categories)
  end
end
