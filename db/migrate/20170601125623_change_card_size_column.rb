class ChangeCardSizeColumn < ActiveRecord::Migration
  def self.up
    remove_column :cards, :size
    add_column :cards, :size_cd, :integer, default: 0
  end

  def self.down
    remove_column :cards, :size_cd
    add_column :cards, :size, :string
  end
end
