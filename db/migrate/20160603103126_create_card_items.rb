class CreateCardItems < ActiveRecord::Migration
  def change
    create_table :card_items do |t|
      t.integer :card_id
      t.string  :name
      t.string  :price
      t.string  :meta
      t.timestamps null: false
    end
  end
end
