class AddPriorityToCards < ActiveRecord::Migration
  def change
    add_column :cards, :priority, :integer, default: 0
    add_column :card_items, :priority, :integer, default: 0
  end
end
