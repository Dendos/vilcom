class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.string :first_text_section
      t.string :second_text_section
      t.timestamps null: false
    end
  end
end
