class VendorCodeToCardItems < ActiveRecord::Migration
  def change
    add_column :card_items, :vendor_code, :string
  end
end
