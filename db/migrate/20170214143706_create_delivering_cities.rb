class CreateDeliveringCities < ActiveRecord::Migration
  def change
    create_table :delivering_cities do |t|
      t.string :name
      t.string :estimated_delivery_time
      t.timestamps null: false
    end
  end
end
