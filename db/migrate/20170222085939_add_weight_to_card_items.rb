class AddWeightToCardItems < ActiveRecord::Migration
  def change
    add_column :card_items, :weight, :string
  end
end
