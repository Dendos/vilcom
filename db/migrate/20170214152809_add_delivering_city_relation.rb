class AddDeliveringCityRelation < ActiveRecord::Migration
  def change
    remove_column :orders, :city
    add_column :orders, :delivering_city_id, :integer
  end
end
