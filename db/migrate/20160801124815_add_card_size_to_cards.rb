class AddCardSizeToCards < ActiveRecord::Migration
  def change
    add_column :cards, :size, :string, default: 'small'
  end
end
