class AddImageStyleToCard < ActiveRecord::Migration
  def change
    add_column :cards, :image_style, :boolean, default: false
  end
end
