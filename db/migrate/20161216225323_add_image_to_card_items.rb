class AddImageToCardItems < ActiveRecord::Migration
  def change
    add_column :card_items, :image, :string
    add_column :card_items, :temp_key, :string
  end
end
