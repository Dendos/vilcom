class ChangeDefaultValueForCardItemsWeight < ActiveRecord::Migration

  def change
    remove_column :card_items, :weight, :string
    add_column    :card_items, :weight, :float, default: 1
  end

end
