class CreatePromoCards < ActiveRecord::Migration
  def change
    create_table :promo_cards do |t|

      t.integer :category_id
      t.string  :title
      t.string  :list
      t.string  :image
      t.string  :card_class

      t.timestamps null: false
    end
  end
end
