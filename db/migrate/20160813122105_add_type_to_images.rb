class AddTypeToImages < ActiveRecord::Migration
  def change
    add_column :images, :image_type, :string, default: 'image'
  end
end
