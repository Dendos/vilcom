class CreateCards < ActiveRecord::Migration
  def change
    create_table :cards do |t|

      t.integer  "category_id", index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
