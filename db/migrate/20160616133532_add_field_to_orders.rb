class AddFieldToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :city, :string
    add_column :orders, :address, :string
    add_column :orders, :contacts, :string
    add_column :orders, :phone, :string
    add_column :orders, :comments, :string
  end
end
