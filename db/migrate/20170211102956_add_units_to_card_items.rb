class AddUnitsToCardItems < ActiveRecord::Migration
  def change
    add_column :card_items, :units, :string
  end
end
