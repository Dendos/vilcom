class AddRememberTokenToClient < ActiveRecord::Migration
  def change
    add_column :clients, :remember_token, :string
  end
end
