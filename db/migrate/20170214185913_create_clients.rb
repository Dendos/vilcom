class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :name
      t.string :tel
      t.string :password
      t.string :discount
      t.string :address
      t.integer :delivery_city_id
      t.timestamps null: false
    end
  end
end
