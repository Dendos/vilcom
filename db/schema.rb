# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170601125623) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "assets", force: :cascade do |t|
    t.string   "storage_uid"
    t.string   "storage_name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "storage_width"
    t.integer  "storage_height"
    t.float    "storage_aspect_ratio"
    t.integer  "storage_depth"
    t.string   "storage_format"
    t.string   "storage_mime_type"
    t.string   "storage_size"
  end

  create_table "card_items", force: :cascade do |t|
    t.integer  "card_id"
    t.string   "name"
    t.string   "price"
    t.string   "meta"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "priority",    default: 0
    t.string   "image"
    t.string   "temp_key"
    t.string   "units"
    t.string   "vendor_code"
    t.float    "weight",      default: 1.0
  end

  create_table "cards", force: :cascade do |t|
    t.integer  "category_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "image"
    t.string   "title"
    t.string   "card_logo"
    t.string   "temp_key"
    t.integer  "priority",    default: 0
    t.boolean  "image_style", default: false
    t.integer  "size_cd",     default: 0
  end

  add_index "cards", ["category_id"], name: "index_cards_on_category_id", using: :btree

  create_table "categories", force: :cascade do |t|
    t.string  "name"
    t.string  "slug"
    t.integer "priority"
    t.integer "parent_id"
    t.integer "lft",                        null: false
    t.integer "rgt",                        null: false
    t.integer "depth",          default: 0, null: false
    t.integer "children_count", default: 0, null: false
  end

  add_index "categories", ["lft"], name: "index_categories_on_lft", using: :btree
  add_index "categories", ["parent_id"], name: "index_categories_on_parent_id", using: :btree
  add_index "categories", ["rgt"], name: "index_categories_on_rgt", using: :btree
  add_index "categories", ["slug"], name: "index_categories_on_slug", unique: true, using: :btree

  create_table "clients", force: :cascade do |t|
    t.string   "name"
    t.string   "tel"
    t.string   "password"
    t.string   "discount"
    t.string   "address"
    t.integer  "delivery_city_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "remember_token"
  end

  create_table "delivering_cities", force: :cascade do |t|
    t.string   "name"
    t.string   "estimated_delivery_time"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "images", force: :cascade do |t|
    t.string   "file"
    t.string   "temp_key"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "image_type", default: "image"
  end

  create_table "order_items", force: :cascade do |t|
    t.integer "order_id"
    t.integer "card_item_id"
    t.string  "count"
  end

  create_table "orders", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "address"
    t.string   "contacts"
    t.string   "phone"
    t.string   "comments"
    t.integer  "delivering_city_id"
  end

  create_table "post_category_relations", force: :cascade do |t|
    t.integer  "post_id"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "post_category_relations", ["category_id"], name: "index_post_category_relations_on_category_id", using: :btree
  add_index "post_category_relations", ["post_id", "category_id"], name: "index_post_category_relations_on_post_id_and_category_id", unique: true, using: :btree
  add_index "post_category_relations", ["post_id"], name: "index_post_category_relations_on_post_id", using: :btree

  create_table "post_images", force: :cascade do |t|
    t.integer  "post_id"
    t.string   "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "posts", force: :cascade do |t|
    t.string   "title"
    t.string   "first_text_section"
    t.string   "second_text_section"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "url"
  end

  create_table "promo_cards", force: :cascade do |t|
    t.integer  "category_id"
    t.string   "title"
    t.string   "list"
    t.string   "image"
    t.string   "card_class"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "taggings", force: :cascade do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       limit: 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true, using: :btree
  add_index "taggings", ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree

  create_table "tags", force: :cascade do |t|
    t.string  "name"
    t.integer "taggings_count", default: 0
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true, using: :btree

end
