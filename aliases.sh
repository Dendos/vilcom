# Alises
echo "alias vag='cd /vagrant'" >> ~/.bashrc
echo "alias zi='export ZEUSSOCK=/tmp/zeus.sock'" >> ~/.bashrc
echo "alias ok='ping google.com'" >> ~/.bashrc
echo "alias zc='zeus c'" >> ~/.bashrc
echo "alias zrr='zeus rake routes'" >> ~/.bashrc
echo "alias zrrg='zeus rake routes | grep '" >> ~/.bashrc
echo "alias zs='zeus s -b 0.0.0.0'" >> ~/.bashrc
