## Подготовка среды

### Mac OS

1. Установить [Homebrew](https://brew.sh):
    ```shell
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    ```

2. Установить [PostgreSQL](https://www.postgresql.org/):
    ```shell
    brew install postgresql
    ```

3. Установить [Node.js + NPM](https://nodejs.org/en/):
    ```shell
    brew install node
    ```

4. Уставновить [Bower](https://bower.io/):
    ```shell
    npm install -g bower
    ```

5. Установить [rbenv](https://github.com/rbenv/rbenv#readme):
    ```shell
    brew install rbenv
    ```

6. Установить [Ruby 2.3.1](https://github.com/rbenv/rbenv#readme):
    ```shell
    rbenv install 2.3.1
    ```

7. Установить [Bundler](https://bundler.io/):
    ```shell
    gem install bundler
    ```

## Как начать пользоваться

1. Перейти в папку проекта.

2. Установить зависимости, если это первый запуск проекта:
    ```shell
    bin/setup
    ```

2. Запустить сайт:
    ```shell
    bundle exec rails s
    ```

3. Сайт будет доступен в браузере по адресу [http://localhost:3000](http://localhost:3000).
