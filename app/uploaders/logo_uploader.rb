class LogoUploader < CommonUploader
  include CarrierWave::MiniMagick

  process :resize_to_limit => [100, nil]

end
