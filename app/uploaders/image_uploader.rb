class ImageUploader < CommonUploader
  include CarrierWave::MiniMagick

  process :resize_to_limit => [600, nil]

  # version :horizontal do
  #   process resize_to_fill: [300, 200]
  # end
  # version :vertical do
  #   process resize_to_fill: [300, 450]
  # end

  version :small do
    process resize_to_fill: [50, 50]
  end

end
