$ ->
  $('.phone-mask').inputmask
    mask: '8 (999) 999-99-99'
    placeholder: ''
    autoUnmask: true
    jitMasking: true
    showMaskOnHover: false
    showMaskOnFocus: false
