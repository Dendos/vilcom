$ ->
  $('.validate_phone').focus ->
    form = $(this).parents('form')
    form.removeClass('error no-phone')

  $('.validate_form').submit (e) ->
    console.log('form changed', $(this))
    submitBtn       = $(this).find('.validate_form_submit')
    phoneField      = $(this).find('.validate_phone')
    phoneValidation = if phoneField[0] then validatePhone(phoneField.val()) else true
    validator       = phoneValidation
    if validator
      $(this).removeClass('error no-phone')
    else
      e.preventDefault()
      $(this).addClass('error no-phone')

validatePhone = (phone) ->
  validator = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im
  if phone.match(validator)
    true
  else
    false