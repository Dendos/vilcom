$ ->
  $('.hover-slider').each ()->
    if $(this).find('.hover-slide').length > 0
      $(this).closest('.card-photo').addClass('hover-slider-cont')

  $(document).on 'mousemove','.hover-slider',(e)->
    img_count = $(this).find('.hover-slide').length
    cont_width = $(this).width()

    parentOffset = $(this).offset();    
    relX = Math.round(e.pageX - parentOffset.left);

    hover_step = Math.round(cont_width/img_count)
    img_index  = Math.ceil(relX/hover_step)
    $(this).find('.hover-slide').hide() 

    $('.card-item').removeClass('active')
    item_photo = $($(this).find('.hover-slide').get(img_index - 1))
    item_id = item_photo.data('id')
    $(".card-item#" + item_id + "").addClass('active')
    item_photo.show()

  $(document).on 'mouseleave','.hover-slider',(e)->
    $(this).find('.hover-slide').hide()
    $('.card-item').removeClass('active')

  # SHOW CARD ITEMS IMAGES ON CARD ITEMS HOVER
  $(document).on 'mousemove', '.card-item', ()->
    item_id = $(this).attr('id')
    item_image = $(".hover-slide[data-id=" + item_id + "]").show()

  $(document).on 'mouseleave', '.card-item', ()->
    item_id = $(this).attr('id')
    item_image = $(".hover-slide[data-id=" + item_id + "]").hide()