quick_editor = ()->
  _editor  = null
  _item    = null
  _get_form_path = null

  init = (el)->
    _editor = el

  open = (item,get_form_path, callback = null)->
    _item    = item
    _get_form_path = get_form_path
    show_item(callback)
    _editor.addClass 'active'

  close = (callback = null)->
    _item    = null
    _get_form_path = null
    _editor.removeClass 'active'
    empty_editor()
    do callback if callback

  ### P R I V A T E ###

  empty_editor = ()->
    _editor.empty()

  get_item_coordinates = ()->

  get_item_html = (callback)->

  show_item = (callback)->
    $.get _get_form_path, (response)->
      html = response.html
      _editor.append(html)
      offset = _item.offset()
      _editor.find(">").offset(offset)
      do callback if callback

  return api:
    init:  init
    open:  open
    close: close

window.quick_editor = quick_editor().api
