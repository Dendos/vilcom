$ ->
  $units = $('.item_units_field')
  $field = $('.item_weight_field')
  $(document).on 'mousemove', '.card-item ', (e)->
    $field = $(this).find('.item_units_field')
    # console.log($field)
    if $field.val() == 'kilo'
      $(this).find('.item_weight_field').show()
    else 
      $(this).find('.item_weight_field').hide()
  $(document).on 'change','.item_units_field',(e)->
    # console.log($(this).val())
    if $(this).val() == 'kilo'
      $(this).parent().parent().find('.item_weight_field').show()
    else 
      $(this).parent().parent().find('.item_weight_field').hide()
