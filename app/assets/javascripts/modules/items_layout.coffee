items_layout = ()->
  _grid  = null

  init = (container, item, sortBy)->
    # console.log('container, item, sortBy', [container, item, sortBy])
    _grid = $(container).isotope
      itemSelector: item
      sortBy: sortBy
      sortAscending: true
      masonry: 
        horizontalOrder: true
        columnWidth: 300
        gutter: 20
        isFitWidth: true
      getSortData:
        cardId: '[data-card-id]'
        priority: '[data-priority]'
  ### P R I V A T E ###

  get_layout = ()->
    return _grid

  return api:
    init:  init
    get_layout: get_layout

window.items_layout = items_layout().api
