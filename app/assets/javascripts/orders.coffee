# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->

  form = orders_store.get()
  if form
    $('#order_comments').val(form.comments)
    $('#order_city').val(form.city)
    $('#order_address').val(form.address)
    $('#order_contacts').val(form.contacts)
    $('#order_phone').val(form.phone)

  $('#text_area_comment').on 'change keyup paste', ()->
    $(this).val()
    obj = comments: $(this).val()
    orders_store.save(obj);

  $('#new_order').submit (e)->
    obj =
      comments: $('#order_comments').val()
      city: $( "input[name*='order[delivering_city_id]']._active" ).val()
      address: $('#order_address').val()
      contacts: $('#order_contacts').val()
      phone: $('#order_phone').val()
    $('#items_ids').val(items_store.get())
    orders_store.save(obj);
    items_store.clear()

  $('.new-order').click ->
    event.preventDefault();
    items = items_store.get();
    items = items.filter (x) -> x != ''
    $.ajax({
      url: "/orders/get_items",
      dataType: 'json'
      data: {items: items},
      success: (data)->
        window.location = data.location if  data.location
        # console.log data
      ,
      error: (data)->
        window.location.pathname = '/orders/new'
    });

  $('body').on 'click', '.close-row', ->
    parent = $(this).parent('.order-table-row')
    if !parent.hasClass('_removed')
      itemID = $(this).parent().find('.item').data('item-id').toString()
      parent.toggleClass '_removed'
      $(this).text('✓')
      items = items_store.get()
      index = items.indexOf(itemID)
      items.splice(index, 1)
      items_store.save(items)
      $('#items_ids').val(items_store.get())

      #ПЕРЕСЧЕТ ФОРМЫ
      parent = $(this).parents('.order-table-row')
      price = parent.find('.price').data('amount')
      count = parent.find('.item').val()
      amount = +$('.sum').text()
      discount = +$('.discount_amount').text()
      cost = price * count
      amount = amount - cost
      discount = amount/100 * discount
      $('.sum').text(amount.toFixed(2))
      $('.discount').text(discount.toFixed(2))
      final_amount = amount - discount
      $('.cost-final').text(final_amount.toFixed(2))


  $('body').on 'click', '.order-table-row._removed', ->
    itemID = $(this).find('.item').data('item-id').toString()
    $(this).toggleClass '_removed'
    $(this).find('.close-row').text('x')
    items = items_store.get()
    items.push(itemID)
    items_store.save(items)
    $('#items_ids').val(items_store.get())
    #ПЕРЕСЧЕТ ФОРМЫ
    parent = $(this)
    price = parent.find('.price').data('amount')
    count = parent.find('.item').val()
    amount = +$('.sum').text()
    discount = +$('.discount_amount').text()
    cost = price * count
    amount = amount + cost
    discount = amount/100 * discount
    $('.sum').text(amount.toFixed(2))
    $('.discount').text(discount.toFixed(2))
    final_amount = amount - discount
    $('.cost-final').text(final_amount.toFixed(2))




  $('.input-item input[type="radio"]').on 'click', ->
    $('.input-item label').removeClass('_active')
    $('.input-item input[type="radio"]').removeClass('_active')

    $(this).parent().find('label').addClass('_active')
    $(this).addClass('_active')

  #CHANGE ITEM COUNT
  $('.item_count_field').on 'change keyup paste', (e) ->
    # e.preventDefault()
    val = $(this).val()
    console.log('thisVAL', val)
    count = parseInt($(this).val())
    console.log('CHANGE COUNT', count)
    if count
      parent = $(this).parents('.order-table-row')
      price = get_price($(this))
      cost = parent.find('.cost')
      discount = +$('.discount_amount').text()
      amount = 0
      # console.log(price)
      cost.text(price * count)

      $('.cost').each (index,value)->
        amount += parseInt($(value).text().replace("≈", ""))

      discount = amount/100 * discount
      $('.sum').text(amount.toFixed(2))
      $('.discount').text(discount.toFixed(2))
      final_amount = amount - discount
      $('.cost-final').text(final_amount.toFixed(2))
      change_item_count(this)

get_price = (element) ->
  parent = $(element).parents('.order-table-row')
  weight = element.data('item-weight')
  console.log(weight)
  if weight > 0
    price = parseInt(parent.find('.price').text())
    price = price * weight
  else
    price = parseInt(parent.find('.price').text())
  return price
change_item_count = (input) ->
  id = $(input).data('item-id')
  $(".#{id}").val($(input).val())
