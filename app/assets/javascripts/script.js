var orderItemsCount = 0;
var orderSum = 0;

var orderItems = [];


ready = function() {
    get_checked_items();
    $(document).on('click', '.card-item', function(event) {
      event.preventDefault();
      var itemID = $(this).attr('id');
      if (itemID) {
        orderItemsCount = $('.child').length;
        var amount = $('.order-sum-value').data('amount');
        if (!amount) {
          amount = 0;
        }
        amount = parseFloat(amount);
        if (amount > 0) {
          orderSum = amount.toFixed(2);
        }
        vh = $(window).height() - 360;
        max_items = parseInt(vh/30);
        if (max_items == 0) {
          max_items = 1;
        }
        if (!$(this).hasClass('checked')) {
            orderItemsCount += 1;
            orderItems.push(itemID);
            if (orderItemsCount > max_items) {
              $('.order-items-count-wrapper').show();
              list = $('.order-items-wrapper');
              current_items_count = orderItemsCount - max_items;
              $('.order-items-count-wrapper').text('и еще ' + current_items_count + declOfNum(current_items_count, [' наименование', ' наименования', ' наименований']));
              var currentItemName = $(this).find('.item-name').text();
              list.prepend('<div class="order-list-item child" id="orders-list-' + itemID + '"><span class="js-list-item list-item-name">' + currentItemName + '</span></div>');
              items = items_store.get();
              items.push(itemID.split('-')[1]);
              items_store.save(items);
              $('.order-list-item').each(function(){ 
                index = $(this).index();
                if ( index > (max_items - 1) ){
                  $(this).hide();
                }
              })
            } else {
              $('.order-items-count-wrapper').hide();
              $('.order-info__title').show()
              $('.order-list-item.child').show();
              var currentSum = $(this).find('.item-price-value').data('amount');
              orderSum = parseFloat(orderSum)
              orderSum += parseFloat(currentSum);
              orderSum = parseFloat(orderSum);
              var currentItemName = $(this).find('.item-name').text();
              $('.order-items-wrapper').prepend('<div class="order-list-item child" id="orders-list-' + itemID + '"><span class="js-list-item list-item-name">' + currentItemName + '</span></div>');
              items = items_store.get();
              items.push(itemID.split('-')[1]);
              items_store.save(items);
            }
        } else {
          orderItemsCount -= 1;
          var currentSum = $(this).find('.item-price-value').data('amount');
          orderSum -= currentSum;
          if (orderItemsCount > max_items){
            $('.order-items-count-wrapper').show();
            items = items_store.get();
            items = items.filter(function(el){
              return el != itemID.split('-')[1]
            })
            items_store.save(items)
            $('#orders-list-' + itemID).remove();
            $('.order-list-item.child').eq(max_items - 1).show();
            current_items_count = orderItemsCount - max_items;
            $('.order-items-count-wrapper').text('и еще ' + current_items_count + declOfNum(current_items_count, [' наименование', ' наименования', ' наименований']));
            $('.order-list-item').each(function() { 
              index = $(this).index();
              if (index > (max_items - 1)) {
                $(this).hide();
              }
            });
          } else {
            $('.order-items-count-wrapper').hide();
            $('.order-list-item.child').show();
            $('#orders-list-' + itemID).remove();
            items = items_store.get();
            items = items.filter(function(el) {
              return el != itemID.split('-')[1]
            });
            items_store.save(items);
          }
        }
        $(this).toggleClass('checked');
        $('.order-sum-value').text(orderSum.toFixed(2));
        $('.order-sum-value').data('amount', orderSum.toFixed(2));
        if (orderItemsCount > 0) {
            $('.js-without-choosen').removeClass('active');
            $('.order-info').addClass('active');
        } else {
          $('.js-without-choosen').addClass('active');
          $('.order-info').removeClass('active');
        }
      }
    });

    $('.test-switch-class').click(function(){
      $('.order-box').toggleClass('wide');
    })
    

    $('body').on('click', '.order-list-item.child', function() {
      var orderItemsCount = $('.child').length,
          itemID = $(this).attr('id'),
          cardItems = $('.card-item');

      $(this).toggleClass('_removed');

      if ($(this).hasClass('_removed')) {
        orderItemsCount -= 1;
        items = items_store.get();
        items = items.filter(function(el){
          return el != itemID.split('-')[3]
        })
        items_store.save(items)

        cardItems.map(function(index, item) {
          var id = $(item).attr('id');
          if (itemID.split('-')[3] === id.split('-')[1]) {
            $('#'+id).removeClass('checked')
          }
        });
      }

      else {
        orderItemsCount += 1;
        orderItems.push(itemID);
        items = items_store.get();
        items.push(itemID.split('-')[3]);
        items_store.save(items);

        cardItems.map(function(index, item) {
          var id = $(item).attr('id');
          if (itemID.split('-')[3] === id.split('-')[1]) {
            $('#'+id).addClass('checked')
          }
        });
      }

    });


    $('.scroll-link').click(function(e) {
        e.preventDefault();

        var linkOffset = -40;
        if ($($.attr(this, 'href')).data('scroll-link-offset')) {
            linkOffset += $($.attr(this, 'href')).data('scroll-link-offset')
        };

        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top + linkOffset

        }, 500);
    });

    $('.promoblock').click(function(event) {
        window.location.href = $(this).find('a').attr('href');
    });
    $('.promoblock').hover(function() {
        $(this).find('a').addClass('hovered');
    }, function() {
        $(this).find('a').removeClass('hovered');
    });

};

function declOfNum(number, titles) {  
    cases = [2, 0, 1, 1, 1, 2];  
    return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];  
}

function get_sidebar(ids) {
  $.ajax({
    url: "/orders/get_sidebar",
    dataType: 'json',
    data: {
      items: ids
    },
    success: function(data) {
      if (data.html) {
        $('.order-items-wrapper').append(data.html);
        $('.js-without-choosen').removeClass('active');
        $('.order-sum-value').data('amount', data.amount.toFixed(2));
        $('.order-sum-value').text(data.amount.toFixed(2));
        $('.order-info').addClass('active');
      }
      vh = $(window).height() - 360;
      max_items = parseInt(vh / 30);
      orderItemsCount = $('.child').length;
      if (max_items === 0) {
        max_items = 1;
      }
      if (orderItemsCount > max_items) {
        $('.order-items-count-wrapper').show();
        list = $('.order-items-wrapper');
        current_items_count = orderItemsCount - max_items;
        $('.order-items-count-wrapper').text('и еще ' + current_items_count + declOfNum(current_items_count, [' наименование', ' наименования', ' наименований']));
        $('.order-list-item').each(function() {
          var index;
          index = $(this).index();
          console.log(index);
          if (index > max_items - 1) {
            $(this).hide();
          }
        });
      }
    },
    error: function(data){
      // console.log('error');
    }
  });
}

function get_checked_items() {
    items = items_store.get();
    $.each(items, function(index, value) {
        $('#item-' + value).addClass('checked');
        var itemID = $('#item-' + value).attr('id');
    });
    items = items.filter(function(x) {
      return x !== '';
    });
    // console.log(items);
    if (items.length > 0) get_sidebar(items);
}


$(document).ready(ready);
