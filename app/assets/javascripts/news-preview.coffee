$ ->
  $('.post-preview__image').each ->
    wrapper = $(this)
    img = $(this).find('img')
    if img.width() > img.height()
      wrapper.addClass 'horizontal'
    else
      wrapper.addClass 'vertical'
    return
