init_uploader = ()->
  $items_layout = items_layout.get_layout()
  new Awesomplete('input[data-multiple]',
    minChars: 1,
    autoFirst: true,
    filter: (text, input) ->
      Awesomplete.FILTER_CONTAINS text, input.match(/[^,]*$/)[0]
    replace: (text) ->
      before = @input.value.match(/^.+,\s*|/)[0]
      @input.value = before + text + ', '
      return
  )
  Dropzone.autoDiscover = false
  window.dropzone_params = (el)->
    label = if $(el).hasClass('logo-upload') then "Лого" else "Кликните или перенесите файлы для загрузки" 
    img   = if $(el).hasClass('image-upload') then $('.card-form .card-photo img') else if $(el).hasClass('logo-upload') then $('.card-form .card-logo img') else $(el).find('.card-sub-photo img')
    {
      url: el.data('action')
      maxFilesize: 10
      maxFiles: 100
      addRemoveLinks: true
      thumbnailWidth: null
      thumbnailHeight: null
      acceptedFiles: ".jpeg,.jpg,.png"
      dictDefaultMessage: label
      addRemoveLinks: false
      dictRemoveFile: ""
      dictCancelUpload: ""
      dictDefaultMessage: label
      dictInvalidFileType: "Неверный тип файла"
      dictFileTooBig: "Максимальный размер файла 10МБ"
      init: ()->
        dropzone = this
        if img.length > 0
          mockFile = { name: img.data('name'), size: img.data('size'),mockFile: true }
          dropzone.emit("addedfile", mockFile)
          dropzone.emit("thumbnail", mockFile,img.attr('src'))
          dropzone.emit("complete",  mockFile)
          dropzone.files.push mockFile
          $(el).css height: img.height()
      processing: ()->
        this.removeAllFiles()
      success: ()->
        $('#card_image_changed').val(true)
    }

  window.dropZoneImage   = new Dropzone $(".dropzone.image-upload").get(0), dropzone_params($(".dropzone.image-upload"))
  window.dropZoneLogo    = new Dropzone $(".dropzone.logo-upload").get(0), dropzone_params($(".dropzone.logo-upload"))
  window.dropZoneSubImg  = new Array
  $(".dropzone.subimg-upload").each (i) ->
    $(this).data('index',i)
    window.dropZoneSubImg[i] = new Dropzone $(this).get(0), dropzone_params($(this))
  
  Sortable.create(document.getElementById("card_items_sortable"), {
    onEnd: ()->
      $('.card-form .card-items .card-item').each (val, index)->
        $(this).find('.card_items_priority').val(index)
  })

$ ->
  items_layout.init('.cards', '.card', 'priority')
  $items_layout = items_layout.get_layout()
  quick_editor.init($('.quick-card-editor'))
  $(document).on 'click', '.quick-card-editor', (e)->
    quick_editor.close() if e.target.className.includes('quick-card-editor')

  $(document).on 'keyup', 'html', (e)->
    quick_editor.close() if e.keyCode is 27

  $(document).on 'click', '.edit-icon', (e)->
    item = $(this).closest('.card')
    edit_item_path = $(this).closest('.card').data('edit-url')
    quick_editor.open item,edit_item_path, ()->
      init_uploader()

  $(document).on 'click', '.create_card', (e)->
    item             = $(this)
    create_item_path = $(this).data('create-url')
    quick_editor.open item, create_item_path, ()->
      init_uploader()

  $(document).on("ajax:success","form.card-form.update_card", (event, jqxhr, settings, exception) ->
    quick_editor.close ()->
      $(".card[data-card-id=#{jqxhr.id}]").replaceWith(jqxhr.html)
      $items_layout.isotope('reloadItems').isotope()
  ).on("ajax:error","form.card-form", (event, jqxhr, settings, exception) ->
    console.log 'fail'
  )

  $(document).on("ajax:success","form.card-form.new_card", (event, jqxhr, settings, exception) ->
    # console.log('AJ SUC', jqxhr)
    quick_editor.close ()->
      # console.log('EDI CL')
      $(".create_card").after(jqxhr.html)
      $items_layout.isotope('reloadItems').isotope()
  ).on("ajax:error","form.card-form", (event, jqxhr, settings, exception) ->
    console.log 'fail'
  )

  $(document).on 'click', '.remove_item', (e)->
    e.preventDefault()
    item = $(this).closest('.card-item')
    id = item.find('.card_items_id').val()
    if id > 0
      $.ajax
        url: '/card_items/' + id,
        type: 'DELETE',
        success: (result)->  -
          item.remove()
    else
      item.remove()

  $(document).on 'click', '.remove_image', (e)->
    e.preventDefault()
    id = $(this).data('card')
    item = $(this).siblings('.image-upload')
    $.ajax
      url: '/cards/update_image/' + id + '/delete_image'
      type: 'POST',
      success: (result)->  -
        item.find('.dz-image-preview').hide()
        $(this).hide()

  $(document).on 'click', '.remove_logo', (e)->
    e.preventDefault()
    id = $(this).data('card')
    item = $(this).siblings('.logo-upload')
    $.ajax
      url: '/cards/update_image/' + id + '/delete_logo'
      type: 'POST',
      success: (result)->  -
        item.find('.dz-image-preview').hide()
        $(this).hide()

  $(document).on 'click', '.remove_subimg', (e)->
    e.preventDefault()
    card_id = $(this).data('card')
    card_item_id = $(this).data('item')
    image = $(this).parents('.card-item').find('.dz-image-preview')
    $.ajax
      url: '/cards/update_image/' + card_id + '/delete_subimg?card_item=' + card_item_id + ''
      type: 'POST',
      success: (result)->  -
        image.hide()
        $(this).hide()

  $(document).on 'click', '.dropzone.image-upload .dz-image', (e)->
    dropZoneImage.hiddenFileInput.click()
  $(document).on 'click', '.dropzone.logo-upload .dz-image', (e)->
    dropZoneLogo.hiddenFileInput.click()
  $(document).on 'click', '.dropzone.subimg-upload .dz-image', (e)->
    i = $(this).closest('.dropzone').data('index')
    dropZoneSubImg[i].hiddenFileInput.click()

  $(document).on 'click','.add_fields', (e)->
    setTimeout ()->
      new Dropzone $('.card-item:last .dropzone.subimg-upload').get(0), dropzone_params($('.card-item:last .dropzone.subimg-upload'))
    , 200
  $sortable = document.getElementById("cards_sortable");
  if $sortable
    Sortable.create($sortable, {
      onEnd: ()->
        request = []
        $('#cards_sortable .card').each ()->
          request.push $(this).data('card-id')
        # console.log request
        $.post document.getElementById("cards_sortable").dataset.sortUrl, {ids: request}, ( itemsData )->
          # console.log('data', itemsData);
          itemsData.map (item) ->
            card = $(".card[data-card-id=#{item.id}]")[0]
            # console.log('CARD', card)
            card.dataset.priority = item.priority
            $items_layout.isotope('reloadItems').isotope()
    })
