ActiveAdmin.register Client do

  menu priority: 10


  permit_params :name, :tel, :password, :discount

  index do
    selectable_column
    id_column
    column :name
    column :tel
    column :password
    column :discount
    actions
  end

  filter :id
  filter :name

  form do |f|
    f.inputs "Create client:" do
      f.input :name
      f.input :tel
      f.input :discount, as: :number
    end
    f.actions
  end
  show do
    attributes_table do
      row :name
      row :tel 
      row :password
      row :discount
      row :created_at
      row :updated_at
    end
  end

end