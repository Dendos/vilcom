ActiveAdmin.register CardItem do

  menu priority: 3
  config.per_page = 10

  permit_params :card_id, :name, :price, :meta, :vendor_code ,:units

  filter :card_id
  filter :name

  action_item :import_items, only: :index do
    render "import_form"
  end

  form do |f|
    f.inputs "Card info" do
      f.input :card_id
      f.input :name
      f.input :price
      f.input :meta
      f.input :vendor_code
    end
    f.actions
  end

  index do
    selectable_column
    id_column
    column :card_id
    column :name
    column :price
    column :meta
    column :vendor_code
    column :created_at
    column :updated_at
    actions
  end

  show do
    attributes_table do
      row :id
      row :card_id
      row :name
      row :price
      row :meta
      row :vendor_code
      row :updated_at
      row :created_at
    end
  end
end
