ActiveAdmin.register DeliveringCity do

  menu priority: 9


  permit_params :name, :estimated_delivery_time

  index do
    selectable_column
    id_column
    column :name
    column :estimated_delivery_time
    actions
  end

  filter :id
  filter :name

  form do |f|
    f.inputs "Create delivering city:" do
      f.input :name
      f.input :estimated_delivery_time
    end
    f.actions
  end
  show do
    attributes_table do
      row :name
      row :estimated_delivery_time
    end
  end

end