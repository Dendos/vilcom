ActiveAdmin.register Tag do

  menu priority: 8


  permit_params :name

  index do
    selectable_column
    id_column
    column :name
    actions
  end

  filter :id
  filter :name

  form do |f|
    f.inputs "Create tag:" do
      f.input :name
    end
    f.actions
  end
  show do
    attributes_table do
      row :name
      row :created_at
      row :updated_at
    end
  end

end