ActiveAdmin.register Card do

  menu priority: 2
  config.per_page = 10

  permit_params :category_id, :title, :image, :card_logo, :size, category_ids: []


  filter :category
  filter :title

  form do |f|
    f.inputs "Card info" do
      f.input :category
      f.input :title
      f.input :card_logo
      f.input :size, :as => :select, :collection => enum_option_pairs(Card, Card.sizes)
      f.input :image
    end
    f.actions
  end

  index do
    selectable_column
    id_column
    column :category
    column :title
    column :image do |c|
      image_tag(c.image.small)
    end
    column :card_logo do |c|
      image_tag(c.card_logo)
    end
    column :size
    column :created_at
    column :updated_at
    actions
  end

  show do
    attributes_table do
      row :id
      row :category
      row :title
      row :items do |o|
        div do
          o.card_items.each do |item|
            span do
              link_to item.name, admin_card_item_path(item)
            end
          end
        end
      end

      row :Image do |c|
        image_tag(c.image.url)
      end

      row :Card_logo do |c|
        image_tag(c.card_logo.url)
      end
      row :size
      row :updated_at
      row :created_at
    end
  end
end
