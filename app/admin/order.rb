ActiveAdmin.register Order do

  menu priority: 2
  config.per_page = 10

  permit_params :address, :contacts, :phone, :comments, :delivering_city_id


  filter :city
  filter :contacts
  filter :phone
  filter :address

  form do |f|
    f.inputs "Order info" do
      f.input :delivering_city, as: :select
      f.input :address
      f.input :contacts
      f.input :phone
      f.input :comments
    end
    f.actions
  end

  index do
    selectable_column
    id_column
    column :phone
    column :contacts
    column :delivering_city
    column :address
    column :comments do |o|
      o.comments #.truncate(20)
    end
    column :created_at
    column :updated_at
    actions
  end

  show do
    attributes_table do
      row :id
      row :contacts
      row :phone
      row :delivering_city

      row :items do |o|
        div do
          span do
            sum = o.card_items.map{|i| i.price.to_f}.sum
            client = Client.find_by(tel: o.phone)
            if client && client.discount.present?
              discount = client.discount.to_f
            else
              discount = 5
            end
            sum = (sum.to_f / 100) * (100 - discount.to_f)
            'Сумма заказа: ' + sum.to_s + ' руб.'
          end
          br
          o.card_items.each do |item|
            span do
              link_to item.name, admin_card_item_path(item)
            end
          end
        end
      end
      row :address
      row :comments do |o|
        o.comments.truncate(20)
      end
      row :created_at
      row :updated_at
    end
  end
end
