ActiveAdmin.register Category do
  menu priority: 1
  permit_params :name,:parent_id,:lft,:rgt,:depth,:slug, :priority

  index do
    selectable_column
    id_column
    column :name
    column :parent
    column :priority
    actions
  end

  show do
    attributes_table do
      row :name
      row :parent
      row :priority
    end
  end

  filter :name
  filter :slug
  filter :parent

  form do |f|
    f.inputs "Category info" do
      f.input :name
      f.input :parent_id,   :label => "Parent category",:as => :select, :collection => nested_set_options(Category, @category) {|i| "#{'-' * i.level} #{i.name}" }
      f.input :priority
    end
    f.submit
  end

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end


end
