ActiveAdmin.register Post do

  menu priority: 5
  config.per_page = 10

  permit_params :title, :first_text_section, :second_text_section, :url, :created_at, post_images_attributes: [:id, :post_id, :image, :_destroy], category_ids: []


  filter :title
  filter :updated_at
  filter :created_at

  form do |f|
    f.inputs "New post:" do
      f.input :url
      f.input :title, as: :wysihtml5, commands: [ :link ]
      f.input :categories, as: :check_boxes, :collection => Category.where.not(name: 'Все продукты')
      f.has_many :post_images, :allow_destroy => true, :new_record => true do |t|
        t.input :image, :hint => t.template.image_tag(t.object.image.url, width: 150, height: 150)
      end
      f.input :first_text_section, as: :wysihtml5, commands: 'all', blocks: 'all', height: 'huge'
      f.input :created_at, :as => :datepicker
    end
    f.actions
  end

  index do
    selectable_column
    id_column
    column :url
    column :title
    column :post_images do |post|
      div do
        post.post_images.each do |image|
          span do
            image_tag(image.image.url, height: '200', width: '500')
          end
        end
      end
    end
    column :first_text_section
    column :categories do |post|
      post.categories.pluck(:name).to_sentence
    end
    column :created_at
    column :updated_at
    actions
  end

  show do
    attributes_table do
      row :id
      row :url
      row :title
      row :post_images do |post|
        div do
          post.post_images.each do |image|
            span do
              image_tag(image.image.url, height: '200', width: '500')
            end
          end
        end
      end
      row :first_text_section
      row :categories do |post|
        post.categories.pluck(:name).to_sentence
      end
      row :updated_at
      row :created_at
    end
  end
end
