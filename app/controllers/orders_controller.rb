class OrdersController < ApplicationController
  include ClientsHelper
  
  def index
  end

  def create
    @order = Order.new order_params
    item_ids = params[:items][:ids].split(',') if params[:items]
    if item_ids
      item_ids.each do |id|
        item = CardItem.find_by_id id
        @order.order_items.build card_item_id: id, count: params['items']['counts'][id].to_i
      end
    end
    if @order.valid?
      @order.save
      OrdersMailer.new_order(@order).deliver_now
    end
    session[:current_city] = params[:order][:delivering_city_id]
    flash[:success] = '<h1> Спасибо! </h1> <p> Мы свяжемся с вами в течение часа. </p>'
    redirect_to root_path
  end

  def order_simple
  end

  def show
  end

  def new
    if params['items']
      @items = CardItem.find params['items'] 
      @amount = 0
      @items.each do |item|
        if item.kilo?
          @amount += item.price.to_f * item.weight.to_f
        else 
          @amount += item.price.to_f
        end
      end
      if (current_client = current_сlient?) && current_client.discount.present?
        @discount = current_client.discount
      else
        @discount = 5
      end
      @percent = (@amount / 100) * @discount.to_f
      @item_ids = params['items'].join(',')
    end
  end

  def get_items
    if params['items']
      items = params['items'].split(',')
    else
      items = params['items']
    end
    items = CardItem.find items
    respond_to do |format|
        format.json {
          render json: {
              location: new_order_url(items: items),
              items: items
          }, status: 200
        }
    end
  end

  def get_sidebar
    if params['items']
      items = params['items'].split(',')
    else
      items = params['items']
    end
    @items = CardItem.find items
    @amount = @items.map {|item| item.price.to_f}
    @amount = @amount.reduce(:+)
    respond_to do |format|
      format.html {render('get_sidebar', layout: false)}
      format.json {render json: { orderItemsCount: @items.count, amount: @amount, html: render_to_string('get_sidebar', layout: false)}, status: 200 }
    end
  end

  def order_params
    params.require(:order).permit(:comments, :delivering_city_id, :address, :contacts, :phone)
  end
end
