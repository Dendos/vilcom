class NewsController < ApplicationController

  def index
    @news = Post.all.order(created_at: :desc)
  end

  def show
    @new = Post.find_by id: params[:id]
    if !@new
    	@new = Post.find_by url: params[:id]
    end
    if !@new
      redirect_to news_index_path
    end
  end
end
