class CardsController < ApplicationController
  skip_before_filter :verify_authenticity_token, only: [:create_image, :update_image]

  before_filter do
    redirect_to root_path if !admin_user_signed_in?
  end
  before_action :set_card, only: [:edit, :update, :update_image]

  def new
    @card = Card.new
    @card.category_id = params[:category_id]
    @card.temp_key = get_temp_key
    @card.card_items.build
    @tags = Card.all_tags.pluck(:name).join(",")
    respond_to do |format|
      format.html {}
      format.json {render json: { html: render_to_string('create', layout: false), status: "success"}}
    end
  end

  def create
    @card = Card.new card_params
    respond_to do |format|
      if @card.save
        format.json { render json: {html: render_to_string('main/_card.html', layout: false), id: @card.id, status: "success"}}
      end
    end
  end

  def edit
    @tags = ""
    @counter = 0
    @tags = Card.all_tags.pluck(:name).join(",")
    respond_to do |format|
      format.html {}
      format.json {render json: { html: render_to_string('edit', layout: false), status: "success"}}
    end
  end

  def update
    respond_to do |format|
      @card.assign_attributes(card_params)
      if @card.save
        format.json { render json: {html: render_to_string('main/_card.html', layout: false), id: @card.id, status: "success"}}
      end
    end
  end

  def update_image
    if params[:card_item].present?
      card_item = @card.card_items.find_by(id: params[:card_item])
      card_item.image = params[:file]
      card_item.save
    else
      if params[:type] == 'image'
        @card.image = params[:file]
      elsif !params[:type].include?('delete')
        @card.card_logo = params[:file]
      end
    end
    if params[:type] == 'delete_logo'
      @card.remove_card_logo!
    elsif params[:type] == 'delete_image'
      @card.remove_image!
    elsif params[:type] == 'delete_subimg'
      #binding.pry
      card_item = @card.card_items.find(params[:card_item])
      card_item.remove_image!
      card_item.save
    end
    @card.save
    render json: @card
  end

  def create_image
    @image = Image.new({file: params[:file], temp_key: params[:temp_key], image_type: params[:type]})
    @image.save
    render json: @image
  end

  def destroy
    @card = Card.find(params[:id])
    @card.destroy
    respond_to do |format|
      format.html { redirect_to all_cats_url }
      format.js   { render :layout => false }
    end
  end

  def sort
    ids = params[:ids]
    @cards = Card.find(ids)
    @cards.each do |card|
      position = ids.find_index card.id.to_s
      card.update(priority: position)
    end
    @cards = @cards.sort_by(&:priority)
    render json: @cards
  end

  private
  def get_temp_key
    o = [('a'..'z'), ('A'..'Z')].map { |i| i.to_a }.flatten
    (0...50).map { o[rand(o.length)] }.join
  end

  def set_card
    @card = Card.find(params[:id])
  end

  def card_params
    params.require(:card).permit(:title, :image_style, :tag_list, :category_id, :temp_key, :size_cd, card_items_attributes: card_items_attributes)
  end

  def card_items_attributes
      [:id, :name, :meta,:price,:prirority,:image,:temp_key, :units, :vendor_code, :weight]
  end
end
