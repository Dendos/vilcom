class CardItemsController < ApplicationController
  include CardItemsHelper

  def destroy
    @item = CardItem.find(params[:id])
    @item.delete()
    render json: @item
  end

  def parse_data
    if admin_user_signed_in?
      items_table = params[:items_table]
      response    = update_prices_from_table(items_table)
      flash[response[:status]] = response[:message]
    else
      flash[:error] = 'Вы не можете выполнить это действие.'
    end
    redirect_to :back
  end

end
