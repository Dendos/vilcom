class MainController < ApplicationController
  def index
    # @promo_cards = PromoCard.all
    card_ids = Card.ids.shuffle(random: Random.new(Time.now.to_i))
    @cards = Card.where(id: card_ids)
  end

  def show
    category = params[:category]
    @category = Category.includes(:cards).find_by_id category
    if @category
      @cards = @category.cards.includes(:card_items, :tags)
    else
      redirect_to root_path
    end
  end

  def all_cats
    @categories = Category.all.includes(:cards)
  end

  def search
    if params[:tag]
      @cards = Card.tagged_with params[:tag]
      @category = @cards.first.category
    else
      redirect_to root_path
    end
  end

  def about
  end
end
