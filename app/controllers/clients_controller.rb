class ClientsController < ApplicationController
  include ClientsHelper

  def login
    if params[:show_password]
      @show_password = true
    else
      @show_password = false
    end
  end
  
  def signin
      phone = params[:phone].gsub(/\D/, "")
      pp = phone.split(//).last(10).join("").to_s
      @client = Client.where(tel: ["7#{pp}", "8#{pp}", "+7#{pp}"]).first || nil
      if @client
        if params[:submit] == 'Продолжить →'
          #ПОЛУЧАЕМ ТОКЕН ДЛЯ АПИ
          uri = URI.parse("https://integrationapi.net/rest/user/sessionid")
          uri.query = URI.encode_www_form( { login: 'nfrjtdjnbvz', password: 'Devinop55wrd' } )
          http = Net::HTTP.new(uri.host, uri.port)
          http.use_ssl = true
          request = Net::HTTP::Get.new(uri.request_uri)

          code = http.request(request) #токен лежит в code.body
          #// Получаем токен для апи
          @client.password = 100000 + rand(89999)
          @client.save
          # ОТПРАВКА СМС
          uri = URI.parse("https://integrationapi.net/rest/Sms/Send")
          uri.query = URI.encode_www_form( { SessionID: code.body.gsub(/"/, ''), SourceAddress: 'Vilcom', DestinationAddress: @client.tel, Data: "Код: #{@client.password}", Validity: 10  } )
          http = Net::HTTP.new(uri.host, uri.port)
          http.use_ssl = true
          request = Net::HTTP::Post.new(uri.request_uri)
          res = http.request(request)
          #// ОТПРАВКА СМС
          flash[:notice] = 'Мы отправили пароль на номер ' + phone
          redirect_to login_path(phone: phone, show_password: true)
        elsif (params[:submit] == 'Войти' && (@client.nil? || @client.password.nil? || params[:password] != @client.password))
          flash[:error] = 'Вы ввели неправильный пароль.'
          redirect_to login_path(phone: params[:phone])
        elsif (params[:submit] == 'Войти' && @client.password && @client.password == params[:password])
          client_sign_in @client
          flash[:success] = '<h1>Вход выполнен.</h1> <p>Ваша персональная скидка: ' + @client.discount + '% </p>'
          redirect_to root_path
        else
          flash[:error] = '<h1> Ошибка </h1>'
          redirect_to login_path(phone: phone)
        end
      else
        flash[:error] = '<h2>Извините, вашего номера нет в системе.</h2><p> Позвоните нам: 8 938 888-12-12</p>'
        redirect_to login_path(phone: phone)
      end
  end
end