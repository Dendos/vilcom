module ClientsHelper
  def client_sign_in(user)
    remember_token = Client.new_remember_token
    cookies.permanent[:remember_token] = remember_token
    user.update_attribute(:remember_token, Client.encrypt(remember_token))
    self.current_client = user
  end

  def current_client=(user)
    @current_client = user
  end

  def current_сlient?
    remember_token = Client.encrypt(cookies[:remember_token])
    @current_client ||= Client.find_by(remember_token: remember_token)
  end
end