module ApplicationHelper
  def all_tags(category)
    tags = []
    category.cards.each do |card|
      if !card.tag_list.blank?
        card.tag_list.each do |tag|
          tags << tag unless tags.include? tag
        end
      end
    end
    html = ''
    tags.each do |tag|
      html += content_tag(:div, :class => "category-menu-item") do
        link_to tag, search_path(tag: tag)
      end
    end
    html.html_safe
  end

  def title(text)
    content_for :title, text
  end

  def meta_tag(tag, text)
    content_for :"meta_#{tag}", text
  end

  def yield_meta_tag(tag, default_text='')
    content_for?(:"meta_#{tag}") ? content_for(:"meta_#{tag}") : default_text
  end
  # def news_path news
  #   if news.url.blank?
  #     "/news/#{news.id}"
  #   else
  #     "/news/#{news.url}"
  #   end
  # end
end
