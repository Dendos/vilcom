module CardItemsHelper

  def update_prices_from_table table
    parsed_items = parse_items(table)
    if parsed_items.is_a?(String)
      response = {status: 'error', message: parsed_items}
    else
      updated_items_count = 0
      parsed_items.each do |item|
        same_items = CardItem.where(vendor_code: item['vendor_code'])
        same_items.update_all(price: item['price'])
        updated_items_count += same_items.count
      end
      response = {status: 'success', message: updated_items_count.to_s + ' card items successfully updated.'}
    end
    return response
  end

  #===========TABLE PARSING METHODS
  def parse_items file
    spreadsheet = open_spreadsheet(file)
    return spredsheet if spreadsheet.is_a?(String)

    response    = []
    header      = parse_headers(get_row(spreadsheet, 1, 2))
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, get_row(spreadsheet, i, header.length)].transpose]
      response.push row unless row['vendor_code'].blank?
    end
    return response
  end

  #TRANSLATE RUSSIAN HEADER TITLES TO MODEL FIELD NAMES
  def parse_headers header
    parsed_header = []
    header.each do |header_item|
      case header_item
      when 'Артикул' then parsed_header.push('vendor_code')
      when 'Цена'    then parsed_header.push('price')
      else parsed_header.push(header_item)
      end
    end
    return parsed_header
  end

  def get_row(spreadsheet, row_number, cols_required)
    row = spreadsheet.row(row_number)
    if row.length < cols_required
      temp_row = row[0].split(';')
      row = temp_row.length < cols_required ? Array.new(cols_required) : temp_row
    end
    return row
  end

  def open_spreadsheet file
    ext_name = File.extname(File.basename(file.path))
    case ext_name
    when ".csv"  then  Roo::CSV.new(file.path)
    when ".xls"  then  Roo::Excel.new(file.path)
    when ".xlsx" then  Roo::Excelx.new(file.path)
    else "Unknown file type: #{ext_name}"
    end
  end

end