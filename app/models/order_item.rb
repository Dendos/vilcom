class OrderItem < ActiveRecord::Base
  belongs_to :order
  belongs_to :card_item
end
