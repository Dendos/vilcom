class CardItem < ActiveRecord::Base
  belongs_to :card
  has_many :order_items
  has_many :orders, through: :order_items
  mount_uploader :image, ImageUploader
  as_enum :units, [:piece, :kilo], source: :units, map: :string ## [:liter, :pack]

  default_scope -> {order("priority ASC, created_at DESC")}
  
  after_create :bind_photos

  private
  def bind_photos
    images = Image.where(temp_key: temp_key).all
    images.each do |image|
      if image.image_type == 'image'
        self.image = Rails.root.join(image.file.path).open
      else
        self.card_logo = Rails.root.join(image.file.path).open
      end
      image.delete
      self.save
    end
  end
end
