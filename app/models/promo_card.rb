class PromoCard < ActiveRecord::Base
  belongs_to :category

  mount_uploader :image, ImageUploader

  default_scope { order 'id ASC' }
end
