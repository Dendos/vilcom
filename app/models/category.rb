class Category < ActiveRecord::Base

  has_many :cards
  has_many :post_category_relations, dependent: :destroy
  has_many :posts, through: :post_category_relations
  
  scope :all_cat, -> { where name: "Все продукты" }

  acts_as_nested_set


  def get_current_and_chield_ids
    category_ids = Category.where(parent_id: self.id).pluck(:id)
    category_ids.push self.id
  end
end
