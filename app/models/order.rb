class Order < ActiveRecord::Base
  has_many :order_items
  has_many :card_items, through: :order_items
  belongs_to :delivering_city
  accepts_nested_attributes_for :card_items
end
