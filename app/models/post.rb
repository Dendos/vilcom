class Post < ActiveRecord::Base
  has_many :post_category_relations, dependent: :destroy
  has_many :categories, through: :post_category_relations
  has_many :post_images, dependent: :destroy
  accepts_nested_attributes_for :post_images, allow_destroy: true
  accepts_nested_attributes_for :categories, reject_if: :all_blank, allow_destroy: true

  def get_alias
    if url.blank?
      id
    else
      url
    end
  end  
end
