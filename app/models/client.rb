class Client < ActiveRecord::Base
  before_create :create_remember_token

  def Client.new_remember_token
    SecureRandom.urlsafe_base64
  end

  def Client.encrypt(token)
    Digest::SHA1.hexdigest(token.to_s)
  end

  private

    def create_remember_token
      self.remember_token = Client.encrypt(Client.new_remember_token)
    end
end
