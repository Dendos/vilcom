class Card < ActiveRecord::Base
  belongs_to :category
  has_many :card_items

  mount_uploader :image, ImageUploader
  mount_uploader :card_logo, LogoUploader

  default_scope -> {order("priority ASC, created_at DESC")}

  as_enum :size, small: 0, big: 1

  acts_as_taggable
  acts_as_taggable_on :tag_list

  accepts_nested_attributes_for :card_items, reject_if: :all_blank, allow_destroy: true

  after_create :bind_photos

  private
  def bind_photos
    images = Image.where(temp_key: temp_key).all
    images.each do |image|
      if image.image_type == 'image'
        self.image = Rails.root.join(image.file.path).open
      else
        self.card_logo = Rails.root.join(image.file.path).open
      end
      image.delete
      self.save
    end
  end
end
