# frozen_string_literal: true

source 'https://rubygems.org'
ruby '2.3.1'

# Core
gem 'pg'
gem 'rails', '4.2.1'

# Auth
gem 'devise'

# Localization
gem 'russian', '~> 0.6.0'

# Model extensions
gem 'acts-as-taggable-on'
gem 'awesome_nested_set'
gem 'date_validator'
gem 'simple_enum'

# Assets
gem 'bootstrap-glyphicons'
gem 'bower-rails', '~> 0.11.0'
gem 'coffee-rails', '~> 4.1.0'
gem 'dropzonejs-rails'
gem 'jquery-rails'
gem 'normalize-rails'
gem 'sass-rails', '~> 5.0'
gem 'slim-rails'
gem 'sortable-rails'
gem 'uglifier', '>= 1.3.0'

# View helpers
gem 'active_link_to'
gem 'cocoon'
gem 'formtastic', '~> 3.0'
gem 'meta-tags'

# Admin
gem 'active_admin_theme'
gem 'activeadmin', github: 'activeadmin'
gem 'activeadmin-dragonfly', github: 'stefanoverna/activeadmin-dragonfly'
gem 'activeadmin-wysihtml5', github: 'stefanoverna/activeadmin-wysihtml5'

# File uploads
gem 'carrierwave'
gem 'mini_magick'

# Deployment
gem 'capistrano', '~> 3.4.0'
gem 'capistrano-bundler', '~> 1.1.2'
gem 'capistrano-rails', '~> 1.1.1'
gem 'capistrano-rbenv', github: 'capistrano/rbenv'

# Auxiliary
gem 'rails-erd'
gem 'roo', '~> 2.7.0'
gem 'roo-xls'
gem 'yaml_db'

group :development, :test do
  gem 'byebug'
  gem 'web-console', '~> 2.0'
end

group :development do
  gem 'guard'
  gem 'nokogiri'
  gem 'pry-rails'
  gem 'spring'
  gem 'thin'
end

group :test do
  gem 'factory_girl_rails'
  gem 'rspec-rails'
  gem 'shoulda'
  gem 'shoulda-matchers'
end
