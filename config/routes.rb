Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config

  ActiveAdmin.routes(self)

  root to: 'main#index'

  # resources :orders
  get '/orders/new', to: 'orders#new', as: 'new_order'
  post '/orders', to: 'orders#create'
  get '/orders/order_simple', to: 'orders#order_simple'
  get '/orders/get_items', to: 'orders#get_items', as: 'get_items'
  get '/orders/get_sidebar', to: 'orders#get_sidebar', as: 'get_sidebar'

  get '/category/:category', to: 'main#show', as: 'show'
  get '/all_products', to: 'main#all_cats', as: 'all_cats'
  get '/search', to: 'main#search', as: 'search'
  get '/about', to: 'main#about', as: 'about'

  get  'cards/new/:category_id',  to: 'cards#new',  as: :new_card
  post 'cards/create_image/:temp_key/:type', to: 'cards#create_image', as: :create_image
  post 'cards/update_image/:id/:type', to: 'cards#update_image', as: :update_image_card

  post '/parse_card_items', to: 'card_items#parse_data', as: :parse_card_items

  resources :news, only: [:index, :show]

  resources :clients, only: [:signin]
  get '/login', to: 'clients#login', as: :login
  post '/signin', to: 'clients#signin', as: :signin

  resources :cards, only: [:create, :edit, :update, :destroy] do
    collection do
      post 'sort'
    end
  end

  resources :card_items, only: [:destroy]
end
